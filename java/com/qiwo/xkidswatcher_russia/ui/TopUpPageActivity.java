package com.qiwo.xkidswatcher_russia.ui;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.util.TLog;
import com.qiwo.xkidswatcher_russia.widget.ProgressWebView;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import butterknife.InjectView;

public class TopUpPageActivity extends BaseActivity {

	@InjectView(R.id.webview)
	ProgressWebView webview;
	
	@InjectView(R.id.linearLayout_l)
	LinearLayout linearLayout_l;
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.linearLayout_l:
			TopUpPageActivity.this.finish();
			break;

		default:
			break;
		}
	}

	@Override
	public void initView() {
		// TODO Auto-generated method stub
		linearLayout_l.setOnClickListener(this);
	}

	@Override
	protected int getLayoutId() {
		// TODO Auto-generated method stub
		return R.layout.activity_totup_page;
	}
	
	@Override
	protected boolean hasActionBar() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void initData() {
		
		String sim = KidsWatConfig.getUseCountryCode() + KidsWatConfig.getUserPhone();
		String iccid = AppContext.getInstance().currentFamily.device_iccid;
		TLog.log("phone number >>> " + sim);
		TLog.log("iccid >>> " + iccid);
		loadWebView(sim, iccid);
	}
	
	private void get_device_timeleft (String sim, String iccid) {
//		final String url = "http://kids_radar.clients4.arealidea.ru/api/v1/mobile_service/balance/";
		final String url = "http://kidsradar.lexand.ru/api/v1/mobile_service/balance/";
		getData(url);
	}
	
	private void getData (String url) {

		String iccid = AppContext.getInstance().currentFamily.device_iccid;
		String sim = KidsWatConfig.getUseCountryCode() + KidsWatConfig.getUserPhone();
		String token = "4teGENskDv2AXepVstwj";
		RequestBody requestBody = new MultipartBuilder()
		     .type(MultipartBuilder.FORM)
		     .addPart(Headers.of(
		          "Content-Disposition", 
		              "form-data; name=\"watchICCID\""), 
		          RequestBody.create(null, String.format("[\"%s\"]", iccid)))
		     .addPart(Headers.of(
		    		 "Content-Disposition", 
		    		 "form-data; name=\"phoneNumber\""), 
		    		 RequestBody.create(null, sim))
		     .addPart(Headers.of(
		    		 "Content-Disposition", 
		    		 "form-data; name=\"token\""), 
		    		 RequestBody.create(null, token))
		     .build();
		
		Request request = new Request.Builder()
		    .url(url)
		    .post(requestBody)
		    .build();
		
		Call call = new OkHttpClient().newCall(request);
		call.enqueue(new Callback() {
			 @Override
	            public void onFailure(Request request, IOException e)
	            {
				 TLog.log("Failure");
				 String sim = null;
					int timeleft = 0;
					
					webview.loadUrl(String.format("http://misafes1.qiwocloud2.com/view/pay/index.html?sim=%s&timeleft=%d", sim, timeleft));
					webview.getSettings().setJavaScriptEnabled(true);
					
					webview.setWebViewClient(new WebViewClient(){
						@Override
						public boolean shouldOverrideUrlLoading(WebView view, String url) {
							view.loadUrl(url);
							return true;
						}
					});
	            }
			 	
	            @Override
	            public void onResponse(final Response response) throws IOException
	            {
	            	try {
	            		
	            		String resp_str = response.body().string();
	            		TLog.log(resp_str);
						JSONObject json = new JSONObject(resp_str);
						
						if (json.getInt("success") == 1) {
							String sim = AppContext.getInstance().currentFamily.sim;
							JSONArray arrays = (JSONArray) json.get("balance");
							
							if (arrays == null || arrays.length() <= 0) {
								return;
							}
							
							int timeleft = ((JSONObject)arrays.get(0)).getInt("useDays");
							webview.loadUrl(String.format("http://misafes1.qiwocloud2.com/view/pay/index.html?sim=%s&timeleft=%d", sim, timeleft));
							webview.getSettings().setJavaScriptEnabled(true);
							
							webview.setWebViewClient(new WebViewClient(){
								@Override
								public boolean shouldOverrideUrlLoading(WebView view, String url) {
									view.loadUrl(url);
									return true;
								}
							});
						} else {
							String sim = null;
							int timeleft = 0;
							
							webview.loadUrl(String.format("http://misafes1.qiwocloud2.com/view/pay/index.html?sim=%s&timeleft=%d", sim, timeleft));
							webview.getSettings().setJavaScriptEnabled(true);
							
							webview.setWebViewClient(new WebViewClient(){
								@Override
								public boolean shouldOverrideUrlLoading(WebView view, String url) {
									view.loadUrl(url);
									return true;
								}
							});
						}
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						
						String sim = null;
						int timeleft = 0;
						
						webview.loadUrl(String.format("http://misafes1.qiwocloud2.com/view/pay/index.html?sim=%s&timeleft=%d", sim, timeleft));
						webview.getSettings().setJavaScriptEnabled(true);
						
						webview.setWebViewClient(new WebViewClient(){
							@Override
							public boolean shouldOverrideUrlLoading(WebView view, String url) {
								view.loadUrl(url);
								return true;
							}
						});
					}
	            }
		});
	}
    
	// ----------------
	//设置回退 
	//覆盖Activity类的onKeyDown(int keyCoder,KeyEvent event)方法 
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) { 
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) { 
        	webview.goBack(); //goBack()表示返回WebView的上一页面 
            return true; 
        } else if(keyCode == KeyEvent.KEYCODE_BACK && !webview.canGoBack()){
        	TopUpPageActivity.this.finish();
        	return true;
        }
        return false; 
    } 
	
	public void loadWebView (String phone_number, String iccid) {
//		String pay_url = String.format("http://kids_radar.clients4.arealidea.ru/api/v1/mobile_service/pay/"
//				+ "?watchICCID=%s&phoneNumber=%s&token=4teGENskDv2AXepVstwj",
//				iccid, phone_number);
		//换新的url
		String pay_url = String.format("http://kidsradar.lexand.ru/api/v1/mobile_service/pay/"
				+ "?watchICCID=%s&phoneNumber=%s&token=4teGENskDv2AXepVstwj",
				iccid, phone_number);
		TLog.log("pay_url >>> " + pay_url);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.loadUrl(pay_url);
		
		webview.setWebViewClient(new WebViewClient(){
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return true;
			}
		});
	}
}
