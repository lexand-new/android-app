package com.qiwo.xkidswatcher_russia.ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;
import org.kymjs.kjframe.http.HttpParams;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.AppManager;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.base.BaseDialog;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___LoginMember;
import com.qiwo.xkidswatcher_russia.db.SqlDb;
import com.qiwo.xkidswatcher_russia.event.BaseEvent;
import com.qiwo.xkidswatcher_russia.thread.CommonThreadMethod;
import com.qiwo.xkidswatcher_russia.thread.DateUtils;
import com.qiwo.xkidswatcher_russia.thread.ThreadParent;
import com.qiwo.xkidswatcher_russia.util.Base64Encode;
import com.qiwo.xkidswatcher_russia.util.Contanst;
import com.qiwo.xkidswatcher_russia.util.Encoder;
import com.qiwo.xkidswatcher_russia.util.FileUtil;
import com.qiwo.xkidswatcher_russia.util.ImageUtils;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import com.qiwo.xkidswatcher_russia.util.SimpleDateUtil;
import com.qiwo.xkidswatcher_russia.util.StringUtils;
import com.qiwo.xkidswatcher_russia.util.TLog;
import com.qiwo.xkidswatcher_russia.widget.APopupWindow.ItemPosition;
import com.qiwo.xkidswatcher_russia.widget.APopupWindow.onClickItemListener;
import com.qiwo.xkidswatcher_russia.widget.BottomPopupWindow;
import com.qiwo.xkidswatcher_russia.widget.CircleImageView;
import com.qiwo.xkidswatcher_russia.widget.DatePickerPopupWindow;
import com.qiwo.xkidswatcher_russia.widget.DatePickerPopupWindow2;
import com.qiwo.xkidswatcher_russia.widget.EditTextWithDel;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * 
 */
public class KidsProfileActivity extends BaseActivity {

	@InjectView(R.id.isadmin_image)
	ImageView isadmin_image;

	@InjectView(R.id.imageview_l)
	ImageView imageview_back;

	@InjectView(R.id.imageView_baby)
	CircleImageView imageView_baby;

	@InjectView(R.id.linearLayout_cc)
	LinearLayout linearLayout_cc;

	@InjectView(R.id.linearLayout_cc2)
	LinearLayout linearLayout_cc2;

	@InjectView(R.id.linearLayout_l)
	LinearLayout linearLayout_l;

	@InjectView(R.id.relativeLayout_img)
	RelativeLayout relativeLayout_img;

	@InjectView(R.id.relativeLayout_relation)
	RelativeLayout relativeLayout_relation;

	@InjectView(R.id.relativeLayout_nickname)
	RelativeLayout relativeLayout_nickname;

	@InjectView(R.id.relativeLayout_birthday)
	RelativeLayout relativeLayout_birthday;

	@InjectView(R.id.relativeLayout_sex)
	RelativeLayout relativeLayout_sex;

	@InjectView(R.id.relativeLayout_height)
	RelativeLayout relativeLayout_height;

	@InjectView(R.id.relativeLayout_weight)
	RelativeLayout relativeLayout_weight;

	@InjectView(R.id.view_line_relation)
	View view_line_relation;

	@InjectView(R.id.textView_relation)
	TextView textView_relation;

	@InjectView(R.id.textView_nickname)
	TextView textView_nickname;

	@InjectView(R.id.textView_birthday)
	TextView textView_birthday;

	@InjectView(R.id.textView_sex)
	TextView textView_sex;

	@InjectView(R.id.textView_height)
	TextView textView_height;

	@InjectView(R.id.textView_weight)
	TextView textView_weight;

	// ---------more icon----------
	@InjectView(R.id.imageView_more_photo)
	ImageView imageView_more_photo;

	@InjectView(R.id.imageView_more_nickname)
	ImageView imageView_more_nickname;

	@InjectView(R.id.imageView_more_gender)
	ImageView imageView_more_gender;

	@InjectView(R.id.imageView_more_birthday)
	ImageView imageView_more_birthday;

	@InjectView(R.id.imageView_more_height)
	ImageView imageView_more_height;

	@InjectView(R.id.imageView_more_weight)
	ImageView imageView_more_weight;
	// --------------
	@InjectView(R.id.button_ok)
	Button button_ok;
	// ------------

	final String TAG = KidsProfileActivity.class.getSimpleName();

	public static final int REQUSET_CODE_USER_INFO_EX = 1200;
	final int SET_RELATION_SHIP = 300;

	beanForDb___Family fBaby = null;
	beanForDb___LoginMember loginUser_kid = null;

	private int sex = 0;
	private float height = 1.00f;
	private float weight = 20f;
	private long birthday = System.currentTimeMillis() / 1000;
	private String nickname = "Ребенок";
	private String img_base64_d = "";
	// -----------
	private String action = null;
	private String qrcode, phone, country;
	private String relation_for_add = null;

	@Override
	protected int getLayoutId() {
		return R.layout.activity_kids_profile;
	}

	@Override
	protected boolean hasActionBar() {
		return false;
	}

	@Override
	protected void init(Bundle savedInstanceState) {
		super.init(savedInstanceState);
	}
	
	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.linearLayout_l:
			finish();
			break;
		case R.id.button_ok:
			// Intent intent = new Intent(this, ScanGuideCameraActivity.class);
			// startActivityForResult(intent, SHOW_SCAN_QRCODE);

			button_ok.setEnabled(false);

			if (action.equals(Contanst.ACTION_ADD)) {
				add_watch(qrcode, relation_for_add);
			}
			break;
		case R.id.relativeLayout_img:
			if (userIsAllowed())
				showSelecPhotoPopupWindow(linearLayout_cc2);
			break;
		case R.id.relativeLayout_relation:
			Intent intent = new Intent(this, SetRelationActivity2.class);
			startActivityForResult(intent, SET_RELATION_SHIP);
			break;
		case R.id.relativeLayout_nickname:
			if (userIsAllowed())
				editNickName();
			break;
		case R.id.relativeLayout_birthday:
			if (userIsAllowed()) {
				DatePickerPopupWindow2.OnDatePickerInfoListener listener = new DatePickerPopupWindow2.OnDatePickerInfoListener() {
					public void onDateReceiver(String date) {
						textView_birthday.setText(date);
						// showShortToast(date);
						try {
//							SimpleDateFormat sdf = new SimpleDateFormat(
//									"yyyy-MM-dd");
							SimpleDateFormat sdf = new SimpleDateFormat(
									"dd-MM-yyyy");
							Date xdate = sdf.parse(date);
							birthday = xdate.getTime() / 1000;
							if (action.equalsIgnoreCase(Contanst.ACTION_EDIT)) {
								update_baby_info();
							}
						} catch (ParseException e) {
							birthday = System.currentTimeMillis() / 1000;
							showLongToast(e.toString());
						}
					}
				};
				showSelectBirthdayPopupWindow(linearLayout_cc2, listener);
			}
			break;
		case R.id.relativeLayout_sex:
		case R.id.relativeLayout_height:
		case R.id.relativeLayout_weight:
			if (userIsAllowed())
				setUserInfoEx();
			break;
		default:
			break;
		}
	}

	@Override
	public void initView() {
		linearLayout_l.setOnClickListener(this);
		relativeLayout_img.setOnClickListener(this);
		relativeLayout_relation.setOnClickListener(this);
		relativeLayout_nickname.setOnClickListener(this);
		relativeLayout_birthday.setOnClickListener(this);
		relativeLayout_sex.setOnClickListener(this);
		relativeLayout_height.setOnClickListener(this);
		relativeLayout_weight.setOnClickListener(this);
		button_ok.setOnClickListener(this);
	}

	@Override
	public void initData() {
		action = getIntent().getStringExtra(Contanst.KEY_ACTION);
		if (action.equals(Contanst.ACTION_ADD)) {
			Intent intent = getIntent();
			qrcode = intent.getStringExtra("qrcode");
			relation_for_add = intent.getStringExtra("relation");
			phone = intent.getStringExtra("phone");
			country = intent.getStringExtra("country");

			relativeLayout_relation.setVisibility(View.GONE);
			view_line_relation.setVisibility(View.GONE);

//			textView_birthday.setText(new SimpleDateFormat("yyyy-MM-dd")
//					.format(new Date()));
			textView_birthday.setText(new SimpleDateFormat("dd-MM-yyyy")
					.format(new Date()));

		} else if (action.equals(Contanst.ACTION_EDIT)) {
			button_ok.setVisibility(View.INVISIBLE);
			showKidsProfile();
		}
	}

	private boolean userIsAllowed() {
		if (action.equals(Contanst.ACTION_ADD)) {
			return true;
		} else if (action.equals(Contanst.ACTION_EDIT)) {
			if (loginUser_kid.isAdmin == 1) {
				return true;
			}

		}
		return false;
	}

	private void showMoreIcon(boolean show) {
		int visible = show ? View.VISIBLE : View.GONE;
		imageView_more_photo.setVisibility(visible);
		imageView_more_nickname.setVisibility(visible);
		imageView_more_gender.setVisibility(visible);
		imageView_more_birthday.setVisibility(visible);
		imageView_more_height.setVisibility(visible);
		imageView_more_weight.setVisibility(visible);
	}

	private void showKidsProfile() {

		fBaby = AppContext.getInstance().currentFamily;
		loginUser_kid = AppContext.getInstance().loginUser_kid;
		
		sex = fBaby.sex;
		height = fBaby.height;
		weight = fBaby.weight;
		nickname = fBaby.nickname;
		birthday = fBaby.birthday;

		if (loginUser_kid.isAdmin == 0) {
			showMoreIcon(false);
		}
		
		textView_relation.setText(loginUser_kid.relation);
		textView_nickname.setText(fBaby.nickname);
//		String birthdayText = fBaby.birthdayText;
//		textView_birthday.setText(birthdayText);

		String b=SimpleDateUtil.convert2String(
				birthday * 1000, SimpleDateUtil.DATE_FORMAT);
		textView_birthday.setText(b);
//		Log.e("----","----birthdayText="+birthdayText+"---"+fBaby.birthday+"----"+b);
		String sexText = fBaby.sexText;
		
		if("boy".equalsIgnoreCase(sexText)){
			sexText = "мальчик ";
		}else{
			sexText = "девочка";
		}
		
		textView_sex.setText(sexText);
		textView_height.setText(fBaby.height + "м");
		textView_weight.setText(fBaby.weight + "кг");
		imageView_baby.setImageBitmap(KidsWatUtils.getBabyImg_v3(this, fBaby.family_id, fBaby.sex, AppContext.getInstance().currentFamily));
		
		if (AppContext.getInstance().loginUser_kid.isAdmin == 1)
			isadmin_image.setVisibility(0);
		else
			isadmin_image.setVisibility(8);
	}

	private void editNickName() {
		final String nameStr = action.equals(Contanst.ACTION_ADD) ? ""
				: fBaby.nickname;

		View view = mInflater.inflate(R.layout.dialog_edittext_view, null);
		final EditTextWithDel editView = (EditTextWithDel) view
				.findViewById(R.id.edittext);
		editView.setHint(getApplicationContext().getResources().getString(R.string.nickname) + "(0-9)");
		editView.setText(nameStr);
		editView.setSelection(editView.getText().length());
		editView.setFilters(new InputFilter[] { new InputFilter.LengthFilter(9) });

		final BaseDialog b = new BaseDialog(this);
		b.setTitle(getApplicationContext().getResources().getString(R.string.nickname));
		b.setContentView(view);
		b.setOnShowListener(new OnShowListener() {
			@Override
			public void onShow(DialogInterface dialog) {
				// inputMethodManager.showSoftInput(editView, 0);
			}
		});
		b.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				// inputMethodManager.hideSoftInputFromWindow(
				// editView.getWindowToken(), 0);
			}
		});
		b.setNegativeButton(R.string.cancle, new OnClickListener() {
			@Override
			public void onClick(View v) {
				b.dismiss();
			}
		}, R.style.button_default);
		b.setPositiveButton(R.string.ok, new OnClickListener() {
			@Override
			public void onClick(View v) {

				nickname = editView.getText().toString().trim();
				TLog.log(nickname);
				b.dismiss();
				if (action.equals(Contanst.ACTION_EDIT)) {
					update_baby_info();
				} else if (action.equals(Contanst.ACTION_ADD)) {
					textView_nickname.setText(nickname);
				}
				// 设置名字
			}
		}, R.style.button_default);
		b.show();

	}

	private void setUserInfoEx() {
		Intent intent = new Intent(this, BabyInfoActivity.class);
		intent.putExtra("sex", sex);
		intent.putExtra("height", height * 100);
		intent.putExtra("weight", weight);
		startActivityForResult(intent, REQUSET_CODE_USER_INFO_EX);
	}

	private BottomPopupWindow mSelectPhotoPopupWindow = null;

	protected void showSelecPhotoPopupWindow(View relyView) {
		if (mSelectPhotoPopupWindow == null) {
			mSelectPhotoPopupWindow = new BottomPopupWindow(this, 0, relyView);

			// 添加“拍照”
			mSelectPhotoPopupWindow.addItem(getApplicationContext().getResources().getString(R.string.take_photo),
					new onClickItemListener() {

						@Override
						public void clickItem(View v) {
							startActionCamera();
						}
					}, ItemPosition.TOP);

			// 添加“从相册选择”
			mSelectPhotoPopupWindow.addItem(getApplicationContext().getResources().getString(R.string.choose_from_albulm),
					new onClickItemListener() {

						@Override
						public void clickItem(View v) {
							startImagePick();
						}
					}, ItemPosition.BOTTOM);

			// 添加“取消”
			mSelectPhotoPopupWindow.addCancelItem("Отменить", null, 10);
		}
		mSelectPhotoPopupWindow.show();
	}

	private DatePickerPopupWindow2 mDatePickerPopupWindow = null;

	protected DatePickerPopupWindow2 showSelectBirthdayPopupWindow(
			View relyView,
			DatePickerPopupWindow2.OnDatePickerInfoListener listener) {
		if (mDatePickerPopupWindow == null) {
			mDatePickerPopupWindow = new DatePickerPopupWindow2(this, relyView,
					listener);
		}
		// mDatePickerPopupWindow.se
		mDatePickerPopupWindow.show();
		return mDatePickerPopupWindow;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUSET_CODE_USER_INFO_EX) {
			if (resultCode == Activity.RESULT_OK && data != null) {
				sex = data.getIntExtra(BabyInfoActivity.KEY_GENDER, sex);
				float xxheight = data.getFloatExtra(
						BabyInfoActivity.KEY_HEIGHT, height);
				height = xxheight / 100;
				weight = data
						.getFloatExtra(BabyInfoActivity.KEY_WEIGHT, weight);
				if (sex == 0) {
					// layout_gender.setContent(R.string.gender_0);
					textView_sex.setText(getApplicationContext().getResources().getString(R.string.girl));
				} else if (sex == 1) {
					// layout_gender.setContent(R.string.gender_1);
					textView_sex.setText(getApplicationContext().getResources().getString(R.string.boy));
				}
				
				textView_height.setText(height + "м");
				textView_weight.setText(weight + "кг");
				if (action.equals(Contanst.ACTION_EDIT)) {
					beanForDb___Family f = AppContext.getInstance().currentFamily;
					// if (f != null) {
					if (!TextUtils.isEmpty(f.img_path)) {
						
					} else {
						int imgSrcId = sex == 1 ? R.drawable.baby_boy
								: R.drawable.baby_girl;
						imageView_baby.setImageResource(imgSrcId);
					}
					// } else {
					// int imgSrcId = sex == 1 ? R.drawable.baby_boy
					// : R.drawable.baby_girl;
					// imageView_baby.setImageResource(imgSrcId);
					// }

					update_baby_info();
				} else if (action.equalsIgnoreCase(Contanst.ACTION_ADD)) {
					 int imgSrcId = sex == 1 ? R.drawable.baby_boy
					 : R.drawable.baby_girl;
					 imageView_baby.setImageResource(imgSrcId);
				}

			}
			return;
		} else if (requestCode == SET_RELATION_SHIP) {
			if (resultCode == Activity.RESULT_OK && data != null) {
				Bundle bundle = data.getExtras();
				String result = bundle.getString("relation");
				Log.d(TAG, "relation=" + result);
				updateRelationShip(result);

			}
		}

		else if (requestCode == ImageUtils.REQUEST_CODE_GETIMAGE_BYCAMERA) {
			if (resultCode == RESULT_OK)
				startActionCrop(origUri);
			// 拍照后裁剪
		} else if (requestCode == ImageUtils.REQUEST_CODE_GETIMAGE_BYCROP) {
			if (data != null && data.getData() != null)
				startActionCrop(data.getData());// 选图后裁剪
		} else if (requestCode == ImageUtils.REQUEST_CODE_GETIMAGE_BYSDCARD) {
			if (resultCode != RESULT_OK)
				return;
			// uploadNewPhoto();// 上传新照片`
			if (!StringUtils.isEmpty(protraitPath) && protraitFile.exists()) {
				protraitBitmap = ImageUtils.loadImgThumbnail(protraitPath, 120,
						120);
			}
			if (protraitBitmap != null) {
				try {
					// String filename = FileUtils.getFileName("photo.png");
					ImageUtils.saveImage(KidsProfileActivity.this, "photo.jpg",
							protraitBitmap);
					// }
					String imgbase64 = Base64Encode
							.encodeBase64FileFromContext(
									KidsProfileActivity.this, "photo.jpg");
					img_base64_d = imgbase64;

					if (action.equals(Contanst.ACTION_EDIT)) {
//						updateBabyImg(imgbase64);
						updateBabyImg_v2(imgbase64);
						// ---------------------------
					} else if (action.equalsIgnoreCase(Contanst.ACTION_ADD)) {
						imageView_baby.setImageBitmap(protraitBitmap);
					}

					// msg.obj = res;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		if (resultCode != Activity.RESULT_OK || data == null) {
			super.onActivityResult(requestCode, resultCode, data);
			return;
		}
	}

	// ----------------
	private final static int CROP = 120;
	private final static String FILE_SAVEPATH = KidsWatConfig.getTempFilePath();

	private Uri origUri;
	private Uri cropUri;
	private File protraitFile;
	private Bitmap protraitBitmap;
	private String protraitPath;

	/**
	 * 选择图片裁剪
	 * 
	 * @param output
	 */
	private void startImagePick() {
		Intent intent;
		if (Build.VERSION.SDK_INT < 19) {
			intent = new Intent();
			intent.setAction(Intent.ACTION_GET_CONTENT);
			intent.setType("image/*");
			startActivityForResult(intent,
					ImageUtils.REQUEST_CODE_GETIMAGE_BYCROP);
		} else {
			intent = new Intent(Intent.ACTION_PICK,
					Images.Media.EXTERNAL_CONTENT_URI);
			intent.setType("image/*");
			startActivityForResult(intent,
					ImageUtils.REQUEST_CODE_GETIMAGE_BYCROP);
		}

	}

	/**
	 * 相机拍照
	 * 
	 * @param output
	 */
	private void startActionCamera() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, this.getCameraTempFile());
		startActivityForResult(intent,
				ImageUtils.REQUEST_CODE_GETIMAGE_BYCAMERA);
	}

	/**
	 * 拍照后裁剪
	 * 
	 * @param data
	 *            原始图片
	 * @param output
	 *            裁剪后图片
	 */
	private void startActionCrop(Uri data) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(data, "image/*");
		intent.putExtra("output", this.getUploadTempFile(data));
		intent.putExtra("crop", "true");
		intent.putExtra("aspectX", 1);// 裁剪框比例
		intent.putExtra("aspectY", 1);
		intent.putExtra("outputX", CROP);// 输出图片大小
		intent.putExtra("outputY", CROP);
		intent.putExtra("scale", true);// 去黑边
		intent.putExtra("scaleUpIfNeeded", true);// 去黑边
		startActivityForResult(intent,
				ImageUtils.REQUEST_CODE_GETIMAGE_BYSDCARD);
	}

	// 拍照保存的绝对路径
	private Uri getCameraTempFile() {
		String storageState = Environment.getExternalStorageState();
		if (storageState.equals(Environment.MEDIA_MOUNTED)) {
			File savedir = new File(FILE_SAVEPATH);
			if (!savedir.exists()) {
				savedir.mkdirs();
			}
		} else {
			Toast.makeText(KidsProfileActivity.this, "无法保存上传的头像，请检查SD卡是否挂载",
					Toast.LENGTH_SHORT).show();
			return null;
		}

		String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss")
				.format(new Date());
		// 照片命名
		String cropFileName = "ks_camera_" + timeStamp + ".jpg";
		// String cropFileName = "bb_camera_"
		// + AppContext.getInstance().currentFamily.family_id + ".jpg";

		// 裁剪头像的绝对路径
		protraitPath = FILE_SAVEPATH + cropFileName;
		protraitFile = new File(protraitPath);
		cropUri = Uri.fromFile(protraitFile);
		this.origUri = this.cropUri;
		return this.cropUri;
	}

	// 裁剪头像的绝对路径
	private Uri getUploadTempFile(Uri uri) {
		String storageState = Environment.getExternalStorageState();
		if (storageState.equals(Environment.MEDIA_MOUNTED)) {
			File savedir = new File(FILE_SAVEPATH);
			if (!savedir.exists()) {
				savedir.mkdirs();
			}
		} else {
			Toast.makeText(KidsProfileActivity.this, "无法保存上传的头像，请检查SD卡是否挂载",
					Toast.LENGTH_SHORT).show();
			return null;
		}
		String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss")
				.format(new Date());
		String thePath = ImageUtils.getAbsolutePathFromNoStandardUri(uri);

		// 如果是标准Uri
		if (StringUtils.isEmpty(thePath)) {
			thePath = ImageUtils.getAbsoluteImagePath(KidsProfileActivity.this,
					uri);
		}
		String ext = FileUtil.getFileFormat(thePath);
		ext = StringUtils.isEmpty(ext) ? "jpg" : ext;
		// 照片命名
		// String cropFileName = "ks_crop_" + timeStamp + "." + ext;
		String cropFileName = "bb_" + KidsWatConfig.getDefaultFamilyId() + "."
				+ "jpg";
		// 裁剪头像的绝对路径
		protraitPath = FILE_SAVEPATH + cropFileName;
		saveBbImgPath = protraitPath;
		protraitFile = new File(protraitPath);

		cropUri = Uri.fromFile(protraitFile);
		return this.cropUri;
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	private void update_baby_info() {

		// -----------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		final String family_id = KidsWatConfig.getDefaultFamilyId();

		// beanForDb___Family f = AppContext.getInstance().currentFamily;

		final String request_url = KidsWatApiUrl.getUrlFor___update_baby_info(
				uid, access_token, family_id, nickname, birthday, sex, height,
				weight, 0);

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));

				try {
					JSONObject response = new JSONObject(t);

					int code = response.getInt("error");
					if (code == 0) {
						SqlDb db = SqlDb.get(KidsProfileActivity.this);
						int rows = db.updateFamily(family_id, nickname,
								birthday, sex, height, weight);

						// 同步更新数据
						AppContext.getInstance().currentFamily = db
								.getFamilyBy_fid(family_id);

						// 同步更新数据
						AppContext.getInstance().map_familyinfo.put(family_id,
								AppContext.getInstance().currentFamily);

						db.closeDb();

						String birthdayText = SimpleDateUtil.convert2String(
								birthday * 1000, SimpleDateUtil.DATE_FORMAT);
						TLog.log("birthday=" + birthday);
						textView_birthday.setText(birthdayText);
						textView_nickname.setText(nickname);

						AppContext.getInstance().currentFamily.nickname = nickname;
						AppContext.getInstance().currentFamily.birthday = birthday;
						AppContext.getInstance().currentFamily.birthdayText = birthdayText;

						BaseEvent event = new BaseEvent(
								BaseEvent.MSGTYPE_3___CHANGE_KIDS_NICKNAME,
								"change_nickname");
						EventBus.getDefault().post(event);

					} else {
						String msg = response.toString();
						showLongToast(msg);
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				if (errorNo == -1) {
					showLongToast("PПроверьте доступ в сеть и попробуйте снова	...");
				} else {
					String msg = String.format("%s(error=%s)",
							errorNo == -1 ? "Connect to the server failed"
									: strMsg, errorNo);
					showLongToast(msg);
				}
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 2);
			}
			// --------------

		});

		// -------------------------

	}

	private void updateRelationShip(final String relation) {

		// -----------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		final String family_id = KidsWatConfig.getDefaultFamilyId();
		final String version = AppContext.getInstance().currentFamily.version;
		final String device_id = AppContext.getInstance().currentFamily.device_id;

		final String request_url = KidsWatApiUrl.getUrlFor___update_relation(
				uid, access_token, family_id, relation, version, device_id);
		// -------------------------
		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));

				try {
					JSONObject response = new JSONObject(t);

					int code = response.getInt("error");
					if (code == 0) {
						textView_relation.setText(relation);
						SqlDb db = SqlDb.get(KidsProfileActivity.this);
						db.set___relation_by_uid_fid(uid, family_id, relation);
						AppContext.getInstance().loginUser_kid = db
								.getLoginMemberBy_uid_fid(uid, family_id);
						db.closeDb();
					} else {
						String message = response.getJSONObject("info")
								.getString("message");
						showLongToast(message);
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String
						.format("%s(error=%s)",
								errorNo == -1 ? "Connect to the server failed"
										: strMsg, errorNo);
				TLog.log(msg);
				showLongToast(msg);
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});
		// -------------------------
	}

	private void add_watch(final String qrcode, String relation) {
		// -----------------
//		new Thread(new ThreadParent(KidsWatApiUrl.getApiUrl(),
//				CommonThreadMethod.JsonFormat(GetStateParameter()), handler,
//				getdevicevoicelevelCode)).start();
		
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		String country_code = "", userPhone = "", version = "", device_country_code = "", device_sim_no = "";
		country_code = KidsWatConfig.getUseCountryCode();
		userPhone = KidsWatConfig.getUserPhone();
		String mobile = KidsWatConfig.getUserPhone();
		if (AppContext.codess == 461 || AppContext.codess == 5612 || AppContext.codess == 5613) {
			if (country != null)
				device_country_code = country;
			if (phone != null)
				device_sim_no = phone;
			version = AppContext.codess + "";
		} else if(AppContext.codess == 4620 || AppContext.codess == 361 || AppContext.codess == 601){
			version = AppContext.codess + "";
		}
		
		showWaitDialog("...Связывание устройства, подождите...");
		new Thread(new ThreadParent(KidsWatApiUrl.getApiUrl(), CommonThreadMethod.JsonFormat(
				get_add_watch_params(country_code, userPhone, version, device_country_code, device_sim_no, qrcode, relation, mobile)), handler, 123)).start();
		
		// final String family_id = AppContext.getDefaultFamilyId();
//		final String request_url = KidsWatApiUrl.getUrlFor___add_watch(uid,
//				access_token, qrcode, country_code, userPhone, relation,
//				version, device_country_code, device_sim_no);
//
//		ApiHttpClient.get(request_url, new HttpCallBack() {
//
//			@Override
//			public void onPreStart() {
//				showWaitDialog("...Связывание устройства, подождите...");
//			}
//
//			@Override
//			public void onSuccess(String t) {
//				super.onSuccess(t);
//				TLog.log(String.format("url:%s\nt:%s", request_url, t));
//
//				try {
//					JSONObject response = new JSONObject(t);
//					int code = response.getInt("error");
//					if (code == 0) {
//						// --------
//						// {"error":0,"info":{"message":"Successful operation!","device_id":"0000000000989688","family_id":10000001}}
//						String family_id = response.getJSONObject("info")
//								.getString("family_id");
//						// String device_id = response.getJSONObject(
//						// "info").getString("device_id");
//
//						// String nickname = textView_nickname.getText()
//						// .toString().trim();
//						add_baby_info(family_id, img_base64_d);
//						// active_watch(device_id);
//					} else if (code == 1233) {
//						showLongToast("Устройство не сети, попробуйте снова!");
//					}
//					// else if (code == 2015002) {
//					// showLongToast("The device is busy, please try again later");
//					// }
//					else {
//						String message = response.getJSONObject("info")
//								.getString("message");
//						showConfirmInformation(message);
//					}
//
//				} catch (JSONException e) {
//					e.printStackTrace();
//				}
//
//			}
//
//			@Override
//			public void onFailure(int errorNo, String strMsg) {
//				super.onFailure(errorNo, strMsg);
//				if (errorNo == -1) {
//					showLongToast("Please check your connection and try again.");
//				} else {
//					String msg = String.format("%s(error=%s)",
//							errorNo == -1 ? "Connect to the server failed"
//									: strMsg, errorNo);
//					showLongToast(msg);
//				}
//
//				button_ok.setEnabled(true);
//			}
//
//			@Override
//			public void onFinish() {
//				dismissDialog(null, 2);
//			}
//		});
	}

	private Map<String, String> get_add_watch_params(String country_code, String userPhone, String version, String device_country_code, String device_sim_no
			, String qrcode, String relation, String mobile) {
		
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
//		String country_code = "", userPhone = "", version = "", device_country_code = "", device_sim_no = "";
//		country_code = KidsWatConfig.getUseCountryCode();
//		userPhone = KidsWatConfig.getUserPhone();
//		
//		if (AppContext.codess == 461 || AppContext.codess == 5612 || AppContext.codess == 5613) {
//			if (country != null)
//				device_country_code = country;
//			if (phone != null)
//				device_sim_no = phone;
//			version = AppContext.codess + "";
//		} else if(AppContext.codess == 4620 || AppContext.codess == 361){
//			version = AppContext.codess + "";
//		}
		
//		final String action = "add_watch";
//		Map<String, String> inputParams = new HashMap<String, String>();
//		addExtraParaToMap(action, inputParams);
//		addTokenToMap(inputParams, uid, access_token);
//
//		inputParams.put("country_code", country_code);
//		inputParams.put("qrcode", qrcode);
//		inputParams.put("mobile", mobile);
//		inputParams.put("relation", relation);
//		inputParams.put("version", version);
//
//		inputParams.put("device_country_code", device_country_code);
//		inputParams.put("device_sim_no", device_sim_no);
//		// --------------------
//		return makeURL(getApiUrl(), inputParams);
		
		Map<String, String> hash = new HashMap<String, String>();
		hash.put("uid", uid);
		hash.put("access_token", access_token);
		hash.put("country_code", country_code);
		hash.put("userPhone", userPhone);
		hash.put("version", version);
		hash.put("mobile", mobile);
		hash.put("customer_no", 1 + "");
		
		hash.put("relation", relation);
		hash.put("qrcode", qrcode);
		
		hash.put("device_country_code", device_country_code);
		hash.put("device_sim_no", device_sim_no);
		KidsWatApiUrl.addExtraPara("add_watch", hash);

		return hash;
	}

	private void add_baby_info(String family_id, String imgbase64) {
		// --------------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		// final String family_id = AppContext.getDefaultFamilyId();

		HttpParams params = new HttpParams();

		TLog.log("img=" + imgbase64);

		long cur_time = System.currentTimeMillis() / 1000;
		String apitoken = Encoder.encode("MD5", String.valueOf(cur_time)
				+ KidsWatApiUrl.api_secret);// SHA-1

		params.put("time", String.valueOf(cur_time));
		params.put("api_token", apitoken);

		params.put("uid", uid);
		params.put("access_token", access_token);

		params.put("family_id", family_id);

		params.put("nickname", nickname);
		params.put("birthday", String.valueOf(birthday));
		params.put("sex", String.valueOf(sex));
		params.put("height", String.valueOf(height));
		params.put("weight", String.valueOf(weight));
		params.put("grade", 0);
		params.put("customer_no",1);
//		params.put("img", imgbase64);
		
		File imageFile = new File(saveBbImgPath);
		params.put("img", imageFile);

		final String request_url = KidsWatApiUrl.getApiUrl()
				+ "?action=add_baby_info";

		ApiHttpClient.getHttpClient().post(request_url, params, false,
				new HttpCallBack() {

					@Override
					public void onPreStart() {
						showWaitDialog("...Связывание устройства, подождите…");
					}

					@Override
					public void onSuccess(String t) {
						super.onSuccess(t);
						TLog.log(String.format("url:%s\nt:%s", request_url, t));

						try {
							JSONObject response = new JSONObject(t);

							int code = response.getInt("error");
							if (code == 0) {
								String message = response.getJSONObject("info")
										.getString("message");
								TLog.log(message);
								// showLongToast("add device successfully");
								dismissDialog(null, 10);

								DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										if (AppContext.getInstance()
												.getFamilyListCount() > 0) {
											EventBus.getDefault()
													.post(new BaseEvent(
															BaseEvent.MSGTYPE_1___ADD_KIDS_OK,
															"add baby ok"));

											KidsProfileActivity.this.finish();

										} else {
											// Intent intent = new Intent(
											// KidsProfileActivity.this,
											// MainActivity.class);
											// startActivity(intent);
											AppManager
													.AppRestart(KidsProfileActivity.this);
											KidsProfileActivity.this.finish();
										}
									}
								};
								String title = getApplicationContext().getResources().getString(R.string.app_name);
								String messagex = "Привязка устройства прошла успешно";
								String positiveText = "OK";
								String negativeText = null;

								showConfirmDialog(title, messagex,
										positiveText, negativeText,
										positiveListener);

							} else {
								// {"error":401,"info":{"message":"Parameter Error!"}}
								String msg = response.toString();
								TLog.log(msg);
								// Toast.makeText(getApplicationContext(), msg,
								// Toast.LENGTH_SHORT).show();
								String message = response.getJSONObject("info")
										.getString("message");
								showConfirmInformation(getApplicationContext().getResources().getString(R.string.app_name), message);
								// showConfirmInformation("Kid's Watcher",
								// "Pairing fails, please try again.");
							}

						} catch (JSONException e) {
							e.printStackTrace();
							showConfirmInformation(getApplicationContext().getResources().getString(R.string.app_name),
									"спаривание неудачу, Пожалуйста, попробуйте еще раз.");
						}

						button_ok.setEnabled(true);
					}

					@Override
					public void onFailure(int errorNo, String strMsg) {
						super.onFailure(errorNo, strMsg);
						if (errorNo == -1) {
//							showLongToast("Please check your connection and try again.");
							showLongToast(getApplicationContext().getResources().getString(R.string.check_conn));
						} else {
							/*String msg = String
									.format("%s(error=%s)",
											errorNo == -1 ? "Connect to the server failed"
													: strMsg, errorNo);
							showLongToast(msg);*/
							showLongToast(getApplicationContext().getResources().getString(R.string.check_conn));
						}
					}

					@Override
					public void onFinish() {
						dismissDialog(null, 2);
					}
			});
		}
	
		// TODO 上传头像
		private void updateBabyImg_v2(final String imgbase64) {
			// --------------------
			final String uid = KidsWatConfig.getUserUid();
			final String access_token = KidsWatConfig.getUserToken();
			final String family_id = KidsWatConfig.getDefaultFamilyId();

			HttpParams params = new HttpParams();

			long cur_time = System.currentTimeMillis() / 1000;
			String apitoken = Encoder.encode("MD5", String.valueOf(cur_time)
					+ KidsWatApiUrl.api_secret);// SHA-1

			params.put("time", String.valueOf(cur_time));
			params.put("api_token", apitoken);

			params.put("customer_no", 1);//俄罗斯标志
			params.put("uid", uid);
			params.put("access_token", access_token);
			params.put("family_id", family_id);
			File imageFile = new File(saveBbImgPath);
			params.put("img", imageFile);
			

			final String request_url = KidsWatApiUrl.getApiUrl()
					+ "?action=update_baby_img_v2";

			ApiHttpClient.getHttpClient().post(request_url, params, false,
					new HttpCallBack() {

						@Override
						public void onPreStart() {
							showWaitDialog("...Загрузка...");
						}

						@Override
						public void onSuccess(String t) {
							super.onSuccess(t);
//							imageFile.delete();
							TLog.log(String.format("url:%s\nt:%s", request_url, t));

							try {
								JSONObject response = new JSONObject(t);
								int code = response.getInt("error");
								System.out.println("errorcode="+code);
								if (code == 0) {

									SqlDb db = SqlDb.get(KidsProfileActivity.this);
									db.saveFamily(family_id, imgbase64,
											saveBbImgPath);
									AppContext.getInstance().currentFamily = db
											.getFamilyBy_fid(family_id);
									db.closeDb();
									
									try {
										File bbimg = new File(saveBbImgPath);

										Bitmap bmp = MediaStore.Images.Media
												.getBitmap(KidsProfileActivity.this
														.getContentResolver(), Uri
														.fromFile(bbimg));
										imageView_baby.setImageBitmap(bmp);
										
										//
										AppContext.getInstance().currentFamily.img_path = saveBbImgPath;
										AppContext.getInstance().map_familyinfo.put(family_id, AppContext.getInstance().currentFamily);
										BaseEvent event = new BaseEvent(
												BaseEvent.MSGTYPE_3___WHITE_SETTING_CHANGE_KIDS_PHOTO,
												saveBbImgPath);
										EventBus.getDefault().post(event);

									} catch (FileNotFoundException e) {
									} catch (IOException e) {
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}

							// ------------------
						}

						@Override
						public void onFailure(int errorNo, String strMsg) {
							super.onFailure(errorNo, strMsg);
							if (errorNo == -1) {
								showLongToast("Please check your connection and try again.");
							} else {
								String msg = String
										.format("%s(error=%s)",
												errorNo == -1 ? "Connect to the server failed"
														: strMsg, errorNo);
								showLongToast(msg);
							}
						}

						@Override
						public void onFinish() {
							dismissDialog(null, 10);
						}
					});

		}
		
	private void updateBabyImg(final String imgbase64) {

		// --------------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		final String family_id = KidsWatConfig.getDefaultFamilyId();

		HttpParams params = new HttpParams();
		// --------------

		long cur_time = System.currentTimeMillis() / 1000;
		String apitoken = Encoder.encode("MD5", String.valueOf(cur_time)
				+ KidsWatApiUrl.api_secret);// SHA-1

		params.put("time", String.valueOf(cur_time));
		params.put("api_token", apitoken);

		params.put("uid", uid);
		params.put("access_token", access_token);
		params.put("family_id", family_id);
		params.put("img", imgbase64);
		params.put("customer_no", 1);//俄罗斯标志

		final String request_url = KidsWatApiUrl.getApiUrl()
				+ "?action=update_baby_img";

		ApiHttpClient.getHttpClient().post(request_url, params, false,
				new HttpCallBack() {

					@Override
					public void onPreStart() {
						showWaitDialog("...Загрузка...");
					}

					@Override
					public void onSuccess(String t) {
						super.onSuccess(t);
						TLog.log(String.format("url:%s\nt:%s", request_url, t));
						
						try {
							JSONObject response = new JSONObject(t);
							int code = response.getInt("error");
							if (code == 0) {

								SqlDb db = SqlDb.get(KidsProfileActivity.this);
								db.saveFamily(family_id, imgbase64,
										saveBbImgPath);
								AppContext.getInstance().currentFamily = db
										.getFamilyBy_fid(family_id);
								db.closeDb();
								try {
									File bbimg = new File(saveBbImgPath);

									Bitmap bmp = MediaStore.Images.Media
											.getBitmap(KidsProfileActivity.this
													.getContentResolver(), Uri
													.fromFile(bbimg));
									imageView_baby.setImageBitmap(bmp);

									//
									BaseEvent event = new BaseEvent(
											BaseEvent.MSGTYPE_3___WHITE_SETTING_CHANGE_KIDS_PHOTO,
											saveBbImgPath);
									EventBus.getDefault().post(event);

								} catch (FileNotFoundException e) {
								} catch (IOException e) {
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						} catch (JSONException e) {
							e.printStackTrace();
						}

						// ------------------

					}

					@Override
					public void onFailure(int errorNo, String strMsg) {
						super.onFailure(errorNo, strMsg);
						if (errorNo == -1) {
							showLongToast("Please check your connection and try again.");
						} else {
							String msg = String
									.format("%s(error=%s)",
											errorNo == -1 ? "Connect to the server failed"
													: strMsg, errorNo);
							showLongToast(msg);
						}
					}

					@Override
					public void onFinish() {
						dismissDialog(null, 10);
					}
				});

	}

	String saveBbImgPath = "";

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
		}
		return false;
	}

	Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			String mess = msg.obj.toString();
			int code = msg.arg1;

			if (mess == null || mess.equals(""))
				return;
			
			JSONObject json = null;
			try {
				json = new JSONObject(mess);

				int errorCode = json.getInt("error");

				switch (code) {
				case 123:
					if (errorCode == 0) {
						String family_id = json.getJSONObject("info")
								.getString("family_id");
						KidsWatConfig.setDefaultFamilyId(family_id);
						
						add_baby_info(family_id, img_base64_d);
					} else if (errorCode == 1233) {
						showLongToast("Устройство не сети, попробуйте снова!");
					} else {
						String message = json.getJSONObject("info")
								.getString("message");
						showConfirmInformation(message);
					}
					dismissDialog(null, 2);
					break;
				}
			} catch (Exception e) {
				// TODO: handle exception
				dismissDialog(null, 2);
			}
				

		};
	};
}
