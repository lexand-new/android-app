package com.qiwo.xkidswatcher_russia.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.SwipeListView;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.adapter.TimeZoneAdapter;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.bean.TimeZone;
import com.qiwo.xkidswatcher_russia.thread.CommonThreadMethod;
import com.qiwo.xkidswatcher_russia.thread.ThreadParent;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.InjectView;

/***
 * 设置时区
 */
public class SettingTimeZoneActivity extends BaseActivity {

    @InjectView(R.id.linearLayout_l)
    LinearLayout linearLayout_l;

    @InjectView(R.id.tv_r)
    TextView tv_r;

    @InjectView(R.id.listview)
    SwipeListView listview;

    private TimeZoneAdapter timeZoneAdapter;
    private List<TimeZone> timeZones;
    public static final String[] timeZoneList = {"-11", "-10", "-9", "-8", "-7", "-6", "-5", "-4", "-3", "-2", "-1", "+0", "+1", "+2", "+3", "+4", "+5", "+6", "+7", "+8", "+9", "+10", "+11", "+12"};
    public static final Integer[] timeZoneListInt = {-11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    private String timeZone = "";
    private int inActivity = 0;
    private int time_zone_state = 0;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_setting_time_zone;
    }

    @Override
    protected boolean hasActionBar() {
        return false;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        super.init(savedInstanceState);
    }

    @Override
    public void initView() {
        linearLayout_l.setOnClickListener(this);
        tv_r.setOnClickListener(this);
    }

    @Override
    public void initData() {
        time_zone_state = getIntent().getIntExtra(Watch_SettingsActivity.SETTINGTIMEZONESTATE, -100);
        inActivity = getIntent().getIntExtra(Watch_SettingsActivity.INACTIVITY, -100);
        timeZones = new ArrayList<TimeZone>();
//        for (int i = 11; i > 0; i--) {
//            TimeZone timeZone = new TimeZone();
//            timeZone.setTimeZone("GMT -" + i);
//            timeZone.setOK(false);
//            timeZones.add(timeZone);
//        }
//        for (int i = 0; i < 13; i++) {
//            TimeZone timeZone = new TimeZone();
//            timeZone.setTimeZone("GMT +" + i);
//            timeZone.setOK(false);
//            timeZones.add(timeZone);
//        }
        for (int i = 0; i < timeZoneList.length; i++) {
            TimeZone timeZone = new TimeZone();
            timeZone.setTimeZone("GMT " + timeZoneList[i]);
            timeZone.setOK(false);
            timeZones.add(timeZone);
        }

        if (time_zone_state != -100) {
            String timeZone = String.valueOf(time_zone_state);
            for (int i = 0; i < 24; i++) {
                if (timeZone.equals(timeZoneList[i])) {
                    TimeZone mTimeZone = timeZones.get(i);
                    mTimeZone.setOK(true);
                    timeZones.set(i, mTimeZone);
                }
            }
        }
        timeZoneAdapter = new TimeZoneAdapter(timeZones, this);
        listview.setAdapter(timeZoneAdapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                if (timeZones != null && timeZones.size() > index) {
                    int size = timeZones.size();
                    boolean isSelect = timeZones.get(index).isOK();
                    for (int i = 0; i < size; i++) {
                        if (timeZones.get(i).isOK())
                            timeZones.get(i).setOK(false);
                    }
                    timeZones.get(index).setOK(!isSelect);
                    if (timeZoneAdapter != null)
                        timeZoneAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.linearLayout_l:
                finish();
                break;
            case R.id.tv_r:
                if (checkSelectTimeZone()) {
                    showWaitDialog("Загрузить в ...");
                    sendUpdateTimeZone();
                } else {
                    showShortToast(getString(R.string.setting_time_zone_check));
                }
                break;
            default:
                break;
        }
    }

    /**
     * 是否选中了一个时区
     */
    private boolean checkSelectTimeZone() {
        boolean isSelect = false;
        if (timeZones != null && timeZones.size() > 0) {
            int size = timeZones.size();
            if (size > 0) {
                for (TimeZone t : timeZones) {
                    if (t.isOK()) {
                        isSelect = true;
                        break;
                    }
                }
            }
        }
        return isSelect;
    }

    /**
     * 更新时区信息 上传数据到云端
     */
    private void sendUpdateTimeZone() {
        new Thread(new ThreadParent(KidsWatApiUrl.getApiUrl(),
                CommonThreadMethod.JsonFormat(SetStateParameter("update_device_time_zone_config")),
                handler, Watch_SettingsActivity.setdevicevoicelevelCode)).start();
    }

    /*
     * 获取状态的参数
     */
    public Map<String, String> SetStateParameter(String action) {

        Map<String, String> hash = new HashMap<String, String>();

        int size = timeZones.size();
        for (int i = 0; i < size; i++) {
            if (timeZones.get(i).isOK()) {
                timeZone = this.timeZoneList[i];
                time_zone_state = i;
            }
        }
        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        final String device_id = KidsWatConfig.getUserDeviceId();
        hash.put("uid", uid);
        hash.put("access_token", access_token);
        hash.put("time_zone", timeZone);
        hash.put("device_id", device_id);

        KidsWatApiUrl.addExtraPara(action, hash);

        return hash;
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            String mess = msg.obj.toString();
            int code = msg.arg1;

            if (mess == null || mess.equals(""))
                return;
            JSONObject json = null;
            try {
                json = new JSONObject(mess);

                int errorCode = json.getInt("error");
                switch (code) {
                    case Watch_SettingsActivity.setdevicevoicelevelCode:
                        JSONObject infoJson = json.getJSONObject("info");
                        if (errorCode == 0) {
                            hideWaitDialog();
                            showLongToast("Обновление информации о часовом поясе прошло успешно.");
                            new Handler() {
                                @Override
                                public void handleMessage(Message msg) {
                                    /**等待0.5秒执行**/
                                    finishSettingTimeZoneActivity();
                                }
                            }.sendEmptyMessageDelayed(0, 500);
                        } else {
                            showLongToast("Не удалось обновить");
                        }

                        break;
                    default:
                        break;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };


    /**finish activity**/
    private void finishSettingTimeZoneActivity() {
        Intent intent;
        if (inActivity == 0) {
            intent = new Intent(SettingTimeZoneActivity.this, Watch_SettingsActivity.class);
        } else {
            intent = new Intent(SettingTimeZoneActivity.this, LocatingModeActivity.class);
        }
        intent.putExtra(Watch_SettingsActivity.SETTINGTIMEZONESTATE, time_zone_state);
        setResult(Watch_SettingsActivity.SETTINGTIMEZONECODE, intent);
        SettingTimeZoneActivity.this.finish();
    }
}
