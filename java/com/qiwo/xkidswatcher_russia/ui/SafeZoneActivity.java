package com.qiwo.xkidswatcher_russia.ui;

import org.kymjs.kjframe.http.HttpCallBack;

import com.handmark.pulltorefresh.library.SwipeListView;
import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.adapter.SafeZoneSwipeAdapter;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.bean.beanFor___delete_safe_area_config;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_safe_area_config;
import com.qiwo.xkidswatcher_russia.util.ACache;
import com.qiwo.xkidswatcher_russia.util.Contanst;
import com.qiwo.xkidswatcher_russia.util.TLog;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import butterknife.InjectView;

@SuppressLint("InflateParams")
public class SafeZoneActivity extends BaseActivity {

	@InjectView(R.id.linearLayout_l)
	LinearLayout linearLayout_l;

	@InjectView(R.id.button_add)
	Button button_add;

	@InjectView(R.id.content_empty)
	LinearLayout content_empty;

	@InjectView(R.id.listview)
	SwipeListView listview;

	final String TAG = SafeZoneActivity.class.getSimpleName();

	private beanFor___get_safe_area_config mm_get_safe_area_config_Status;
	private SafeZoneSwipeAdapter adapter;

	// -----------

	@Override
	protected int getLayoutId() {
		return R.layout.activity_safe_zone;
	}

	@Override
	protected boolean hasActionBar() {
		return false;
	}

	@Override
	protected void init(Bundle savedInstanceState) {
		super.init(savedInstanceState);
		// -----------
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.linearLayout_l:
			finish();
			break;
		case R.id.button_add:
			if (mm_get_safe_area_config_Status == null
					|| mm_get_safe_area_config_Status.info == null
					|| mm_get_safe_area_config_Status.info.ddata == null
					|| mm_get_safe_area_config_Status.info.ddata.config == null
					|| mm_get_safe_area_config_Status.info.ddata.config.size() < 10) {

//				Intent dIntent = new Intent(SafeZoneActivity.this,
//						SetSafeZoneActivity.class);
				Intent dIntent = new Intent(SafeZoneActivity.this,SetSafeZoneActivity_v2.class);
				
				dIntent.putExtra(Contanst.KEY_ACTION, "add");
				startActivityForResult(dIntent, 200);
			} else {
				showConfirmInformation(null,
						getApplicationContext().getResources().getString(R.string.text_safezone_max));
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void initView() {
		linearLayout_l.setOnClickListener(this);
		button_add.setOnClickListener(this);
		// ---------
	}

	@Override
	public void initData() {
		get_safe_area_config();
	}

	// -------------
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// if (requestCode == 200) {
		if (resultCode == 1110) {
			showConfirmInformations(getApplicationContext().getResources().getString(R.string.reminder), getApplicationContext().getResources().getString(R.string.text_add_safezone_success));
			get_safe_area_config();
		} else if (resultCode == 1111) {
			showConfirmInformations(getApplicationContext().getResources().getString(R.string.reminder), getApplicationContext().getResources().getString(R.string.text_update_safezone_success));
			get_safe_area_config();
		}
		// }
	}

	private void bindData() {

		if (mm_get_safe_area_config_Status.info.ddata.config.size() > 0) {

			listview.setVisibility(View.VISIBLE);
			content_empty.setVisibility(View.GONE);
			adapter = new SafeZoneSwipeAdapter(this,
					listview.getRightViewWidth(),
					new SafeZoneSwipeAdapter.IOnItemRightClickListener() {
						@Override
						public void onRightClick(View v, int position,
								Object object) {
							// TODO Auto-generated method stub
							beanFor___get_safe_area_config.CConfig bean = (beanFor___get_safe_area_config.CConfig) object;
							delete_safe_area_config(bean.latitude,
									bean.longitude, position);
						}
					});
			adapter.setList(mm_get_safe_area_config_Status.info.ddata.config);
			// adapter.setList(mm_familyMemberMsg);
			// listview.addHeaderView(LayoutInflater.from(this).inflate(
			// R.layout.view_family_member_list_footer, null));
			// listview.addFooterView(LayoutInflater.from(this).inflate(
			// R.layout.view_family_member_list_footer, null));

			listview.setAdapter(adapter);
			listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// showLongToast("item onclick " + position);
					beanFor___get_safe_area_config.CConfig safezone_config = mm_get_safe_area_config_Status.info.ddata.config
							.get(position);
					Intent dIntent = new Intent(SafeZoneActivity.this,
							SetSafeZoneActivity_v2.class);
					dIntent.putExtra(Contanst.KEY_ACTION, "edit");
					dIntent.putExtra("index", position);

					Bundle mm_bundle = new Bundle();
					mm_bundle.putSerializable("zone", safezone_config);
					dIntent.putExtras(mm_bundle);
					startActivityForResult(dIntent, 100);
				}
			});

		} else {
			listview.setVisibility(View.GONE);
			content_empty.setVisibility(View.VISIBLE);
		}

	}

	private void get_safe_area_config() {

		// -----------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		String family_id = KidsWatConfig.getDefaultFamilyId();
		// String device_id = AppContext.getInstance().getProperty(
		// "user.device_id");

		final String request_url = KidsWatApiUrl
				.getUrlFor___get_safe_area_config(uid, access_token, family_id);
		// -------------------------

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));

				beanFor___get_safe_area_config response = AppContext
						.getInstance().getGson()
						.fromJson(t, beanFor___get_safe_area_config.class);

				mm_get_safe_area_config_Status = response;
				if (response.error == 0) {
					ACache mCache = ACache.get(SafeZoneActivity.this);
					mCache.put("get_safe_area_config", t, ACache.TIME_DAY * 30);

					bindData();
				} else {
					showLongToast(response.info.message);
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String.format("%s", strMsg);
				System.out.println("error=" + msg);
				showLongToast(msg);
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});

	}

	private void delete_safe_area_config(double latitude, double longitude,
			final int position) {

		// -----------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		String family_id = KidsWatConfig.getDefaultFamilyId();
		// String device_id = AppContext.getInstance().getProperty(
		// "user.device_id");

		final String request_url = KidsWatApiUrl
				.getUrlFor___delete_safe_area_config(uid, access_token,
						family_id, latitude, longitude);
		// -------------------------

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));

				beanFor___delete_safe_area_config response = AppContext
						.getInstance().getGson()
						.fromJson(t, beanFor___delete_safe_area_config.class);

				if (response.error == 0) {
					showLongToast(getApplicationContext().getResources().getString(R.string.delete_ok));
					listview.resetItems();
					adapter.getList().remove(position);
					adapter.notifyDataSetChanged();
				} else {
					showLongToast(response.info.message);
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String.format("%s", strMsg);
				TLog.log("error=" + msg);
				showLongToast(msg);
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
		}
		return false;
	}

	/**
	 * --------------------------
	 */

}
