package com.qiwo.xkidswatcher_russia.ui;

//import android.os.Bundle;
//import android.app.Activity;
//import android.util.Log;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.ImageView;
//
//public class ScanGuideCameraActivity extends Activity {
//
//	final private String TAG = "ScanGuideCameraActivity";
//	private ImageView back_imageview;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.camera);
//		Log.e(TAG, "--onCreate--");
//		// -----------
//		back_imageview = (ImageView) findViewById(R.id.back_imageview);
//
//		back_imageview.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				ScanGuideCameraActivity.this.finish();
//			}
//		});
//
//	}
//	// ----------------
//
//}

import java.io.IOException;
import java.util.Vector;

import org.kymjs.kjframe.http.HttpCallBack;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.google.zxing.camera.CameraManager;
import com.google.zxing.decoding.CaptureActivityHandler;
import com.google.zxing.decoding.InactivityTimer;
import com.google.zxing.view.ViewfinderView;
import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.bean.beanFor___check_qrcode;
import com.qiwo.xkidswatcher_russia.util.TLog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.Toast;

public class ScanGuideCameraActivity extends Activity implements Callback,
		View.OnClickListener {

	public static final int SCAN_QRCODE_OK = 20;

	private CaptureActivityHandler handler;
	private ViewfinderView viewfinderView;
	private boolean hasSurface;
	private Vector<BarcodeFormat> decodeFormats;
	private String characterSet;
	// private TextView txtResult;
	private InactivityTimer inactivityTimer;
	private MediaPlayer mediaPlayer;
	private boolean playBeep;
	private static final float BEEP_VOLUME = 0.02f;
	private boolean vibrate;

	private LinearLayout linearLayout_l;

	// -----------------------
	private ProgressDialog progressDialog = null;

	protected void showWaitDialog(String __msg) {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.setMessage(__msg);
		} else {
			progressDialog = ProgressDialog.show(ScanGuideCameraActivity.this,
					null, __msg);
		}
	}

	protected void dismissDialog() {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
	}

	// -----------------------

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scan_camera);

		CameraManager.init(getApplication());

		viewfinderView = (ViewfinderView) findViewById(R.id.viewfinder_view);
		// txtResult = (TextView) findViewById(R.id.txtResult);
		linearLayout_l = (LinearLayout) findViewById(R.id.linearLayout_l);

		hasSurface = false;
		inactivityTimer = new InactivityTimer(this);
		getActionBar().hide();

		findViewById(R.id.item_lost_qrcard).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						// Toast.makeText(ScanGuideCameraActivity.this,
						// "QRcode не прочитан.",
						// Toast.LENGTH_LONG).show();
						startActivity(new Intent(ScanGuideCameraActivity.this,
								ScanGuideRecoverQrcodeActivity.class));
					}
				});

		linearLayout_l.setOnClickListener(this);
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onResume() {
		super.onResume();
		SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
		SurfaceHolder surfaceHolder = surfaceView.getHolder();
		if (hasSurface) {
			initCamera(surfaceHolder);
		} else {
			surfaceHolder.addCallback(this);
			surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}
		decodeFormats = null;
		characterSet = null;

		playBeep = true;
		AudioManager audioService = (AudioManager) getSystemService(AUDIO_SERVICE);
		if (audioService.getRingerMode() != AudioManager.RINGER_MODE_NORMAL) {
			playBeep = false;
		}
		initBeepSound();
		vibrate = true;
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (handler != null) {
			handler.quitSynchronously();
			handler = null;
		}
		CameraManager.get().closeDriver();
	}

	@Override
	protected void onDestroy() {
		inactivityTimer.shutdown();
		super.onDestroy();
	}

	private void initCamera(SurfaceHolder surfaceHolder) {
		try {
			CameraManager.get().openDriver(surfaceHolder);
		} catch (Exception e) {
			// TODO 加入了初始化摄像头的检测
			// ToastUtil.show(ctx, R.string.addwatch_camera_error);
			Toast.makeText(getApplicationContext(), "初始化摄相头失败.",
					Toast.LENGTH_SHORT).show();
			Log.d("tag", "初始化摄像头失败");
			setResult(Activity.RESULT_CANCELED);
			finish();
			return;
		}
		if (handler == null) {
			handler = new CaptureActivityHandler(this, decodeFormats,
					characterSet);
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {

	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if (!hasSurface) {
			hasSurface = true;
			initCamera(holder);
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		hasSurface = false;
	}

	public ViewfinderView getViewfinderView() {
		return viewfinderView;
	}

	public Handler getHandler() {
		return handler;
	}

	public void drawViewfinder() {
		viewfinderView.drawViewfinder();
	}

	public void handleDecode(Result obj, Bitmap barcode) {
		String text = obj.getText();

		// QR码过滤（如果qr码为url的参数，则取出对应的值）
		text = filterUrlQr(text);
		if (text == null) {
			text = "";
		}

		// Pattern pattern = Pattern.compile("[0-9A-Fa-f]{16}");
		// Matcher matcher = pattern.matcher(text);
		// if (matcher.matches()) {
		inactivityTimer.onActivity();

		// viewfinderView.drawResultBitmap(barcode);

		playBeepSoundAndVibrate();
		// txtResult.setText(obj.getBarcodeFormat().toString() + ":" + text);

		// Intent intent = new Intent(ctx, UserRelationshipActivity.class);
		// intent.putExtra("qrcode", text);
		// startActivity(intent);
		// overridePendingTransition(R.anim.push_left, 0);
		check_qrcode(text);

		// } else {
		// // Toast.makeText(ctx, R.string.addwatch_scan_qrcode_error,
		// // Toast.LENGTH_SHORT).show();
		// Toast.makeText(getApplicationContext(), "扫描条码失败.",
		// Toast.LENGTH_SHORT).show();
		// Log.d("tag", "scan error");
		// // 重扫
		// handler.postDelayed(new Runnable() {
		// @Override
		// public void run() {
		// try {
		// handler.sendEmptyMessage(R.id.restart_preview);
		// } catch (Exception e) {
		// }
		// }
		// }, 0);
		// }

		handler.codestate = false;

	}

	private String filterUrlQr(String text) {
		if (text != null && text.startsWith("http://")) {
			try {
				// TODO 比较严谨的方案，但相对效率较低
				// text = Utils.pauseUrlParas(text).get("qr");
				/*
				 * //TODO 效率较高的方法处理 int textLen = text.length(); if(textLen >
				 * 30){ //http://x.x?qr=16字符 text = text.substring(textLen-16,
				 * textLen); }
				 */
			} catch (Exception e) {
			}
		}
		return text;
	}

	private void initBeepSound() {
		if (playBeep && mediaPlayer == null) {
			// The volume on STREAM_SYSTEM is not adjustable, and users found it
			// too loud,
			// so we now play on the music stream.
			setVolumeControlStream(AudioManager.STREAM_MUSIC);
			mediaPlayer = new MediaPlayer();
			mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mediaPlayer.setOnCompletionListener(beepListener);

			AssetFileDescriptor file = getResources().openRawResourceFd(
					R.raw.beep);//
			try {
				mediaPlayer.setDataSource(file.getFileDescriptor(),
						file.getStartOffset(), file.getLength());
				file.close();
				mediaPlayer.setVolume(BEEP_VOLUME, BEEP_VOLUME);
				mediaPlayer.prepare();
			} catch (IOException e) {
				mediaPlayer = null;
			}
		}
	}

	private static final long VIBRATE_DURATION = 200L;

	private void playBeepSoundAndVibrate() {
		if (playBeep && mediaPlayer != null) {
			mediaPlayer.start();
		}
		if (vibrate) {
			Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
			vibrator.vibrate(VIBRATE_DURATION);
		}
	}

	/**
	 * When the beep has finished playing, rewind to queue up another one.
	 */
	private final OnCompletionListener beepListener = new OnCompletionListener() {
		public void onCompletion(MediaPlayer mediaPlayer) {
			mediaPlayer.seekTo(0);
		}
	};

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.linearLayout_l:
			finish();
			break;
		default:
			break;
		}
	}

	// -------------
	private void check_qrcode(final String qrcode) {
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		// String device_id = AppContext.getInstance().getProperty(
		// "user.device_id");
		// String family_id = AppContext.getDefaultFamilyId();

		final String request_url = KidsWatApiUrl.getUrlFor___check_qrcode(uid,
				access_token, qrcode);

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));
				beanFor___check_qrcode b = AppContext.getInstance().getGson()
						.fromJson(t, beanFor___check_qrcode.class);

				if (b.error == 0) {
					Intent resultIntent = new Intent();
					Bundle bundle = new Bundle();
					bundle.putString("result", qrcode);
					resultIntent.putExtras(bundle);
					ScanGuideCameraActivity.this.setResult(SCAN_QRCODE_OK,
							resultIntent);// RESULT_OK

					finish();
					return;
				} else if (b.error == 1043) {
					showConfirmInformation(getApplicationContext().getResources().getString(R.string.device_has_admin));
				} else if (b.error == -1) {
					showConfirmInformation(getApplicationContext().getResources().getString(R.string.device_not_shaped));
				} else if (b.error == -2) {
					// :{"error":-2,"info":{"message":"This qrcode is not validate!"}}
					showConfirmInformation(getApplicationContext().getResources().getString(R.string.canot_read));
				} else {
					showConfirmInformation("Запросите у администратора возможность сопровождать это устройство");
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String
						.format("%s(error=%s)",
								errorNo == -1 ? "Подключение к серверу отсутствует"
										: strMsg, errorNo);
				TLog.log(msg);
			}

			@Override
			public void onFinish() {
				dismissDialog();
			}
		});

	}

	// private void showLongToast(String message) {
	// Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	// }

	protected void showConfirmInformation(String message) {
		showConfirmInformation(null, message);
	}

	protected void showConfirmInformation(String title, String message) {
		if (title == null)
			message = "\n" + message;
		new AlertDialog.Builder(this).setMessage(message).setTitle(title)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						dialog.dismiss();

						handler.codestate = true;
						handler.postDelayed(new Runnable() {
							@Override
							public void run() {
								try {
									handler.sendEmptyMessage(R.id.restart_preview);
								} catch (Exception e) {
								}
							}
						}, 0);
					}
				}).show();
	}
	// ------------

}
