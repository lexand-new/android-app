package com.qiwo.xkidswatcher_russia.ui;

import java.util.ArrayList;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.ui.SetInvitePhoneNumActivity;
import com.qiwo.xkidswatcher_russia.ui.SetRelationActivity2;
import com.qiwo.xkidswatcher_russia.util.Contanst;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.InjectView;

public class InviteFamilyMemberActivity extends BaseActivity {
	@InjectView(R.id.ll_invite)
	LinearLayout ll_invite;
	
	@InjectView(R.id.linearLayout_l)
	LinearLayout linearLayout_l;
	
	@InjectView(R.id.ll_relationship)
	LinearLayout ll_relationship;
	
	@InjectView(R.id.ll_phone_num)
	LinearLayout ll_phone_num;
	
	@InjectView(R.id.tv_relationship)
	TextView tv_relationship;
	
	@InjectView(R.id.tv_phone)
	TextView tv_phone;
	
	@InjectView(R.id.tv_ok)
	TextView tv_ok;
	
	//邀请家庭圈成员
	String relationship;
	String phone;
	String country_code;
	ArrayList<String> mList;
	
	//选择关系
	public void chooseReletive(){
		Intent intent = new Intent(InviteFamilyMemberActivity.this, SetRelationActivity2.class);
		intent.putExtra("action", "add_family");
		intent.putStringArrayListExtra("exist_relationships", mList);
		startActivityForResult(intent, Contanst.REQUEST_CODE_RELATION);
	}
	
//	//选择联系人-从通讯录添加
//	public void chooseFamilyMember(){
//		Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
//		startActivityForResult(intent, Constants.REQUEST_CODE_FAMILY_MEMBER);
//	}
	
	//添加联系人号码
	public void choosePhoneMember(){
		Intent intent = new Intent(InviteFamilyMemberActivity.this, SetInvitePhoneNumActivity.class);
		intent.setAction("invite_family_member");
		startActivityForResult(intent, Contanst.REQUEST_CODE_FAMILY_MEMBER);
	}
	
	// TODO
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.ll_relationship:
			chooseReletive();
			break;
		
		case R.id.ll_phone_num:
			choosePhoneMember();
			break;
			
		case R.id.linearLayout_l:
			finish();
			break;
			
		case R.id.ll_invite:
			if(tv_ok.isEnabled()){
				Intent intent = new Intent();
				
				intent.putExtra("phone", phone);
				intent.putExtra("relation", relationship);
				intent.putExtra("country_code", country_code);
				
				setResult(600, intent);
				finish();
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void initView() {
		// TODO Auto-generated method stub
		linearLayout_l.setOnClickListener(this);
		ll_invite.setOnClickListener(this);
		ll_relationship.setOnClickListener(this);
		ll_phone_num.setOnClickListener(this);
	}

	@Override
	public void initData() {
		// TODO Auto-generated method stub
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		mList = extras.getStringArrayList("exist_relationships");
	}
	
	@Override
	protected void onActivityResult(int req_code, int res_code, Intent intent) {
		super.onActivityResult(req_code, res_code, intent);
		if(intent == null){
			return;
		}
		
		switch (req_code) {
		case Contanst.REQUEST_CODE_RELATION:
			// TODO 
			// 更新UI
			relationship = intent.getStringExtra("relation");
			
			tv_relationship.setText(relationship);
			
			if(!TextUtils.isEmpty(tv_phone.getText()) && !TextUtils.isEmpty(tv_relationship.getText())){
				tv_ok.setEnabled(true);
			} else{
				tv_ok.setEnabled(false);
			}
			break;

		case Contanst.REQUEST_CODE_COUNTRY_CODE:
			// TODO 
			// 更新UI
			break;
			
		case Contanst.REQUEST_CODE_FAMILY_MEMBER:
			// TODO 
			// 更新UI
			phone = intent.getStringExtra("phone");
			country_code = intent.getStringExtra("country_code");
			
			tv_phone.setText(country_code + " " + phone);
			if(!TextUtils.isEmpty(tv_phone.getText()) && !TextUtils.isEmpty(tv_relationship.getText())){
				tv_ok.setEnabled(true);
			} else{
				tv_ok.setEnabled(false);
			}
			break;
			
		default:
			break;
		}
	}
	
	private String getContactPhone(Cursor cursor) {
		int phoneColumn = cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
		int phoneNum = cursor.getInt(phoneColumn);
		String result = "";
		if (phoneNum > 0) {
			// 获得联系人的ID号
			int idColumn = cursor.getColumnIndex(ContactsContract.Contacts._ID);
			String contactId = cursor.getString(idColumn);
			// 获得联系人电话的cursor
			Cursor phone = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
					ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=" + contactId, null, null);
			if (phone.moveToFirst()) {
				for (; !phone.isAfterLast(); phone.moveToNext()) {
					int index = phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
					int typeindex = phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE);
					int phone_type = phone.getInt(typeindex);
					String phoneNumber = phone.getString(index);
					result = phoneNumber;
				}
				if (!phone.isClosed()) {
					phone.close();
				}
			}
		}
		return result;
	}

	@Override
	protected boolean hasActionBar() {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	protected int getLayoutId() {
		// TODO Auto-generated method stub
		return R.layout.activity_invite_family;
	}
}

