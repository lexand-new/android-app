package com.qiwo.xkidswatcher_russia.ui;

import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.util.StringUtils;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import butterknife.InjectView;

public class SettingActivity extends BaseActivity {

	@InjectView(R.id.linearLayout_l)
	LinearLayout linearLayout_l;

	@InjectView(R.id.editText_api_url)
	EditText editText_api_url;

	@InjectView(R.id.button_ok)
	Button button_ok;

	@Override
	protected int getLayoutId() {
		return R.layout.activity_setting;
	}

	@Override
	protected boolean hasActionBar() {
		return false;
	}

	@Override
	protected void init(Bundle savedInstanceState) {
		super.init(savedInstanceState);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.linearLayout_l:
			finish();
			break;
		case R.id.button_ok:
			String apiUrl = editText_api_url.getText().toString().trim();
			if (StringUtils.isEmpty(apiUrl)) {
				showLongToast("url is empty!");
			} else {
				KidsWatConfig.saveApiUrl(apiUrl);
				showLongToast("setting saved!");
				finish();
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void initView() {
		linearLayout_l.setOnClickListener(this);
		button_ok.setOnClickListener(this);
	}

	@Override
	public void initData() {
		String apiUrl = KidsWatConfig.getApiUrl();
		editText_api_url.setText(apiUrl);
	}
	// ----------------

}
