package com.qiwo.xkidswatcher_russia.ui;

import com.qiwo.xkidswatcher_russia.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SettingCountryOrPhoneActivity extends Activity implements
		OnClickListener {

	/**
	 * 设置国家和手机
	 */
	final private int GET_CODE = 1000;

	private TextView title;

	private LinearLayout linearLayout_l;
	private EditText Country_number;
	private Button change_ok, Country_btn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().hide();
		setContentView(R.layout.activity_change_sim);
		init();
	}

	public void init() {

		//
		linearLayout_l = (LinearLayout) findViewById(R.id.linearLayout_l);
		title = (TextView) findViewById(R.id.title);
		Country_number = (EditText) findViewById(R.id.Country_number);

		Country_btn = (Button) findViewById(R.id.Country_btn);
		change_ok = (Button) findViewById(R.id.change_ok);

		Country_btn.setOnClickListener(this);
		change_ok.setOnClickListener(this);
		linearLayout_l.setOnClickListener(this);

		title.setText("Input SIM Number");
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.Country_btn:
			startActivityForResult(new Intent(this, CountryActivity.class),
					GET_CODE);
			break;
		case R.id.change_ok:

			String editnum = Country_number.getText().toString().trim();
			if (!editnum.equals("")) {
				Intent mintent = new Intent(SettingCountryOrPhoneActivity.this,
						SetRelationActivity2.class);
				Intent intent = getIntent();
				mintent.putExtra("action", "add_device");
				mintent.putExtra("qrcode", intent.getStringExtra("qrcode"));
				mintent.putExtra("country", Country_btn.getText().toString()
						.trim().replace("+", ""));
				mintent.putExtra("phone", Country_number.getText().toString()
						.trim());
				startActivity(mintent);
				finish();
			} else {
				Toast.makeText(SettingCountryOrPhoneActivity.this,
						"Country code is empty!", 1).show();
			}

			break;
		case R.id.linearLayout_l:

			this.finish();
			break;
		default:
			break;
		}
	}

	/**
	 * This method is called when the sending activity has finished, with the
	 * result it supplied.
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == GET_CODE) {
			if (resultCode != RESULT_CANCELED) {
				if (data != null) {
					Country_btn.setText(data.getAction());
				}
			}
		}
	}

}
