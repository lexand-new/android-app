package com.qiwo.xkidswatcher_russia.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import com.handmark.pulltorefresh.library.SwipeListView;
import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.adapter.FamilyMemberSwipeAdapter;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.bean.FamilyMemberMsg;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___LoginMember;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_group_list;
import com.qiwo.xkidswatcher_russia.bean.beanFor___invite_join_family;
import com.qiwo.xkidswatcher_russia.db.SqlDb;
import com.qiwo.xkidswatcher_russia.util.Contanst;
import com.qiwo.xkidswatcher_russia.util.TLog;
import com.qiwo.xkidswatcher_russia.widget.APopupWindow.ItemPosition;
import com.qiwo.xkidswatcher_russia.widget.APopupWindow.onClickItemListener;
import com.qiwo.xkidswatcher_russia.widget.BottomPopupWindow;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import butterknife.InjectView;

@SuppressLint("InflateParams")
public class FamilyMemberActivity extends BaseActivity {

	@InjectView(R.id.linearLayout_l)
	LinearLayout linearLayout_l;

	@InjectView(R.id.linearLayout_cc)
	LinearLayout linearLayout_cc;

	@InjectView(R.id.button_invite)
	Button button_invite;

	@InjectView(R.id.listview)
	SwipeListView listview;

	final String TAG = FamilyMemberActivity.class.getSimpleName();
	private beanFor___get_group_list mm_get_group_listStatus;
	private List<FamilyMemberMsg> mm_familyMemberMsg;

	// -----------
	@Override
	protected int getLayoutId() {
		return R.layout.activity_family_member;
	}

	@Override
	protected boolean hasActionBar() {
		return false;
	}

	@Override
	protected void init(Bundle savedInstanceState) {
		super.init(savedInstanceState);
		// -----------
	}

    private void requrePhonePermission() {
		// TODO Auto-generated method stub
		if(Build.VERSION.SDK_INT >= 23){
        	if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                //拥有权限
        		requreContactPermission();
            } else {
            	ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.READ_PHONE_STATE}, Contanst.REQUEST_PHONE_REQUEST_PERMISSION);
            }
        } else{
    		 getData();
        }
	}
    
    /**
     * 申请联系人和短信权限
     */
    private void requreContactPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            //拥有权限
            //TODO 向SD卡写数据
        	requreSMSPermission();
        } else {
            //申请权限
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.READ_CONTACTS}, Contanst.REQUEST_READ_CONTACT_REQUEST_PERMISSION);
        }
    }
    
    private void requreSMSPermission() {
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {
			//拥有权限
			//TODO 向SD卡写数据
			getData();
		} else {
			//申请权限
			ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.READ_SMS}, Contanst.REQUEST_READ_SMS_REQUEST_PERMISSION);
		}
    }
    
    @SuppressLint("Override")
	@Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    	if(grantResults.length == 0){
    		return;
    	}
    	
        if (requestCode == Contanst.REQUEST_READ_CONTACT_REQUEST_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permission granted
                //TODO 向SD卡写数据
            	requreSMSPermission();
            	return;
            } else {
                //permission denied
                //TODO 显示对话框告知用户必须打开权限
            	Toast.makeText(this, getApplicationContext().getResources().getString(R.string.contact_permission), Toast.LENGTH_LONG).show();
            }
        } else if(requestCode == Contanst.REQUEST_READ_SMS_REQUEST_PERMISSION){
        	 if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                 //permission granted
                 //TODO 向SD卡写数据
        		 getData();
             } else {
                 //permission denied
                 //TODO 显示对话框告知用户必须打开权限
             	Toast.makeText(this, getApplicationContext().getResources().getString(R.string.sms_permission), Toast.LENGTH_LONG).show();
             }
        } else if(requestCode == Contanst.REQUEST_PHONE_REQUEST_PERMISSION){
        	if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permission granted
                //TODO 向SD卡写数据
        		requreContactPermission();
            } else {
                //permission denied
                //TODO 显示对话框告知用户必须打开权限
            	Toast.makeText(this, getApplicationContext().getResources().getString(R.string.phone_permission), Toast.LENGTH_LONG).show();
            }
        }
    }
    
    private void getData() {
		// TODO Auto-generated method stub
		if (AppContext.getInstance().loginUser_kid != null
				&& AppContext.getInstance().loginUser_kid.isAdmin == 1) {
			button_invite.setVisibility(View.VISIBLE);
		} else {
			button_invite.setVisibility(View.GONE);
		}
		mm_familyMemberMsg = new ArrayList<FamilyMemberMsg>();
		get_group_list();
	}
    
	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.linearLayout_l:
			finish();
			break;
		case R.id.button_invite:
			final String uid = KidsWatConfig.getUserUid();
			String family_id = KidsWatConfig.getDefaultFamilyId();

			SqlDb db = SqlDb.get(this);
			beanForDb___LoginMember b = db.getLoginMemberBy_uid_fid(uid,
					family_id);
			db.closeDb();
			
			if (b.isAdmin == 1) {
				if (mm_get_group_listStatus != null
						&& mm_get_group_listStatus.info.total >= 6) {
					showLongToast(getApplicationContext().getResources().getString(R.string.tip_add_family_to_max));
				} else{
					ArrayList<String> arraylist = new ArrayList<String>();
					for (FamilyMemberMsg msg : mm_familyMemberMsg) {
						arraylist.add(msg.relation);
					}
					Intent intent = new Intent(FamilyMemberActivity.this, InviteFamilyMemberActivity.class);
					Bundle bd = new Bundle();
					bd.putStringArrayList("exist_relationships", arraylist);
					intent.putExtras(bd);
					startActivityForResult(intent, 600);
				}
			/*final String uid = KidsWatConfig.getUserUid();
			// final String access_token = KidsWatConfig.getUserToken();
			String family_id = KidsWatConfig.getDefaultFamilyId();

			SqlDb db = SqlDb.get(this);
			beanForDb___LoginMember b = db.getLoginMemberBy_uid_fid(uid,
					family_id);
			db.closeDb();

			if (b.isAdmin == 1) {

				if (mm_get_group_listStatus != null
						&& mm_get_group_listStatus.info.total >= 6) {
					showLongToast("Вы уже добавили 6 членов семьи, больше добавить нельзя");
				} else {

					DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent intent = new Intent(Intent.ACTION_PICK,
									ContactsContract.Contacts.CONTENT_URI);
							startActivityForResult(intent, 100);
							// getActivity().RESULT_FIRST_USER
						}
					};
					DialogInterface.OnClickListener negativeListener = new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

							Intent intent = new Intent(
									FamilyMemberActivity.this,
									CountryActivity.class);
							intent.putExtra("action", "invite");
							startActivityForResult(intent, 300);
						}
					};
					String title = null;
					String message = "\nТот, кого вы добавляете, тоже из вашей страны?";
					String positiveText = "да";
					String negativeText = "нет";

					showConfirmDialog(title, message, positiveText,
							negativeText, positiveListener, negativeListener);
				}*/
			} else {
				showLongToast("Приглашать новых пользователей может только администратор.");
			}

			break;
		default:
			break;
		}
	}

	@Override
	public void initView() {
		linearLayout_l.setOnClickListener(this);
		button_invite.setOnClickListener(this);
		// ---------
	}

	@Override
	public void initData() {
		// ----------
		requrePhonePermission();
		// ---------------
	}

	private String selectMobile = "";
	private String invite_user_country_code = "";

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if(data == null){
			return;
		}
		
		if (requestCode == 100) {
			if (resultCode == RESULT_OK) {
				Uri contactData = data.getData();
				Cursor cursor = managedQuery(contactData, null, null, null,
						null);
				cursor.moveToFirst();
				selectMobile = getContactPhone(cursor);
				selectMobile = selectMobile.replace(" ", "");
				System.out.println("所选手机号为：" + selectMobile);

				FamilyMemberMsg member = null;
				boolean mobilenoIsBind = false;
				for (FamilyMemberMsg f : mm_familyMemberMsg) {
					if (f.mobile.equalsIgnoreCase(selectMobile)) {
						mobilenoIsBind = true;
						member = f;
						break;
					}
				}
				// Kid‘s xxx(亲属关系) has followed the kid.
				if (mobilenoIsBind) {
					String msg = String.format(
							"%s's %s(%s)  сопровождаем ребенка.",
							AppContext.getInstance().currentFamily.nickname,
							member.relation, selectMobile);
					showLongToast(msg);
				} else {
					// 如果手机号码未注册,只发短信
					// check_identifier(selectMobile);

					String sms_body = "Установите приложение Kid\'s Watcher из Google Play или AppStore и следите за ребенком. [Kid\'s Watcher]";

					Uri smsToUri = Uri.parse("smsto:" + selectMobile);
					Intent intent = new Intent(Intent.ACTION_SENDTO, smsToUri);
					intent.putExtra("sms_body", sms_body);
					// startActivity(intent);
					startActivityForResult(intent, 200);
					String from_country_code = KidsWatConfig
							.getUseCountryCode();
//					invite_join_family(from_country_code, selectMobile);
					getContentResolver().registerContentObserver(Uri.parse("content://sms"),
                            true, new SmsObserver(this, new Handler()));
					getContentResolver().registerContentObserver(Uri.parse("content://mms"),
							true, new MmsObserver(this, new Handler()));
				}
			}

		}

		else if (requestCode == 200) {
			
			if(isSend){
				isSend = false;
				String from_country_code = KidsWatConfig.getUseCountryCode();
				invite_join_family(from_country_code, selectMobile);
			}
			
		} else if (requestCode == 300) {
			if (resultCode == RESULT_OK) {
				if (data != null) {
					String action = data.getAction();
					// showLongToast(action);
					TLog.log(action);
					// textView_countrycode.setText(data.getAction());
					invite_user_country_code = action.substring(action
							.indexOf("+"));
					if (invite_user_country_code.endsWith(")")) {
						invite_user_country_code = invite_user_country_code
								.substring(1,
										invite_user_country_code.length() - 1);
					}
					TLog.log("country=" + invite_user_country_code);

					DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent intent = new Intent(Intent.ACTION_PICK,
									ContactsContract.Contacts.CONTENT_URI);

							startActivityForResult(intent, 400);// getActivity().RESULT_FIRST_USER,100
						}
					};
					DialogInterface.OnClickListener negativeListener = new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							//
						}
					};
					String title = "Страна Подтверждение";
					String message = String
							.format("\nПодтвердите, что новый участник вы из\n%s",
									action);
					String positiveText = getApplicationContext().getResources().getString(R.string.ok);
					String negativeText = getApplicationContext().getResources().getString(R.string.cancle);

					showConfirmDialog(title, message, positiveText,
							negativeText, positiveListener, negativeListener);
				}
			}
		} else if (requestCode == 400) {
			if (resultCode == RESULT_OK) {
				Uri contactData = data.getData();
				Cursor cursor = managedQuery(contactData, null, null, null,
						null);
				cursor.moveToFirst();
				selectMobile = getContactPhone(cursor);
				selectMobile = selectMobile.replace(" ", "");
				System.out.println("所选手机号为：" + selectMobile);

				String sms_body = "Установите приложение Kid’s Watcher из Google Play или AppStore и следите за ребенком. [Kid’s Watcher]";

				Uri smsToUri = Uri.parse("smsto:" + selectMobile);
				Intent intent = new Intent(Intent.ACTION_SENDTO, smsToUri);
				intent.putExtra("sms_body", sms_body);
				// startActivity(intent);
				startActivityForResult(intent, 500);
				getContentResolver().registerContentObserver(Uri.parse("content://sms"),
						true, new SmsObserver(this, new Handler()));
				getContentResolver().registerContentObserver(Uri.parse("content://mms"),
						true, new MmsObserver(this, new Handler()));
//				invite_join_family(invite_user_country_code, selectMobile);
			}

		} else if (requestCode == 500) {
			if(isSend){
				isSend = false;
				invite_join_family(invite_user_country_code, selectMobile);
			}
		} else if(requestCode == 600){
			FamilyMemberMsg member = null;
			boolean mobilenoIsBind = false;
			
			String phone = data.getStringExtra("phone");
			String country_code = data.getStringExtra("country_code").replace("+", "");
			String relationship = data.getStringExtra("relation");
			
			for (FamilyMemberMsg f : mm_familyMemberMsg) {
				if (f.mobile.equalsIgnoreCase(phone)) {
					mobilenoIsBind = true;
					member = f;
					break;
				}
			}
			
			if(AppContext.getInstance().currentFamily == null){
				return;
			}
			
			// Kid‘s xxx(亲属关系) has followed the kid.
			if (mobilenoIsBind) {
				String msg = String.format(
						"%s Этот  ребенок  уже  внимание", member.relation);
				showConfirmInformations(getApplicationContext().getResources().getString(R.string.add_contact), msg);
			} else{
				invite_join_family(country_code, phone, relationship);
			}
		}
	}

	private String getContactPhone(Cursor cursor) {
		// TODO Auto-generated method stub
		int phoneColumn = cursor
				.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
		int phoneNum = cursor.getInt(phoneColumn);
		String result = "";
		if (phoneNum > 0) {
			// 获得联系人的ID号
			int idColumn = cursor.getColumnIndex(ContactsContract.Contacts._ID);
			String contactId = cursor.getString(idColumn);
			// 获得联系人电话的cursor
			Cursor phone = getContentResolver().query(
					ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
					null,
					ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "="
							+ contactId, null, null);
			if (phone.moveToFirst()) {
				for (; !phone.isAfterLast(); phone.moveToNext()) {
					int index = phone
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
					int typeindex = phone
							.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE);
					int phone_type = phone.getInt(typeindex);
					String phoneNumber = phone.getString(index);
					result = phoneNumber;
				}
				if (!phone.isClosed()) {
					phone.close();
				}
			}
		}
		return result;
	}
//	 private SMSReceiver sendReceiver;
//	 private static String ACTION_SMS_SEND = "action_sms_send";
	private void bindData() {
		FamilyMemberSwipeAdapter adapter = new FamilyMemberSwipeAdapter(this,
				listview.getRightViewWidth(),
				new FamilyMemberSwipeAdapter.IOnItemRightClickListener() {
					@Override
					public void onRightClick(View v, int position) {
						// TODO Auto-generated method stub
						// showLongToast("right onclick " + position);
						// TODO Auto-generated method stub
						final String uid = KidsWatConfig.getUserUid();
						final String access_token = KidsWatConfig
								.getUserToken();
						String family_id = KidsWatConfig.getDefaultFamilyId();

						SqlDb db = SqlDb.get(FamilyMemberActivity.this);
						beanForDb___LoginMember b = db
								.getLoginMemberBy_uid_fid(uid, family_id);
						db.closeDb();
						if (b.isAdmin == 1) {
							String phone = mm_familyMemberMsg.get(position).mobile;
							String country_code = mm_familyMemberMsg
									.get(position).country_code;
							TLog.log("phone=" + phone);
							delete_family_member(country_code, phone);
						} else {
							showLongToast("разрешено только администратору.");
						}
					}
				});
		adapter.setList(mm_familyMemberMsg);
		// listview.addHeaderView(LayoutInflater.from(this).inflate(
		// R.layout.view_family_member_list_footer, null));
		// listview.addFooterView(LayoutInflater.from(this).inflate(
		// R.layout.view_family_member_list_footer, null));

		listview.setAdapter(adapter);
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// showLongToast("item onclick " + position);
				FamilyMemberMsg itemGroup = mm_familyMemberMsg.get(position);
				if (!itemGroup.uid.equalsIgnoreCase(KidsWatConfig.getUserUid())) {
					curPhone = itemGroup.mobile;
					showItemMenu(linearLayout_cc, itemGroup);
				}
			}
		});
	}

	String curPhone = "";
	private BottomPopupWindow mSelectPhotoPopupWindow = null;

	protected void showItemMenu(View relyView, FamilyMemberMsg itemGroup) {
		if (mSelectPhotoPopupWindow == null) {
			mSelectPhotoPopupWindow = new BottomPopupWindow(this, 0, relyView);

			// 打电话
			String item0 = String.format("Звонок \"%s\"", itemGroup.relation
					.length() > 0 ? itemGroup.relation : itemGroup.mobile);
						
			mSelectPhotoPopupWindow.addItem(item0, new onClickItemListener() {

				@Override
				public void clickItem(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(Intent.ACTION_DIAL, Uri
							.parse("tel:" + curPhone));// ACTION_CALL
					startActivity(intent);
				}
			}, ItemPosition.OTHER);
			// 添加“取消”
			mSelectPhotoPopupWindow.addCancelItem("Отменить", null, 10);
		}
		mSelectPhotoPopupWindow.show();
	}

	private void add_get_group_listStatus_tolist() {
		mm_familyMemberMsg.clear();
		for (beanFor___get_group_list.CData f : mm_get_group_listStatus.info.ddata) {
			FamilyMemberMsg ii_familyMemberMsg = new FamilyMemberMsg();
			ii_familyMemberMsg.msg_type_xx = 1;
			ii_familyMemberMsg.uid = f.uid;
			ii_familyMemberMsg.admin = f.admin;
			ii_familyMemberMsg.relation = f.relation;
			ii_familyMemberMsg.country_code = f.country_code;
			ii_familyMemberMsg.mobile = f.mobile;
			ii_familyMemberMsg.status = f.status;
			ii_familyMemberMsg.image_path = f.image_path;
			ii_familyMemberMsg.family_id = f.family_id;
			// ii_familyMemberMsg.msg_type = f.msg_type;

			mm_familyMemberMsg.add(ii_familyMemberMsg);
		}

		Collections.sort(mm_familyMemberMsg, Collections.reverseOrder());

		// 如果家庭圈大于5个人就隐藏button
		if (mm_familyMemberMsg.size() > 5)
			linearLayout_cc.setVisibility(8);
		else
			linearLayout_cc.setVisibility(0);
	}

	private void delete_family_member(String country_code, String mobile) {

		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		String family_id = KidsWatConfig.getDefaultFamilyId();
		final String version = AppContext.getInstance().currentFamily.version;
		String device_id = AppContext.getInstance().currentFamily.device_id;
		// String country_code = KidsWatConfig.getUseCountryCode();
		final String request_url = KidsWatApiUrl
				.getUrlFor___delete_family_member(uid, access_token, family_id,
						country_code, mobile, version, device_id);
		// -------------------------
		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				hideWaitDialog();
				TLog.log(String.format("url:%s\nt:%s", request_url, t));

				try {
					JSONObject response = new JSONObject(t);

					int error = response.getInt("error");
					// error==0此手机未注册
					if (error == 0) {
						int bbcount = AppContext.getInstance()
								.getFamilyListCount();
						if (bbcount > 0) {
							mm_familyMemberMsg.clear();
							get_group_list();
						}
					} else {
						showLongToast(response.getString("info"));
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String.format("%s", strMsg);
				TLog.log(String.format("url:%s\nt:%s", request_url, msg));
				showLongToast(msg);
				hideWaitDialog();
			}

			@Override
			public void onFinish() {
				
			}
		});

	}

	private void invite_join_family(String to_country_code, String to_mobile, String relation) {
		// -----------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		String family_id = KidsWatConfig.getDefaultFamilyId();
		final String version = AppContext.getInstance().currentFamily.version;
		final String device_id = AppContext.getInstance().currentFamily.device_id;
		String from_country_code = KidsWatConfig.getUseCountryCode();
		String from_mobile = KidsWatConfig.getUserPhone();
		final String request_url = KidsWatApiUrl
				.getUrlFor___invite_join_family(uid, access_token, family_id,
						from_country_code, from_mobile, to_country_code,
						to_mobile, version, device_id, relation);

		// -------------------------
		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));

				beanFor___invite_join_family response = AppContext
						.getInstance().getGson()
						.fromJson(t, beanFor___invite_join_family.class);

				if (response != null)
					if (response.error == 0) {
						//
						int bbcount = AppContext.getInstance()
								.getFamilyListCount();
						if (bbcount > 0) {
							mm_familyMemberMsg.clear();
							get_group_list();
						}
						Toast.makeText(FamilyMemberActivity.this,
								"Приглашение отправлено!", 1).show();
					} else {
						showLongToast(response.info.message);
					}
			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
				TLog.log(String.format("url:%s\nt:%s", request_url, msg));
				showLongToast(msg);
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});
	}
	
	private void invite_join_family(String to_country_code, String to_mobile) {
		// -----------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		String family_id = KidsWatConfig.getDefaultFamilyId();
		final String version = AppContext.getInstance().currentFamily.version;
		final String device_id = AppContext.getInstance().currentFamily.device_id;
		String from_country_code = KidsWatConfig.getUseCountryCode();
		String from_mobile = KidsWatConfig.getUserPhone();
		final String request_url = KidsWatApiUrl
				.getUrlFor___invite_join_family(uid, access_token, family_id,
						from_country_code, from_mobile, to_country_code,
						to_mobile, version, device_id);
		
		// -------------------------
		ApiHttpClient.get(request_url, new HttpCallBack() {
			
			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}
			
			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));
				
				beanFor___invite_join_family response = AppContext
						.getInstance().getGson()
						.fromJson(t, beanFor___invite_join_family.class);
				
				if (response != null)
					if (response.error == 0) {
						//
						int bbcount = AppContext.getInstance()
								.getFamilyListCount();
						if (bbcount > 0) {
							mm_familyMemberMsg.clear();
							get_group_list();
						}
						Toast.makeText(FamilyMemberActivity.this,
								"Приглашение отправлено!", 1).show();
					} else {
						showLongToast(response.info.message);
					}
				
			}
			
			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
				TLog.log(String.format("url:%s\nt:%s", request_url, msg));
				showLongToast(msg);
			}
			
			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});
		
	}

	private void get_group_list() {

		// -----------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		String family_id = KidsWatConfig.getDefaultFamilyId();
		// String device_id = AppContext.getInstance().getProperty(
		// "user.device_id");

		final String request_url = KidsWatApiUrl.getUrlFor___get_group_list(
				uid, access_token, family_id);
		// -------------------------

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));

				beanFor___get_group_list response = AppContext.getInstance()
						.getGson().fromJson(t, beanFor___get_group_list.class);

				mm_get_group_listStatus = response;
				if (response.error == 0) {
					add_get_group_listStatus_tolist();
					bindData();
				} else {
					showLongToast(response.info.message);
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				if (errorNo == -1) {
					showLongToast("Проверьте доступ в сеть и попробуйте снова");
				} else {
					String msg = String.format("%s(error=%s)",
							errorNo == -1 ? "Connect to the server failed"
									: strMsg, errorNo);
					showLongToast(msg);
				}
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
		}
		return false;
	}

	/**
	 * --------------------------
	 */
	public class SmsObserver extends ContentObserver {        
        private Context context;
        private final String[] SMS_PROJECTION = new String[] {
               "address","person","date","type","body",
            };

        public SmsObserver(Context context,Handler handler) {
                super(handler);                
                this.context = context;
                TLog.log("Leo-SmsObserver", "My Oberver on create");
        }
        
        public void onChange (boolean selfChange) {
        	
              TLog.log("SmsObserver", "sms onChange###### ");      
              //查询发件箱里的内容       
              Uri outSMSUri = Uri.parse("content://sms/outbox") ;
              Cursor c = FamilyMemberActivity.this.getContentResolver().query(outSMSUri, null, null, null,"date desc"); 
              if(c != null){  
                  
                  TLog.log(TAG, "the number of send is"+c.getCount()) ;  
                    
                  StringBuilder sb = new StringBuilder() ;  
                  //循环遍历  
                  while(c.moveToNext()){  
                      sb.append("发件人手机号码: "+c.getInt(c.getColumnIndex("address")))  
                        .append("信息内容: "+c.getString(c.getColumnIndex("body")))  
                        .append("\n");  
                  }  
                  
                  if(c.getCount() == 0){
//                	  isSend = false;
                  }else{
                	  isSend = true;
                  }
                  c.close();            
//                mHandler.obtainMessage(MSG_OUTBOXCONTENT, sb.toString()).sendToTarget(); 
              }  
        }
	}
	
	/**
	 * --------------------------
	 */
	public class MmsObserver extends ContentObserver {        
		private Context context;
		private final String[] MMS_PROJECTION = new String[] {
				"address","person","date","type","body",
		};
		
		public MmsObserver(Context context,Handler handler) {
			super(handler);                
			this.context = context;
			TLog.log("Leo-SmsObserver", "My Oberver on create");
		}
		
		public void onChange (boolean selfChange) {
			
			//查询发件箱里的内容       
			Uri outSMSUri = Uri.parse("content://mms/outbox") ;
			Cursor c = FamilyMemberActivity.this.getContentResolver().query(outSMSUri, null, null, null,"date desc"); 
			if(c != null){  
				
				TLog.log(TAG, "the number of send is"+"---mms---"+c.getCount()) ;  
				
				StringBuilder sb = new StringBuilder() ;  
				//循环遍历  
				while(c.moveToNext()){  
					sb.append("发件人手机号码: "+c.getInt(c.getColumnIndex("address")))  
					.append("信息内容: "+c.getString(c.getColumnIndex("body")))  
					.append("\n");  
				}  
				
				if(c.getCount() == 0){
//                	  isSend = false;
				}else{
					isSend = true;
				}
				c.close();            
			}  
		}
	}
	
	boolean isSend;
}
