package com.qiwo.xkidswatcher_russia.ui;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.widget.BabyWeightRule;
import com.qiwo.xkidswatcher_russia.widget.HeightRule;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.InjectView;

//宝贝信息设置界面
public class BabyInfoActivity extends BaseActivity {

	public static final String KEY_GENDER = "sex";
	public static final String KEY_HEIGHT = "height";
	public static final String KEY_WEIGHT = "weight";

	@InjectView(R.id.imageview_l)
	ImageView imageview_back;
	
	@InjectView(R.id.linearLayout_l)
	LinearLayout linearLayout_l;

	@InjectView(R.id.gender_btn)
	ImageView mGenderBtn;

	@InjectView(R.id.baby_icon)
	ImageView mGenderIcon;

	@InjectView(R.id.weight_rule)
	BabyWeightRule mWRule;

	@InjectView(R.id.height_rule)
	HeightRule mHRule;

	@InjectView(R.id.height_info)
	TextView mHInfo;

	@InjectView(R.id.button_ok)
	Button button_ok;

	// private Button mGenderBtn = null; // 性别按钮
	// private ImageView mGenderIcon = null; // 中间的示例图片
	private int mIsBoy = 1;
	// private BabyWeightRule mWRule = null;
	// private HeightRule mHRule = null;
	// private TextView mHInfo = null;
	// private ImageView rightBtn;
	private Handler mHandler = new Handler();

	@Override
	protected int getLayoutId() {
		return R.layout.baby_info_activity;
	}

	@Override
	protected boolean hasActionBar() {
		return false;
	}

	@Override
	protected void init(Bundle savedInstanceState) {
		super.init(savedInstanceState);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.linearLayout_l:
			finish();
			break;
		case R.id.button_ok:
			float weight = mWRule.getWeightValue();
			float height = mHRule.getHeightValue();

			Intent intent = new Intent();
			intent.putExtra(KEY_GENDER, mIsBoy);
			intent.putExtra(KEY_HEIGHT, height);
			intent.putExtra(KEY_WEIGHT, weight);
			setResult(RESULT_OK, intent);

			finish(); // 结束当前界面
			break;
		case R.id.gender_btn:
			mIsBoy = 1 - mIsBoy;
			updateGengerInfo();
			break;
		default:
			break;
		}
	}

	@Override
	public void initView() {
		linearLayout_l.setOnClickListener(this);
		mGenderBtn.setOnClickListener(this);
		button_ok.setOnClickListener(this);
	}

	@Override
	public void initData() {
		int genderValue = getIntent().getIntExtra(KEY_GENDER, 1);
		float heightValue = getIntent().getFloatExtra(KEY_HEIGHT, 110);
		float weightValue = getIntent().getFloatExtra(KEY_WEIGHT, 20);
		mIsBoy = genderValue;

		mWRule.setWeightValue(weightValue);

		mHRule.setUpdateView(mHInfo);
		mHRule.setHeightValue(heightValue);
		updateGengerInfo();
	}

	private void updateGengerInfo() {
		if (mIsBoy == 1) {
			mGenderBtn.setBackgroundResource(R.drawable.s_sex_boy);
			mGenderIcon.setBackgroundResource(R.drawable.s_boy);
		} else {
			mGenderBtn.setBackgroundResource(R.drawable.s_sex_girl);
			mGenderIcon.setBackgroundResource(R.drawable.s_girl);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
		}
		return false;
	}
}
