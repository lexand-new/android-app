package com.qiwo.xkidswatcher_russia.ui;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;
import org.kymjs.kjframe.utils.PreferenceHelper;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.AppManager;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.bean.beanFor___login;
import com.qiwo.xkidswatcher_russia.util.Contanst;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import com.qiwo.xkidswatcher_russia.util.TLog;
import com.qiwo.xkidswatcher_russia.widget.EditTextWithDel;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.InjectView;

public class RegisterNewAccountActivity extends BaseActivity {

	final private String TAG = "RegisterNewAccountActivity";

	@InjectView(R.id.linearLayout_l)
	LinearLayout linearLayout_l;

	@InjectView(R.id.button_next)
	Button button_next;

	@InjectView(R.id.send_sms_again)
	Button send_sms_again;

	@InjectView(R.id.smscode)
	EditText smscode_text;

	@InjectView(R.id.register_new_account_tip)
	TextView register_new_account_tip;

	@InjectView(R.id.password_tv)
	EditTextWithDel password_text;

	@InjectView(R.id.password_tv2)
	EditTextWithDel password_text2;

	@InjectView(R.id.imageview_l)
	ImageView imageview_l;

	private String country_code;
	private String mobile;
	private String password;

	// -------------
	private TimeCount time;

	private static final int COUNT_INTERVAL = 1000;
	private static final int COUNT_TOTAL = 120 * 1000;

	// ----------

	@Override
	protected int getLayoutId() {
		return R.layout.activity_register_new_account;
	}

	@Override
	protected boolean hasActionBar() {
		return false;
	}

	@Override
	protected void init(Bundle savedInstanceState) {
		super.init(savedInstanceState);
	}

	@Override
	public void initView() {
		country_code = getIntent().getStringExtra("country_code");
		mobile = getIntent().getStringExtra("mobile");
		if (mobile.length() == 0) {
			Toast.makeText(RegisterNewAccountActivity.this,
					getApplicationContext().getResources().getString(R.string.tip_input_number), Toast.LENGTH_SHORT).show();
			finish();
			return;
		}
		register_new_account_tip.setText(register_new_account_tip.getText()
				.toString() + mobile);

		time = new TimeCount(COUNT_TOTAL, COUNT_INTERVAL);
		time.start();

		linearLayout_l.setOnClickListener(this);
		button_next.setOnClickListener(this);

		send_sms_again.setOnClickListener(this);
	}

	@Override
	public void initData() {
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.linearLayout_l:
			finish();
			break;
		case R.id.send_sms_again:
			SendSMS_CoundDown();
			break;
		case R.id.button_next:
			String smscode = smscode_text.getText().toString().trim();
			password = password_text.getText().toString();
			String password2 = password;// password_text2.getText().toString();

			Log.d(TAG, "smdcode=" + smscode);
			Log.d(TAG, "password=" + password);
			if (password.length() > 5 && password.length() < 21) {
				if (password.equals(password2)) {
					KidsWatConfig.setUserCountryCode(country_code);
					register_new_account(mobile, password, smscode);
				} else {
					Toast.makeText(RegisterNewAccountActivity.this,
							getApplicationContext().getResources().getString(R.string.tip_pws_notmatch), Toast.LENGTH_SHORT).show();
				}
			} else {
				showConfirmInformation(null,
						getApplicationContext().getResources().getString(R.string.tip_confirm_pwd_length));
			}

			break;
		default:
			break;
		}
	}

	// -----------------
	/**
	 * countdown
	 */

	public class TimeCount extends CountDownTimer {
		// Parameters of total length, and time interval
		public TimeCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// send_sms_again.setTextColor(getResources().getColor(
			// R.color.login_bg));
			send_sms_again.setEnabled(false);
			send_sms_again.setBackgroundResource(R.drawable.editview_press_bg);
		}

		@Override
		public void onTick(long millisUntilFinished) {
			try {
				send_sms_again.setText((millisUntilFinished / 1000) + " s");
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		@Override
		public void onFinish() {
			try {
				send_sms_again.setText(getResources().getString(
						R.string.sent_again));
				// send_sms_again.setTextColor(Color.WHITE);
				send_sms_again.setEnabled(true);
				send_sms_again.setBackgroundResource(R.drawable.editview_bg);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// do something
		try {
			time.cancel();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/*
	 * countdown
	 */
	public void SendSMS_CoundDown() {
		time = new TimeCount(COUNT_TOTAL, COUNT_INTERVAL);// 构造CountDownTimer对象
		time.start();
		requre_sms_code(country_code, mobile);
	}

	// ----------------

	private void validate_code(final String phone, final String password,
			final String code) {
		final String request_url = KidsWatApiUrl.getUrlFor___validate_code(
				phone, code);
		// -------------------------

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));
				try {
					JSONObject response = new JSONObject(t);

					int error = response.getInt("error");
					if (error == 0) {
						register_new_account(phone, password, code);
					} else {
						showConfirmInformation(null,
								getApplicationContext().getResources().getString(R.string.tip_wrong_varifivcation));
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String xmsg = String.format("errorNo:%s\n%s", errorNo, strMsg);
				TLog.log(String.format("url:%s\nt:%s", request_url, xmsg));

				String msg = String
						.format("%s(error=%s)",
								errorNo == -1 ? "Подключение к серверу не удалось"
										: strMsg, errorNo);
				showLongToast(msg);
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});

	}

	private void requre_sms_code(String country_code, String phone) {
		// -----------------
		final String request_url = KidsWatApiUrl.getUrlFor___requre_sms_code(
				country_code, phone);

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));
				try {
					JSONObject response = new JSONObject(t);

					int code = response.getInt("error");
					if (code == 0) {

					} else {
						// {"error":400,"info":{"message":"Parameter Error!"}}
						showConfirmInformation(null,
								getApplicationContext().getResources().getString(R.string.tip_wrong_varifivcation));
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String xmsg = String.format("errorNo:%s\n%s", errorNo, strMsg);
				TLog.log(String.format("url:%s\nt:%s", request_url, xmsg));

				String msg = String
						.format("%s(error=%s)",
								errorNo == -1 ? "Подключение к серверу не удалось"
										: strMsg, errorNo);
				showLongToast(msg);
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});

	}

	// ----------------
	private void register_new_account(final String mobileno,
			final String password, final String smscode) {
		final String request_url = KidsWatApiUrl.getUrlFor___register(
				country_code, mobileno, password, smscode);
		// -------------------------

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));
				try {
					JSONObject response = new JSONObject(t);

					int code = response.getInt("error");
					if (code == 0) {
						KidsWatConfig.setUserCountryCode(country_code);
						KidsWatConfig.setUserPhone(mobileno);
						KidsWatConfig.setUserPassword(password);

						// AppContext.getInstance().setProperty("user.name",
						// mobileno);

						/**直接登录*/
						userLogin(country_code,mobileno,password);
					} else {
						// {"error":400,"info":{"message":"Parameter Error!"}}
						String message = response.getJSONObject("info")
								.getString("message");
						showConfirmInformation(null, "Регистрация не удалась. Попробуйте еще раз.");

					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String xmsg = String.format("errorNo:%s\n%s", errorNo, strMsg);
				TLog.log(String.format("url:%s\nt:%s", request_url, xmsg));

				String msg = String
						.format("%s(error=%s)",
								errorNo == -1 ? "Подключение к серверу не удалось"
										: strMsg, errorNo);
				showLongToast(msg);
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});

	}

	// ----------------

	private void userLogin(final String country_code, final String umobileno,
						   final String mmpassword) {
		// -----------------
		String pushToken = KidsWatUtils.getGooglePushToken();
		if (pushToken == null) {
			// showShortToast("Connect to Google Play failure, some functions(SOS,Google map) will not be available.");
			pushToken = "jiadetoken";
		}
		System.out.println("push token = "+pushToken);

		final String request_url = KidsWatApiUrl.getUrlFor___login(
				country_code, umobileno, mmpassword, pushToken);
		// -------------------------

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));

				beanFor___login response = AppContext.getInstance().getGson()
						.fromJson(t, beanFor___login.class);

				if (response.error == 0) {
					// ApplicationHelper.getInstance().mm_beanFor_login
					// = response;
					KidsWatConfig.saveUserInfo(response.info.user, mmpassword);

					//---------record username
					PreferenceHelper.write(RegisterNewAccountActivity.this, Contanst.PREF_NAME, "umobileno", umobileno);
					PreferenceHelper.write(RegisterNewAccountActivity.this,Contanst.PREF_NAME, "country_code", country_code);

					System.out.println("user imge_path : "+response.info.user.img_path);
					KidsWatConfig.setUserImagepath(response.info.user.img_path);

					AppManager.AppRestart(RegisterNewAccountActivity.this);
					RegisterNewAccountActivity.this.finish();
					AppManager.getAppManager().finishAllActivity();
				} else {
					showConfirmInformation(getApplicationContext().getResources().getString(R.string.incorrect_password),
							"Вы ввели  неверный пароль  , пожалуйста, введите снова");
				}
			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String xmsg = String.format("errorNo:%s\n%s", errorNo, strMsg);
				TLog.log(String.format("url:%s\nt:%s", request_url, xmsg));

				if (errorNo == -1) {
					dismissDialog(null, 50);
					showConfirmInformation("Time out",
							"Please check your connection and try again.");
				} else {
					String msg = String.format("%s(error=%s)",
							errorNo == -1 ? "Connect to the server failed"
									: strMsg, errorNo);
					showLongToast(msg);
					dismissDialog(msg, 800);
				}
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});
	}

}
