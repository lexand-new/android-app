package com.qiwo.xkidswatcher_russia.ui;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabContentFactory;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;

import com.qiwo.xkidswatcher_russia.AppConfig;
import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.AppManager;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.adapter.PhotoAdapter;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.bean.Baby;
import com.qiwo.xkidswatcher_russia.bean.FamilyMember;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___LoginMember;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_family_info;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_family_list;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_watch_data_latest;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_watch_info;
import com.qiwo.xkidswatcher_russia.bean.beanFor___login;
import com.qiwo.xkidswatcher_russia.db.SqlDb;
import com.qiwo.xkidswatcher_russia.event.BaseEvent;
import com.qiwo.xkidswatcher_russia.event.BleDisconnect;
import com.qiwo.xkidswatcher_russia.event.BleWritePwdSuccess;
import com.qiwo.xkidswatcher_russia.interf.BaseViewInterface;
import com.qiwo.xkidswatcher_russia.interf.OnTabReselectListener;
import com.qiwo.xkidswatcher_russia.util.DoubleClickExitHelper;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import com.qiwo.xkidswatcher_russia.util.StringUtils;
import com.qiwo.xkidswatcher_russia.util.TLog;
import com.qiwo.xkidswatcher_russia.util.UpdateManager;
import com.qiwo.xkidswatcher_russia.widget.APopupWindow.ItemPosition;
import com.qiwo.xkidswatcher_russia.widget.APopupWindow.onClickItemListener;
import com.qiwo.xkidswatcher_russia.widget.BottomPopupWindow;
import com.qiwo.xkidswatcher_russia.widget.CircleImageView;
import com.qiwo.xkidswatcher_russia.widget.MyFragmentTabHost;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

//import com.qiwo.xkidswatcher_russia.fragment.beanFor___get_watch_data_latest;

public class MainActivity extends ActionBarActivity implements
		// NavigationDrawerFragment.NavigationDrawerCallbacks,
		OnTabChangeListener, BaseViewInterface, View.OnClickListener, OnTouchListener {

	private DoubleClickExitHelper mDoubleClickExit;
	private Handler mHandler;
	
	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */

	@InjectView(android.R.id.tabhost)
	public MyFragmentTabHost mTabHost;

	@InjectView(R.id.linearLayout_l)
	LinearLayout linearLayout_l;

	@InjectView(R.id.imageview_l)
	CircleImageView imageview_l;

	@InjectView(R.id.imageview_pop)
	ImageView imageview_pop;

	@InjectView(R.id.linearLayout_r)
	LinearLayout linearLayout_r;

	@InjectView(R.id.imageview_r)
	ImageView imageview_r;

	@InjectView(R.id.textView_r)
	TextView textView_r;

	@InjectView(R.id.textView_title)
	TextView textView_title;

	@InjectView(R.id.textView_edit)
	TextView textView_edit;

	@InjectView(R.id.drawer_layout)
	DrawerLayout drawer_layout;

	// private BadgeView mBvNotice;

	ListView listview_user;
	private PopupWindow popupwindow;

	String titleArray[] = { "Карта",
			// ""Недавние,
			"Уведомления", "Настройки" };

	List<Baby> listBaby = new ArrayList<Baby>();
	PhotoAdapter babyAdapter = null;

	int mStep = 0;
	private ProgressDialog progressDialog = null;

	protected void showWaitDialog(String __msg) {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.setMessage(__msg);
		} else {
			progressDialog = ProgressDialog.show(MainActivity.this, null, __msg);
		}
	}

	protected void dismissDialog() {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getWindow().setFormat(PixelFormat.TRANSLUCENT);
		super.onCreate(savedInstanceState);
		System.out.println("--------onCreate--------");
		setContentView(R.layout.activity_main);
		ButterKnife.inject(this);
		// ----------
		// mTabHost = (MyFragmentTabHost) findViewById(android.R.id.tabhost);
		// mAddBt = (View) findViewById(R.id.quick_option_iv);
		// -----------
		getActionBar().hide();
		initView();
		AppManager.getAppManager().addActivity(this);
		initData();
		handleIntent(getIntent());
		//
		// login();
		showWaitDialog(getApplicationContext().getResources().getString(R.string.loading));
		get_family_list();
		KidsWatUtils.get_notice_list_ex();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		handleIntent(intent);
	}

	/**
	 * 处理传进来的intent
	 * 
	 * @return void
	 * @param intent
	 */
	private void handleIntent(Intent intent) {

	}

	@Override
	public void initView() {
		if (!EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().register(this);
		}
		mHandler = new Handler();
		mDoubleClickExit = new DoubleClickExitHelper(this);

		mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);
		if (android.os.Build.VERSION.SDK_INT > 10) {
			mTabHost.getTabWidget().setShowDividers(0);
		}

		initTabs();

		mTabHost.setCurrentTab(0);
		mTabHost.setOnTabChangedListener(this);

		if (KidsWatConfig.isFristStart()) {
			KidsWatConfig.setFristStart(false);
		}
		// ------------
		linearLayout_l.setOnClickListener(this);
		linearLayout_r.setOnClickListener(this);

		// checkUpdate();
	}

	private void checkUpdate() {
		if (!AppContext.get(AppConfig.KEY_CHECK_UPDATE, true)) {
			return;
		}
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				new UpdateManager(MainActivity.this, false).checkUpdate();
			}
		}, 2000);
	}

	protected void showLongToast(String message) {
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		System.out.println("--------onDestroy--------");
		if (EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().unregister(this);
		}
		ApiHttpClient.cancelAll();
	}

	boolean isActive = true;

	@Override
	protected void onStop() {
		//
		super.onStop();

		boolean isAppOnForeground = KidsWatUtils.isTopActivity(MainActivity.this);
		if (!isAppOnForeground) {
			// app 进入后台
			// 全局变量isActive = false 记录当前已经进入后台
			isActive = false;
		}
	}

	private boolean isActivityFocus = true;

	@Override
	protected void onResume() {
		//
		super.onResume();
		if (!isActive) {
			// app 从后台唤醒，进入前台
			if (getCurrentTabIndex() == 0) {
				BaseEvent event = new BaseEvent(BaseEvent.MSGTYPE_3___BACKGROUND_TO_FRONT, "backGround_to_front");
				EventBus.getDefault().post(event);
			}
			isActive = true;
		}
		isActivityFocus = true;
	}

	@Override
	protected void onPause() {
		super.onPause();
		isActivityFocus = false;
	}

	@Override
	public void initData() {
		if (AppContext.getInstance().getFamilyListCount() == 0) {
			imageview_l.setVisibility(View.INVISIBLE);
			imageview_pop.setVisibility(View.INVISIBLE);
		}
		babyAdapter = new PhotoAdapter(this, listBaby);

		String filename = KidsWatConfig.getTempFilePath() + "alarm.wav";
		java.io.File f = new java.io.File(filename);
		if (f.exists()) {
			//
		} else {
			try {
				KidsWatUtils.copyBigDataToSD(this, "alarm.wav", filename);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	private void initTabs() {
		MainTab[] tabs = MainTab.values();
		final int size = tabs.length;
		for (int i = 0; i < size; i++) {
			MainTab mainTab = tabs[i];
			TabSpec tab = mTabHost.newTabSpec(getString(mainTab.getResName()));
			View indicator = LayoutInflater.from(getApplicationContext()).inflate(R.layout.tab_indicator, null);
			TextView title = (TextView) indicator.findViewById(R.id.tab_title);
			@SuppressWarnings("deprecation")
			Drawable drawable = this.getResources().getDrawable(mainTab.getResIcon());
			title.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
			title.setText(getString(mainTab.getResName()));
			// ----------
			View cn = indicator.findViewById(R.id.tab_new_msg);
			// if (i == 2) {
			// cn.setVisibility(View.VISIBLE);
			// } else {
			// cn.setVisibility(View.INVISIBLE);
			// }
			if (i == 1) {
				cn.setVisibility(View.VISIBLE);
			} else {
				cn.setVisibility(View.INVISIBLE);
			}
			// -----------
			tab.setIndicator(indicator);
			tab.setContent(new TabContentFactory() {

				@Override
				public View createTabContent(String tag) {
					return new View(MainActivity.this);
				}
			});
			// if (mainTab.equals(MainTab.CHAT)) {
			// View cn = indicator.findViewById(R.id.tab_mes);
			// mBvNotice = new BadgeView(MainActivity.this, cn);
			// mBvNotice.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
			// mBvNotice.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
			// mBvNotice.setBackgroundResource(R.drawable.notification_bg);
			// mBvNotice.setGravity(Gravity.CENTER);
			// mBvNotice.hide();
			// cn.setVisibility(View.INVISIBLE);
			// } else if (mainTab.equals(MainTab.NOTIFICATION)) {
			// View cn = indicator.findViewById(R.id.tab_mes);
			// cn.setVisibility(View.VISIBLE);
			// } else {
			// View cn = indicator.findViewById(R.id.tab_mes);
			// cn.setVisibility(View.GONE);
			// }

			mTabHost.addTab(tab, mainTab.getClz(), null);

			mTabHost.getTabWidget().getChildAt(i).setOnTouchListener(this);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private int getCurrentTabIndex() {
		final int size = mTabHost.getTabWidget().getTabCount();
		for (int i = 0; i < size; i++) {
			// View v = mTabHost.getTabWidget().getChildAt(i);
			if (i == mTabHost.getCurrentTab()) {
				return i;
			}
		}
		return 0;
	}

	@Override
	public void onTabChanged(String tabId) {
		final int size = mTabHost.getTabWidget().getTabCount();
		for (int i = 0; i < size; i++) {
			View v = mTabHost.getTabWidget().getChildAt(i);
			if (i == mTabHost.getCurrentTab()) {
				textView_title.setText(titleArray[i]);
				// if (i == 0 || i == 2 || i == 1) {
				if (i == 0 || i == 1) {
					linearLayout_r.setVisibility(View.VISIBLE);
					if (i == 0) {
						imageview_r.setVisibility(View.VISIBLE);
						textView_r.setVisibility(View.GONE);
						textView_edit.setVisibility(View.GONE);
					} else if (i == 1) {
						// } else if (i == 2) {
						SqlDb db = SqlDb.get(this);
						int rows = db.getMessageCount(KidsWatConfig.getUserUid());
						db.closeDb();
						if (rows > 0) {
							imageview_r.setVisibility(View.GONE);
							textView_r.setVisibility(View.VISIBLE);
							textView_edit.setVisibility(View.GONE);
						} else {
							imageview_r.setVisibility(View.GONE);
							textView_r.setVisibility(View.INVISIBLE);
							textView_edit.setVisibility(View.GONE);
						}
					} /*
						 * else if (i == 1) {
						 * imageview_r.setVisibility(View.GONE);
						 * textView_r.setVisibility(View.GONE); }
						 */
				} else {
					linearLayout_r.setVisibility(View.GONE);
				}
				v.setSelected(true);
			} else {
				v.setSelected(false);
			}
			// ----------------
		}
		// if (tabId.equals(getString(MainTab.CHAT.getResName()))) {
		// mBvNotice.setText(5 + "");
		// mBvNotice.show();
		// }
		supportInvalidateOptionsMenu();
	}

	private void showClearAndRedPoint(boolean isShow) {
		if (getCurrentTabIndex() == 1) {
			// if (getCurrentTabIndex() == 2) {
			if (isShow) {
				imageview_r.setVisibility(View.VISIBLE);
				textView_r.setVisibility(View.GONE);
			} else {
				imageview_r.setVisibility(View.GONE);
				textView_r.setVisibility(View.GONE);
			}
		}

		final int size = mTabHost.getTabWidget().getTabCount();
		for (int i = 0; i < size; i++) {
			if (i == 1) {
				// if (i == 2) {

				View v = mTabHost.getTabWidget().getChildAt(i);
				View cn = v.findViewById(R.id.tab_new_msg);
				if (isShow)
					cn.setVisibility(View.VISIBLE);
				else
					cn.setVisibility(View.INVISIBLE);

			}
		}
		supportInvalidateOptionsMenu();
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.linearLayout_l:
			if (popupwindow != null && popupwindow.isShowing()) {
				popupwindow.dismiss();
				return;
			} else {
				if (AppContext.getInstance().getFamilyListCount() > 1) {
					initmPopupWindowView();
					popupwindow.showAsDropDown(v, 0, 5);
				}
			}
			break;
		case R.id.linearLayout_r:
			int currentTabIndex = getCurrentTabIndex();
			if (currentTabIndex == 0) {
				Intent intent = new Intent(this, HistoryActivity_v2.class);
				startActivity(intent);
				// } else if (currentTabIndex == 2) {
			} else if (currentTabIndex == 1) {
				showClearPopupWindow(drawer_layout);
			}
			break;
		default:
			break;
		}
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		super.onTouchEvent(event);
		boolean consumed = false;
		// use getTabHost().getCurrentTabView to decide if the current tab is
		// touched again
		if (event.getAction() == MotionEvent.ACTION_DOWN && v.equals(mTabHost.getCurrentTabView())) {
			// use getTabHost().getCurrentView() to get a handle to the view
			// which is displayed in the tab - and to get this views context
			Fragment currentFragment = getCurrentFragment();
			if (currentFragment != null && currentFragment instanceof OnTabReselectListener) {
				OnTabReselectListener listener = (OnTabReselectListener) currentFragment;
				listener.onTabReselect();
				consumed = true;
			}
		}
		return consumed;
	}

	private Fragment getCurrentFragment() {
		return getSupportFragmentManager().findFragmentByTag(mTabHost.getCurrentTabTag());
	}

	// ------------------
	public void onEventMainThread(BleWritePwdSuccess event) {
		TLog.log("BleWritePwdSuccess" + event.getClass().getSimpleName());
		// textView_Msg.setText("In Accompanying Mode");
		//
		if (isActivityFocus) {
			// KidsWatUtils.isTopActivity(MainActivity.this)
			TLog.log("show write pwd ok msg.");
			SqlDb db = SqlDb.get(MainActivity.this);
			beanForDb___Family b = db.getFamilyBy_bluetoothAddress(event.getBluetoothAddress());
			db.closeDb();

			String message = String.format("%s Переключено в режим сопровождения.", event.getBlueNickname());

			Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
		}
	}

	public void onEventMainThread(BleDisconnect event) {
		TLog.log("onEventMainThread收到了消息：" + event.getClass().getSimpleName());

		SqlDb db = SqlDb.get(MainActivity.this);
		beanForDb___Family b = db.getFamilyBy_bluetoothAddress(event.getBleAddress());
		db.closeDb();

		String message = String.format("ВНИМАНИЕ! %s далеко от Вас. Проверьте, все ли в порядке.",
				b.nickname);
		TLog.log(message);

		Intent mintent = new Intent(MainActivity.this, EventAlertActivity.class);
		mintent.putExtra("message", message);
		mintent.putExtra("bledisconnect", 1);
		startActivity(mintent);
	}

	public void onEventMainThread(BaseEvent event) {
		int type = event.getType();
		String msg = event.getMsg();

		if (type == BaseEvent.MSGTYPE_1___RELOGIN || type == BaseEvent.MSGTYPE_1___ADD_KIDS_OK) {
			showWaitDialog(getApplicationContext().getResources().getString(R.string.loading));
			mHandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					AppContext.getInstance().currentFamily = null;
					login();
					// -----------------------
				}
			}, 450);

		} else if (type == BaseEvent.MSGTYPE_1___LOGOUT) {
			mHandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					// -----------------
					Intent login_intent = new Intent(MainActivity.this, UserLogin2Activity.class);
					startActivity(login_intent);
					MainActivity.this.finish();
					// -----------------------
				}
			}, 300);
		} else if (type == BaseEvent.MSGTYPE_2___FAMILY_LIST_EMPTY) {
			// imageview_l.setVisibility(View.INVISIBLE);
			// imageview_pop.setVisibility(View.INVISIBLE);

			Intent intent = new Intent(this, ScanGuideAActivity.class);
			intent.putExtra("action", "add_watcher");
			startActivity(intent);
			finish();
		} else if (type == BaseEvent.MSGTYPE_2___FAMILY_LIST_HAS_DATA) {
			imageview_l.setVisibility(View.VISIBLE);
			final String uid = KidsWatConfig.getUserUid();
			SqlDb db = SqlDb.get(AppContext.getInstance());
			int unReadMsgRows = db.getUnReadMessageCount(uid);
			db.closeDb();
			if (unReadMsgRows > 0) {
				showClearAndRedPoint(true);
			} else {
				showClearAndRedPoint(false);
			}
		} else if (type == BaseEvent.MSGTYPE_3___CLEAR_NOTICE_DONE || type == BaseEvent.MSGTYPE_3___NO_UNREAD_MSG) {

			showClearAndRedPoint(false);
		} else if (type == BaseEvent.MSGTYPE_3___HAS_UNREAD_MSG) {
			showClearAndRedPoint(true);
		}
		// else if (type == BaseEvent.MSGTYPE_3___NO_UNREAD_MSG) {
		// showClearAndRedPoint(false);
		// }
		// -------------------------
		else if (type == BaseEvent.MSGTYPE_1___DELETE_DEVICE) {
			AppContext.getInstance().currentFamily = null;
			login();
			DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// -----------------
					// -----------------------
				}
			};
			new AlertDialog.Builder(this).setMessage("The device has been discarded.").setTitle("Reminder")
					.setPositiveButton("OK", positiveListener).show();
		} else if (type == BaseEvent.MSGTYPE_1___ADMIN_ADD_MEMBER) {
			AppContext.getInstance().currentFamily = null;
			login();
			DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// -----------------
					// -----------------------
				}
			};
			// String message = String.format(
			// "You are invited to follow baby by Babe's father", "");
			new AlertDialog.Builder(this).setMessage(msg).setTitle("Reminder").setPositiveButton("OK", positiveListener)
					.show();
		} else if (type == BaseEvent.MSGTYPE_1___ADMIN_DELETE_MEMBER) {
			AppContext.getInstance().currentFamily = null;
			login();
			DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// -----------------
					// -----------------------
				}
			};
			// String message = String.format(
			// " You are revoked following baby by the admin. ", "");
			new AlertDialog.Builder(this).setMessage(msg).setTitle("Reminder").setPositiveButton("OK", positiveListener)
					.show();
		}
		// --------------------
		else if (type == BaseEvent.MSGTYPE_3___CHANGE_KIDS_PHOTO) {
			// imageview_l.re
			beanForDb___Family b = AppContext.getInstance().currentFamily;
			// setCircleImage(imageview_l, b.family_id, b.base64_img, b.sex);
			if (b != null) {
				// KidsWatUtils.setKidsImage(MainActivity.this, imageview_l,
				// b.family_id, b.base64_img, b.sex);
				imageview_l.setImageBitmap(KidsWatUtils.getBabyImg_v3(this, b.family_id, b.sex, b));
				// listBaby
				for (Baby bt : listBaby) {
					if (bt.getFamily_id().equalsIgnoreCase(b.family_id)) {
						bt.setBase64Img(b.base64_img);
						bt.setImg_path(b.img_path);
						bt.setSex(b.sex);
					}
				}
			}
			babyAdapter.notifyDataSetChanged();

		} else if (type == BaseEvent.MSGTYPE_3___CHANGE_KIDS_NICKNAME) {
			// imageview_l.re
			beanForDb___Family b = AppContext.getInstance().currentFamily;
			for (Baby bt : listBaby) {
				if (bt.getFamily_id().equalsIgnoreCase(b.family_id)) {
					bt.setNickname(b.nickname);
				}
			}

			if (TextUtils.isEmpty(AppContext.getInstance().currentFamily.img_path)) {
				imageview_l.setImageResource(
						AppContext.getInstance().currentFamily.sex == 0 ? R.drawable.icon_girl : R.drawable.icon_boy);
			}

			babyAdapter.notifyDataSetChanged();
		}

		else if (type == BaseEvent.MSGTYPE_5___USERLIST_INDEX_CHANGE) {
			String family_id = msg;
			beanForDb___Family bbf = AppContext.getInstance().map_familyinfo.get(family_id);
			// beanForDb___Family bbf = AppContext.getInstance().currentFamily;
			for (int i = 0; i < listBaby.size(); i++) {
				if (listBaby.get(i).getFamily_id().equalsIgnoreCase(family_id)) {
					bbf.sex = listBaby.get(i).getSex();
					bbf.base64_img = listBaby.get(i).getBase64Img();
					bbf.birthday = listBaby.get(i).getBirthday();
					bbf.birthdayText = listBaby.get(i).getBirthdayText();
					bbf.weight = listBaby.get(i).getWeight();
					bbf.nickname = listBaby.get(i).getNickname();
					bbf.height = listBaby.get(i).getHeight();
					// bbf.family_id = listBaby.get(i).getFamily_id();
					break;
				}
			}

			AppContext.getInstance().currentFamily = bbf;

			SqlDb db = SqlDb.get(this);
			beanForDb___LoginMember b_lk = db.getLoginMemberBy_uid_fid(KidsWatConfig.getUserUid(), family_id);
			db.closeDb();

			AppContext.getInstance().loginUser_kid = b_lk;

			KidsWatConfig.setDefaultFamilyId(family_id);
			KidsWatUtils.saveFamilyIdAndDeviceId(bbf.family_id, bbf.device_id);

			// setCircleImage(imageview_l, family_id, bbf.base64_img, bbf.sex);

			// KidsWatUtils.setKidsImage(MainActivity.this, imageview_l,
			// family_id, bbf.base64_img, bbf.sex);

			imageview_l.setImageBitmap(KidsWatUtils.getBabyImg_v3(MainActivity.this, family_id, bbf.sex, bbf));
			// listBaby.add(bb);
			babyAdapter.setCurrentFamailyId(family_id);
			
//			get_device_remian_days(bbf.sim);
			get_device_timeleft(KidsWatConfig.getUseCountryCode() + KidsWatConfig.getUserPhone(), AppContext.getInstance().currentFamily.device_iccid);
			
			if (listBaby.size() > 0) {
				babyAdapter.notifyDataSetChanged();

				BaseEvent eventx = new BaseEvent(BaseEvent.MSGTYPE_3___CHANGE_USER, "change user");
				EventBus.getDefault().post(eventx);

				BaseEvent eventx1 = new BaseEvent(BaseEvent.MSGTYPE_6___WHITE_TO_FRONT, "change user");
				EventBus.getDefault().post(eventx1);
				
				BaseEvent eventx2 = new BaseEvent(BaseEvent.MSGTYPE_3_SHOW_RECORD,
						"" + AppContext.getInstance().currentFamily.version);
				EventBus.getDefault().post(eventx2);
			}

			popupwindow.dismiss();

		}
	}

	public void initmPopupWindowView() {
		// 获取自定义布局文件的视图
		View customView = MainActivity.this.getLayoutInflater().inflate(R.layout.view_popwindow, null, false);
		// 创建PopupWindow宽度和高度
		popupwindow = new PopupWindow(customView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, true);
		/**
		 * 设置动画效果 ,从上到下加载方式等，不设置自动的下拉，最好 [动画效果不好，不加实现下拉效果，不错]
		 */
		// popupwindow.setAnimationStyle(R.style.AnimationFade);
		popupwindow.setOutsideTouchable(true);
		// 点击屏幕其他部分及Back键时PopupWindow消失
		popupwindow.setBackgroundDrawable(new BitmapDrawable());
		// 自定义view添加触摸事件
		customView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (popupwindow != null && popupwindow.isShowing()) {
					popupwindow.dismiss();
					popupwindow = null;
				}
				return false;
			}
		});

		// String family_id = KidsWatConfig.getDefaultFamilyId();
		// SqlDb db = SqlDb.get(this);
		// beanForDb___Family bf = db.getFamilyBy_did(family_id);
		// db.closeDb();
		// popupwindow弹出的listview
		listview_user = (ListView) customView.findViewById(R.id.listView_select);

		listview_user.setAdapter(babyAdapter);
		// --------------
	}

	private BottomPopupWindow mClearWindow = null;

	protected void showClearPopupWindow(View relyView) {
		if (mClearWindow == null) {
			mClearWindow = new BottomPopupWindow(this, 0, relyView);

			mClearWindow.addItem(getApplicationContext().getResources().getString(R.string.clear),
					new onClickItemListener() {

						@Override
						public void clickItem(View v) {
							//
							clear_user_notice_list();
						}
					}, ItemPosition.OTHER);

			// 添加“取消”
			mClearWindow.addCancelItem(getApplicationContext().getResources().getString(R.string.cancle), null, 10);
		}
		mClearWindow.show();
	}

	private void login() {
		final String country_code = KidsWatConfig.getUseCountryCode();
		final String userPhone = KidsWatConfig.getUserPhone();
		final String password = KidsWatConfig.getUserPassword();
		String pushToken = KidsWatUtils.getGooglePushToken();

		if (pushToken == null) {
			// showLongToast("Connect to Google Play failure, some
			// functions(SOS,Google map) will not be available.");
			pushToken = "jiadetoken";
		}
		final String req_url = KidsWatApiUrl.getUrlFor___login(country_code, userPhone, password, pushToken);

		AppContext.getInstance().map_familyinfo.clear();
		listBaby.clear();

		ApiHttpClient.get(req_url, new HttpCallBack() {

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", req_url, t));
				// gson.fromJson("{"user":"admin","pwd":"123456"}",
				beanFor___login b = AppContext.getInstance().getGson().fromJson(t, beanFor___login.class);

				if (b.error == 0) {
					AppContext.isLogin = true;
					KidsWatConfig.saveUserInfo(b.info.user, password);
					SqlDb db = SqlDb.get(AppContext.getInstance());
					db.saveMember(b);
					db.deleteMember_family_By_uid(b.info.user.uid);
					db.closeDb();
					showWaitDialog(getApplicationContext().getResources().getString(R.string.loading));
					get_family_list();
					KidsWatUtils.get_notice_list_ex();
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);

				if (errorNo == -1) {
					// showLongToast("Please check your connection and try
					// again.");
					showLongToast(getApplicationContext().getResources().getString(R.string.check_conn));
				} else {
					String msg = String.format("%s(error=%s)", errorNo == -1 ? "Connect to the server failed" : strMsg,
							errorNo);
					showLongToast(getApplicationContext().getResources().getString(R.string.check_conn));
				}
			}

			@Override
			public void onFinish() {
				dismissDialog();
			}

		});
	}

	private List<String> mFamily_id_list;

	private void get_family_list() {
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		final String req_url = KidsWatApiUrl.getUrlFor___get_family_list(uid, access_token);

		ApiHttpClient.get(req_url, new HttpCallBack() {
			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", req_url, t));
				beanFor___get_family_list b = AppContext.getInstance().getGson().fromJson(t,
						beanFor___get_family_list.class);
				
				if (b.getError() == 0) {
					AppContext.getInstance().family_list = b;
					SqlDb db = SqlDb.get(AppContext.getInstance());
					db.save___member_family(b);
					db.closeDb();

					int count = b.getInfo().getDdata().size();
					if (count > 0) {
						EventBus.getDefault().post(
								new BaseEvent(BaseEvent.MSGTYPE_2___FAMILY_LIST_HAS_DATA, "get_family_list.count.>0"));

						if (count > 1) {
							imageview_pop.setVisibility(View.VISIBLE);
						} else {
							imageview_pop.setVisibility(View.INVISIBLE);
						}

						if (count == 1) {
							imageview_l.setVisibility(View.INVISIBLE);
						}

						int j = 0;
						mFamily_id_list = new ArrayList<String>();
						for (FamilyMember m : b.getInfo().getDdata()) {
							String family_id = m.getFamily_id();
							mFamily_id_list.add(family_id);
							get_family_info(family_id, true, j, count);
							j++;
						}

					} else {
						EventBus.getDefault().post(
								new BaseEvent(BaseEvent.MSGTYPE_2___FAMILY_LIST_EMPTY, "get_family_list.count.0"));
					}
				}
			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String xmsg = String.format("errorNo:%s\n%s", errorNo, strMsg);
				TLog.log(String.format("url:%s\nt:%s", req_url, xmsg));
			}

			@Override
			public void onFinish() {
				dismissDialog();
			}

		});

	}

	private void get_family_info(final String family_id, final boolean isDefaultFamily, final int index,
			final int total) {

		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();

		final String req_url = KidsWatApiUrl.getUrlFor___get_family_info(uid, access_token, family_id);

		ApiHttpClient.get(req_url, new HttpCallBack() {

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", req_url, t));

				final beanFor___get_family_info b = AppContext.getInstance().getGson().fromJson(t,
						beanFor___get_family_info.class);

				if (b.getError() == 0) {
					SqlDb db = SqlDb.get(AppContext.getInstance());
					db.saveFamily(family_id, b);
					beanForDb___Family bf = db.getFamilyBy_fid(family_id);
					beanForDb___LoginMember b_lk = db.getLoginMemberBy_uid_fid(uid, family_id);
					db.closeDb();

					Baby bb = new Baby();
					bb.setNickname(b.getInfo().getBaby().getNickname());
					bb.setBase64Img(b.getInfo().getBaby().getBase64Img());
					bb.setImg_path(b.getInfo().getBaby().getImg_path());
					bb.setFamily_id(family_id);
					bb.setSex(b.getInfo().getBaby().getSex());
					listBaby.add(bb);

					String device_id = b.getInfo().getDdata().getDevice_id();
					String lastLoginBaby = KidsWatConfig.getDefaultFamilyId();
					
					if (mFamily_id_list.contains(lastLoginBaby) && !TextUtils.isEmpty(lastLoginBaby)) {
						if (lastLoginBaby.equals(family_id)) {
							AppContext.getInstance().currentBeanFamily = b;
							AppContext.getInstance().currentFamily = bf;
							AppContext.getInstance().loginUser_kid = b_lk;

							KidsWatConfig.setDefaultFamilyId(family_id);
							KidsWatConfig.saveDeviceId(device_id);

							if (listBaby.size() > 0) {
								babyAdapter.setCurrentFamailyId(family_id);
								babyAdapter.notifyDataSetChanged();
							}

							if (b.getInfo().getBaby().getImg_path() != null) {
								AppContext.imageLoader.displayImage("http://" + b.getInfo().getBaby().getImg_path(),
										imageview_l, AppContext.commenOptions);
							} else {
								int imgSrcId = b.getInfo().getBaby().getSex() == 1 ? R.drawable.icon_boy
										: R.drawable.icon_girl;
								imageview_l.setImageResource(imgSrcId);
							}

							BaseEvent event = new BaseEvent(BaseEvent.MSGTYPE_3___LOAD_FAMILY_FIRST_ITEM,
									"load_family");
							
							EventBus.getDefault().post(event);

							BaseEvent eventx2 = new BaseEvent(BaseEvent.MSGTYPE_3_SHOW_RECORD,
									"" + AppContext.getInstance().currentFamily.version);
							EventBus.getDefault().post(eventx2);
						}
					} else if (index == 0) {
						//
						bf.base64_img = b.getInfo().getBaby().getBase64Img();

						AppContext.getInstance().currentBeanFamily = b;
						AppContext.getInstance().currentFamily = bf;
						AppContext.getInstance().loginUser_kid = b_lk;
						KidsWatConfig.setDefaultFamilyId(family_id);
						KidsWatConfig.saveDeviceId(device_id);
						if (listBaby.size() > 0) {
							babyAdapter.setCurrentFamailyId(family_id);
							babyAdapter.notifyDataSetChanged();
						}

						// imageview_l.setImageBitmap(KidsWatUtils.getBabyImg_v3(MainActivity.this,
						// family_id,
						// b.getInfo().getBaby().getSex(), b));
						if (b.getInfo().getBaby().getImg_path() != null) {
							AppContext.imageLoader.displayImage("http://" + b.getInfo().getBaby().getImg_path(),
									imageview_l, AppContext.commenOptions);
						} else {
							int imgSrcId = b.getInfo().getBaby().getSex() == 1 ? R.drawable.icon_boy
									: R.drawable.icon_girl;
							imageview_l.setImageResource(imgSrcId);
						}

						BaseEvent event = new BaseEvent(BaseEvent.MSGTYPE_3___LOAD_FAMILY_FIRST_ITEM,
								"load_family_index_0");
						EventBus.getDefault().post(event);
						BaseEvent eventx2 = new BaseEvent(BaseEvent.MSGTYPE_3_SHOW_RECORD,
								"" + AppContext.getInstance().currentFamily.version);
						EventBus.getDefault().post(eventx2);
					}

					new Thread(new Runnable() {
						public void run() {
							System.out.println("img_path:" + b.getInfo().getBaby().getImg_path());
							if (!TextUtils.isEmpty(b.getInfo().getBaby().getImg_path())) {

								File file = new File(
										String.format("%sbb_%s.jpg", KidsWatConfig.getTempFilePath(), family_id));

								dlImg("http://" + b.getInfo().getBaby().getImg_path(), family_id);
								// imageview_l.setImageBitmap(KidsWatUtils.getBabyImg_v2(MainActivity.this,family_id,b.getInfo().getBaby()
								// .getSex()));
							} else {
								BaseEvent event_change_kids_photo = new BaseEvent(
										BaseEvent.MSGTYPE_3___CHANGE_KIDS_PHOTO, "change_image");
								EventBus.getDefault().post(event_change_kids_photo);
							}
						}
					}).start();
					get_watch_step();
					get_watch_info(device_id, family_id);
					get_device_validate(device_id);
				}
			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String xmsg = String.format("errorNo:%s\n%s", errorNo, strMsg);
				TLog.log(String.format("url:%s\nt:%s", req_url, xmsg));
			}

			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				super.onFinish();
			}
		});
	}

	public void dlImg(String url, String family_id) {
		try {
			System.out.println("url=" + url);
			String filepath = String.format("%sbb_%s.jpg", KidsWatConfig.getTempFilePath(), family_id);
			// 得到图片地址
			byte[] data = readImage(url);
			FileOutputStream outStream = new FileOutputStream(new File(filepath));
			outStream.write(data);
			// 关闭流的这个地方需要完善一下
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			BaseEvent event_change_kids_photo = new BaseEvent(BaseEvent.MSGTYPE_3___CHANGE_KIDS_PHOTO, "change_image");
			EventBus.getDefault().post(event_change_kids_photo);
		}
	}

	// 声明称为静态变量有助于调用
	public byte[] readImage(String path) throws Exception {
		URL url = new URL(path);
		// 记住使用的是HttpURLConnection类
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		// 如果运行超过5秒会自动失效 这是android规定
		conn.setConnectTimeout(5 * 1000);
		InputStream inStream = conn.getInputStream();
		// 调用readStream方法
		return readStream(inStream);
	}

	public byte[] readStream(InputStream inStream) throws Exception {
		// 把数据读取存放到内存中去
		ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = -1;
		while ((len = inStream.read(buffer)) != -1) {
			outSteam.write(buffer, 0, len);
		}
		outSteam.close();
		inStream.close();
		return outSteam.toByteArray();
	}

	private void get_watch_info(final String device_id, final String family_id) {

		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();

		final String req_url = KidsWatApiUrl.getUrlFor___get_watch_info(uid, access_token, device_id);

		ApiHttpClient.get(req_url, new HttpCallBack() {

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", req_url, t));
				// gson.fromJson("{"user":"admin","pwd":"123456"}",
				beanFor___get_watch_info b = AppContext.getInstance().getGson().fromJson(t,
						beanFor___get_watch_info.class);

				if (b.error == 0) {
					SqlDb db = SqlDb.get(AppContext.getInstance());
					db.saveFamily(device_id, b);

					beanForDb___Family bf = db.getFamilyBy_fid(family_id);
					db.closeDb();

					AppContext.getInstance().map_familyinfo.put(family_id, bf);
					
					if(family_id.equals(KidsWatConfig.getDefaultFamilyId())){
//						get_device_remian_days(b.info.ddata.sim);
						get_device_timeleft(KidsWatConfig.getUseCountryCode() + KidsWatConfig.getUserPhone(), AppContext.getInstance().currentFamily.device_iccid);
					}
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String xmsg = String.format("errorNo:%s\n%s", errorNo, strMsg);
				TLog.log(String.format("url:%s\nt:%s", req_url, xmsg));
			}

		});

	}

	private void get_device_validate(final String device_id) {

		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();

		final String req_url = KidsWatApiUrl.getUrlFor___get_device_validate(uid, access_token, device_id);

		ApiHttpClient.get(req_url, new HttpCallBack() {

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", req_url, t));

				try {
					JSONObject jso = new JSONObject(t);
					int code = jso.getInt("error");
					if (code == 0) {
						String device_id = KidsWatConfig.getUserDeviceId();

						String valid_date;
						valid_date = jso.getJSONObject("info").getString("valid_date");

						SqlDb db = SqlDb.get(AppContext.getInstance());
						db.saveFamilyValid_date(device_id, valid_date);
						// beanForDb___Family f = db.getFamilyBy_did(device_id);
						db.closeDb();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String xmsg = String.format("errorNo:%s\n%s", errorNo, strMsg);
				TLog.log(String.format("url:%s\nt:%s", req_url, xmsg));
			}

		});

	}

	private void clear_user_notice_list() {
		// -----------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();

		final String req_url = KidsWatApiUrl.getUrlFor___clear_user_notice_list(uid, access_token);
		// -------------------------

		ApiHttpClient.get(req_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				// showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", req_url, t));
				SqlDb db = SqlDb.get(MainActivity.this);
				int r = db.clearMessage(uid);
				db.closeDb();

				BaseEvent event = new BaseEvent(BaseEvent.MSGTYPE_3___CLEAR_NOTICE_DONE, "clear_notice");
				EventBus.getDefault().post(event);
			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String xmsg = String.format("errorNo:%s\n%s", errorNo, strMsg);
				TLog.log(String.format("url:%s\nt:%s", req_url, xmsg));
			}

			@Override
			public void onFinish() {
//				 dismissDialog();
			}
		});
	}

	/**
	 * 监听返回--是否退出程序
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			return mDoubleClickExit.onKeyDown(keyCode, event);
		}
		return super.onKeyDown(keyCode, event);
	}

	// ------------yandex touchevent -------------
	private ArrayList<MyOnTouchListener> onTouchListeners = new ArrayList<MyOnTouchListener>(10);

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		for (MyOnTouchListener listener : onTouchListeners) {
			listener.onTouch(ev);
		}
		return super.dispatchTouchEvent(ev);
	}

	public void registerMyOnTouchListener(MyOnTouchListener myOnTouchListener) {
		onTouchListeners.add(myOnTouchListener);
	}

	public void unregisterMyOnTouchListener(MyOnTouchListener myOnTouchListener) {
		onTouchListeners.remove(myOnTouchListener);
	}

	public interface MyOnTouchListener {
		public boolean onTouch(MotionEvent ev);
	}

	public void setStepNumber(int step) {
		mStep = step;
	}

	public int getStepNumber() {
		return mStep;
	}

	public void get_watch_step() {
		String uid = KidsWatConfig.getUserUid();
		String access_token = KidsWatConfig.getUserToken();
		String family_id = KidsWatConfig.getDefaultFamilyId();

		if (StringUtils.isEmpty(uid))
			return;

		Calendar a = Calendar.getInstance();
		int year = a.get(Calendar.YEAR);
		int month = a.get(Calendar.MONTH);
		int dayOfMonth = a.get(Calendar.DAY_OF_MONTH);

		long utcToday = KidsWatUtils.getUtcTimeAt0Time(year, month, dayOfMonth);
		final String request_url = KidsWatApiUrl.getUrlFor___get_watch_data_latest(uid, access_token, family_id,
				utcToday);

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog(getApplicationContext().getResources().getString(R.string.loading));
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));

				beanFor___get_watch_data_latest response = AppContext.getInstance().getGson().fromJson(t,
						beanFor___get_watch_data_latest.class);
				if (response.error == 0) {
					if (response.info.lastest_point != null) {
						int step = response.info.lastest_point.step_number;
						setStepNumber(step);
					} else {
						setStepNumber(0);
					}
				} else {
					System.out.println("error=" + response.error);
					setStepNumber(0);
					// showLongToast("За сегодняшний день истории перемещения
					// нет！");
				}
			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
				System.out.println("error=" + msg);
			}

			@Override
			public void onFinish() {
				System.out.println("step1:" + getStepNumber());
				dismissDialog();
			}
		});
	}
	
	/*private void get_family_list_v2(){
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		final String req_url = KidsWatApiUrl.getUrlFor___get_family_list_v2(uid, access_token);

		ApiHttpClient.get(req_url, new HttpCallBack() {
			
			@Override
			public void onPreStart() {
				// TODO Auto-generated method stub
				super.onPreStart();
				if(!KidsWatUtils.isNetworkAvailable(MainActivity.this)){
					dismissDialog();
					Toast.makeText(MainActivity.this, getApplicationContext().getResources().getString(R.string.tip_network_unavailable), Toast.LENGTH_SHORT).show();
				}
			}
			
			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", req_url, t));
				try {
					JSONObject obj = new JSONObject(t);
					int error = (int) obj.get("error");
					if (error == 0) {
						
						JSONObject json = new JSONObject(t);
						JSONArray family_list_data = json.getJSONObject("info").getJSONArray("family_list_data");

						if (family_list_data.length() > 1) {
							imageview_pop.setVisibility(View.VISIBLE);
							imageview_l.setVisibility(View.VISIBLE);
						} else {
							imageview_pop.setVisibility(View.INVISIBLE);
							imageview_l.setVisibility(View.INVISIBLE);
						}

						if (family_list_data.length() > 0) {
							EventBus.getDefault().post(
									new BaseEvent(BaseEvent.MSGTYPE_2___FAMILY_LIST_HAS_DATA, "get_family_list.count.>0"));
						} else {
							EventBus.getDefault().post(
									new BaseEvent(BaseEvent.MSGTYPE_2___FAMILY_LIST_EMPTY, "get_family_list.count.0"));
						}
						
						mFamily_id_list = new ArrayList<String>();
						for (int i = 0; i < family_list_data.length(); i++) {
							JSONObject data = (JSONObject) family_list_data.opt(i);
							String dataString = data.toString();
							final beanFor___get_family_info_v2 b = AppContext.getInstance().getGson()
									.fromJson(dataString, beanFor___get_family_info_v2.class);
							
							String family_id = b.getBaby_info().getFamily_id();
							mFamily_id_list.add(family_id);
						}
						
						for (int i = 0; i < family_list_data.length(); i++) {
							JSONObject data = (JSONObject) family_list_data.opt(i);
							String dataString = data.toString();
							final beanFor___get_family_info_v2 b = AppContext.getInstance().getGson()
									.fromJson(dataString, beanFor___get_family_info_v2.class);
							TLog.log("relation=" + b.getRelation());
							String family_id = b.getBaby_info().getFamily_id();

							SqlDb db = SqlDb.get(AppContext.getInstance());
							db.save___member_family(b);
							db.saveFamily(family_id, b);
							beanForDb___Family bf = db.getFamilyBy_fid(family_id);
							String b_lk_uid = b.getBaby_info().getUid();
							String b_lk_family_id = b.getBaby_info().getFamily_id();
							beanForDb___LoginMember b_lk = db.getLoginMemberBy_uid_fid(uid, b_lk_family_id);
							db.closeDb();

							Baby bb = new Baby();
							bb.setNickname(b.getBaby_info().getNickname());
							bb.setBase64Img(b.getBaby_info().getImg());
							bb.setFamily_id(family_id);
							bb.setSex(b.getBaby_info().getSex());
//							bb.setBb_imgpath(b.getBaby_info().getImg_path());
							bb.setImg_path(b.getBaby_info().getImg_path());
							listBaby.add(bb);

							String device_id = b.getDevice_id();
							String lastLoginBaby = KidsWatConfig.getDefaultFamilyId();

							if (mFamily_id_list.contains(lastLoginBaby) && !TextUtils.isEmpty(lastLoginBaby)) {
								if (lastLoginBaby.equals(family_id)) {
									AppContext.getInstance().currentBeanFamily = b;
									AppContext.getInstance().currentFamily = bf;
									AppContext.getInstance().loginUser_kid = b_lk;
									
									KidsWatConfig.setDefaultFamilyId(family_id);
									KidsWatConfig.saveDeviceId(device_id);

									if (listBaby.size() > 0) {
										babyAdapter.setCurrentFamailyId(family_id);
										babyAdapter.notifyDataSetChanged();
									}
									
									if (!TextUtils.isEmpty(b.getBaby_info().getImg_path().toString().trim())) {
										AppContext.imageLoader.displayImage("http://" + b.getBaby_info().getImg_path(),
												imageview_l, AppContext.commenOptions);
									} else {
										int imgSrcId = b.getBaby_info().getSex() == 1 ? R.drawable.icon_boy
												: R.drawable.icon_girl;
										imageview_l.setImageResource(imgSrcId);
									}
									
									BaseEvent event = new BaseEvent(BaseEvent.MSGTYPE_3___LOAD_FAMILY_FIRST_ITEM,
											"load_family");
									EventBus.getDefault().post(event);
									
									BaseEvent eventx2 = new BaseEvent(BaseEvent.MSGTYPE_3_SHOW_RECORD,
											"" + AppContext.getInstance().currentFamily.version);
									EventBus.getDefault().post(eventx2);
								}
							} else if (i == 0) {
								//
								System.out.println("---------------load first item-----------------");
								bf.base64_img = b.getBaby_info().getImg();

								AppContext.getInstance().currentBeanFamily = b;
								AppContext.getInstance().currentFamily = bf;
								AppContext.getInstance().loginUser_kid = b_lk;
								
								KidsWatConfig.setDefaultFamilyId(family_id);
								KidsWatConfig.saveDeviceId(device_id);
								if (listBaby.size() > 0) {
									babyAdapter.setCurrentFamailyId(family_id);
									babyAdapter.notifyDataSetChanged();
								}
								
								if (!TextUtils.isEmpty(b.getBaby_info().getImg_path().toString().trim())) {
									AppContext.imageLoader.displayImage("http://" + b.getBaby_info().getImg_path(),
											imageview_l, AppContext.commenOptions);
								} else {
									int imgSrcId = b.getBaby_info().getSex() == 1 ? R.drawable.icon_boy
											: R.drawable.icon_girl;
									imageview_l.setImageResource(imgSrcId);
								}

								BaseEvent event = new BaseEvent(BaseEvent.MSGTYPE_3___LOAD_FAMILY_FIRST_ITEM,
										"load_family_index_0");
								EventBus.getDefault().post(event);

								BaseEvent eventx2 = new BaseEvent(BaseEvent.MSGTYPE_3_SHOW_RECORD,
										"" + AppContext.getInstance().currentFamily.version);
								EventBus.getDefault().post(eventx2);
								
//								BaseEvent eventx3 = new BaseEvent(
//										BaseEvent.MSGTYPE_3___DISCARD_RELOGIN, "change_family");
//								EventBus.getDefault().post(eventx3);
							}

							new Thread(new Runnable() {
								public void run() {
									File file = new File(String.format("%sbb_%s.jpg", KidsWatConfig.getTempFilePath(),
											b.getBaby_info().getFamily_id()));
									if (file.exists()) {
										return;
									}
									dlImg("http://" + b.getBaby_info().getImg_path(), b.getBaby_info().getFamily_id());
								}
							}).start();
							
							get_watch_step();
							get_watch_info(device_id, family_id);
							get_device_validate(device_id);
						}
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
			}
			
			@Override
			public void onFinish() {
				super.onFinish();
//				BaseActivity activity = MainActivity;
			}
		});
	}*/
	
	private void get_device_timeleft (String sim, String iccid) {
//		final String url = "http://kids_radar.clients4.arealidea.ru/api/v1/mobile_service/balance/";
		final String url = "http://kidsradar.lexand.ru/api/v1/mobile_service/balance/";
		getData(url);
	}
	
	private void getData (String url) {

		String iccid = AppContext.getInstance().currentFamily.device_iccid;
		String sim = KidsWatConfig.getUseCountryCode() + KidsWatConfig.getUserPhone();
		String token = "4teGENskDv2AXepVstwj";
		RequestBody requestBody = new MultipartBuilder()
		     .type(MultipartBuilder.FORM)
		     .addPart(Headers.of(
		          "Content-Disposition", 
		              "form-data; name=\"watchICCID\""), 
		          RequestBody.create(null, String.format("[\"%s\"]", iccid)))
		     .addPart(Headers.of(
		    		 "Content-Disposition", 
		    		 "form-data; name=\"phoneNumber\""), 
		    		 RequestBody.create(null, sim))
		     .addPart(Headers.of(
		    		 "Content-Disposition", 
		    		 "form-data; name=\"token\""), 
		    		 RequestBody.create(null, token))
		     .build();
		
		Request request = new Request.Builder()
		    .url(url)
		    .post(requestBody)
		    .build();
		
		Call call = new OkHttpClient().newCall(request);
		call.enqueue(new Callback() {
			 @Override
	            public void onFailure(Request request, IOException e)
	            {
				 TLog.log("Failure");
	            }
			 	
	            @Override
	            public void onResponse(final Response response) throws IOException
	            {
	            	try {
	            		
	            		String resp_str = response.body().string();
	            		TLog.log("get_remain_days >>> " + resp_str);
						JSONObject json = new JSONObject(resp_str);
						
						if (json.getInt("success") == 1) {
							String sim = AppContext.getInstance().currentFamily.sim;
							JSONArray arrays = (JSONArray) json.get("balance");
							
							if (arrays == null || arrays.length() <= 0) {
								return;
							}
							
							int timeleft = ((JSONObject)arrays.get(0)).getInt("useDays");
							showRemainDays(timeleft);
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            }
		});
	}
	
	private void get_device_remian_days(String sim) {
		// TODO Auto-generated method stub
		String url = "https://api.kidstracker.lexand-test.ru/device/?sim=" + sim;
		ApiHttpClient.get(url, new HttpCallBack() {
			
			@Override
			public void onPreStart() {
				// TODO Auto-generated method stub
				super.onPreStart();
			}
			
			@Override
			public void onSuccess(String t) {
				// TODO Auto-generated method stub
				super.onSuccess(t);
				
				try {
					String sim = AppContext.getInstance().currentFamily.sim;
					int timeleft = 0;
					
					JSONArray json = new JSONObject(t).getJSONArray("results");
					
					int count = new JSONObject(t).getInt("count");
					
					if(count == 0){
						TLog.log("获取设备剩余时间失败，请确定是否加入后台数据库");
					} else {
						timeleft = ((JSONObject)json.get(0)).getInt("days_remaining");
						
						showRemainDays(timeleft);
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			@Override
			public void onFailure(int errorNo, String strMsg) {
				// TODO Auto-generated method stub
				super.onFailure(errorNo, strMsg);
				TLog.log("faile");
			}
			
			@Override
			public void onFinish() {
				// TODO Auto-generated method stub
				TLog.log("finish");
				super.onFinish();
			}
		});
	}
	
	public void showRemainDays (int leftTime) {
		
		if (leftTime <= 5) {
			AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
			String message = "";
			if(leftTime==0){
				message = String.format("Подписка на сервис заканчивается через %d дней.", leftTime);
			}else{
				message = "Подписка на сервис закончилась. Пожалуйста оплатите.";
			}
			builder.setMessage(message).setPositiveButton(getApplicationContext().getResources().getString(R.string.topup),
					new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					startActivity(new Intent(MainActivity.this, TopUpPageActivity.class));
				}
			}).setNegativeButton(getApplicationContext().getResources().getString(R.string.understand), new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});
			Looper.prepare();
			builder.show();
			Looper.loop();
		}
		
		/*if(leftTime > 10 && leftTime <= 30){
			long showTime = System.currentTimeMillis();
			long preShowTime = KidsWatConfig.getLastShowRemianingTime();
			int intervalDays = (int) ((showTime - preShowTime) / 60 / 60 / 24);
			if(intervalDays < 5){
				return;
			}
			
			AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
			builder.setMessage(String.format("Подписка на сервис заканчивается через %d дней.", leftTime)).setPositiveButton(getApplicationContext().getResources().getString(R.string.topup),
					new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					startActivity(new Intent(MainActivity.this, TopUpPageActivity.class));
				}
			}).setNegativeButton(getApplicationContext().getResources().getString(R.string.understand), new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});
			builder.show();
			KidsWatConfig.setLastShowRemianingTime(showTime);
		} else if(leftTime <= 10){

			AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
			builder.setMessage(String.format("Подписка на сервис заканчивается через %d дней.", leftTime)).setPositiveButton(getApplicationContext().getResources().getString(R.string.topup),
					new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					startActivity(new Intent(MainActivity.this, TopUpPageActivity.class));
				}
			}).setNegativeButton(getApplicationContext().getResources().getString(R.string.understand), new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});
			builder.show();
		}*/
	}
}
