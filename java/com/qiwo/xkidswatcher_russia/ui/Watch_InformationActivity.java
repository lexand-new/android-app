package com.qiwo.xkidswatcher_russia.ui;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.AppManager;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.db.SqlDb;
import com.qiwo.xkidswatcher_russia.event.BaseEvent;
import com.qiwo.xkidswatcher_russia.util.TLog;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

public class Watch_InformationActivity extends BaseActivity {

	/**
	 * 手表信息
	 */

	@InjectView(R.id.Qr_code_linear)
	LinearLayout Qr_code_linear;

	@InjectView(R.id.Change_SIM_card_linear)
	LinearLayout Change_SIM_card_linear;

	@InjectView(R.id.Look_for_device_linear)
	LinearLayout Look_for_device_linear;

	@InjectView(R.id.Discard_btn)
	Button Discard_btn;

	@InjectView(R.id.linearLayout_l)
	LinearLayout linearLayout_l;

	@Override
	protected int getLayoutId() {
		return R.layout.watch_setting_linear;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().hide();
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent();
		switch (v.getId()) {
		/*
		 * 二维码--QR code
		 */
		case R.id.Qr_code_linear:
			SqlDb db = SqlDb.get(Watch_InformationActivity.this);
			beanForDb___Family bb = db.getFamilyBy_fid(KidsWatConfig
					.getDefaultFamilyId());
			db.closeDb();

			intent.setClass(Watch_InformationActivity.this,
					QrcodeActivity.class);
			intent.putExtra("qrcode", bb.qrcode);
			intent.putExtra("sim", bb.sim);
			startActivity(intent);
			break;

		/*
		 * 寻找设备--Looking for equipment
		 */
		case R.id.Look_for_device_linear:
			DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

					final String device_id = KidsWatConfig.getUserDeviceId();
					watch_buzzer(device_id, 1, 60);
				}
			};
			
			String title = getApplicationContext().getResources().getString(R.string.Look_for_device);
			String message = getApplicationContext().getResources().getString(R.string.tip_Look_for_device);
			String positiveText = getApplicationContext().getResources().getString(R.string.ok);
			String negativeText = getApplicationContext().getResources().getString(R.string.cancle);

			showConfirmDialog(title, message, positiveText, negativeText,
					positiveListener);
			
			break;
		/*
		 * 更换SIM卡--SIM card replacement
		 */
		case R.id.Change_SIM_card_linear:

			intent.setClass(Watch_InformationActivity.this,
					ChangeSIMActivity.class);
			startActivity(intent);
			break;

		case R.id.Discard_btn:
			DisCardClick();
			break;
		case R.id.linearLayout_l:
			this.finish();
			break;
		default:
			break;
		}
	}

	public void DisCardClick() {
		String discard = Discard_btn.getText().toString();
		if (discard.equals(getResources().getString(R.string.Discard))) {
			// 解绑
			DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// showLongToast("xxxx");

					DialogInterface.OnClickListener positiveListener_2 = new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// showLongToast("xxxx");
							final String device_id = KidsWatConfig
									.getUserDeviceId();
							delete_watch(device_id);
						}
					};
					String title2 = getApplicationContext().getResources().getString(R.string.reminder);
					String message2 = getApplicationContext().getResources().getString(R.string.reset_message);
					String positiveText2 = "да";
					String negativeText2 = getApplicationContext().getResources().getString(R.string.cancle);

					showConfirmDialog(title2, message2, positiveText2,
							negativeText2, positiveListener_2);

					// final String device_id = KidsWatConfig
					// .getUserDeviceId();
					// delete_watch(device_id);
				}
			};
			String title = getApplicationContext().getResources().getString(R.string.discard_paring);
			String message = getApplicationContext().getResources().getString(R.string.discard_message);
			String positiveText = "да";
			String negativeText = getApplicationContext().getResources().getString(R.string.cancle);

			showConfirmDialog(title, message, positiveText, negativeText,
					positiveListener);
		} else if (discard.equals(getResources().getString(
				R.string.Stop_Following))) {
			// 退出家庭圈
			DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// showLongToast("power off");
					final String family_id = KidsWatConfig.getDefaultFamilyId();
					user_quit_from_family(family_id);
				}
			};
			String title = getApplicationContext().getResources().getString(R.string.stop_following);
			String message = getApplicationContext().getResources().getString(R.string.stop_following_message);
			String positiveText = "да";
			String negativeText = getApplicationContext().getResources().getString(R.string.cancle);

			showConfirmDialog(title, message, positiveText, negativeText,
					positiveListener);
		}

	}

	@Override
	public void initView() {
		Qr_code_linear.setOnClickListener(this);
		Look_for_device_linear.setOnClickListener(this);
		Change_SIM_card_linear.setOnClickListener(this);
		Discard_btn.setOnClickListener(this);
		linearLayout_l.setOnClickListener(this);

		/*
		 * 逻辑
		 */

		if (AppContext.getInstance().currentFamily != null)

			if (AppContext.getInstance().loginUser_kid != null
					&& AppContext.getInstance().loginUser_kid.isAdmin == 1
					&& (AppContext.getInstance().currentFamily.version
							.equals("461") || AppContext.getInstance().currentFamily.version
							.startsWith("561"))) {
				Change_SIM_card_linear.setVisibility(0);
				// set text
				Discard_btn.setText(R.string.Discard);
			} else if (AppContext.getInstance().loginUser_kid != null
					&& AppContext.getInstance().loginUser_kid.isAdmin == 1) {
				Change_SIM_card_linear.setVisibility(8);
				Discard_btn.setText(R.string.Discard);
			} else {
				Change_SIM_card_linear.setVisibility(8);
				Discard_btn.setText(R.string.Stop_Following);
			}
	}

	@Override
	public void initData() {

	}

	/*
	 * 
	 */
	private void watch_buzzer(String device_id, int type, int mtime) {

		// -----------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		// final String device_id = AppContext.getInstance().getProperty(
		// "user.device_id");
		// final String family_id = AppContext.getDefaultFamilyId();
		// -------------------------
		final String request_url = KidsWatApiUrl.getUrlFor___watch_buzzer(uid,
				access_token, device_id, type, mtime);

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));

				try {
					JSONObject response = new JSONObject(t);

					int code = response.getInt("error");
					if (code == 0) {
						// response={"data":{"mode":2},"error":0,"info":{"message":"get success!"}}
						// Operation Succeeds
						showConfirmInformation(null, "Успешная работа");
						// showLongToast("The operation is successful");
					} else if (code == 2015001) {
						// dismissDialog("Device is in sleep mode.", 800);
						dismissDialog(null, 10);
						showConfirmInformation(null,
								"Вы не можете сделать это, пока устройство находится в режиме сна.");
					} else {
						showConfirmInformation(null, "Действие не удалось");
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String
						.format("%s(error=%s)",
								errorNo == -1 ? "Подключение к серверу не удалось"
										: strMsg, errorNo);
				TLog.log(msg);
				showLongToast(msg);
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});
		// -------------------------

	}

	private void delete_watch(final String device_id) {
		if (AppContext.getInstance().currentFamily == null)
			return;
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		final String version = AppContext.getInstance().currentFamily.version;
		// final String device_id = AppContext.getInstance().getProperty(
		// "user.device_id");
		// final String family_id = AppContext.getDefaultFamilyId();
		// -------------------------
		final String request_url = KidsWatApiUrl.getUrlFor___delete_watch(uid,
				access_token, device_id, version);

		// -------------------------

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));

				try {
					JSONObject response = new JSONObject(t);

					int code = response.getInt("error");
					if (code == 0) {
						SqlDb db = SqlDb.get(Watch_InformationActivity.this);
						db.clearMessage(uid);
						db.closeDb();

						// showLongToast("The device has been discarded！");

						DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {

								if (AppContext.getInstance()
										.getFamilyListCount() > 0) {
									EventBus.getDefault()
											.post(new BaseEvent(
													BaseEvent.MSGTYPE_1___RELOGIN,
													"relogin"));
								} else {
									// Intent intent = new Intent(
									// KidsProfileActivity.this,
									// MainActivity.class);
									// startActivity(intent);
									AppManager
											.AppRestart(Watch_InformationActivity.this);
								}

								Watch_InformationActivity.this.finish();

							}
						};
						String title = getApplicationContext().getResources().getString(R.string.app_name);
						String messagex = "Устройство успешно отвязано！";
						String positiveText = "OK";
						String negativeText = null;

						showConfirmDialog(title, messagex, positiveText,
								negativeText, positiveListener);

					} else {
						// showConfirmInformation(null, "Discard Fails");
						showConfirmInformation(
								null,"Вы не сможете узнать, где находится ваш ребёнок, не сможете получать уведомления о его местонахождении. Вы уверены, что хотите отлогиниться?");
						// Toast.makeText(
						// getActivity(),
						// response.getJSONObject("info").getString(
						// "message"), Toast.LENGTH_SHORT).show();
						//
					}

				} catch (JSONException e) {
					e.printStackTrace();

				}
			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				if (errorNo == 500) {
					Watch_InformationActivity.this.finish();
					EventBus.getDefault().post(
							new BaseEvent(BaseEvent.MSGTYPE_1___RELOGIN,
									"relogin"));
				} else {
					String msg = String.format("%s(error=%s)",
							errorNo == -1 ? "Connect to the server failed"
									: strMsg, errorNo);
					TLog.log(msg);
					showLongToast(msg);
				}
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});
		// -------------------------

	}

	private void user_quit_from_family(String family_id) {

		// -----------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		final String version = AppContext.getInstance().currentFamily.version;
		final String device_id = AppContext.getInstance().currentFamily.device_id;
		// final String family_id = AppContext.getDefaultFamilyId();
		// -------------------------
		final String request_url = KidsWatApiUrl
				.getUrlFor___user_quit_from_family(uid, access_token,
						family_id, version, device_id);

		// -------------------------

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));

				try {
					JSONObject response = new JSONObject(t);

					int code = response.getInt("error");
					if (code == 0) {
						// getActivity().finish();
						DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								if (AppContext.getInstance()
										.getFamilyListCount() > 0) {
									EventBus.getDefault()
											.post(new BaseEvent(
													BaseEvent.MSGTYPE_1___RELOGIN,
													"relogin"));
								} else {
									// Intent intent = new Intent(
									// KidsProfileActivity.this,
									// MainActivity.class);
									// startActivity(intent);
									AppManager
											.AppRestart(Watch_InformationActivity.this);

								}
								Watch_InformationActivity.this.finish();
							}
						};
						String title = getApplicationContext().getResources().getString(R.string.app_name);
						String messagex = getResources().getString(R.string.follower_stop_following);
						String positiveText = "OK";
						String negativeText = null;

						showConfirmDialog(title, messagex, positiveText,
								negativeText, positiveListener);

					} else {
						showLongToast("отслеживание  не  остановить.");
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String
						.format("%s(error=%s)",
								errorNo == -1 ? "Подключение к серверу не удалось"
										: strMsg, errorNo);
				TLog.log(msg);
				showLongToast(msg);
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});
		// -------------------------

	}
}
