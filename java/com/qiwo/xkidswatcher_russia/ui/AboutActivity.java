package com.qiwo.xkidswatcher_russia.ui;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.InjectView;

public class AboutActivity extends BaseActivity {

	@InjectView(R.id.linearLayout_l)
	LinearLayout linearLayout_l;

	@InjectView(R.id.about_version)
	TextView mVersion;

	@Override
	protected int getLayoutId() {
		return R.layout.activity_about;
	}

	@Override
	protected boolean hasActionBar() {
		return false;
	}

	@Override
	protected void init(Bundle savedInstanceState) {
		super.init(savedInstanceState);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.linearLayout_l:
			finish();
			break;
		case R.id.button_ok:
			finish();
			break;
		default:
			break;
		}
	}

	@Override
	public void initView() {
		// linearLayout_l.setOnClickListener(this);
		linearLayout_l.setOnClickListener(this);
		// -------
		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					getPackageName(), 0);
			mVersion = (TextView) findViewById(R.id.about_version);
			mVersion.setText(getResources().getString(R.string.app_name) + " V"
					+ info.versionName);
		} catch (NameNotFoundException e) {
			e.printStackTrace(System.err);
		}
	}

	@Override
	public void initData() {
		//
	}
	// ----------------

}
