package com.qiwo.xkidswatcher_russia.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.db.SqlDb;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.InjectView;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.overlay.balloon.BalloonItem;
import ru.yandex.yandexmapkit.utils.GeoPoint;

@Deprecated
public class ViewMessageActivity2 extends BaseActivity{

	@InjectView(R.id.linearLayout_l)
	LinearLayout linearLayout_l;

	// @InjectView(R.id.linearLayout_r)
	// LinearLayout linearLayout_r;

	@InjectView(R.id.linearLayout_safezone_name)
	LinearLayout linearLayout_safezone_name;

	@InjectView(R.id.linearlayout_play)
	LinearLayout linearlayout_play;

	@InjectView(R.id.frameLayout_line)
	FrameLayout frameLayout_line;

	@InjectView(R.id.textView_title)
	TextView textView_title;

	@InjectView(R.id.textView_des)
	TextView textView_des;

	@InjectView(R.id.textView_time)
	TextView textView_time;

	@InjectView(R.id.textView_play)
	TextView textView_play;
	
	@InjectView(R.id.btn_play)
	ImageView btn_play;

	@InjectView(R.id.view_msg_mapview)
	MapView mMapView;

	private double mlatitude = 0f;
	private double mlongitude = 0f;

	private String jsonData;

	private String device_id;
	private String command_no;
	int total_request_number;
	String family_id;

	@Override
	protected int getLayoutId() {
		return R.layout.layout_view_msg2;
	}

	@Override
	protected boolean hasActionBar() {
		return false;
	}

	@Override
	protected void init(Bundle savedInstanceState) {
		super.init(savedInstanceState);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.linearLayout_l:
			if (mp != null && isPlaying) {
				mp.stop();
				mp.release();
				mp = null;
				isPlaying = false;
			}
			finish();
			break;
		case R.id.button_ok:
			finish();
			break;

		case R.id.linearLayout_safezone_name:
			break;
		case R.id.linearlayout_play:

			if (mp != null && isPlaying) {
				mp.stop();
				mp.release();
				mp = null;
				isPlaying = false;
				textView_play.setText(getApplicationContext().getResources().getString(R.string.play));
				btn_play.setImageResource(R.drawable.play_sel);
			} else {
				String filepath = String.format("%saudio_%s_%s.amr",
						KidsWatConfig.getTempFilePath(), device_id, command_no);
				java.io.File f = new java.io.File(filepath);
				
				if (f.exists()) {
					textView_play.setText(getApplicationContext().getResources().getString(R.string.stop));
					btn_play.setImageResource(R.drawable.stop_sel);
					doPlayVoice(filepath);
				} else {
					
					String device_id = null;
					String deviceid = getIntent().getStringExtra("jsonText");

					try {
						JSONObject json = new JSONObject(deviceid);
						if (deviceid.contains("parameter")) {
							JSONObject jsons = json.getJSONObject("parameter");
							if (deviceid.contains("device_id")) {
								device_id = jsons.getString("device_id");
								command_no = jsons.getString("command_no");
							}
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					
					if (device_id == null)
						showLongToast("Play failed.");
					else if (device_id.equals(""))
						showLongToast("Play failed.");
					else {

						KidsWatUtils.fetch_voice_by_voice_id(command_no,
								total_request_number, device_id,
								ViewMessageActivity2.this);

						textView_play.setText(getApplicationContext().getResources().getString(R.string.stop));
						btn_play.setImageResource(R.drawable.stop_sel);
					}
				}
			}

			break;
		// ----------

		default:
			break;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mp != null)
			if (mp.isPlaying())
				mp.pause();

		if (KidsWatUtils.player != null)
			if (KidsWatUtils.player.isPlaying())
				KidsWatUtils.player.pause();
		
		unregisterReceiver(receiveBroadCast);
	}

	private ReceiveBroadCast receiveBroadCast;

	@Override
	public void initView() {
		// linearLayout_l.setOnClickListener(this);
		linearLayout_l.setOnClickListener(this);
		linearLayout_safezone_name.setOnClickListener(this);
		linearlayout_play.setOnClickListener(this);
		mMapController = mMapView.getMapController();
		mOverlayManager = mMapController.getOverlayManager();
		mOverlay = new Overlay(mMapController);
		mOverlayManager.getMyLocation().setEnabled(false);
		
		// -------
		receiveBroadCast = new ReceiveBroadCast();
		IntentFilter filter = new IntentFilter();
		filter.addAction("sendplayer.completion"); // 只有持有相同的action的接受者才能接收此广播
		registerReceiver(receiveBroadCast, filter);
	}

	public class ReceiveBroadCast extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals("sendplayer.completion"))
				textView_play.setText(getApplicationContext().getResources().getString(R.string.play));
			btn_play.setImageResource(R.drawable.play_sel);
		}
	}

	String address = "";

	@Override
	public void initData() {
		jsonData = getIntent().getStringExtra("json");
		System.out.println(jsonData);
		// ------------------------
		textView_title.setText("Safe Zone");

		JSONObject msg_item;
		try {
			msg_item = new JSONObject(jsonData);

			int content_type = msg_item.getInt("content_type");
			String content_type_text = msg_item.getString("content_type_text");
			long time = msg_item.getLong("time");
			String content = msg_item.getString("detail");
			double longitude = msg_item.getJSONObject("parameter")
					.getJSONObject("location").getDouble("longitude");
			mlongitude = longitude;
			double latitude = msg_item.getJSONObject("parameter")
					.getJSONObject("location").getDouble("latitude");
			mlatitude = latitude;
			address = msg_item.getJSONObject("parameter")
					.getJSONObject("location").getString("address");
			family_id = msg_item.getJSONObject("parameter").getString(
					"family_id");
			long location_time = msg_item.getJSONObject("parameter").getLong(
					"location_time");
			String location_type = msg_item.getJSONObject("parameter")
					.getString("location_type");
			String des = msg_item.getJSONObject("parameter").getString("des");
			command_no = msg_item.getJSONObject("parameter").isNull(
					"command_no") ? "" : msg_item.getJSONObject("parameter")
					.getString("command_no");
			device_id = msg_item.getJSONObject("parameter").isNull("device_id") ? ""
					: msg_item.getJSONObject("parameter")
							.getString("device_id");

			total_request_number = msg_item.getJSONObject("parameter").isNull(
					"total_request_number") ? 7 : msg_item.getJSONObject(
					"parameter").getInt("total_request_number");

			String strdate = new java.text.SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss").format(new java.util.Date(
					time * 1000));

			if (content_type == 1) {
				frameLayout_line.setVisibility(View.GONE);
				linearlayout_play.setVisibility(View.VISIBLE);
			} else {
				frameLayout_line.setVisibility(View.GONE);
				linearlayout_play.setVisibility(View.GONE);
			}

//			if (content_type == 24) {
//				textView_title.setText("Arriving Notification");
//			} else if (content_type == 25) {
//				textView_title.setText("Unknown Area Warning");
//			} else if (content_type == 26) {
//				textView_title.setText("Recharge Success");
//			} else if (content_type == 27) {
//				textView_title.setText("Top-up Notification");
//			} else {
//				textView_title.setText(content_type_text);
//			}

			 textView_title.setText(content_type_text);

			textView_time.setText(strdate);
			String s_des = des.length() > 0 ? des : content;
			textView_des.setText(s_des);
			//得到数据之后设局数据.
			setMapMarker();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		// -----------------
	}

	// ----------------
	MediaPlayer mp = new MediaPlayer();
	boolean isPlaying = false;

	private MapController mMapController;

	private OverlayManager mOverlayManager;

	private Overlay mOverlay;

	private void doPlayVoice(String src) {
		if (mp == null) {
			mp = new MediaPlayer();
		}
		// 这里就直接用mp.isPlaying()，因为不可能再报IllegalArgumentException异常了
		if (mp.isPlaying()) {
			mp.stop();
			mp.release();
			mp = null;
			mp = new MediaPlayer();
		}
		try {
			mp.setDataSource(src);
			mp.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp.start();
				}
			});
			// Prepare to async playing
			mp.prepareAsync();
			isPlaying = true;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		mp.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mpxx) {
				mp.release();
				mp = null;
				isPlaying = false;
				// -----------
				textView_play.setText("PLAY");
				btn_play.setImageResource(R.drawable.play_sel);
			}
		});
	}

	// private SafeZoneCircle mCircle;
//------change------ 
	/*private void addCircleToMap(LatLng center, double radius, String family_id,
			int sex, String address) {

		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(center, 14.0f));

		// mMap.addCircle(new CircleOptions().center(center).radius(20)
		// .strokeWidth(0).fillColor(Color.argb(255, 0, 0, 0)));

		// create marker
		MarkerOptions marker = new MarkerOptions().position(center).title(
				address);
		// .title("Baby In Here");
		// Changing marker icon
		// marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.set_logo));

		// final String family_id = KidsWatConfig.getDefaultFamilyId();
		// final int sex = KidsWatUtils.getBabySex();

		marker.icon(KidsWatUtils.getBabyImage(ViewMessageActivity2.this,
				family_id, sex));

		mMap.addMarker(marker);
		mMap.addCircle(new CircleOptions().center(center).radius(500)
				.strokeWidth(1).strokeColor(Color.argb(255, 24, 180, 237))
				.fillColor(Color.argb(80, 24, 180, 237)));
		// mMap.addCircle(new CircleOptions().center(center).radius(radius)
		// .strokeWidth(0).fillColor(Color.argb(50, 0x41, 0x69, 0xe1)));

	}*/
	private void addCircleToMap(GeoPoint center, double radius, String family_id,
			int sex, String address) {

		//mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(center, 14.0f));
		mMapController.setPositionAnimationTo(center);
		// mMap.addCircle(new CircleOptions().center(center).radius(20)
		// .strokeWidth(0).fillColor(Color.argb(255, 0, 0, 0)));
		
		int resSex = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
		Drawable drawable = new BitmapDrawable(getApplication().getResources(),KidsWatUtils.getBabyImg_v3(getApplicationContext(), family_id, sex, AppContext.getInstance().currentFamily));
		OverlayItem overlayItem = new OverlayItem(center, drawable);
		// create marker
		BalloonItem balloon = new BalloonItem(this, center);
		balloon.setText(address);
		overlayItem.setBalloonItem(balloon);
		mOverlay.addOverlayItem(overlayItem);
		mOverlayManager.addOverlay(mOverlay);
//		MarkerOptions marker = new MarkerOptions().position(center).title(
//				address);
		// .title("Baby In Here");
		// Changing marker icon
		// marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.set_logo));

		// final String family_id = KidsWatConfig.getDefaultFamilyId();
		// final int sex = KidsWatUtils.getBabySex();

//		marker.icon(KidsWatUtils.getBabyImage(ViewMessageActivity2.this,
//				family_id, sex));

//		mMap.addMarker(marker);
//		mMap.addCircle(new CircleOptions().center(center).radius(500)
//				.strokeWidth(1).strokeColor(Color.argb(255, 24, 180, 237))
//				.fillColor(Color.argb(80, 24, 180, 237)));
		// mMap.addCircle(new CircleOptions().center(center).radius(radius)
		// .strokeWidth(0).fillColor(Color.argb(50, 0x41, 0x69, 0xe1)));

	}

	//------change------ 
	/*@Override
	public void onMapReady(GoogleMap map) {
		mMap = map;
		mMap.getUiSettings().setZoomControlsEnabled(false);
		// Add lots of markers to the map.
		setMapMarker();

		// Set listeners for marker events. See the bottom of this class for
		// their behavior.
		mMap.setOnMarkerClickListener(this);
		mMap.setOnInfoWindowClickListener(this);
		map.setContentDescription("Map with lots of markers.");
	}*/
//------change------ 
	public void setMapMarker() {
		// zoneitem
		SqlDb db = SqlDb.get(this);
		beanForDb___Family bean = db.getFamilyBy_fid(family_id);
		db.closeDb();
		int sex = bean.sex;

		GeoPoint point = new GeoPoint(mlatitude, mlongitude);

		addCircleToMap(point, 500.0, family_id, sex, address);
		// textView_safezone_name.setText(zoneitem.name);
		// textView_address.setText(zoneitem.address);

		// ----------------------
	}
/*	public void setMapMarker() {
		// zoneitem
		LatLng curLng = new LatLng(mlatitude, mlongitude);
		SqlDb db = SqlDb.get(this);
		beanForDb___Family bean = db.getFamilyBy_fid(family_id);
		db.closeDb();
		
		final int sex = bean.sex;
		
		addCircleToMap(curLng, 500.0, family_id, sex, address);
		// textView_safezone_name.setText(zoneitem.name);
		// textView_address.setText(zoneitem.address);
		
		// ----------------------
	}
*/
	//------change------ 
/*@Override
	public boolean onMarkerClick(final Marker marker) {
		return false;
	}*/
//------change------ 
	/*@Override
	public void onInfoWindowClick(Marker marker) {
		//
	}*/
	// ----------------

}
