package com.qiwo.xkidswatcher_russia.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.InjectView;

public class ViewMessageActivity3 extends BaseActivity{

	@InjectView(R.id.linearLayout_l)
	LinearLayout linearLayout_l;

	@InjectView(R.id.textView_message_title)
	TextView textView_message_title;

	@InjectView(R.id.textView_message_content)
	TextView textView_message_content;

	@InjectView(R.id.textView_time)
	TextView textView_time;

	private String jsonData;

	int total_request_number;
	String family_id;

	@Override
	protected int getLayoutId() {
		return R.layout.layout_view_msg3;
	}

	@Override
	protected boolean hasActionBar() {
		return false;
	}

	@Override
	protected void init(Bundle savedInstanceState) {
		super.init(savedInstanceState);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.linearLayout_l:
			finish();
			break;
		default:
			break;
		}
	}



	@Override
	public void initView() {
		linearLayout_l.setOnClickListener(this);
	}


	String address = "";

	@Override
	public void initData() {
		jsonData = getIntent().getStringExtra("json");
		System.out.println(jsonData);
		// ------------------------
		textView_message_title.setText("Safe Zone");

		JSONObject msg_item;
		try {
			msg_item = new JSONObject(jsonData);

			String content_type_text = msg_item.getString("content_type_text");
			long time = msg_item.getLong("time");
			String content = msg_item.getString("detail");
			address = msg_item.getJSONObject("parameter")
					.getJSONObject("location").getString("address");
			family_id = msg_item.getJSONObject("parameter").getString(
					"family_id");
			String des = msg_item.getJSONObject("parameter").getString("des");

			total_request_number = msg_item.getJSONObject("parameter").isNull(
					"total_request_number") ? 7 : msg_item.getJSONObject(
					"parameter").getInt("total_request_number");

			String strdate = new java.text.SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss").format(new java.util.Date(
					time * 1000));
			textView_message_title.setText(content_type_text);

			textView_time.setText(strdate);
			String s_des = des.length() > 0 ? des : content;
			textView_message_content.setText(s_des);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		// -----------------
	}




}
