package com.qiwo.xkidswatcher_russia.ui;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.InjectView;

//import android.content.DialogInterface.OnClickListener;

public class AlertNotificationActivity extends BaseActivity {

	@InjectView(R.id.linearLayout_l)
	LinearLayout linearLayout_l;
	// ------------------
	@InjectView(R.id.lnearLayout_silence_mode)
	LinearLayout lnearLayout_silence_mode;

	@InjectView(R.id.lnearLayout_silence_mode_time)
	LinearLayout lnearLayout_silence_mode_time;

	@InjectView(R.id.lnearLayout_always_on_mode)
	LinearLayout lnearLayout_always_on_mode;

	@InjectView(R.id.item_silence_mode)
	ImageView item_silence_mode;

	@InjectView(R.id.item_always_on_mode)
	ImageView item_always_on_mode;

	@InjectView(R.id.textView_timeSetting)
	TextView textView_timeSetting;

	@Override
	protected int getLayoutId() {
		return R.layout.activity_alert_notification;
	}

	@Override
	protected boolean hasActionBar() {
		return false;
	}

	@Override
	protected void init(Bundle savedInstanceState) {
		super.init(savedInstanceState);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.linearLayout_l:
			finish();
			break;
		case R.id.back_imageview:
			finish();
			break;
		case R.id.lnearLayout_silence_mode:
			ApiHttpClient.cancelAll();

			// notice_status, notice_alarm_status
			update_user_silent_time(item_silence_mode.isSelected() ? 0 : 1,
					item_always_on_mode.isSelected() ? 1 : 0);
			break;
		case R.id.lnearLayout_silence_mode_time:
			// 保存到服务器
			break;
		case R.id.lnearLayout_always_on_mode:
			ApiHttpClient.cancelAll();
			// notice_status, notice_alarm_status
			update_user_silent_time(item_silence_mode.isSelected() ? 1 : 0,
					item_always_on_mode.isSelected() ? 0 : 1);
			break;
		default:
			break;
		}
	}

	@Override
	public void initView() {
		// linearLayout_l.setOnClickListener(this);
		linearLayout_l.setOnClickListener(this);
		lnearLayout_silence_mode.setOnClickListener(this);
		lnearLayout_silence_mode_time.setOnClickListener(this);
		lnearLayout_always_on_mode.setOnClickListener(this);
		// -------
	}

	@Override
	public void initData() {
		String timeSetting = String.format("%02d:00-%02d:00",
				KidsWatConfig.getSilenceTimeS(),
				KidsWatConfig.getSilenceTimeE());
		textView_timeSetting.setText(timeSetting);

		get_user_silent_time();
	}

	private void update_user_silent_time(final int notice_status,
			final int notice_alarm_status) {
		update_user_silent_time(22, 7, notice_status, notice_alarm_status);
	}

	private void update_user_silent_time(final int start_time,
			final int end_time, final int notice_status,
			final int notice_alarm_status) {
		// -----------------

		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		// String family_id = AppContext.getDefaultFamilyId();
		// String device_id = AppContext.getInstance().getProperty(
		// "user.device_id");

		final String request_url = KidsWatApiUrl
				.getUrlFor___update_user_silent_time(uid, access_token,
						start_time, end_time, notice_status,
						notice_alarm_status);
		// -------------------------

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				System.out.println("update_user_silent_time.response=" + t);
				try {
					JSONObject response = new JSONObject(t);

					int error = response.getInt("error");
					if (error == 0) {

						item_silence_mode.setSelected(notice_status == 1 ? true
								: false);
						item_always_on_mode
								.setSelected(notice_alarm_status == 1 ? true
										: false);

						KidsWatConfig.setSilenceMode(notice_status == 1 ? true
								: false);
						KidsWatConfig
								.setAlwaysOnMode(notice_alarm_status == 1 ? true
										: false);

						KidsWatConfig.setSilenceTimeS(start_time);
						KidsWatConfig.setSilenceTimeE(end_time);

						String timeSetting = String.format("%02d:00-%02d:00",
								start_time, end_time);
						textView_timeSetting.setText(timeSetting);
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
				System.out.println("error=" + msg);
				Toast.makeText(AlertNotificationActivity.this, msg,
						Toast.LENGTH_LONG).show();
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});

	}

	private void get_user_silent_time() {
		// -----------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		
		final String request_url = KidsWatApiUrl
				.getUrlFor___get_user_silent_time(uid, access_token);
		// -------------------------

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				System.out.println("get_user_silent_time.response=" + t);
				try {
					JSONObject response = new JSONObject(t);

					// {
					// "error": 0, //-2 的时候表示 用户还未设置
					// "info": {
					// "message": "获取用户免打扰时间成功!",
					// "parameter": {
					// "start_time": 13, //整型
					// "end_time": 9 //整型
					// "notice_status": 0 //整型 0 -- 关闭免打扰推送[响铃] 1-- 开启免打扰推送[静音]
					// "notice_alarm_status" : 0 //整型 [开启免打扰推送时生效] 0 -- 关闭强制响铃 1
					// -- 开启强制响铃
					// }
					// }

					int error = response.getInt("error");
					if (error == 0) {
						int notice_status = response.getJSONObject("info")
								.getJSONObject("parameter")
								.getInt("notice_status");
						int notice_alarm_status = response
								.getJSONObject("info")
								.getJSONObject("parameter")
								.getInt("notice_alarm_status");
						item_silence_mode.setSelected(notice_status == 1 ? true
								: false);
						item_always_on_mode
								.setSelected(notice_alarm_status == 1 ? true
										: false);

						KidsWatConfig.setSilenceMode(notice_status == 1 ? true
								: false);
						KidsWatConfig
								.setAlwaysOnMode(notice_alarm_status == 1 ? true
										: false);

					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
				System.out.println("error=" + msg);
				Toast.makeText(AlertNotificationActivity.this, msg,
						Toast.LENGTH_LONG).show();
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});

	}

	// ----------------

}
