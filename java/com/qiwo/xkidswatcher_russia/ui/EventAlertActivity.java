package com.qiwo.xkidswatcher_russia.ui;

import java.io.IOException;

import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class EventAlertActivity extends Activity implements
		View.OnClickListener {

	// @InjectView(R.id.linearLayout_l)
	// LinearLayout linearLayout_l;
	//
	// @InjectView(R.id.linearLayout_r)
	// LinearLayout linearLayout_r;

	@InjectView(R.id.btn_ok)
	Button btn_ok;

	@InjectView(R.id.textView_des)
	TextView textView_des;

	@InjectView(R.id.pop_layout)
	LinearLayout layout;

	// ----------------XXXXXXXXXXXXXXX--------------------

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_enent_alert);
		ButterKnife.inject(this);
		initView();
		initData();
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.linearLayout_l:
			finish();
			break;
		case R.id.pop_layout:
			break;
		case R.id.btn_ok:
			if (player != null) {
				player.stop();
			}
			finish();
			break;
		// ----------
		default:
			break;
		}
	}

	public void initView() {
		// linearLayout_l.setOnClickListener(this);
		// linearLayout_r.setOnClickListener(this);
		layout.setOnClickListener(this);
		btn_ok.setOnClickListener(this);
		// ---------
	}

	public void initData() {
		String message = getIntent().getStringExtra("message");
//		String message = getApplicationContext().getResources().getString(R.string.content_sos_text);
		if(getIntent().getIntExtra("bledisconnect", 0) == 1){
			message = getIntent().getStringExtra("message");
		}
		textView_des.setText(message);
		ringAlert();
	}

	// ----------------XXXXXXXXXXXXXXX--------------------

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return true;
	}

	// alarm

	final MediaPlayer player = new MediaPlayer();

	private void ringAlert() {
		try {
			String filename = KidsWatConfig.getTempFilePath() + "alarm.wav";
			player.setLooping(true);
			player.setDataSource(filename);
			player.prepare();
			player.start();

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// -------------------

}
