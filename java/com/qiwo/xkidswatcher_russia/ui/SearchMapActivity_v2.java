package com.qiwo.xkidswatcher_russia.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.util.TLog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import butterknife.ButterKnife;
import butterknife.InjectView;

//public class SearchMapActivity extends BaseActivity {
public class SearchMapActivity_v2 extends Activity implements View.OnClickListener {

	@InjectView(R.id.linearLayout_l)
	LinearLayout linearLayout_l;

	@InjectView(R.id.editText_add)
	EditText editText_add;

	@InjectView(R.id.listView)
	ListView listView;

	@InjectView(R.id.button_ok)
	ImageView button_ok;
	
	@InjectView(R.id.textView_cancel)
	TextView textView_cancel;

	xAdapter adapter;

	private Geocoder geocoder;

	// -----------
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_map_search_list);
		ButterKnife.inject(this);
		initView();
		initData();
	}

	// @Override
	// protected int getLayoutId() {
	// return R.layout.layout_map_search_list;
	// }
	//
	// @Override
	// protected boolean hasActionBar() {
	// return false;
	// }
	//
	// @Override
	// protected void init(Bundle savedInstanceState) {
	// super.init(savedInstanceState);
	// // -----------
	// }

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.linearLayout_l:
		case R.id.textView_cancel:
			finish();
			break;
		case R.id.back_imageview:
			finish();
			break;
		case R.id.button_ok:
			mTask = new searchMapTask();
			mTask.execute(editText_add.getText().toString());
			break;
		default:
			break;
		}
	}

	private void initView() {
		linearLayout_l.setOnClickListener(this);
		textView_cancel.setOnClickListener(this);
		button_ok.setOnClickListener(this);
		// ---------
		// editText_add.addTextChangedListener(watcher)
		editText_add.addTextChangedListener(textChangeListener);
	}

	private void initData() {
		geocoder = new Geocoder(this);

		listView.setOnItemClickListener(new LvItemClickListener());
		adapter = new xAdapter(SearchMapActivity_v2.this);
		listView.setAdapter(adapter);
	}

//	List<Address> addresses = null;
	List<AddressInfo> addresses = null;
	private searchMapTask mTask;
	TextWatcher textChangeListener = new TextWatcher() {
		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			if (mTask != null && mTask.getStatus() == AsyncTask.Status.RUNNING) {
				mTask.cancel(true); // 如果Task还在运行，则先取消它
			}
			mTask = new searchMapTask();
			mTask.execute(editText_add.getText().toString());
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
		}
	};

	class LvItemClickListener implements AdapterView.OnItemClickListener {
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			AddressInfo address = addresses.get(position);

			Intent resultIntent = new Intent();
			Bundle bundle = new Bundle();
			bundle.putDouble("longitude", Double.parseDouble(address.lontitude));
			bundle.putDouble("latitude", Double.parseDouble(address.latitude));
			resultIntent.putExtras(bundle);
			SearchMapActivity_v2.this.setResult(RESULT_OK, resultIntent);// RESULT_OK

			finish();
		}
	}

	private class searchMapTask extends AsyncTask<String, Integer, String> {
		// onPreExecute方法用于在执行后台任务前做一些UI操作
		@Override
		protected void onPreExecute() {
			TLog.log("onPreExecute() called");
		}

		// doInBackground方法内部执行后台任务,不可在此方法内修改UI
		@Override
		protected String doInBackground(String... params) {
			if (isCancelled())
				return null;
			try {
//				addresses = geocoder.getFromLocationName(params[0], 20);
				addresses = new ArrayList<AddressInfo>();
				String address = editText_add.getText().toString().trim();
				
				String url = "https://geocode-maps.yandex.ru/1.x/?geocode=" + address + "&format=json";
				ApiHttpClient.get(url, new HttpCallBack() {
					
					@Override
					public void onSuccess(String t) {
						try {
							JSONArray array = new JSONObject(t).getJSONObject("response").getJSONObject("GeoObjectCollection").getJSONArray("featureMember");
							for (int i = 0; i < array.length(); i++) {
								JSONObject json = (JSONObject) array.get(i);
								String address = json.getJSONObject("GeoObject").getJSONObject("metaDataProperty").getJSONObject("GeocoderMetaData").getString("text");
								String position = json.getJSONObject("GeoObject").getJSONObject("Point").getString("pos");
								AddressInfo addressinfo = new AddressInfo();
								addressinfo.address = address;
								addressinfo.lontitude = position.split(" ")[0];
								addressinfo.latitude = position.split(" ")[1];
								
								TLog.log("addressinfo.address="+addressinfo.address);
								TLog.log("addressinfo.lontitude="+addressinfo.lontitude);
								TLog.log("addressinfo.latitude="+addressinfo.latitude);
								addresses.add(addressinfo);
								
								adapter.notifyDataSetChanged();
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					};
					
					@Override
					public void onFailure(int errorNo, String strMsg) {
						
					};
					
					@Override
					public void onFinish() {
						
					};
				});
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		// onProgressUpdate方法用于更新进度信息
		@Override
		protected void onProgressUpdate(Integer... progresses) {
			TLog.log("onProgressUpdate(Progress... progresses) called");
		}

		// onPostExecute方法用于在执行完后台任务后更新UI,显示结果
		@Override
		protected void onPostExecute(String result) {
			if (isCancelled())
				return;
			TLog.log("onPostExecute(Result result) called");
			adapter.notifyDataSetChanged();
		}

		// onCancelled方法用于在取消执行中的任务时更改UI
		@Override
		protected void onCancelled() {
			TLog.log("onCancelled() called");
			adapter.notifyDataSetChanged();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// ------------
		super.onActivityResult(requestCode, resultCode, data);
	}

	class xAdapter extends BaseAdapter {

		private Context context;
		private LayoutInflater mInflater;

		private ViewHolder holder;

		public xAdapter(Context c) {
			super();
			this.context = c;
			mInflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		}

		private class ViewHolder {
			TextView l_title;
			TextView l_des;

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return addresses == null ? 0 : addresses.size();
		}

		@Override
		public Object getItem(int index) {

			return addresses.get(index);
		}

		@Override
		public long getItemId(int index) {
			return index;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = mInflater.inflate(
						R.layout.layout_map_search_list_item, null);
				holder = new ViewHolder();
				holder.l_title = (TextView) convertView
						.findViewById(R.id.textView_title);
				holder.l_des = (TextView) convertView
						.findViewById(R.id.textView_des);

				convertView.setTag(holder);

			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			AddressInfo address = addresses.get(position);
			if (address != null) {
				// int maxLine = address.getMaxAddressLineIndex();

				String addressText = "";

				holder.l_title.setText(address.address);
				holder.l_des.setText(addressText);
			}
			return convertView;
		}
	}
	
	class AddressInfo{
		
		String address;
		
		String latitude;
		String lontitude;
	}
	// ----------------

}
