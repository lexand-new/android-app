package com.qiwo.xkidswatcher_russia.util;

import com.qiwo.xkidswatcher_russia.AppManager;
import com.qiwo.xkidswatcher_russia.dialog.CommonDialog;
import com.qiwo.xkidswatcher_russia.interf.ICallbackResult;
import com.qiwo.xkidswatcher_russia.service.DownloadService;
import com.qiwo.xkidswatcher_russia.service.DownloadService.DownloadBinder;

//import net.oschina.app.interf.ICallbackResult;
//import net.oschina.app.service.DownloadService;
//import net.oschina.app.service.DownloadService.DownloadBinder;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

/**
 * 界面帮助类
 * 
 */
public class UIHelper {

	/** 全局web样式 */
	// 链接样式文件，代码块高亮的处理
	public final static String linkCss = "<script type=\"text/javascript\" src=\"file:///android_asset/shCore.js\"></script>"
			+ "<script type=\"text/javascript\" src=\"file:///android_asset/brush.js\"></script>"
			+ "<script type=\"text/javascript\" src=\"file:///android_asset/client.js\"></script>"
			+ "<link rel=\"stylesheet\" type=\"text/css\" href=\"file:///android_asset/shThemeDefault.css\">"
			+ "<link rel=\"stylesheet\" type=\"text/css\" href=\"file:///android_asset/shCore.css\">"
			+ "<script type=\"text/javascript\">SyntaxHighlighter.all();</script>"
			+ "<script type=\"text/javascript\">function showImagePreview(var url){window.location.url= url;}</script>";
	public final static String WEB_STYLE = linkCss
			+ "<style>* {font-size:16px;line-height:20px;} p {color:#333;} a {color:#3E62A6;} img {max-width:310px;} "
			+ "img.alignleft {float:left;max-width:120px;margin:0 10px 5px 0;border:1px solid #ccc;background:#fff;padding:2px;} "
			+ "pre {font-size:9pt;line-height:12pt;font-family:Courier New,Arial;border:1px solid #ddd;border-left:5px solid #6CE26C;background:#f6f6f6;padding:5px;overflow: auto;} "
			+ "a.tag {font-size:15px;text-decoration:none;background-color:#cfc;color:#060;border-bottom:1px solid #B1D3EB;border-right:1px solid #B1D3EB;color:#3E6D8E;margin:2px 2px 2px 0;padding:2px 4px;white-space:nowrap;position:relative}</style>";

	public static final String WEB_LOAD_IMAGES = "<script type=\"text/javascript\"> var allImgUrls = getAllImgSrc(document.body.innerHTML);</script>";

	private static final String SHOWIMAGE = "ima-api:action=showImage&data=";

	/**
	 * 显示登录界面
	 * 
	 * @param context
	 */
	public static void showLoginActivity(Context context) {
		// Intent intent = new Intent(context, LoginActivity.class);
		// context.startActivity(intent);
	}

	public static void sendAppCrashReport(final Context context,
			final String crashReport) {
		CommonDialog dialog = new CommonDialog(context);

		dialog.setTitle("error");
		dialog.setMessage("error message");
		dialog.setPositiveButton("report",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						// 发送异常报告
						TDevice.sendEmail(context, "OSCAndroid客户端耍脾气 - 症状诊断报告",
								crashReport, "apposchina@163.com");
						// 退出
						AppManager.getAppManager().AppExit(context);
					}
				});
		dialog.setNegativeButton("cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						// 退出
						AppManager.getAppManager().AppExit(context);
					}
				});
		dialog.show();
	}

	public static void openDownLoadService(Context context, String downurl,
			String tilte) {
		final ICallbackResult callback = new ICallbackResult() {

			@Override
			public void OnBackResult(Object s) {
			}
		};
		ServiceConnection conn = new ServiceConnection() {

			@Override
			public void onServiceDisconnected(ComponentName name) {
			}

			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				DownloadBinder binder = (DownloadBinder) service;
				binder.addCallback(callback);
				binder.start();

			}
		};
		Intent intent = new Intent(context, DownloadService.class);
		intent.putExtra(DownloadService.BUNDLE_KEY_DOWNLOAD_URL, downurl);
		intent.putExtra(DownloadService.BUNDLE_KEY_TITLE, tilte);
		context.startService(intent);
		context.bindService(intent, conn, Context.BIND_AUTO_CREATE);
	}

	public static void sendAppCrashReport(final Context context) {
		CommonDialog dialog = new CommonDialog(context);
		dialog.setTitle("app_error");
		dialog.setMessage("error_message");
		dialog.setNegativeButton("ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				System.exit(-1);
			}
		});
		dialog.show();
	}

}
