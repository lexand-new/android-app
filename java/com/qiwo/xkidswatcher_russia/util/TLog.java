package com.qiwo.xkidswatcher_russia.util;

import android.util.Log;

import com.google.code.microlog4android.Logger;
import com.google.code.microlog4android.LoggerFactory;

public class TLog {
	public static final String LOG_TAG = "tlog";
	public static boolean DEBUG = true;
	private static final Logger logger = LoggerFactory.getLogger();
	public TLog() {
	}

	public static final void analytics(String log) {
		if (DEBUG)
			Log.d(LOG_TAG, log);
	}

	public static final void error(String log) {
		if (DEBUG)
			Log.e(LOG_TAG, "" + log);
	}

	public static final void log(String log) {
		if (DEBUG && log != null) {
			logger.debug(log);
//			Log.i(LOG_TAG, log);
		}
	}

	public static final void log(String tag, String log) {
		if (DEBUG){
			logger.debug(log);
//			Log.i(tag, log);
		}
	}

	public static final void logv(String log) {
		if (DEBUG)
			Log.v(LOG_TAG, log);
	}

	public static final void warn(String log) {
		if (DEBUG)
			Log.w(LOG_TAG, log);
	}
}
