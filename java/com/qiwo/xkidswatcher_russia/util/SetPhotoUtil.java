package com.qiwo.xkidswatcher_russia.util;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;


public class SetPhotoUtil {

	private final static int PHOTO_FOR_CROP = 3019;
	public static String imagePath = null;

	public SetPhotoUtil() {
		super();
	}


	public static void doCrop(Activity context, Uri uri, Uri imageUri) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		intent.putExtra("crop", "true");
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("outputX", 120);
		intent.putExtra("outputY", 120);
		intent.putExtra("scale", true);
//		intent.putExtra("return-data", true);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
//		intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
//		intent.putExtra("noFaceDetection", false);
		context.startActivityForResult(intent, PHOTO_FOR_CROP);
	}

	public static void doCrop(Activity context, Uri uri) {

		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		intent.putExtra("crop", "true");
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("outputX", 120);
		intent.putExtra("outputY", 120);
		intent.putExtra("scale", true);
//		intent.putExtra("return-data", true);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
//		intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
//		intent.putExtra("noFaceDetection", false);
		context.startActivityForResult(intent, PHOTO_FOR_CROP);
	}

}
