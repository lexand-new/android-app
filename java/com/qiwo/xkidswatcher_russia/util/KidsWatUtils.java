package com.qiwo.xkidswatcher_russia.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.bean.Baby;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.db.SqlDb;
import com.qiwo.xkidswatcher_russia.event.BaseEvent;
import com.qiwo.xkidswatcher_russia.widget.CircleImageView;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.location.Address;
import android.location.Geocoder;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.SoundPool;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import de.greenrobot.event.EventBus;

/**
 * @author Administrator
 *
 */
/**
 * @author Administrator
 * 
 */
public class KidsWatUtils {

	public static final String SHAREDPREFERENCES_NAME = "kidswatcher_pref";

	/**
	 * 默认的字符集编码 UTF-8 一个汉字占三个字节
	 */
	private static String CHAR_ENCODE = "UTF-8";// "UTF-8";ASCII
	private static final double EARTH_RADIUS = 6378137;// 赤道半径(单位m)

	/**
	 * 设置全局的字符编码
	 * 
	 * @param charEncode
	 */
	public static void configCharEncode(String charEncode) {
		CHAR_ENCODE = charEncode;
	}

	/**
	 * @param str
	 *            源字符串转换成字节数组的字符串
	 * @return
	 */
	public static byte[] StringToByte(String str) {
		return StringToByte(str, CHAR_ENCODE);
	}

	/**
	 * 
	 * @param srcObj
	 *            源字节数组转换成String的字节数组
	 * @return
	 */
	public static String ByteToString(byte[] srcObj) {
		return ByteToString(srcObj, CHAR_ENCODE);
	}

	/**
	 * UTF-8 一个汉字占三个字节
	 * 
	 * @param str
	 *            源字符串 转换成字节数组的字符串
	 * @return
	 */
	public static byte[] StringToByte(String str, String charEncode) {
		byte[] destObj = null;
		try {
			if (null == str || str.trim().equals("")) {
				destObj = new byte[0];
				return destObj;
			} else {
				destObj = str.getBytes(charEncode);
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		StringBuilder stringBuilder = new StringBuilder();
		for (byte byteChar : destObj)
			stringBuilder.append(String.format("%02X ", byteChar));
		System.out.println("HEX:" + stringBuilder.toString());
		System.out.println("-----------");
		return destObj;
	}

	/**
	 * @param srcObj
	 *            源字节数组转换成String的字节数组
	 * @return
	 */
	public static String ByteToString(byte[] srcObj, String charEncode) {
		String destObj = null;
		try {
			destObj = new String(srcObj, charEncode);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return destObj.replaceAll("\0", " ");
	}

	public static int getSeekBarProgress(int value, int stepSize) {
		// int halfStep = stepSize / 2;

		// 计算有多少步
		int steps = (value + stepSize / 2) / stepSize;
		// TLog.log("steps=" + steps);

		int xValue = stepSize * steps;
		// TLog.log("value=" + xValue);
		return xValue;
	}

	/**
	 * 转化为弧度(rad)
	 * */
	private static double rad(double d) {
		return d * Math.PI / 180.0;
	}

	/**
	 * 基于googleMap中的算法得到两经纬度之间的距离,计算精度与谷歌地图的距离精度差不多，相差范围在0.2米以下
	 * 
	 * @param lat1
	 *            第一点的纬度
	 * @param lng1
	 *            第一点的经度
	 * @param lat2
	 *            第二点的纬度
	 * @param lng2
	 *            第二点的经度
	 * @return 返回的距离，单位km
	 * */
	public static double GetDistance(double lat1, double lng1, double lat2,
			double lng2) {
		double radLat1 = rad(lat1);
		double radLat2 = rad(lat2);
		double a = radLat1 - radLat2;
		double b = rad(lng1) - rad(lng2);
		double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2)
				+ Math.cos(radLat1) * Math.cos(radLat2)
				* Math.pow(Math.sin(b / 2), 2)));
		s = s * EARTH_RADIUS;
		// s = Math.round(s * 10000) / 10000;
		return s;
	}

	// -----------------------------------

	public static String getGooglePushToken() {
		if (AppContext.google_gcm_appid != null
				&& AppContext.google_gcm_appid.length() > 12) {
			return AppContext.google_gcm_appid;
		} else {
			String googlePushToken = KidsWatConfig.getGcmAppid();
			if (googlePushToken != null && googlePushToken.length() > 12) {
				return googlePushToken;
			} else {
				return null;
				// 4413705768088069239:1105098904785834807
			}
		}

	}

	public static void copyBigDataToSD(Context c, String assetFile,
			String strOutFileName) throws IOException {
		InputStream myInput;
		OutputStream myOutput = new FileOutputStream(strOutFileName);
		myInput = c.getAssets().open(assetFile);
		byte[] buffer = new byte[1024];
		int length = myInput.read(buffer);
		while (length > 0) {
			myOutput.write(buffer, 0, length);
			length = myInput.read(buffer);
		}

		myOutput.flush();
		myInput.close();
		myOutput.close();
	}

	public static String getTxtFromAssets(Context c, String fileName) {
		try {
			InputStreamReader inputReader = new InputStreamReader(c
					.getResources().getAssets().open(fileName));
			BufferedReader bufReader = new BufferedReader(inputReader);
			String line = "";
			String Result = "";
			while ((line = bufReader.readLine()) != null)
				Result += line;
			return Result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public static void soundRing(Context context) throws IOException {

		final SoundPool soundPool = new SoundPool(10,
				AudioManager.STREAM_SYSTEM, 5);
		AssetFileDescriptor file = context.getResources().getAssets()
				.openFd("alarm.wav");
		final int id = soundPool.load(file, 1);

		AudioManager mgr = (AudioManager) context
				.getSystemService(Context.AUDIO_SERVICE);

		float streamVolumeCurrent = mgr
				.getStreamVolume(AudioManager.STREAM_MUSIC);
		float streamVolumeMax = mgr
				.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		final float volume = streamVolumeCurrent / streamVolumeMax;
		// final int id = soundPool.load(c, R.raw.kacha, 1);

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				soundPool.play(id, volume, volume, 1, 0, 1f);
			}
		}, 20);

	}

	public static boolean isNetworkAvailable(Context context) {
		try {
			ConnectivityManager connectivity = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity == null) {
				return false;
			}
			NetworkInfo[] networkInfos = connectivity.getAllNetworkInfo();
			if (networkInfos == null) {
				return false;
			}
			for (NetworkInfo networkInfo : networkInfos) {
				if (networkInfo.isConnectedOrConnecting()) {
					return true;
				}
			}
			return false;
		} catch (Throwable e) {
			return false;
		}
	}

	/**
	 * @param year
	 *            年
	 * @param month
	 *            月1月为0,12月为11
	 * @param dayOfMonth
	 *            日
	 * @return 返回指定年月日凌晨0时UTC时间戳
	 */
	public static long getUtcTimeAt0Time(int year, int month, int dayOfMonth) {
		Calendar a = Calendar.getInstance();
		// a.set(year, month, dayOfMonth);
		a.set(year, month, dayOfMonth, 0, 0, 0);
		return a.getTimeInMillis() / 1000;
	}

	public static boolean isSameDay(Calendar a, Calendar b) {
		boolean bl = b.get(Calendar.YEAR) == a.get(Calendar.YEAR)
				&& b.get(Calendar.MONTH) == a.get(Calendar.MONTH)
				&& b.get(Calendar.DAY_OF_MONTH) == a.get(Calendar.DAY_OF_MONTH);

		return bl;
	}

	/**
	 * 返回当前的应用是否处于前台显示状态
	 * 
	 * @param $context
	 * @return
	 */
	public static boolean isTopActivity(Context context) {
		ActivityManager activityManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> appProcesses = activityManager
				.getRunningAppProcesses();
		for (RunningAppProcessInfo appProcess : appProcesses) {
			if (appProcess.processName.equals(context.getPackageName())) {
				if (appProcess.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
					// .IMPORTANCE_BACKGROUND
					Log.i("前台", appProcess.processName);
					return true;
				} else {
					Log.i("后台", appProcess.processName);
					return false;
				}
			}
		}
		return false;
	}

	public static void setKidsDefaultImage(CircleImageView img, int sex) {
		int imgSrcId = sex == 1 ? R.drawable.baby_boy : R.drawable.baby_girl;
		img.setImageResource(imgSrcId);
	}

	public static void setKidsDefaultImage(ImageView img, int sex) {
		int imgSrcId = sex == 1 ? R.drawable.baby_boy : R.drawable.baby_girl;
		img.setImageResource(imgSrcId);
	}

	public static void setKidsImage(Context ct, ImageView img,
			String family_id, String baseImg64, int sex) {

		TLog.log("sex=" + sex);
		TLog.log("family_id=" + family_id);
		if (baseImg64 != null && baseImg64.length() > 20) {
			String filepath = String.format("%sbb_%s.jpg",
					KidsWatConfig.getTempFilePath(), family_id);
			File file = new java.io.File(filepath);
			if (!file.exists()) {
				String filepaths = String.format("%sbb_%s.txt",
						KidsWatConfig.getTempFilePath(), family_id);
				// writeFileData(filepaths, baseImg64, ct);
				TLog.log("no exist:" + filepath);
				try {
					Base64Encode.decoderBase64File(baseImg64, filepath);

				} catch (FileNotFoundException e) {
				} catch (IOException e) {
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

					int imgSrcId = sex == 1 ? R.drawable.icon_boy
							: R.drawable.icon_girl;
					img.setImageResource(imgSrcId);

				}
			}
			Bitmap bmp;
			try {

				bmp = MediaStore.Images.Media.getBitmap(
						ct.getContentResolver(),
						Uri.fromFile(new File(filepath)));
				if (bmp != null)
					img.setImageBitmap(bmp);
				TLog.log("show image ok.");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			int imgSrcId = sex == 1 ? R.drawable.icon_boy
					: R.drawable.icon_girl;
			img.setImageResource(imgSrcId);
		}

	}

	public static BitmapDescriptor getBabyImage(Context ct, String family_id,
			int sex) {

		// BitmapDescriptorFactory
		// .fromResource(R.drawable.icon_boy)
		
		TLog.log("sex=" + sex);
		TLog.log("family_id=" + family_id);

		String filepath = String.format("%sbb_%s.jpg",
				KidsWatConfig.getTempFilePath(), family_id);
		File file = new java.io.File(filepath);
		if (file.exists()) {
			Bitmap bmp;
			try {
				bmp = MediaStore.Images.Media.getBitmap(
						ct.getContentResolver(),
						Uri.fromFile(new File(filepath)));
				Bitmap xbmp = toRoundBitmap(bmp);
				if (xbmp != null) {
					return BitmapDescriptorFactory.fromBitmap(xbmp);
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		int imgSrcId = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
		return BitmapDescriptorFactory.fromResource(imgSrcId);
	}
	
	public static Bitmap getBabyImg_v2(Context ct, String family_id,
			int sex) {
		//String img_path = AppContext.getInstance().currentFamily.img_path;
		String filepath = String.format("%sbb_%s.jpg",
				KidsWatConfig.getTempFilePath(), family_id);
		//String filepath = "file://storage//emulated//0//pictures//abcp//"+"bb_img";
		System.out.println("filePath="+filepath);
		
		File file = new File(filepath);
		System.out.println(file.exists()+"");
		if (file.exists()) {
			Bitmap bmp;
			try {
				bmp = MediaStore.Images.Media.getBitmap(
						ct.getContentResolver(),
						Uri.fromFile(file));
				Bitmap xbmp = toRoundBitmap(bmp);
				return xbmp;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		int imgSrcId = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
		return BitmapFactory.decodeResource(ct.getResources(), imgSrcId);
		
	}
	
	public static Bitmap getBabyImg_v3(Context ct, String family_id,
			int sex, beanForDb___Family bb) {
		
		if(bb != null && TextUtils.isEmpty(bb.img_path)){
			int imgSrcId = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
			return BitmapFactory.decodeResource(ct.getResources(), imgSrcId);
		}

		//String img_path = AppContext.getInstance().currentFamily.img_path;
		String filepath = String.format("%sbb_%s.jpg",
				KidsWatConfig.getTempFilePath(), family_id);
		//String filepath = "file://storage//emulated//0//pictures//abcp//"+"bb_img";
		System.out.println("filePath="+filepath);
		
		File file = new File(filepath);
		System.out.println(file.exists()+"");
		if (file.exists()) {
			Bitmap bmp;
			try {
				bmp = MediaStore.Images.Media.getBitmap(
						ct.getContentResolver(),
						Uri.fromFile(file));
				Bitmap xbmp = toRoundBitmap(bmp);
				return xbmp;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		int imgSrcId = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
		return BitmapFactory.decodeResource(ct.getResources(), imgSrcId);
		
	}
	
	public static Bitmap getBabyImg_v3(Context ct, String family_id,
			int sex, String bb) {
		
		if(bb != null && TextUtils.isEmpty(bb)){
			int imgSrcId = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
			return BitmapFactory.decodeResource(ct.getResources(), imgSrcId);
		}
		
		//String img_path = AppContext.getInstance().currentFamily.img_path;
		String filepath = String.format("%sbb_%s.jpg",
				KidsWatConfig.getTempFilePath(), family_id);
		//String filepath = "file://storage//emulated//0//pictures//abcp//"+"bb_img";
		System.out.println("filePath="+filepath);
		
		File file = new File(filepath);
		System.out.println(file.exists()+"");
		if (file.exists()) {
			Bitmap bmp;
			try {
				bmp = MediaStore.Images.Media.getBitmap(
						ct.getContentResolver(),
						Uri.fromFile(file));
				Bitmap xbmp = toRoundBitmap(bmp);
				return xbmp;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		int imgSrcId = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
		return BitmapFactory.decodeResource(ct.getResources(), imgSrcId);
		
	}
	
	public static Bitmap getBabyImg_v3(Context ct, String family_id,
			int sex, Baby bb) {
		
		if(bb != null && TextUtils.isEmpty(bb.getImg_path())){
			int imgSrcId = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
			return BitmapFactory.decodeResource(ct.getResources(), imgSrcId);
		}
		
		//String img_path = AppContext.getInstance().currentFamily.img_path;
		String filepath = String.format("%sbb_%s.jpg",
				KidsWatConfig.getTempFilePath(), family_id);
		//String filepath = "file://storage//emulated//0//pictures//abcp//"+"bb_img";
		System.out.println("filePath="+filepath);
		
		File file = new File(filepath);
		System.out.println(file.exists()+"");
		if (file.exists()) {
			Bitmap bmp;
			try {
				bmp = MediaStore.Images.Media.getBitmap(
						ct.getContentResolver(),
						Uri.fromFile(file));
				Bitmap xbmp = toRoundBitmap(bmp);
				return xbmp;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		int imgSrcId = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
		return BitmapFactory.decodeResource(ct.getResources(), imgSrcId);
		
	}
	
	public static Bitmap getBabyImageForYandex(Context ct, String family_id,
			int sex) {

		// BitmapDescriptorFactory
		// .fromResource(R.drawable.icon_boy)

		TLog.log("sex=" + sex);
		TLog.log("family_id=" + family_id);

		String filepath = String.format("%sbb_%s.jpg",
				KidsWatConfig.getTempFilePath(), family_id);
		File file = new java.io.File(filepath);
		if (file.exists()) {
			Bitmap bmp;
			try {
				bmp = MediaStore.Images.Media.getBitmap(
						ct.getContentResolver(),
						Uri.fromFile(new File(filepath)));
				Bitmap xbmp = toRoundBitmap(bmp);
				if (xbmp != null) {
					return xbmp;
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		int imgSrcId = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
		return BitmapFactory.decodeResource(ct.getResources(), imgSrcId);
	}
	/**
	 * 转换图片成圆形
	 * 
	 * @param bitmap
	 *            传入Bitmap对象
	 * @return
	 */
	public static Bitmap toRoundBitmap(Bitmap bitmap) {
		if (bitmap == null)
			return null;
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();
		float roundPx;
		float left, top, right, bottom, dst_left, dst_top, dst_right, dst_bottom;
		if (width <= height) {
			roundPx = width / 2;
			top = 0;
			bottom = width;
			left = 0;
			right = width;
			height = width;
			dst_left = 0;
			dst_top = 0;
			dst_right = width;
			dst_bottom = width;
		} else {
			roundPx = height / 2;
			float clip = (width - height) / 2;
			left = clip;
			right = width - clip;
			top = 0;
			bottom = height;
			width = height;
			dst_left = 0;
			dst_top = 0;
			dst_right = height;
			dst_bottom = height;
		}

		Bitmap output = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect src = new Rect((int) left, (int) top, (int) right,
				(int) bottom);
		final Rect dst = new Rect((int) dst_left, (int) dst_top,
				(int) dst_right, (int) dst_bottom);
		final RectF rectF = new RectF(dst);

		paint.setAntiAlias(true);

		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, src, dst, paint);
		return output;
	}

	public static void saveDefaultFamilyInfo(String country_code,
			String family_id, String device_id) {

		if (!StringUtils.isEmpty(country_code)) {
			KidsWatConfig.setUserCountryCode(country_code);
		}

		if (!StringUtils.isEmpty(family_id)) {
			KidsWatConfig.setDefaultFamilyId(family_id);
		}

		if (!StringUtils.isEmpty(device_id)) {
			KidsWatConfig.saveDeviceId(device_id);
		}
	}

	public static void saveFamilyIdAndDeviceId(String family_id,
			String device_id) {
		saveDefaultFamilyInfo(null, family_id, device_id);
	}

	public static String getPointAddress(Context c, double latitude,
			double longitude) {
		// ---------------

		// Geocoder geoCoder = new Geocoder(c, Locale.getDefault());
		Geocoder geoCoder = new Geocoder(c, Locale.US);
		try {
			List<Address> addresses = geoCoder.getFromLocation(latitude,
					longitude, 1);
			if (addresses.size() > 0) {
				Address address = addresses.get(0);
				// Log.d(TAG, address.getCountryName()+address.get);
				int maxLine = address.getMaxAddressLineIndex();

				String add = "";
				for (int i = 0; i < maxLine; i++) {
					if (i > 3)
						break;
					add += address.getAddressLine(i);
				}

				return add;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return "";
	}

	public static int getBabySex() {
		if (AppContext.getInstance().currentFamily != null) {
			return AppContext.getInstance().currentFamily.sex;
		} else {
			return 1;
		}
	}

	public static void LogRequestToFile(String message) {
		logTxtByDay("req", message);
	}

	public static void logHttpTxt(String message) {
		logTxtByTicks("http", message);
	}

	public static void logDebugTxt(String message) {
		logTxtByTicks("debug", message);
	}

	private static void logTxtByDay(String filePrefix, String message) {
		String externalSdpath = Environment.getExternalStorageDirectory()
				.getPath();
		if (!externalSdpath.endsWith("/")) {
			externalSdpath += "/";
		}
		String path = externalSdpath + "kidswatcher/log/";
		System.out.println(path);
		File dir = new File(path);

		if (!dir.exists()) {
			dir.mkdirs();
		}

		String fileName = String.format("%s_%s.txt", filePrefix,
				new SimpleDateFormat("yyyy_MM_dd").format(new Date()));

		String filepath = path + fileName;
		writeTxtToFile(filepath, message);
	}

	private static void logTxtByTicks(String filePrefix, String message) {
		String externalSdpath = Environment.getExternalStorageDirectory()
				.getPath();
		if (!externalSdpath.endsWith("/")) {
			externalSdpath += "/";
		}
		String path = externalSdpath + "kidswatcher/log/";
		System.out.println(path);
		File dir = new File(path);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		String fileName = String.format("%s_%s_%s.txt", filePrefix,
				new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()),
				System.currentTimeMillis());

		String filepath = path + fileName;
		writeTxtToFile(filepath, message);
	}

	private static void writeTxtToFile(String filepath, String message) {
		File f = new File(filepath);
		try {
			FileWriter fw = new FileWriter(f, true);
			String datestr = new java.text.SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss").format(new java.util.Date(System
					.currentTimeMillis()));
			fw.write(datestr + ":\n" + message + "\n");
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// ----------------
	public static void get_notice_list_ex() {
		// -----------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		final String family_id = KidsWatConfig.getDefaultFamilyId();

		if (access_token == null)
			return;
		final String req_url = KidsWatApiUrl.getUrlFor___get_notice_list_ex(
				uid, access_token, 1, 20);

		ApiHttpClient.get(req_url, new HttpCallBack() {

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", req_url, t));

				try {
					JSONObject response = new JSONObject(t);

					SqlDb db = SqlDb.get(AppContext.getInstance());

					db.saveMessage(uid, family_id, response);
					int unReadMsgRows = db.getUnReadMessageCount(uid);
					db.closeDb();
					if (unReadMsgRows > 0) {
						BaseEvent event = new BaseEvent(
								BaseEvent.MSGTYPE_3___HAS_UNREAD_MSG,
								"new_unread_msg");
						EventBus.getDefault().post(event);
					} else {
						BaseEvent event = new BaseEvent(
								BaseEvent.MSGTYPE_3___NO_UNREAD_MSG,
								"no_unread_msg");
						EventBus.getDefault().post(event);
					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
				TLog.log(String.format("url:%s\nt:%s", req_url, msg));
			}

		});

		// -------------------------

	}

	public static void fetch_voice_by_voice_id(final String command_no,
			final int total_request_number) {
		// -----------------

		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		// final String family_id = KidsWatConfig.getDefaultFamilyId();
		final String device_id = KidsWatConfig.getUserDeviceId();

		final String req_url = KidsWatApiUrl
				.getUrlFor___fetch_voice_by_voice_id(uid, access_token,
						device_id, command_no, total_request_number);
		// -------------------------

		ApiHttpClient.get(req_url, new HttpCallBack() {
			@Override
			public void onPreStart() {

			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				System.out.println("fetch_voice_by_voice_id.response=" + t);
				try {
					JSONObject response = new JSONObject(t);

					int code = response.getInt("error");
					if (code == 0) {
						String filepath = String.format("%saudio_%s_%s.amr",
								KidsWatConfig.getTempFilePath(), device_id,
								command_no);
						String voice_stream = response.getJSONObject("info")
								.getString("voice_stream");
						Base64Encode.decoderBase64FileForAudio(voice_stream,
								filepath);
						MediaPlayer player = new MediaPlayer();
						player.setDataSource(filepath);
						player.prepare();
						player.start();
					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
				TLog.log(String.format("url:%s\nt:%s", req_url, msg));
			}

			@Override
			public void onFinish() {
				//
			}
		});

	}

	public static MediaPlayer player = null;
	
	public static MediaPlayer fetch_voice_by_voice_id(final String command_no,
			final int total_request_number, final String device_id,
			final Context context) {

		// -----------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		// final String family_id = KidsWatConfig.getDefaultFamilyId();

		final String req_url = KidsWatApiUrl
				.getUrlFor___fetch_voice_by_voice_id(uid, access_token,
						device_id, command_no, total_request_number);
		// -------------------------

		ApiHttpClient.get(req_url, new HttpCallBack() {
			@Override
			public void onPreStart() {

			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				System.out.println("fetch_voice_by_voice_id.response=" + t);
				try {
					JSONObject response = new JSONObject(t);

					int code = response.getInt("error");
					if (code == 0) {
						String filepath = String.format("%saudio_%s_%s.amr",
								KidsWatConfig.getTempFilePath(), device_id,
								command_no);
						String voice_stream = response.getJSONObject("info")
								.getString("voice_stream");
						Base64Encode.decoderBase64FileForAudio(voice_stream,
								filepath);
						player = new MediaPlayer();
						player.setDataSource(filepath);
						player.prepare();
						player.start();
						player.setOnCompletionListener(new OnCompletionListener() {

							@Override
							public void onCompletion(MediaPlayer mp) {
								context.sendBroadcast(new Intent(
										"sendplayer.completion"));
							}
						});
					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
				TLog.log(String.format("url:%s\nt:%s", req_url, msg));
			}

			@Override
			public void onFinish() {
				//
			}
		});

		return player;

	}

	public static void charge_money_of_app(final String device_id,
			String policy_sequence, String currency_country_code,
			String currency_symbol, String price, final String order_id) {

		// --------------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		// final String family_id = KidsWatConfig.getDefaultFamilyId();
		// final String device_id = KidsWatConfig.getUserDeviceId();

		final String req_url = KidsWatApiUrl
				.getUrlFor___charge_money_of_app_v2(uid, access_token,
						device_id, policy_sequence, currency_country_code,
						currency_symbol, price, order_id);
		// -------------------------
		ApiHttpClient.get(req_url, new HttpCallBack() {
			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				System.out.println("charge_money_of_app.response=" + t);
				try {
					JSONObject response = new JSONObject(t);

					int code = response.getInt("error");
					if (code == 0) {
						String valid_date = response.getJSONObject("info")
								.getString("valid_date");
						SqlDb db = SqlDb.get(AppContext.getInstance());
						db.saveFamilyValid_date(device_id, valid_date);
						db.setGoogleBillingUploadedBy_did(device_id, order_id);
						db.closeDb();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
				TLog.log(String.format("url:%s\nt:%s", req_url, msg));
			}

		});
	}

	// -------------------------

	// --------------

	//

	/**
	 * 一、私有文件夹下的文件存取（/data/data/包名/files）
	 * 
	 * @param fileName
	 * @param message
	 */
	public static void writeFileData(String fileName, String message, Context ct) {
		try {
			FileOutputStream fout = ct
					.openFileOutput(fileName, ct.MODE_PRIVATE);
			byte[] bytes = message.getBytes();
			fout.write(bytes);
			fout.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//判断当前设备版本号
	public static boolean is361(){
		beanForDb___Family currentFamily = AppContext.getInstance().currentFamily;
		if(currentFamily == null)
			return false;
		return currentFamily.version.equals("361");
	}
	
	//判断当前设备版本号
	public static boolean is461(){
		beanForDb___Family currentFamily = AppContext.getInstance().currentFamily;
		if(currentFamily == null)
			return false;
		return currentFamily.version.equals("461");
	}
	
	//判断当前设备版本号
	public static boolean is561(){
		beanForDb___Family currentFamily = AppContext.getInstance().currentFamily;
		if(currentFamily == null)
			return false;
		return currentFamily.version.equals("561");
	}
	
	//判断当前设备版本号
	public static boolean isRussianVersion(){
		beanForDb___Family currentFamily = AppContext.getInstance().currentFamily;
		if(currentFamily == null)
			return false;
		return currentFamily.version.equals("4620");
	}
}
