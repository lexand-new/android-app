package com.qiwo.xkidswatcher_russia.dialog;

import java.io.IOException;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.util.SetPhotoUtil;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

@SuppressLint("SdCardPath")
public class ShowDialogActivity extends Activity implements OnClickListener {

	private Button camera, photo, cancle;
	private final int CAMERA_FOR_PHOTO = 3017;
	private final int PHOTO_FOR_PHOTO = 3018;
	private final int PHOTO_FOR_CROP = 3019;

	private Uri imageUri;
	public static Bitmap photoBitmap = null;
	public static int photoMatrix = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AppContext.imageLoader.clearDiscCache();
		AppContext.imageLoader.clearMemoryCache();;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window window = getWindow();
		WindowManager.LayoutParams wl = window.getAttributes();
		wl.flags = WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
		wl.alpha = 0.9f;
		window.setAttributes(wl);

		setContentView(R.layout.dialog_activity_photo);
		imageUri = Uri.parse("file://" + getPath() + "/photo.png");
		initView();
		addListener();
	}
	
	public void initView() {
		camera = (Button) this.findViewById(R.id.user_dialog_creama);
		photo = (Button) this.findViewById(R.id.user_dialog_photo);
		cancle = (Button) this.findViewById(R.id.user_dialog_cancle);
	}

	
	private String getPath() {
		String path = null;
		boolean isExit = Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED);
		if (isExit) {
			path = Environment.getExternalStorageDirectory().getPath();
		} else {
			path = Environment.getDataDirectory().getPath();
		}
		return path;
	}

	private void addListener() {
		camera.setOnClickListener(this);
		photo.setOnClickListener(this);
		cancle.setOnClickListener(this);
	}

	@Override
	public void finish() {
		super.finish();
		this.overridePendingTransition(0, R.anim.activity_out);
	}

	// private File file;

	@Override
	public void onClick(View v) {
		Intent intent = null;
		switch (v.getId()) {
		case R.id.user_dialog_creama:

			intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			System.out.println("imgurl = "+imageUri);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
			startActivityForResult(intent, CAMERA_FOR_PHOTO);

			break;
		case R.id.user_dialog_photo:
			intent = new Intent(Intent.ACTION_PICK, null);
			intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
					"image/*");
			startActivityForResult(intent, PHOTO_FOR_PHOTO);
			break;
		case R.id.user_dialog_cancle:
			this.finish();
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode != Activity.RESULT_OK) {
			if (resultCode == 0) {
				if (null != data && !"".equals(data)) {
					if (data.getExtras() != null) {
						photoBitmap = (Bitmap) data.getExtras().get("data");
					}
				} else {
					return;
				}
			} else {
				return;
			}
		}
		switch (requestCode) {
		case PHOTO_FOR_CROP:
			System.out.println(getPath()+"--path---");
			SetPhotoUtil.imagePath = getPath() + "/photo.png";
			setResult(resultCode);
//			System.gc();
			finish();
			break;
		case CAMERA_FOR_PHOTO:
			photoMatrix = getBitmapDegree(getPath() + "/photo.png");
			
			System.out.println("imgurl:"+imageUri+",path:"+getPath() + "/photo.png");
			SetPhotoUtil.doCrop(this, imageUri, imageUri);
			break;
		case PHOTO_FOR_PHOTO:
			System.out.println("data.getdata:"+data.getData()+",imgurl:"+imageUri);
			SetPhotoUtil.doCrop(this, data.getData(), imageUri);
			break;
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	private int getBitmapDegree(String path) {
		int degree = 0;
		try {

			ExifInterface exifInterface = new ExifInterface(path);

			int orientation = exifInterface.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);

			switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_90:
				degree = 90;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				degree = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_270:
				degree = 270;
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return degree;
	}

}
