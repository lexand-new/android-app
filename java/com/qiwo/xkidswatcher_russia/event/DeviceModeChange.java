package com.qiwo.xkidswatcher_russia.event;

public class DeviceModeChange {
	private String family_id;
	private int mode;

	public DeviceModeChange(String _family_id, int _mode) {
		family_id = _family_id;
		mode = _mode;
	}

	public String getFamilyId() {
		return family_id;
	}

	public int getMode() {
		return mode;
	}

}
