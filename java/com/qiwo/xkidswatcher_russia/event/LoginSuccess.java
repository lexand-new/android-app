package com.qiwo.xkidswatcher_russia.event;

public class LoginSuccess {
	private String uid;
	private String access_token;

	public LoginSuccess(String _uid,String _access_token) {
		// TODO Auto-generated constructor stub
		uid = _uid;
		access_token = _access_token;
	}

	public String getUid() {
		return uid;
	}

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

}
