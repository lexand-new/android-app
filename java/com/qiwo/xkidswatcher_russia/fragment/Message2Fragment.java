package com.qiwo.xkidswatcher_russia.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.SwipeListView;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.adapter.Message2Adapter;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Message;
import com.qiwo.xkidswatcher_russia.db.SqlDb;
import com.qiwo.xkidswatcher_russia.event.BaseEvent;
import com.qiwo.xkidswatcher_russia.ui.SafeZoneActivity;
import com.qiwo.xkidswatcher_russia.ui.ViewMessageActivity3;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import com.qiwo.xkidswatcher_russia.util.TLog;
import com.qiwo.xkidswatcher_russia.widget.TipInfoLayout;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * 
 */
public class Message2Fragment extends Fragment implements OnScrollListener,
		View.OnClickListener {

	public static final int MESSAGE_STATE_NONE = -2;
	public static final int MESSAGE_STATE_ERROR = -1;
	public static final int MESSAGE_STATE_EMPTY = 0;
	public static final int MESSAGE_STATE_MORE = 1;
	public static final int MESSAGE_STATE_FULL = 2;
	public static final int MESSAGE_STATE_LOADING = 3;

	@InjectView(R.id.listview)
	SwipeListView listview;

	@InjectView(R.id.swipeRefreshLayout)
	SwipeRefreshLayout swipeRefreshLayout;

	@InjectView(R.id.linearLayout_cc)
	LinearLayout linearLayout_cc;

	@InjectView(R.id.linearLayout_message_empty)
	LinearLayout linearLayout_message_empty;

	@InjectView(R.id.tip_info)
	TipInfoLayout mTipInfo;

	MediaPlayer player = new MediaPlayer();

	final String TAG = SafeZoneActivity.class.getSimpleName();
	final int PAGE_SIZE = 20;

	private Message2Adapter adapter;
	List<beanForDb___Message> msgList;
	int mCurrentPage = 1;
	// boolean isRefreshLoading = false;
	private int dataState = MESSAGE_STATE_NONE;

	private View mFooterView;
	private View mFooterProgressBar;
	private TextView mFooterTextView;
	private View rootView;
	// ------------
	private ProgressDialog progressDialog = null;

	protected void showWaitDialog(String __msg) {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.setMessage(__msg);
		} else {
			progressDialog = ProgressDialog.show(getActivity(), null, __msg);
		}
	}

	protected void dismissDialog() {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
	}

	// -----------

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);

		TLog.log("onCreateView");

		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_mesage_x2, container,
					false);
			ButterKnife.inject(this, rootView);
			initView(rootView);
			initData();
		}

		ViewGroup parent = (ViewGroup) rootView.getParent();
		if (parent != null) {
			parent.removeView(rootView);
		}
		return rootView;

	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.linearLayout_l:
			break;
		case R.id.button_add:
			break;
		default:
			break;
		}
	}

	public void initView(View view) {
		if (!EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().register(this);
		}
		mFooterView = LayoutInflater.from(getActivity()).inflate(
				R.layout.listview_footer, null, false);
		mFooterProgressBar = mFooterView
				.findViewById(R.id.listview_foot_progress);
		mFooterTextView = (TextView) mFooterView
				.findViewById(R.id.listview_foot_more);

		listview.setOnScrollListener(this);
		listview.addFooterView(mFooterView);
		// mTipInfo.setOnClick(new View.OnClickListener() {
		// @Override
		// public void onClick(View v) {
		// mTipInfo.setVisibility(View.VISIBLE);
		// mTipInfo.setEmptyData("...loading...");
		// System.out.println("---mCurrentPage=" + mCurrentPage);
		// get_notice_list_ex(mCurrentPage);
		// }
		// });
		// 设置卷内的颜色
		swipeRefreshLayout.setColorSchemeResources(
				android.R.color.holo_blue_bright,
				android.R.color.holo_green_light,
				android.R.color.holo_orange_light,
				android.R.color.holo_red_light);
		// 设置下拉刷新监听
		swipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
			public void onRefresh() {
				if (dataState != MESSAGE_STATE_LOADING) {
					mCurrentPage = 1;
					get_notice_list_ex(mCurrentPage);
				}
			}
		});

		// ---------------
	}

	public void initData() {
		msgList = new ArrayList<beanForDb___Message>();
		loadDataFromDb();
		bindData();
	}

	@Override
	public void onDetach() {
		super.onDetach();
		if (EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().unregister(this);
		}
	}

	public void onEventMainThread(BaseEvent event) {
		// BaseEvent(310, "clear_notice")
		TLog.log("------onEventMainThread----" + event.getMsg());
		if (event.getType() == BaseEvent.MSGTYPE_3___CLEAR_NOTICE_DONE) {
			if (msgList != null)
				msgList.clear();
			adapter.notifyDataSetChanged();
			mTipInfo.setVisibility(View.GONE);

			linearLayout_cc.setVisibility(View.GONE);
			linearLayout_message_empty.setVisibility(View.VISIBLE);

			// textView_edit.setText("Edit");
			// content_fl.setBackgroundResource(R.drawable.bg_no_message);
		} else if (event.getType() == BaseEvent.MSGTYPE_3___HAS_UNREAD_MSG) {
			// showClearAndRedPoint(true);
			loadDataFromDb();
		}else if(event.getType() == BaseEvent.MSGTYPE_6___WHITE_HOME_VOICE_PUSH){
			get_notice_list_ex(1);
		}
	}

	private void loadDataFromDb() {
		final String uid = KidsWatConfig.getUserUid();
		String family_id = KidsWatConfig.getDefaultFamilyId();
		if (mCurrentPage == 1) {
			msgList.clear();
		}
		
		SqlDb db = SqlDb.get(getActivity());
		List<beanForDb___Message> mlist = db.getMessage(uid, family_id,
				PAGE_SIZE, mCurrentPage);
		int unReadMsgRows = db.getUnReadMessageCount(uid);
		db.closeDb();

		// if (unReadMsgRows > 0) {
		// BaseEvent event = new BaseEvent(
		// BaseEvent.MSGTYPE_3___HAS_UNREAD_MSG, "new_unread_msg");
		// EventBus.getDefault().post(event);
		// } else {
		// BaseEvent event = new BaseEvent(
		// BaseEvent.MSGTYPE_3___NO_UNREAD_MSG, "no_unread_msg");
		// EventBus.getDefault().post(event);
		// }

		if (mlist.size() > 0) {
			linearLayout_cc.setVisibility(View.VISIBLE);
			linearLayout_message_empty.setVisibility(View.GONE);

			msgList.addAll(mlist);
			if (mlist.size() < PAGE_SIZE) {
				dataState = MESSAGE_STATE_FULL;
				setFooterFullState();
			}
		} else {
			linearLayout_cc.setVisibility(View.GONE);
			linearLayout_message_empty.setVisibility(View.VISIBLE);
		}
		// bindData();
	}

	/**
	 * 设置底部已加载全部的状态
	 */
	void setFooterFullState() {
		if (mFooterView != null) {
			mFooterProgressBar.setVisibility(View.GONE);
			mFooterTextView.setText(getActivity().getResources().getString(R.string.loading_completed));
		}
	}

	/**
	 * 设置底部加载中的状态
	 */
	void setFooterLoadingState() {
		if (mFooterView != null) {
			mFooterProgressBar.setVisibility(View.VISIBLE);
			mFooterTextView.setText(getActivity().getResources().getString(R.string.loading));
		}
	}

	protected void showLongToast(String message) {
		Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
	}

	// -------------
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
		}
	}

	boolean isLastRow = false;

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// 判断是否滚到最后一行
		if (firstVisibleItem + visibleItemCount == totalItemCount
				&& totalItemCount > 0) {
			TLog.log("-------lastlow-------");
			isLastRow = true;
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		TLog.log("-------onScrollStateChanged-------");
		if (adapter == null || adapter.getCount() == 0) {
			return;
		}
		if (mCurrentPage == 1 && adapter.getCount() < PAGE_SIZE) {
			return;
		}
		// 数据已经全部加载，或数据为空时，或正在加载，不处理滚动事件
		if (dataState == MESSAGE_STATE_FULL || dataState == MESSAGE_STATE_EMPTY
				|| dataState == MESSAGE_STATE_LOADING) {
			return;
		}

		// 当滚到最后一行且停止滚动时，执行加载
		if (isLastRow
				&& scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
			// 加载元素
			TLog.log("-------load more-------");
			++mCurrentPage;
			// -------------
			setFooterLoadingState();
			String uid = KidsWatConfig.getUserUid();
			SqlDb db = SqlDb.get(getActivity());
			int msgRows = db.getMessageCount(uid);
			db.closeDb();

			int loadRows = (mCurrentPage - 1) * PAGE_SIZE;
			System.out.println("mCurrentPage=" + mCurrentPage);
			System.out.println("loadRows=" + loadRows);
			System.out.println("msgRows=" + msgRows);

			if (loadRows < msgRows) {
				loadDataFromDb();
				adapter.notifyDataSetChanged();
			} else {
				get_notice_list_ex(mCurrentPage);
			}

			isLastRow = false;

		}

	}

	private void bindData() {

		if (msgList.size() > 0) {

			adapter = new Message2Adapter(getActivity(),
					listview.getRightViewWidth(),
					new Message2Adapter.IOnItemRightClickListener() {
						@Override
						public void onRightClick(View v, int position,
								Object object) {
							// TODO Auto-generated method stub
							beanForDb___Message bean = (beanForDb___Message) object;
							String message_id = bean.message_id;
							delete_notice_by_message_id(message_id);
						}
					});

			adapter.setPlayListener(new Message2Adapter.IOnItemRightClickListener() {
				@Override
				public void onRightClick(View v, int position, Object object) {
					// TODO Auto-generated method stub

					beanForDb___Message bean = (beanForDb___Message) object;
					String msg = String.format("%s:%s[%s]%s", position,
							bean.content_type_text, bean.content, bean.time);
					TLog.log(msg);
					// showLongToast(msg);
					// --------------
					String jsonText = bean.jsonText;
					JSONObject json;
					String command_no = "";
					String device_id = "";
					int total_request_number = 7;
					try {
						json = new JSONObject(jsonText);

						total_request_number = json.getJSONObject("parameter")
								.isNull("total_request_number") ? 7 : json
								.getJSONObject("parameter").getInt(
										"total_request_number");

						command_no = json.getJSONObject("parameter").isNull(
								"command_no") ? "" : json.getJSONObject(
								"parameter").getString("command_no");

						device_id = json.getJSONObject("parameter").isNull(
								"device_id") ? "" : json.getJSONObject(
								"parameter").getString("device_id");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					String filepath = String.format("%saudio_%s_%s.amr",
							KidsWatConfig.getTempFilePath(), device_id,
							command_no);
					java.io.File f = new java.io.File(filepath);
					if (f.exists()) {
						try {
							// ------------
							doPlayVoice(filepath);

							adapter.setAudioPlayingIndex(position);
							adapter.notifyDataSetChanged();
						} catch (Exception e) {
							e.printStackTrace();
						}

					} else {
						String device_ids = null;

						String deviceid = msgList.get(position).jsonText;

						try {
							JSONObject jsons = new JSONObject(deviceid);
							if (deviceid.contains("parameter")) {
								JSONObject jsonss = jsons
										.getJSONObject("parameter");
								if (deviceid.contains("device_id")) {
									device_ids = jsonss.getString("device_id");
									command_no = jsonss.getString("command_no");
								}
							}

						} catch (JSONException e) {
							e.printStackTrace();
						}

						if (device_ids == null)
							showLongToast("Play failed.");
						else if (device_id.equals(""))
							showLongToast("Play failed.");
						else {
							KidsWatUtils.fetch_voice_by_voice_id(command_no,
									total_request_number, device_ids,
									getActivity());
							adapter.setAudioPlayingIndex(position);
							adapter.notifyDataSetChanged();
						}
					}
					// --------------
				}
			});

			adapter.setList(msgList);

			listview.setAdapter(adapter);
			listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					if (position >= msgList.size() || position < 0)
						return;
					// showLongToast("item onclick " + position);
					String output = String.format(
							"parent:%s,view:%s,position:%s,id:%s",
							parent.getId(), view, position, id);
					System.out.println(output);

					Intent dIntent = new Intent(getActivity(),
							ViewMessageActivity3.class).putExtra("jsonText",
							msgList.get(position).jsonText);
//					Intent dIntent = new Intent(getActivity(),
//							ViewMessageActivity.class).putExtra("jsonText",
//									msgList.get(position).jsonText);
					dIntent.putExtra("json", msgList.get(position).jsonText);
					getActivity().startActivity(dIntent);

					if (msgList.get(position).isRead == 0) {
						String uid = KidsWatConfig.getUserUid();
						String message_id = msgList.get(position).message_id;
						SqlDb db = SqlDb.get(getActivity());
						db.setMessageReadedBy_mid(message_id);
						int unReadMsgRows = db.getUnReadMessageCount(uid);
						db.closeDb();

						if (unReadMsgRows > 0) {
							//
						} else {
							BaseEvent event = new BaseEvent(
									BaseEvent.MSGTYPE_3___NO_UNREAD_MSG,
									"no_unread_msg");
							EventBus.getDefault().post(event);
						}

						int i = 0;
						for (beanForDb___Message m : msgList) {
							if (m.message_id.equalsIgnoreCase(message_id)) {
								m.isRead = 1;
								break;
							}
							i++;
						}
						adapter.notifyDataSetChanged();

						// showDataView();
					}

				}
			});

		} else {
			// content_empty.setVisibility(View.VISIBLE);
		}

	}

	public static boolean playState = false;

	// ---------------
	private void doPlayVoice(String src) {

		if (player == null) {
			player = new MediaPlayer();
		}
		
		// 这里就直接用mp.isPlaying()，因为不可能再报IllegalArgumentException异常了
		if (player.isPlaying()) {
			player.stop();
			player.release();
			player = null;
			player = new MediaPlayer();
			
			playState = false;
			return;
		}
		
		try {
//			player.setDataSource(src);
			FileInputStream fis = new FileInputStream(new File(src));
			player.setDataSource(fis.getFD());
			player.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer player) {
					player.start();
				}
			});
			// Prepare to async playing
			player.prepareAsync();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		player.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mpxx) {

				playState = false;

				player.release();
				player = null;
				adapter.setAudioPlayingIndex(-1);
				adapter.notifyDataSetChanged();

			}
		});
	}

	// -------------------------
	private void get_notice_list_ex(final int pageindex) {
		// -----------------
		boolean isNetworkAvailable = KidsWatUtils
				.isNetworkAvailable(getActivity());
		if (!isNetworkAvailable) {
			showLongToast(getActivity().getResources().getString(R.string.tip_network_unavailable));
			return;
		}
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		final String family_id = KidsWatConfig.getDefaultFamilyId();

		final String request_url = KidsWatApiUrl
				.getUrlFor___get_notice_list_ex(uid, access_token, pageindex,
						PAGE_SIZE);
		// -------------------------

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				dataState = MESSAGE_STATE_LOADING;
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));

				try {
					JSONObject response = new JSONObject(t);

					try {
						SqlDb db = SqlDb.get(getActivity());
						int row = response.getJSONArray("data").length();
						if (row > 0) {
							// dataState
							if (row < PAGE_SIZE) {
								dataState = MESSAGE_STATE_FULL;
								setFooterFullState();
							} else {
								dataState = MESSAGE_STATE_NONE;
							}
							db.saveMessage(uid, family_id, response);
						} else {
							dataState = MESSAGE_STATE_NONE;
							db.clearMessage(uid);
						}
						db.closeDb();
					} catch (NullPointerException ex) {
						ex.printStackTrace();
						dataState = MESSAGE_STATE_NONE;
					}

					loadDataFromDb();
					adapter.notifyDataSetChanged();
					// adapter.notifyDataSetChanged();
					// 停止刷新动画
					// swipeRefreshLayout.setRefreshing(false);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					dataState = MESSAGE_STATE_NONE;
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String
						.format("%s(error=%s)",
								errorNo == -1 ? "Connect to the server failed"
										: strMsg, errorNo);
				TLog.log(msg);
				showLongToast(msg);
			}

			@Override
			public void onFinish() {
				TLog.log("-----finish-------");
				// dataState = MESSAGE_STATE_NONE;
				swipeRefreshLayout.setRefreshing(false);
			}
		});
		// -------------------------

	}

	private void delete_notice_by_message_id(final String message_id_array) {
		// -----------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		// final String family_id = KidsWatConfig.getDefaultFamilyId();

		final String request_url = KidsWatApiUrl
				.getUrlFor___delete_notice_by_message_id(uid, access_token,
						message_id_array);
		// -------------------------

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);

				SqlDb db = SqlDb.get(getActivity());
				db.deleteMessageBy_mid(message_id_array);
				int unReadMsgRows = db.getUnReadMessageCount(KidsWatConfig.getUserUid());
				db.closeDb();
				
				// List<beanForDb___Message> listMessage
				int i = 0;
				for (beanForDb___Message m : msgList) {
					if (m.message_id.equalsIgnoreCase(message_id_array)) {
						listview.resetItems();
						msgList.remove(i);
						break;
					}
					i++;
				}
				
				if(unReadMsgRows == 0){
					BaseEvent event = new BaseEvent(
							BaseEvent.MSGTYPE_3___NO_UNREAD_MSG,
							"no_unread_msg");
					EventBus.getDefault().post(event);
				}
				adapter.notifyDataSetChanged();
				
				if(msgList.size() == 0){
					linearLayout_cc.setVisibility(View.GONE);
					linearLayout_message_empty.setVisibility(View.VISIBLE);
				}
			}
			
			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
				System.out.println("error=" + msg);
				Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onFinish() {
				dismissDialog();
			}
		});
	}

	/**
	 * --------------------------
	 */
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		TLog.log("on resume");
		loadDataFromDb();
		bindData();
	}
	
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		TLog.log("on stop");
		
		if (player != null){
			if (player.isPlaying()){
				player.stop();
				player.release();
				player = null;
			}
		}
	}
}
