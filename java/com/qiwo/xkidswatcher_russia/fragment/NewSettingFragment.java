package com.qiwo.xkidswatcher_russia.fragment;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseFragment;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_watch_data;
import com.qiwo.xkidswatcher_russia.event.BaseEvent;
import com.qiwo.xkidswatcher_russia.ui.BleAccompanyActivity;
import com.qiwo.xkidswatcher_russia.ui.FamilyMemberActivity;
import com.qiwo.xkidswatcher_russia.ui.KidsProfileActivity;
import com.qiwo.xkidswatcher_russia.ui.LocatingModeActivity;
import com.qiwo.xkidswatcher_russia.ui.MainActivity;
import com.qiwo.xkidswatcher_russia.ui.SafeZoneActivity;
import com.qiwo.xkidswatcher_russia.ui.ScanGuideAActivity;
import com.qiwo.xkidswatcher_russia.ui.SystemSettingActivity_v2;
import com.qiwo.xkidswatcher_russia.ui.TopUpActivity;
import com.qiwo.xkidswatcher_russia.ui.TopUpPageActivity;
import com.qiwo.xkidswatcher_russia.ui.Watch_InformationActivity;
import com.qiwo.xkidswatcher_russia.ui.Watch_SettingsActivity;
import com.qiwo.xkidswatcher_russia.util.Contanst;
import com.qiwo.xkidswatcher_russia.util.ImageUtils;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import com.qiwo.xkidswatcher_russia.util.StringUtils;
import com.qiwo.xkidswatcher_russia.util.TLog;
import com.qiwo.xkidswatcher_russia.widget.APopupWindow.ItemPosition;
import com.qiwo.xkidswatcher_russia.widget.APopupWindow.onClickItemListener;
import com.qiwo.xkidswatcher_russia.widget.BottomPopupWindow;
import com.qiwo.xkidswatcher_russia.widget.CircleImageView;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

public class NewSettingFragment extends BaseFragment {

	/**
	 * 新版设置页面
	 */
	private View rootView;

	@InjectView(R.id.Watch_Information_linear_P)
	LinearLayout Watch_Information_linear_P;

	@InjectView(R.id.baby_info_linear)
	LinearLayout baby_info_linear;

	@InjectView(R.id.setting_linears)
	LinearLayout setting_linears;

	@InjectView(R.id.imageView_baby)
	CircleImageView imageView_baby;

	@InjectView(R.id.baby_name_text)
	TextView baby_name_text;

	@InjectView(R.id.baby_calorie_text)
	TextView baby_calorie_text;

	@InjectView(R.id.Contact_linear)
	LinearLayout Contact_linear;

	@InjectView(R.id.SafeZones_linear)
	LinearLayout SafeZones_linear;

	@InjectView(R.id.Accompany_linear)
	LinearLayout Accompany_linear;

	@InjectView(R.id.Watch_Setting_linear)
	LinearLayout Watch_Setting_linear;

	@InjectView(R.id.Power_Off_linear)
	LinearLayout Power_Off_linear;

	@InjectView(R.id.Top_up_linear)
	LinearLayout Top_up_linear;

	@InjectView(R.id.Watch_Information_linear)
	LinearLayout Watch_Information_linear;

	@InjectView(R.id.add_device_linear)
	LinearLayout add_device_linear;

	@InjectView(R.id.setting_linear)
	LinearLayout setting_linear;
	
	@InjectView(R.id.webview_totup)
	LinearLayout webview_totup;



	// 远程关机权限
	// private boolean PowerOFFState = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.new_fragment_setting,
					container, false);
			ButterKnife.inject(this, rootView);
			initData();
			initView(rootView);
		}

		ViewGroup parent = (ViewGroup) rootView.getParent();
		if (parent != null) {
			parent.removeView(rootView);
		}
		return rootView;
	}

	@Override
	public void initView(View view) {
		super.initView(view);

		if (!EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().register(this);
		}

		/*
		 * click
		 */
		baby_info_linear.setOnClickListener(this);
		Contact_linear.setOnClickListener(this);
		SafeZones_linear.setOnClickListener(this);
		Accompany_linear.setOnClickListener(this);
		Watch_Setting_linear.setOnClickListener(this);
		Power_Off_linear.setOnClickListener(this);
		Watch_Information_linear.setOnClickListener(this);
		setting_linear.setOnClickListener(this);
		Top_up_linear.setOnClickListener(this);
		add_device_linear.setOnClickListener(this);
		webview_totup.setOnClickListener(this);
	}

	@Override
	public void initData() {
		super.initData();

		showKidsProfile();
	}

	@Override
	public void onClick(View v) {
		
		Intent intent = new Intent();
		
		switch (v.getId()) {

		/*
		 * baby info setting
		 */
		case R.id.baby_info_linear:
			
			if(AppContext.getInstance().currentFamily == null){
				Toast.makeText(getActivity(), " Загрузка  данных не удалось ", Toast.LENGTH_SHORT).show();
				return;
			}
			intent.setClass(getActivity(), KidsProfileActivity.class);
			intent.putExtra(Contanst.KEY_ACTION, Contanst.ACTION_EDIT);
			startActivity(intent);
			break;
		/*
		 * Contact --联系人
		 */
		case R.id.Contact_linear:
			
			if(AppContext.getInstance().currentFamily == null){
				Toast.makeText(getActivity(), " Загрузка  данных не удалось ", Toast.LENGTH_SHORT).show();
				return;
			}
			intent.setClass(getActivity(), FamilyMemberActivity.class);
			// intent.putExtra(Contanst.KEY_ACTION, Contanst.ACTION_EDIT);
			startActivity(intent);

			break;
		/*
		 * SafeZones --安全范围
		 */
		case R.id.SafeZones_linear:
			
			if(AppContext.getInstance().currentFamily == null){
				Toast.makeText(getActivity(), " Загрузка  данных не удалось ", Toast.LENGTH_SHORT).show();
				return;
			}
			intent.setClass(getActivity(), SafeZoneActivity.class);
			// intent.putExtra(Contanst.KEY_ACTION, Contanst.ACTION_EDIT);
			startActivity(intent);

			break;
		/*
		 * Accompany --蓝牙随行
		 */
		case R.id.Accompany_linear:
			
			if(AppContext.getInstance().currentFamily == null){
				Toast.makeText(getActivity(), " Загрузка  данных не удалось ", Toast.LENGTH_SHORT).show();
				return;
			}
			beanForDb___Family currentFamily = AppContext.getInstance().currentFamily;
			if(currentFamily.version != null && (currentFamily.version.equalsIgnoreCase("601") || currentFamily.version.equalsIgnoreCase("5613"))){
				Toast.makeText(getActivity(), "устройство не поддерживает  bluetooth ", Toast.LENGTH_SHORT).show();
				return;
			}
			intent.setClass(getActivity(), BleAccompanyActivity.class);
			// intent.putExtra(Contanst.KEY_ACTION, Contanst.ACTION_EDIT);
			startActivity(intent);

			break;
		/*
		 * Watch_Setting --手表设置
		 */
		case R.id.Watch_Setting_linear:
			
			if(AppContext.getInstance().currentFamily == null){
				Toast.makeText(getActivity(), " Загрузка  данных не удалось ", Toast.LENGTH_SHORT).show();
				return;
			}
			if (KidsWatUtils.isRussianVersion() || (AppContext.getInstance().currentFamily.version.equals("461") 
					|| AppContext.getInstance().currentFamily.version.startsWith("561"))
					&& AppContext.getInstance().loginUser_kid.isAdmin == 1)
				intent.setClass(getActivity(), Watch_SettingsActivity.class);
			else
				intent.setClass(getActivity(), LocatingModeActivity.class);
			startActivity(intent);
			break;
		/*
		 * Power_Off --远程关机
		 */
		case R.id.Power_Off_linear:
			
			if(AppContext.getInstance().currentFamily == null){
				Toast.makeText(getActivity(), " Загрузка  данных не удалось ", Toast.LENGTH_SHORT).show();
				return;
			}
			DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// showLongToast("power off");
					final String device_id = KidsWatConfig.getUserDeviceId();
					watch_shutdown(device_id);
				}
			};

			String title,
			message,
			positiveText,
			negativeText;
			title = getActivity().getResources().getString(R.string.remote_power_off);
			positiveText = getActivity().getApplicationContext().getResources().getString(R.string.ok);
			negativeText = getActivity().getApplicationContext().getResources().getString(R.string.cancle);
			if (AppContext.getInstance().loginUser_kid.isAdmin == 1) {
				message = getActivity().getResources().getString(R.string.tip_remote_power_off);
				showConfirmDialog(title, message, positiveText, negativeText,
						positiveListener);
			} else {
				message = "Доступно только администратору";
				showConfirmInformation(title, message);
			}

			break;
		/*
		 * Top_up 充值
		 */
		case R.id.Top_up_linear:
			
			if(AppContext.getInstance().currentFamily == null){
				Toast.makeText(getActivity(), " Загрузка  данных не удалось ", Toast.LENGTH_SHORT).show();
				return;
			}
			intent.setClass(getActivity(), TopUpActivity.class);
			// intent.putExtra("qrcode", bb.qrcode);
			// intent.putExtra("sim", bb.sim);
			startActivity(intent);

			break;
		/*
		 * qr_code --二维码
		 */
		case R.id.Watch_Information_linear:
			
			if(AppContext.getInstance().currentFamily == null){
				Toast.makeText(getActivity(), " Загрузка  данных не удалось ", Toast.LENGTH_SHORT).show();
				return;
			}
			intent.setClass(getActivity(), Watch_InformationActivity.class);
			startActivity(intent);

			break;

		/*
			 * 
			 */
		case R.id.add_device_linear:
			
			if(AppContext.getInstance().currentFamily == null){
				Toast.makeText(getActivity(), " Загрузка  данных не удалось ", Toast.LENGTH_SHORT).show();
				return;
			}
			intent.setClass(getActivity(), ScanGuideAActivity.class);
			startActivity(intent);
			break;
		/*
		 * setting --设置
		 */
		case R.id.setting_linear:
			intent.setClass(getActivity(), SystemSettingActivity_v2.class);
			startActivity(intent);
			break;
		case R.id.webview_totup:
			
			if(AppContext.getInstance().currentFamily == null){
				Toast.makeText(getActivity(), " Загрузка  данных не удалось ", Toast.LENGTH_SHORT).show();
				return;
			}
			intent.setClass(getActivity(), TopUpPageActivity.class);
			startActivity(intent);
			break;
		default:
			break;
		}
	}

	/**
	 * function
	 */

	@SuppressLint("SimpleDateFormat")
	private void watch_shutdown(String device_id) {

		// -----------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		// final String device_id = AppContext.getInstance().getProperty(
		// "user.device_id");
		// final String family_id = AppContext.getDefaultFamilyId();
		// -------------------------
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
		String wtime = df.format(new Date());
		final String request_url = KidsWatApiUrl.getUrlFor___watch_shutdown(
				uid, access_token, device_id, wtime);

		// -------------------------

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));

				try {
					JSONObject response = new JSONObject(t);

					int code = response.getInt("error");
					if (code == 0) {
						// response={"data":{"mode":2},"error":0,"info":{"message":"get success!"}}
						showLongToast("Отключение произведено удачно");
					} else if (code == 2015001) {
						// dismissDialog("Device is in sleep mode.", 800);
						dismissDialog(null, 10);
						showLongToast("Вы не можете");
					} else {
						showLongToast("Выключение не удалось");
					}

				} catch (JSONException e) {
					e.printStackTrace();
					showLongToast("Выключение не удалось");
				}
			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
//				String msg = String
//						.format("%s(error=%s)",
//								errorNo == -1 ? "Connect to the server failed"
//										: strMsg, errorNo);
//				TLog.log(msg);
				showLongToast(getActivity().getResources().getString(R.string.check_conn));
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();

		BaseEvent event = new BaseEvent(
				BaseEvent.MSGTYPE_3___CHANGE_KIDS_PHOTO, "change_image");
		EventBus.getDefault().post(event);
	}

	private void showKidsProfile() {
		beanForDb___Family fBaby = AppContext.getInstance().currentFamily;
		// db.getFamilyBy_fid(family_id);
		if (fBaby != null) {
//			imageView_baby.setImageBitmap(KidsWatUtils.getBabyImg_v3(getActivity(), fBaby.family_id, fBaby.sex, fBaby));
			if(!TextUtils.isEmpty(fBaby.img_path))
				AppContext.getInstance().imageLoader.displayImage("http://"+fBaby.img_path, imageView_baby, AppContext.commenOptions);
			else{
				int imgSrcId = fBaby.sex == 1 ? R.drawable.icon_boy
						: R.drawable.icon_girl;
				imageView_baby.setImageResource(imgSrcId);
			}
			baby_name_text.setText(fBaby.nickname);
		}
		
		Calendar a = Calendar.getInstance();
		int year = a.get(Calendar.YEAR);
		int month = a.get(Calendar.MONTH);
		int dayOfMonth = a.get(Calendar.DAY_OF_MONTH);
		long utcNow = KidsWatUtils.getUtcTimeAt0Time(year, month, dayOfMonth);
		if (fBaby != null){
			baby_calorie_text.setText(((MainActivity)getActivity()).getStepNumber() + "");
		}
//			get_watch_data(utcNow, fBaby.family_id);

		// top up
		/**
		 * 逻辑判断 --Logical judgment
		 */
		if (AppContext.getInstance().loginUser_kid != null) {
			// 充值判断是否显示 -- Recharge judgment whether to display
			String version = AppContext.getInstance().currentFamily.version;
			if (version.equals("361") || version.equals("362")) {
				Top_up_linear.setVisibility(View.GONE);
			} else {
				Top_up_linear.setVisibility(View.GONE);
			}
		}
	}

	public void onEventMainThread(BaseEvent event) {
		int type = event.getType();
		String msg = event.getMsg();

		if (type == BaseEvent.MSGTYPE_3___WHITE_SETTING_CHANGE_KIDS_PHOTO) {
			Log.e("", "baihao:MSGTYPE_3___WHITE_SETTING_CHANGE_KIDS_PHOTO");
			beanForDb___Family currentFamily = AppContext.getInstance().currentFamily;
			String path = currentFamily.img_path;
			path.toString();
			File bbimg = new File(msg);
			Bitmap bmp = null;
			try {
				bmp = MediaStore.Images.Media.getBitmap(getActivity()
						.getContentResolver(), Uri.fromFile(bbimg));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			if (bmp != null)
				imageView_baby.setImageBitmap(bmp);
		} else if (type == BaseEvent.MSGTYPE_5___USERLIST_INDEX_CHANGE) {
			Log.e("", "baihao:MSGTYPE_5___USERLIST_INDEX_CHANGE");
			showKidsProfile();
		} else if (type == BaseEvent.MSGTYPE_6___WHITE_TO_FRONT) {
			// showKidsProfile(3);
		} else if (type == BaseEvent.MSGTYPE_3___LOAD_FAMILY_FIRST_ITEM) {
			showKidsProfile();
		} else if(type == BaseEvent.MSGTYPE_3___CHANGE_KIDS_NICKNAME){
			showKidsProfile();
		} else if(type == BaseEvent.MSGTYPE_3___CHANGE_USER){
			get_watch_step();
		}
	}

	private void get_watch_data(long date, String family_id) {

		// -----------------
		String uid = KidsWatConfig.getUserUid();
		String access_token = KidsWatConfig.getUserToken();

		final String request_url = KidsWatApiUrl.getUrlFor___get_watch_data(
				uid, access_token, family_id, date);
		
		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));

				beanFor___get_watch_data response = AppContext.getInstance()
						.getGson().fromJson(t, beanFor___get_watch_data.class);

				if (response.error == 0) {
					if (response.info.ddata.rows.size() > 0)
						baby_calorie_text.setText(response.info.ddata.rows
								.get(0).step_number + "");
					else
						baby_calorie_text.setText("0");
				} else {
					showLongToast("В аккаунте авторизовано другое устройство, попробуйте еще раз");
				}
			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String
						.format("%s(error=%s)",
								errorNo == -1 ? "Connect to the server failed"
										: strMsg, errorNo);
				TLog.log(String.format("url:%s\nt:%s", request_url, msg));
				showLongToast(msg);
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});
		// -------------------------
	}

	/*-
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */

	private BottomPopupWindow mSelectPhotoPopupWindow = null;

	protected void showSelecPhotoPopupWindow(View relyView) {
		if (mSelectPhotoPopupWindow == null) {
			mSelectPhotoPopupWindow = new BottomPopupWindow(getActivity(), 0,
					relyView);

			// 添加“拍照”
			mSelectPhotoPopupWindow.addItem("Take Photo",
					new onClickItemListener() {

						@Override
						public void clickItem(View v) {
							startActionCamera();
						}
					}, ItemPosition.TOP);

			// 添加“从相册选择”
			mSelectPhotoPopupWindow.addItem("Choose From Albums",
					new onClickItemListener() {

						@Override
						public void clickItem(View v) {
							startImagePick();
						}
					}, ItemPosition.BOTTOM);

			// 添加“取消”
			mSelectPhotoPopupWindow.addCancelItem("Cancel", null, 10);
		}
		mSelectPhotoPopupWindow.show();
	}

	/**
	 * 相机拍照
	 */
	private void startActionCamera() {
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, getCameraTempFile());
		startActivityForResult(intent,
				ImageUtils.REQUEST_CODE_GETIMAGE_BYCAMERA);
	}

	/**
	 * 选择图片裁剪
	 */
	private void startImagePick() {
		Intent intent;
		if (Build.VERSION.SDK_INT < 19) {
			intent = new Intent();
			intent.setAction(Intent.ACTION_GET_CONTENT);
			intent.setType("image/*");
			startActivityForResult(intent,
					ImageUtils.REQUEST_CODE_GETIMAGE_BYCROP);
		} else {
			intent = new Intent(Intent.ACTION_PICK,
					Images.Media.EXTERNAL_CONTENT_URI);
			intent.setType("image/*");
			startActivityForResult(intent,
					ImageUtils.REQUEST_CODE_GETIMAGE_BYCROP);
		}
	}

	private final static int CROP = 120;
	private final static String FILE_SAVEPATH = KidsWatConfig.getTempFilePath();

	private Uri origUri;
	private Uri cropUri;
	private File protraitFile;
	private Bitmap protraitBitmap;
	private String protraitPath;

	// 拍照保存的绝对路径
	private Uri getCameraTempFile() {
		String storageState = Environment.getExternalStorageState();
		if (storageState.equals(Environment.MEDIA_MOUNTED)) {
			File savedir = new File(FILE_SAVEPATH);
			if (!savedir.exists()) {
				savedir.mkdirs();
			}
		} else {
			Toast.makeText(getActivity(), "无法保存上传的头像，请检查SD卡是否挂载",
					Toast.LENGTH_SHORT).show();
			return null;
		}

		String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss")
				.format(new Date());
		// 照片命名
		String cropFileName = "ks_camera_" + timeStamp + ".jpg";
		// String cropFileName = "bb_camera_"
		// + AppContext.getInstance().currentFamily.family_id + ".jpg";

		// 裁剪头像的绝对路径
		protraitPath = FILE_SAVEPATH + cropFileName;
		protraitFile = new File(protraitPath);
		cropUri = Uri.fromFile(protraitFile);
		this.origUri = this.cropUri;
		return this.cropUri;
	}

	public void get_watch_step() {
		String uid = KidsWatConfig.getUserUid();
		String access_token = KidsWatConfig.getUserToken();
		String family_id = KidsWatConfig.getDefaultFamilyId();

		if (StringUtils.isEmpty(uid))
			return;

		Calendar a = Calendar.getInstance();
		int year = a.get(Calendar.YEAR);
		int month = a.get(Calendar.MONTH);
		int dayOfMonth = a.get(Calendar.DAY_OF_MONTH);

		long utcToday = KidsWatUtils.getUtcTimeAt0Time(year, month, dayOfMonth);
		final String request_url = KidsWatApiUrl.getUrlFor___get_watch_data(
				uid, access_token, family_id, utcToday);

		ApiHttpClient.get(request_url, new HttpCallBack() {
			
			@Override
			public void onPreStart() {
				System.out.println("-----get watch step------");
				showWaitDialog();
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));

				beanFor___get_watch_data response = AppContext.getInstance()
						.getGson().fromJson(t, beanFor___get_watch_data.class);
				if (response.error == 0) {
					
					if (response.info.ddata.total_rows > 0) {
						int step = response.info.ddata.rows.get(0).step_number;
						setStep(step);
						showKidsProfile();
					} else {
						setStep(0);
					}
					
				} else {
					System.out.println("error="+ response.error);
					setStep(0);
					showLongToast("load track history failed.");
				}
			}

			private void setStep(int i) {
				// TODO Auto-generated method stub
				((MainActivity)getActivity()).setStepNumber(i);
				showKidsProfile();
			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
				System.out.println("error=" + msg);
			}

			@Override
			public void onFinish() {
				hideWaitDialog();
			}

		});

	}

}
