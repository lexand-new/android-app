package com.qiwo.xkidswatcher_russia.fragment;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseFragment;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_watch_data;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_watch_data.CRows;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_watch_data_latest;
import com.qiwo.xkidswatcher_russia.db.SqlDb;
import com.qiwo.xkidswatcher_russia.event.BaseEvent;
//import com.qiwo.xkidswatcher_russia.fragment.beanFor___get_watch_data_latest.CInfo.CData;
import com.qiwo.xkidswatcher_russia.ui.MainActivity;
import com.qiwo.xkidswatcher_russia.ui.SleepModeSpeciActivity;
import com.qiwo.xkidswatcher_russia.util.Base64Encode;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import com.qiwo.xkidswatcher_russia.util.StringUtils;
import com.qiwo.xkidswatcher_russia.util.TLog;
import com.qiwo.xkidswatcher_russia.yandexmap.MyCircleOverlay;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.renderscript.RSInvalidStateException;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;
import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.map.MapEvent;
import ru.yandex.yandexmapkit.map.OnMapListener;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import ru.yandex.yandexmapkit.utils.ScreenPoint;

public class Home3Fragment extends BaseFragment implements OnMapListener {

    @InjectView(R.id.textView_time)
    TextView mTvTime;

    @InjectView(R.id.textView_electricity)
    TextView mTvElectricity;

    @InjectView(R.id.textView_recrod)
    TextView mTvRecord;

    @InjectView(R.id.textView_location)
    TextView mTvLocation;

    @InjectView(R.id.textView_address)
    TextView mTvAddress;

    @InjectView(R.id.textView_jd)
    TextView mTvJd;

    @InjectView(R.id.textView_range)
    TextView textView_range;

    @InjectView(R.id.content_xx)
    FrameLayout frameLayout_conent;

    @InjectView(R.id.linearLayout_record)
    LinearLayout linearLayout_record;

    @InjectView(R.id.lnearLayout_audio_play)
    LinearLayout lnearLayout_audio_play;

    @InjectView(R.id.linearLayout_location)
    LinearLayout linearLayout_location;

    @InjectView(R.id.lnearLayout_address)
    LinearLayout lnearLayout_address;

    @InjectView(R.id.linearLayout_sleepmode)
    LinearLayout linearLayout_sleepmode;

    @InjectView(R.id.linearLayout_electric)
    LinearLayout linearLayout_electric;

    @InjectView(R.id.imageView_electricity)
    ImageView imageView_electricity;

    @InjectView(R.id.mapview)
    MapView mapview;

    // 缓存 fragment view
    private View rootView;

    private MapController mapController;
    private OverlayManager overlayManager;
    private Overlay overlay;

    //	public static int tempCount = 0;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TLog.log("onCreate");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        TLog.log("onDetach");
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public boolean onBackPressed() {
        TLog.log("onBackPressed");
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        TLog.log("onCreateView");

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_home2, container,
                    false);
            ButterKnife.inject(this, rootView);
            initData();
            initView(rootView);
        }

        // 缓存的rootView需要判断是否已经被加过parent，
        // 如果有parent需要从parent删除，要不然会发生这个rootview已经有parent的错误。
        ViewGroup parent = (ViewGroup) rootView.getParent();
        if (parent != null) {
            parent.removeView(rootView);
        }

        return rootView;
    }

    @Override
    public void initData() {
        TLog.log("initData");
    }

    @Override
    public void initView(View view) {
        TLog.log("initView");
        //注册eventbus
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        //设置监听事件
        linearLayout_record.setOnClickListener(this);
        linearLayout_location.setOnClickListener(this);
        lnearLayout_audio_play.setOnClickListener(this);
        linearLayout_sleepmode.setOnClickListener(this);
        mapController = mapview.getMapController();
        overlayManager = mapController.getOverlayManager();
        overlay = new Overlay(mapController);
        mapController.setZoomCurrent(12);
        overlayManager.getMyLocation().setEnabled(true);
        overlayManager.getMyLocation().clearOverlayItems();
        setMapListener();
    }

    @SuppressWarnings("deprecation")
    private void setMapListener() {
        final GestureDetector mGestureDetector = new GestureDetector(new GestureDetector.OnGestureListener() {

            @Override
            public boolean onDown(MotionEvent e) {
                return false;
            }

            @Override
            public void onShowPress(MotionEvent e) {
            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {

                if (flag) {
                    return false;
                }

                TLog.log("click listener");

                float screenX = e.getX();
                float screenY = e.getY();

                DisplayMetrics dm = new DisplayMetrics();
                //获取屏幕信息
                getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
                int screenHeigh = dm.heightPixels;

                if (screenY < 300 || screenY > screenHeigh - 300) {
                    return false;
                }

                float top = 300;
                ScreenPoint screenPoint = new ScreenPoint(screenX, screenY - top);
                System.out.println("显示圆");
                setPointMarker();

                GeoPoint geopoint = mapController.getGeoPoint(screenPoint);
                TLog.log("lat=" + geopoint.getLat() + ",lon=" + geopoint.getLon());

                List<Double> mDistanseList = new ArrayList<Double>();
                double mMinDistanse = 0;
                int i = 0;
                if (bean___watch_data == null || bean___watch_data.info == null || bean___watch_data.info.ddata == null || bean___watch_data.info.ddata.rows == null || bean___watch_data.info.ddata.rows.size() == 0) {
                    return false;
                }
                final CRows marker = bean___watch_data.info.ddata.rows.get(0);
                double lat = marker.latitude;
                double lon = marker.longitude;
                if (Math.abs(lat - geopoint.getLat()) < 0.02 && Math.abs(lon - geopoint.getLon()) < 0.02) {
                    Bitmap drawable;
                    drawable = KidsWatUtils.getBabyImg_v3(getActivity(), AppContext.getInstance().currentFamily.family_id,
                            AppContext.getInstance().currentFamily.sex, AppContext.getInstance().currentFamily);
                    OverlayItem overlayItem = (OverlayItem) overlay.getOverlayItems().get(0);
                    mapController.setPositionAnimationTo(overlayItem.getGeoPoint());
                    overlayItem.setDrawable(new BitmapDrawable(getActivity().getResources(), drawable));

                    //显示信息在linearlayout中
                    long ltime = marker.time;
                    int location_type = marker.type;
                    final String strdate = new java.text.SimpleDateFormat("HH:mm")
                            .format(new java.util.Date(ltime * 1000));
                    final String otherText = String.format("%s",
                            location_type == 1 ? "GPS"
                                    : "GSM/WIFI");
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            System.out.println("on marker" + "temp" + " click");
                            mTvAddress.setText(marker.address);
                            mTvTime.setText(strdate);
                            textView_range.setText(otherText);
                            mTvElectricity.setText(marker.electricity + "%");
                            setDL(marker.electricity);
                            if (marker.electricity == 0) {
                                mTvAddress.setText(getResources().getString(
                                        R.string.tip_available));
                            }
                        }
                    });
                } else {
                    OverlayItem overlayItem = (OverlayItem) overlay.getOverlayItems().get(0);
                    mapController.setPositionAnimationTo(overlayItem.getGeoPoint());
                }

				/*//得到距离点击位置最近的点的距离集合
				if(bean___watch_data == null){
					return false;
				}
				
				for (final beanFor___get_watch_data.CRows row : bean___watch_data.info.ddata.rows) {
					GeoPoint scnPoint = new GeoPoint(row.latitude,row.longitude);
					double scnX = mapController.getScreenPoint(scnPoint).getX();
					double scnY = mapController.getScreenPoint(scnPoint).getY();
					double difX = Math.abs(scnX - screenX);
					double difY = Math.abs(scnY - screenY) - 250;
					if(difX < 100 && difY < 100){
						double distanse = Math.sqrt(Math.pow(difX, 2.0)+Math.pow(difY,2.0));
						mDistanseList.add(distanse);
					}
					
				}
				
				if(mDistanseList.size()!=0){
					Collections.sort(mDistanseList);
					mMinDistanse  = mDistanseList.get(0);
				}
				
				//点击标注点
				for (final beanFor___get_watch_data.CRows row : bean___watch_data.info.ddata.rows) {
					final double lat = row.latitude;
					final double lon = row.longitude;
//					double difX = lat-geoPoint.getLat();
//					double difY = lon-geoPoint.getLon();
					GeoPoint scnPoint = new GeoPoint(row.latitude,row.longitude);
					double scnX = mapController.getScreenPoint(scnPoint).getX();
					double scnY = mapController.getScreenPoint(scnPoint).getY();
					double difX = Math.abs(scnX - screenX);
					double difY = Math.abs(scnY - screenY) - 250;
					
					if(mMinDistanse == Math.sqrt(Math.pow(difX, 2.0)+Math.pow(difY,2.0))){
						//显示圆
						//显示头像
						List<OverlayItem> overlayItems = overlay.getOverlayItems();
						for(OverlayItem tempOverlayItem : overlayItems){
							if(tempOverlayItem.getGeoPoint().getLat() == lat && tempOverlayItem.getGeoPoint().getLon()==lon){
//								showCircle(scnX,scnY);
								int sex = KidsWatUtils.getBabySex();
								int resSex = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
								tempOverlayItem.setDrawable(new BitmapDrawable(getApplication().getResources(),KidsWatUtils.getBabyImg_v2(getActivity(), AppContext.getInstance().currentFamily.family_id, AppContext.getInstance().currentFamily.sex)));
								mapController.setPositionAnimationTo(tempOverlayItem.getGeoPoint());
							}
						}
						//显示信息在linearlayout中
						long ltime = row.time;
						final int location_type = row.type;
						final String strdate = new java.text.SimpleDateFormat("HH:mm")
								.format(new java.util.Date(ltime * 1000));
						final String otherText = String.format("%s",
								location_type == 1 ? "GPS"
										: "GSM/WIFI");
						getActivity().runOnUiThread(new Runnable() {
							public void run() {
								mTvAddress.setText(row.address);
								
								mTvTime.setText(strdate);
								textView_range.setText(otherText);
								mTvElectricity.setText(row.electricity + "%");
								setDL(row.electricity);
								if (row.electricity == 0) {
									mTvAddress.setText(getResources().getString(
											R.string.tip_available));
								}
							}
						});
						break;
					}else{
						GeoPoint center = mapController.getMapCenter();
						mapController.setPositionAnimationTo(mapController.getMapCenter());
					}
				}*/
                return false;
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                return false;
            }

            @Override
            public void onLongPress(MotionEvent e) {
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                return false;
            }
        });
        MainActivity.MyOnTouchListener myOnTouchListener = new MainActivity.MyOnTouchListener() {
            @Override
            public boolean onTouch(MotionEvent ev) {
                boolean result = mGestureDetector.onTouchEvent(ev);
                return result;
            }
        };

        ((MainActivity) getActivity()).registerMyOnTouchListener(myOnTouchListener);
    }

    public void onEventMainThread(BaseEvent event) {
        TLog.log("------onEventMainThread----" + event.getMsg());
        int type = event.getType();
        String msg = event.getMsg();

        if (type == BaseEvent.MSGTYPE_3___LOAD_FAMILY_FIRST_ITEM) {
            get_device_runing_status();
            get_watch_data();
            get_location_ex(true);
        } else if (type == BaseEvent.MSGTYPE_3___BACKGROUND_TO_FRONT) {
            get_watch_data();
            get_location(true);
        } else if (type == BaseEvent.MSGTYPE_3___CHANGE_DEVICE_MODE) {
            showSleepMode();
        } else if (type == BaseEvent.MSGTYPE_6___WHITE_HOME_VOICE_PUSH) {
            String[] str = event.getMsg().split(",");
            if (str.length == 2)
                if (str[1].equals(KidsWatConfig.getUserDeviceId())) {
                    fetch_voice_by_voice_id(str[0], 7, str[1], 1);
                }
        } else if (type == BaseEvent.MSGTYPE_3___CHANGE_USER) {
            lnearLayout_audio_play.setVisibility(View.GONE);
            get_device_runing_status();
            get_watch_data();
//			get_location(true);
        } else if (type == BaseEvent.MSGTYPE_3___GCM_LOCATION) {
            TLog.log("yandex", "GCM_LOCATION");
            try {
                // 收到定位成功的push
                get_watch_data_lastest_step();
                dismissDialog("Местоположение определено!", 2000);

                JSONObject xjson = new JSONObject(msg);
                JSONObject detail = xjson.getJSONObject("detail");
                String address = detail.getString("address");
                double latitude = detail.getDouble("latitude");
                double longitude = detail.getDouble("longitude");
                long time = detail.getLong("time");
                int location_type = detail.getInt("location_type");

                int electricityxx = 100;
                if (bean___watch_data != null
                        && bean___watch_data.info.ddata.rows.size() > 0) {
                    electricityxx = bean___watch_data.info.ddata.rows.get(bean___watch_data.info.ddata.rows.size() - 1).electricity;
                }

                beanFor___get_watch_data.CRows row = new beanFor___get_watch_data.CRows();
                row.address = address;
                row.time = time;
                row.latitude = latitude;
                row.longitude = longitude;
                row.electricity = electricityxx;
                row.type = location_type;

                String otherText = String.format("%s",
                        location_type == 1 ? "GPS"
                                : "GSM/WIFI");

                String strdate = new java.text.SimpleDateFormat("HH:mm")
                        .format(new java.util.Date(time * 1000));

                mTvAddress.setText(address);
                mTvTime.setText(strdate);
                textView_range.setText(otherText);
                mTvElectricity.setText(electricityxx + "%");
                setDL(electricityxx);
                if (electricityxx == 0) {
                    mTvAddress.setText(getResources().getString(
                            R.string.tip_available));
                }

                setLastLocation(row);
                showLastLocation();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (type == BaseEvent.MSGTYPE_3_SHOW_RECORD) {
            TLog.log("--msg-->:" + msg);
            if (msg.equals("361") || msg.equals("601")) {
                linearLayout_record.setVisibility(View.VISIBLE);
            } /*else if(msg.equals("461") || msg.equals("5612") || msg.equals("5613")){
				linearLayout_record.setVisibility(View.GONE);
			} */ else {
                linearLayout_record.setVisibility(View.GONE);
            }
        }
    }

    private void get_device_runing_status() {

        // --------------------
        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        final String device_id = KidsWatConfig.getUserDeviceId();

        final String request_url = KidsWatApiUrl
                .getUrlFor___get_device_runing_status(uid, access_token,
                        device_id);
        // -------------------------

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                // showWaitDialog("...loading...");
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                System.out.println("get_device_runing_status.response=" + t);
                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");

                    // 返回值:
                    // -1 --- 参数错误
                    // 0 --- 正常模式
                    // 2015001 --- 飞行模式
                    if (code == 0) {
                        SqlDb dbx = SqlDb.get(getActivity());
                        dbx.setDeviceModeBy_did(device_id, 1);
                        AppContext.getInstance().currentFamily = dbx
                                .getFamilyBy_did(device_id);
                        dbx.closeDb();

                        showSleepMode();
                    } else if (code == 2015001) {
                        SqlDb dbx = SqlDb.get(getActivity());
                        dbx.setDeviceModeBy_did(device_id, 2);
                        AppContext.getInstance().currentFamily = dbx
                                .getFamilyBy_did(device_id);
                        dbx.closeDb();
                        showSleepMode();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                System.out.println("error=" + msg);
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 10);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linearLayout_record:
                // showToast("linearLayout_record");
                exec_voice_record();
                break;
            case R.id.linearLayout_sleepmode:
                // showToast("linearLayout_record");
                Intent mIntent = new Intent();
                mIntent.setClass(getActivity(), SleepModeSpeciActivity.class);
                startActivity(mIntent);
                break;
            case R.id.lnearLayout_audio_play:
                String audio_file = lnearLayout_audio_play.getTag() == null ? ""
                        : lnearLayout_audio_play.getTag().toString();
                if (audio_file.length() > 0 && !isPlaying) {
                    lnearLayout_audio_play
                            .setBackgroundResource(R.anim.record_play_animations);
                    mAnimationDrawable = (AnimationDrawable) lnearLayout_audio_play
                            .getBackground();
                    mAnimationDrawable.start();

                    doPlayVoice(audio_file);
                } else if (audio_file.length() > 0 && isPlaying) {
                    mAnimationDrawable.stop();

                    mp.stop();
                    mp.release();
                    mp = null;
                    isPlaying = false;
                } else {
                    showLongToast(getActivity().getApplicationContext().getResources().getString(R.string.tip_norecord));
                }
                break;
            case R.id.linearLayout_location:
                get_location_ex(false);
                break;
        }
    }

    MediaPlayer mp = new MediaPlayer();
    boolean isPlaying = false;

    private void doPlayVoice(String src) {
        if (mp == null) {
            mp = new MediaPlayer();
        }
        // 这里就直接用mp.isPlaying()，因为不可能再报IllegalArgumentException异常了
        if (mp.isPlaying()) {
            mp.stop();
            mp.release();
            mp = null;
            mp = new MediaPlayer();
        }
        try {
            mp.setDataSource(src);
            mp.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
            // Prepare to async playing
            mp.prepareAsync();
            isPlaying = true;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mp.setOnCompletionListener(new OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mpxx) {
                mp.release();
                mp = null;
                isPlaying = false;
                // -----------
                lnearLayout_audio_play
                        .setBackgroundResource(R.drawable.icon_record3);
            }
        });
    }

    private void get_watch_data() {
        String uid = KidsWatConfig.getUserUid();
        String access_token = KidsWatConfig.getUserToken();
        String family_id = KidsWatConfig.getDefaultFamilyId();

        if (StringUtils.isEmpty(uid))
            return;

        Calendar a = Calendar.getInstance();
        int year = a.get(Calendar.YEAR);
        int month = a.get(Calendar.MONTH);
        int dayOfMonth = a.get(Calendar.DAY_OF_MONTH);

        long utcToday = KidsWatUtils.getUtcTimeAt0Time(year, month, dayOfMonth);

        final String request_url = KidsWatApiUrl.getUrlFor___get_watch_data(
                uid, access_token, family_id, utcToday);

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                showWaitDialog("...Загрузка...");
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                TLog.log(String.format("url:%s\nt:%s", request_url, t));

                beanFor___get_watch_data response = AppContext.getInstance()
                        .getGson().fromJson(t, beanFor___get_watch_data.class);
                bean___watch_data = response;

                if (overlay.getOverlayItems() != null) {
                    overlay.clearOverlayItems();
                }

                //重新得到map数据
                if (response.error == 0) {

                    if (response.info.ddata.total_rows > 0) {
                        // -------add marker to map--------
                        if (overlay.getOverlayItems() != null) {
                            overlay.clearOverlayItems();
                            int i = 0;
                            int rows = response.info.ddata.rows.size();
                            {
                                CRows row = response.info.ddata.rows.get(0);
                                int type = row.type;
                                String address = row.address;
                                long time = row.time;
                                // int reason = row.reason;
                                double latitude = row.latitude;
                                double longitude = row.longitude;
                                int electricity = row.electricity;
                                int location_type = row.type;

                                AppContext.lng = new GeoPoint(latitude, longitude);

                                GeoPoint point = new GeoPoint(latitude, longitude);
                                Drawable image = location_type == 2 ? getResources().getDrawable(R.drawable.point_select) : getResources().getDrawable(R.drawable.ing);
                                OverlayItem overlayItem = new OverlayItem(point, image);
                                overlay.addOverlayItem(overlayItem);

                                String otherText = String.format("%s",
                                        location_type == 1 ? "GPS"
                                                : "GSM/WIFI");

                                String strdate = new java.text.SimpleDateFormat("HH:mm")
                                        .format(new java.util.Date(time * 1000));

                                setDL(electricity);
                                mTvElectricity.setText(electricity + "%");
                                mTvAddress.setText(address);
                                mTvTime.setText(strdate);
                                textView_range.setText(otherText);
                                ((MainActivity) getActivity()).setStepNumber(row.step_number);

                                if (electricity == 0) {
                                    mTvAddress.setText(getResources().getString(
                                            R.string.tip_available));
                                    mTvTime.setText("No locating results");
                                }
                            }
							/*int rows = response.info.ddata.rows.size();
							for (beanFor___get_watch_data.CRows row : response.info.ddata.rows) {
								String address = row.address;
								long time = row.time;
								// int reason = row.reason;
								double latitude = row.latitude;
								double longitude = row.longitude;
								int electricity = row.electricity;
								int location_type = row.type;
								String strdate = new java.text.SimpleDateFormat("HH:mm")
										.format(new java.util.Date(time * 1000));
								String otherText = String.format("%s",
										location_type == 1 ? "GPS"
												: "GSM/WIFI");
								GeoPoint point = new GeoPoint(latitude,longitude);
								
								if(i == 0){
									setDL(electricity);
									mTvElectricity.setText(electricity + "%");
									mTvAddress.setText(address);
									mTvTime.setText(strdate);
									textView_range.setText(otherText);
									((MainActivity)getActivity()).setStepNumber(row.step_number);
								}
								
								Drawable image = location_type == 2 ? getResources().getDrawable(R.drawable.point_select) : getResources().getDrawable(R.drawable.ing);
								OverlayItem overlayItem = new OverlayItem(point, image);
								
								overlay.addOverlayItem(overlayItem);
								i++;
							}*/
                            overlayManager.addOverlay(overlay);
                            mapController.setZoomCurrent(12);
                            mapController.setPositionAnimationTo(((OverlayItem) (overlay.getOverlayItems().get(0))).getGeoPoint());
                        }
                    } else {
                        mapController.setPositionAnimationTo(new GeoPoint(0f, 0f), 2f);
                        showNoLocationResult();
                    }
                } else {
                    showLongToast(getActivity().getApplicationContext().getResources().getString(R.string.tip_loadtrack_faild));
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                System.out.println("error=" + msg);
            }

            @Override
            public void onFinish() {
                dismissDialog(null, 300);
            }

        });

    }

    private long fetch_time_start = 0;

    private void exec_voice_record() {
        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();
        final String device_id = KidsWatConfig.getUserDeviceId();
        // final String family_id = AppContext.getDefaultFamilyId();

        final String request_url = KidsWatApiUrl.getUrlFor___exec_voice_record(
                uid, access_token, device_id);

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                mTvRecord.setText("запись\n...");
                linearLayout_record
                        .setBackgroundResource(R.drawable.icon_record_2);
                showWaitDialog(getActivity().getResources().getString(R.string.locating));
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                TLog.log(String.format("url:%s\nt:%s", request_url, t));

                try {
                    JSONObject jso = new JSONObject(t);
                    int code = jso.getInt("error");
                    if (code == 0) {
                        final String command_no = jso.getJSONObject("info")
                                .getString("cmdno");
                        // -----------------
                        SqlDb db = SqlDb.get(getActivity());
                        db.saveFamily___last_record_no(device_id, command_no);
                        db.closeDb();
                        // --------------------
                        // showWaitDialog("...Recording...");
                        mHandler.postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                fetch_time_start = System.currentTimeMillis() / 1000;
                                showWaitDialog(getActivity().getResources().getString(R.string.transmiting));
                                fetch_voice_by_voice_id(command_no, 7,
                                        KidsWatConfig.getUserDeviceId(), -1);
                            }
                        }, 10 * 1000);

//						KidsWatUtils.get_notice_list_ex();
                    } else if (code == 2015001) {
                        showLongToast(getActivity().getApplicationContext().getResources().getString(R.string.tip_operate_sleep_mode));
                        setRecordFailed();
                    } else {
                        setRecordFailed();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    setRecordFailed();
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                System.out.println("error=" + msg);
                setRecordFailed();
            }

            @Override
            public void onFinish() {
                //
            }
        });

    }

    private void fetch_voice_by_voice_id(final String command_no,
                                         final int total_request_number, final String Device_id, int i) {

        if (i == -1) {
            long timespan = System.currentTimeMillis() / 1000
                    - fetch_time_start;

            if (timespan > 40.0) {
                fetch_time_start = System.currentTimeMillis() / 1000;
                setRecordFailed();
                return;
            }
        }

        final String uid = KidsWatConfig.getUserUid();
        final String access_token = KidsWatConfig.getUserToken();

        final String request_url = KidsWatApiUrl
                .getUrlFor___fetch_voice_by_voice_id(uid, access_token,
                        Device_id, command_no, total_request_number);

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                TLog.log(String.format("url:%s\nt:%s", request_url, t));
                try {
                    JSONObject jso = new JSONObject(t);
                    int code = jso.getInt("error");
                    System.out.println("errorcode=" + code);
                    if (code == 0) {
                        String filepath = String.format("%saudio_%s_%s.amr",
                                KidsWatConfig.getTempFilePath(), Device_id,
                                command_no);
                        String voice_stream = jso.getJSONObject("info")
                                .getString("voice_stream");
                        Base64Encode.decoderBase64FileForAudio(voice_stream,
                                filepath);
                        System.out.println("save ok");
                        lnearLayout_audio_play.setVisibility(View.VISIBLE);
                        lnearLayout_audio_play.setTag(filepath);
                        // ----------
                        mTvRecord.setText("запись");
                        linearLayout_record
                                .setBackgroundResource(R.drawable.icon_record_1);
                        dismissDialog(getActivity().getResources().getString(R.string.record_successful), 800);
                    } else {
                        if (code == -1) {
                            hideWaitDialog();
                        }

                        fetch_voice_by_voice_id(command_no,
                                total_request_number, Device_id, -1);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    setRecordFailed();
                } catch (Exception e) {
                    e.printStackTrace();
                    fetch_voice_by_voice_id(command_no, total_request_number,
                            Device_id, -1);
                    dismissDialog(null);
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                System.out.println("error=" + msg);
                fetch_voice_by_voice_id(command_no, total_request_number,
                        Device_id, -1);
                if (!KidsWatUtils.isNetworkAvailable(getActivity())) {
                    Toast.makeText(getActivity(), "network unaviable.", Toast.LENGTH_SHORT).show();
                    dismissDialog("");
                    setRecordFailed();
                }
            }

            @Override
            public void onFinish() {
                //
            }
        });
    }

    beanFor___get_watch_data bean___watch_data;
    private AnimationDrawable mAnimationDrawable;

    private void showLastLocation() {
        if (bean___watch_data != null) {
            int rows = bean___watch_data.info.ddata.rows.size();
            if (rows > 0) {
                if (overlay != null && overlay.getOverlayItems().size() > 0) {
                    double latitude = bean___watch_data.info.ddata.rows.get(0).latitude;
                    double longitude = bean___watch_data.info.ddata.rows.get(0).longitude;
                    add_marker_to_map(new GeoPoint(latitude, longitude));

                    final String family_id = KidsWatConfig.getDefaultFamilyId();
                    final int sex = KidsWatUtils.getBabySex();
                    setPointMarker();

                    List<OverlayItem> overlayItems = overlay.getOverlayItems();
                    GeoPoint lastPoint = overlayItems.get(0).getGeoPoint();

                    int resSex = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
                    Drawable drawable = new BitmapDrawable(getApplication().getResources(), KidsWatUtils.getBabyImg_v3(getActivity(), family_id, sex, AppContext.getInstance().currentFamily));

                    overlayItems.get(0).setDrawable(drawable);
                    mapController.setZoomCurrent(12);

                    // wtf 调用两次才能移动地图 dont no why,先这样弄
                    // TODO
                    mapController.setPositionAnimationTo(lastPoint);
                    mapController.setPositionAnimationTo(lastPoint);
                }
            }
        }
    }

    private void setPointMarker() {
        List<OverlayItem> overlayItems = overlay.getOverlayItems();

        for (OverlayItem m : overlayItems) {
            m.setDrawable(getResources().getDrawable(R.drawable.point_select));
        }
    }

    private void get_location_ex(final boolean isBackground) {
        TLog.log("get_location_ex");
        //mZoomCurrent = mapController.getZoomCurrent();
        String uid = KidsWatConfig.getUserUid();
        String access_token = KidsWatConfig.getUserToken();
        String device_id = KidsWatConfig.getUserDeviceId();
        String family_id = KidsWatConfig.getDefaultFamilyId();

        final String request_url = KidsWatApiUrl.getUrlFor___get_location_ex(
                uid, access_token, device_id, family_id);

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                if (!isBackground) {
                    mTvLocation.setText("Положение\n...");
                    linearLayout_location
                            .setBackgroundResource(R.drawable.icon_location_2);
                    showWaitDialog("Определение местоположения...");
                }
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                TLog.log(String.format("url:%s\nt:%s", request_url, t));
                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");
                    if (code == 0) {
                        lnearLayout_audio_play.setVisibility(View.GONE);
                        // int content_type = response.getJSONObject("data")
                        // .getInt("content_type");
                        long ltime = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("location_time");
                        String address = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getString("address");
                        double longitude = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getJSONObject("location")
                                .getDouble("longitude");
                        double latitude = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getJSONObject("location")
                                .getDouble("latitude");
                        int electricity = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("electricity");
                        int location_type = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("location_type");// 1基站,0,gps
                        // ----------------
                        String strdate = new java.text.SimpleDateFormat("HH:mm")
                                .format(new java.util.Date(ltime * 1000));
                        String otherText = String.format("%s",
                                location_type == 1 ? "GPS"
                                        : "GSM/WIFI");
                        mTvAddress.setText(address);
                        mTvTime.setText(strdate);
                        textView_range.setText(otherText);
                        mTvElectricity.setText(electricity + "%");

                        if (electricity == 0) {
                            mTvAddress.setText(getResources().getString(
                                    R.string.tip_available));
                            mTvTime.setText("Нет результатов местоположения");
                        }

                        setDL(electricity);

                        beanFor___get_watch_data.CRows row = new beanFor___get_watch_data.CRows();
                        row.address = address;
                        row.time = ltime;
                        row.latitude = latitude;
                        row.longitude = longitude;
                        row.electricity = electricity;
                        row.type = location_type;

                        setLastLocation(row);
                        showLastLocation();

                        // 获取最新步数
                        get_watch_data_lastest_step();

                        // ---------------
                        if (!isBackground) {
                            showWaitDialog("Местоположение определено. Адрес обновится в течение 1 минуты.");
                        }

                    } else if (code == 2015001) {
                        showLongToast(getActivity().getApplicationContext().getResources().getString(R.string.tip_operate_sleep_mode));
                    } else if (code == -2) {
                        int electricity = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("electricity");
                        mTvElectricity.setText(electricity + "%");

						/*if (electricity == 0) {
							mTvAddress.setText(getResources().getString(
									R.string.tip_available));
							mTvTime.setText("Нет результатов местоположения");
						}*/
                        setDL(electricity);

                        showShortToast("Местоположение не определено!");
                    } else {
                        if (!isBackground) {
                            showWaitDialog("Местоположение не определено!");
                        }
                    }
                } catch (JSONException e) {
                    if (!isBackground) {
                        showWaitDialog("Местоположение не определено!");
                    }
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                System.out.println("error=" + msg);
                if (!isBackground) {
                    showWaitDialog("Местоположение не определено!");
                }
            }

            @Override
            public void onFinish() {
                if (!isBackground) {
                    linearLayout_location
                            .setBackgroundResource(R.drawable.icon_location_1);
                    mTvLocation.setText("Положение");

                    dismissDialog(null, 800);
                }
            }
        });
    }

    private void get_location(final boolean isBackground) {
        String uid = KidsWatConfig.getUserUid();
        String access_token = KidsWatConfig.getUserToken();
        String device_id = KidsWatConfig.getUserDeviceId();
        String family_id = KidsWatConfig.getDefaultFamilyId();

        // KidsWatApi.get_location_ex(uid, access_token, device_id, family_id,
        final String request_url = KidsWatApiUrl.getUrlFor___get_location(
                uid, access_token, device_id, family_id);

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                if (!isBackground) {
                    mTvLocation.setText("Location\n...");
                    linearLayout_location
                            .setBackgroundResource(R.drawable.icon_location_2);
                    showWaitDialog();
                }
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                TLog.log(String.format("url:%s\nt:%s", request_url, t));
                try {
                    JSONObject response = new JSONObject(t);

                    int code = response.getInt("error");
                    if (code == 0) {
                        lnearLayout_audio_play.setVisibility(View.GONE);
                        // int content_type = response.getJSONObject("data")
                        // .getInt("content_type");
                        long ltime = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("location_time");
                        String address = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getString("address");
                        double longitude = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getJSONObject("location")
                                .getDouble("longitude");
                        double latitude = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getJSONObject("location")
                                .getDouble("latitude");
                        int electricity = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("electricity");
                        int location_type = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("location_type");// 1基站,0,gps
                        // ----------------
                        String strdate = new java.text.SimpleDateFormat("HH:mm")
                                .format(new java.util.Date(ltime * 1000));
                        String otherText = String.format("%s",
                                location_type == 1 ? "GPS"
                                        : "GSM/WIFI");
                        mTvAddress.setText(address);
                        mTvTime.setText(strdate);
                        textView_range.setText(otherText);
                        mTvElectricity.setText(electricity + "%");

                        // Please make sure the watch is on and the network is
                        // available.

                        if (electricity == 0) {
                            mTvAddress.setText(getResources().getString(
                                    R.string.tip_available));
                            mTvTime.setText("Нет результатов местоположения");
                        }

                        setDL(electricity);
                        // -----------------

                        beanFor___get_watch_data.CRows row = new beanFor___get_watch_data.CRows();
                        row.address = address;
                        row.time = ltime;
                        row.latitude = latitude;
                        row.longitude = longitude;
                        row.electricity = electricity;
                        row.type = location_type;

                        setLastLocation(row);
                        showLastLocation();
                        // ---------------
                        if (!isBackground) {
                            showWaitDialog("Местоположение не определено!");
                        }
//						if(clicked){
//							showWaitDialog("Success. Update Address after 1~2 minutes.");
//						}
                    } else if (code == 2015001) {
                        //安全模式下，点击定位才去显示toast
						/*if(clicked){
							showLongToast(getActivity().getResources().getString(R.string.tip_in_sleep_mode));
						}else{
							mTvAddress.setText(getActivity().getResources().getString(R.string.tip_available));
							mTvTime.setText(getActivity().getResources().getString(R.string.tip_no_location_results));
						}*/
                        showLongToast(getActivity().getApplicationContext().getResources().getString(R.string.tip_operate_sleep_mode));
                    } else if (code == -2) {
                        int electricity = response.getJSONObject("data")
                                .getJSONObject("parameter")
                                .getInt("electricity");
                        mTvElectricity.setText(electricity + "%");

                        if (electricity == 0) {
                            mTvAddress.setText(getResources().getString(
                                    R.string.tip_available));
                            mTvTime.setText("Нет результатов местоположения");
                        }
                        setDL(electricity);

                        showShortToast("Местоположение не определено!");
                    } else {
                        if (!isBackground) {
                            showWaitDialog("Местоположение не определено!");
                        }
                    }

                } catch (JSONException e) {
                    if (!isBackground) {
                        showWaitDialog("Местоположение не определено!");
                    }
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
                String msg = String.format("errorNo:%s\n%s", errorNo, strMsg);
                System.out.println("error=" + msg);
                if (!isBackground) {
                    showWaitDialog("Местоположение не определено!");
                }
            }

            @Override
            public void onFinish() {
                if (!isBackground) {
                    linearLayout_location
                            .setBackgroundResource(R.drawable.icon_location_1);
                    mTvLocation.setText("Положение");
                    dismissDialog(null, 800);
                }
            }
        });
    }

    private void setLastLocation(CRows row) {
        if (bean___watch_data != null) {
            int rows = bean___watch_data.info.ddata.rows.size();
            if (rows > 0) {
                bean___watch_data.info.ddata.rows.set(0, row);
            }
        }
    }

    public void getFocus(CRows row) {
        long ltime = row.time;
        int location_type = row.type;
        String strdate = new java.text.SimpleDateFormat("HH:mm")
                .format(new java.util.Date(ltime * 1000));
        String otherText = String.format("%s",
                location_type == 1 ? "GPS"
                        : "GSM/WIFI");
        mTvAddress.setText(row.address);
        mTvTime.setText(strdate);
        textView_range.setText(otherText);
        mTvElectricity.setText(row.electricity + "%");
        setDL(row.electricity);
        if (row.electricity == 0) {
            mTvAddress.setText(getResources().getString(
                    R.string.tip_available));
        }
    }

    private void setRecordFailed() {
        mTvRecord.setText("запись");
        linearLayout_record.setBackgroundResource(R.drawable.icon_record_1);
        dismissDialog(getActivity().getResources().getString(R.string.record_faild), 800);
    }

    private void showSleepMode() {
        if (AppContext.isLogin) {

            beanForDb___Family f = AppContext.getInstance().currentFamily;
            if (f != null && f.d_mode == 2) {
                linearLayout_sleepmode.setVisibility(View.VISIBLE);
                linearLayout_electric.setVisibility(View.INVISIBLE);
            } else {
                linearLayout_sleepmode.setVisibility(View.INVISIBLE);
                linearLayout_electric.setVisibility(View.VISIBLE);
            }
        } else {
            linearLayout_sleepmode.setVisibility(View.INVISIBLE);
            linearLayout_electric.setVisibility(View.VISIBLE);
        }
    }

    public void setDL(int dl) {

        if (dl > 80)
            imageView_electricity.setBackgroundResource(R.drawable.batteryf);
        else if (dl > 60)
            imageView_electricity.setBackgroundResource(R.drawable.batterye);
        else if (dl > 60)
            imageView_electricity.setBackgroundResource(R.drawable.batteryd);
        else if (dl > 20)
            imageView_electricity.setBackgroundResource(R.drawable.batteryc);
        else if (dl > 10)
            imageView_electricity.setBackgroundResource(R.drawable.batteryb);
        else
            imageView_electricity.setBackgroundResource(R.drawable.batterya);
    }

    @Override
    public void onMapActionEvent(MapEvent event) {
        switch (event.getMsg()) {
            case MapEvent.MSG_LONG_PRESS:
                System.out.println("long press");

                List<Double> mDistanseList = new ArrayList<Double>();
                double mMinDistanse = 0;

                //点击显示默认图片
                System.out.println("点击地图,先显示圆");
                setPointMarker();

                float x = event.getX();
                float y = event.getY();

                ScreenPoint screenPoint = new ScreenPoint(x, y);
                GeoPoint geopoint = mapController.getGeoPoint(screenPoint);
                System.out.println("screen lat:" + geopoint.getLat() + "lon:" + geopoint.getLon());
                int i = 0;

                final CRows marker = bean___watch_data.info.ddata.rows.get(0);
                double lat = marker.latitude;
                double lon = marker.longitude;
                if (Math.abs(lat - geopoint.getLat()) < 0.02 && Math.abs(lon - geopoint.getLon()) < 0.02) {
                    final int sex = KidsWatUtils.getBabySex();
                    int resSex = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
                    Drawable drawable = getResources().getDrawable(resSex);
                    OverlayItem overlayItem = (OverlayItem) overlay.getOverlayItems().get(0);
                    overlayItem.setDrawable(drawable);

                    //显示信息在linearlayout中
                    long ltime = marker.time;
                    int location_type = marker.type;
                    final String strdate = new java.text.SimpleDateFormat("HH:mm")
                            .format(new java.util.Date(ltime * 1000));
                    final String otherText = String.format("%s",
                            location_type == 1 ? "GPS"
                                    : "GSM/WIFI");
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            System.out.println("on marker" + "" + " click");
                            mTvAddress.setText(marker.address);
                            mTvTime.setText(strdate);
                            textView_range.setText(otherText);
                            mTvElectricity.setText(marker.electricity + "%");
                            setDL(marker.electricity);
                            if (marker.electricity == 0) {
                                mTvAddress.setText(getResources().getString(
                                        R.string.tip_available));
                            }
                        }
                    });
                }
                //得到距离点击位置最近的点的距离集合
				/*for (final beanFor___get_watch_data.CRows row : bean___watch_data.info.ddata.rows) {
					double lat = row.latitude;
					double lon = row.longitude;
					i++;
					System.out.println("history : "+ i +"----time : "+new Date(row.time*1000).toLocaleString());
					if(Math.abs(lat-geopoint.getLat()) < 0.02 && Math.abs(lon-geopoint.getLon()) < 0.02){
						double distanse = Math.sqrt(Math.pow(lat, 2.0)+Math.pow(lon,2.0));
						mDistanseList.add(distanse);
					}
				}
				Collections.sort(mDistanseList);
				mMinDistanse  = mDistanseList.get(0);*/
				
				/*//点击标注点
				for (final beanFor___get_watch_data.CRows row : bean___watch_data.info.ddata.rows) {
					final double lat = row.latitude;
					final double lon = row.longitude;
					if(mMinDistanse == Math.sqrt(Math.pow(lat, 2.0)+Math.pow(lon,2.0))){
						System.out.println("latest click");
						//显示圆
						//显示头像
						List<OverlayItem> overlayItems = overlay.getOverlayItems();
						for(OverlayItem overlayItem : overlayItems){
							if(overlayItem.getGeoPoint().getLat() == lat && overlayItem.getGeoPoint().getLon()==lon){
								System.out.println("点击的是标记点，显示头像");
								final int sex = KidsWatUtils.getBabySex();
								int resSex = sex == 1 ? R.drawable.icon_boy : R.drawable.icon_girl;
								Drawable drawable = getResources().getDrawable(resSex);
								overlayItem.setDrawable(drawable);
							}
						}
						//显示信息在linearlayout中
						long ltime = row.time;
						int location_type = row.type;
						final String strdate = new java.text.SimpleDateFormat("HH:mm")
								.format(new java.util.Date(ltime * 1000));
						final String otherText = String.format("%s",
								location_type == 1 ? "GPS"
										: "GSM/WIFI");
						getActivity().runOnUiThread(new Runnable() {
							public void run() {
								System.out.println("on marker"+""+" click");
								mTvAddress.setText(row.address);
								mTvTime.setText(strdate);
								textView_range.setText(otherText);
								mTvElectricity.setText(row.electricity + "%");
								setDL(row.electricity);
								if (row.electricity == 0) {
									mTvAddress.setText(getResources().getString(
											R.string.tip_available));
								}
							}
						});
					}
				}*/
                break;
        }
    }

    // 此方法未能实现在地图上画圆，无用
    public void showCircle(double lat, double lon) {
        MyCircleOverlay myCircleOverlay = new MyCircleOverlay(getActivity(), mapController, mapview, (float) lat, (float) lon, 50f);

        overlayManager.addOverlay(myCircleOverlay);
    }

    boolean flag;

    @Override
    public void onResume() {
        flag = false;
        TLog.log("on resume");
        showLastLocation();
        super.onResume();
    }

    @Override
    public void onPause() {
        flag = true;
        super.onPause();
    }

    private void showNoLocationResult() {
        // TODO Auto-generated method stub
        setDL(0);
        mTvElectricity.setText("0%");
        mTvAddress.setText("Нет результатов местоположения");
        mTvTime.setText("15 минут назад");
    }

    private void add_marker_to_map(GeoPoint point) {
        // TODO Auto-generated method stub

        overlay.clearOverlayItems();
        OverlayItem item = new OverlayItem(point, getActivity().getResources().getDrawable(R.drawable.point_select));
        overlay.addOverlayItem(item);
    }

    // 获取最新点的数据
    public void get_watch_data_lastest_step() {
        String uid = KidsWatConfig.getUserUid();
        String access_token = KidsWatConfig.getUserToken();
        String family_id = KidsWatConfig.getDefaultFamilyId();

        if (StringUtils.isEmpty(uid))
            return;

        Calendar a = Calendar.getInstance();
        int year = a.get(Calendar.YEAR);
        int month = a.get(Calendar.MONTH);
        int dayOfMonth = a.get(Calendar.DAY_OF_MONTH);

        long utcToday = KidsWatUtils.getUtcTimeAt0Time(year, month, dayOfMonth);
        final String request_url = KidsWatApiUrl.getUrlFor___get_watch_data_latest(uid, access_token, family_id,
                utcToday);

        ApiHttpClient.get(request_url, new HttpCallBack() {

            @Override
            public void onPreStart() {
                super.onPreStart();
            }

            @Override
            public void onSuccess(String t) {
                super.onSuccess(t);
                // 从服务器获取最新点接口返回数据
                beanFor___get_watch_data_latest response = AppContext.getInstance().getGson().fromJson(t,
                        beanFor___get_watch_data_latest.class);
                // 如果返回没有错误
                if (response.error == 0) {

                    // 如果有数据
                    if (response.info.lastest_point != null) {
                        if (overlay.getOverlayItems() != null) {
                            // 获取最近点
                            beanFor___get_watch_data_latest.CInfo.CData lastest_point = response.info.lastest_point;

                            ((MainActivity) getActivity()).setStepNumber(lastest_point.step_number);
                        }
                    }
                }
            }

            @Override
            public void onFailure(int errorNo, String strMsg) {
                super.onFailure(errorNo, strMsg);
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        });
    }


}

