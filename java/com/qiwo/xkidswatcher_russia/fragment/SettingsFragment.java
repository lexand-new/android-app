package com.qiwo.xkidswatcher_russia.fragment;

//import net.oschina.app.AppContext;
//import net.oschina.app.R;
//import net.oschina.app.base.BaseFragment;
//import net.oschina.app.bean.NotebookData;
//import net.oschina.app.bean.SimpleBackPage;
//import net.oschina.app.db.NoteDatabase;
//import net.oschina.app.ui.SimpleBackActivity;
//import net.oschina.app.ui.dialog.CommonDialog;
//import net.oschina.app.ui.dialog.DialogHelper;
//import net.oschina.app.util.KJAnimations;
//import net.oschina.app.util.StringUtils;
//import net.oschina.app.util.UIHelper;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.AppManager;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.api.remote.KidsWatApiUrl;
import com.qiwo.xkidswatcher_russia.base.BaseFragment;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.db.SqlDb;
import com.qiwo.xkidswatcher_russia.event.BaseEvent;
import com.qiwo.xkidswatcher_russia.ui.BleAccompanyActivity;
import com.qiwo.xkidswatcher_russia.ui.ChangeSIMActivity;
import com.qiwo.xkidswatcher_russia.ui.FamilyMemberActivity;
import com.qiwo.xkidswatcher_russia.ui.KidsProfileActivity;
import com.qiwo.xkidswatcher_russia.ui.LocatingModeActivity;
import com.qiwo.xkidswatcher_russia.ui.QrcodeActivity;
import com.qiwo.xkidswatcher_russia.ui.SafeZoneActivity;
import com.qiwo.xkidswatcher_russia.ui.ScanGuideAActivity;
import com.qiwo.xkidswatcher_russia.ui.SystemSettingsActivity;
import com.qiwo.xkidswatcher_russia.ui.TopUpActivity;
import com.qiwo.xkidswatcher_russia.ui.Watch_SettingsActivity;
import com.qiwo.xkidswatcher_russia.util.Contanst;
import com.qiwo.xkidswatcher_russia.util.TLog;

import org.json.JSONException;
import org.json.JSONObject;
import org.kymjs.kjframe.http.HttpCallBack;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * 
 */
@Deprecated
public class SettingsFragment extends BaseFragment {

	@InjectView(R.id.recyclerview)
	RecyclerView recyclerView;

	LinearLayoutManager layoutManager;
	List<RecycleItem> datas;
	StRecycleAdapter mAdapter;
	private View rootView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);

		TLog.log("onCreateView");

		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_x_settings,
					container, false);
			ButterKnife.inject(this, rootView);
			initData();
			initView(rootView);
		}

		ViewGroup parent = (ViewGroup) rootView.getParent();
		if (parent != null) {
			parent.removeView(rootView);
		}
		return rootView;

	}

	@Override
	public void onClick(View v) {
		//
	}

	@Override
	public void initData() {
		datas = new ArrayList<RecycleItem>();

		initListData();
		// ---------------
		layoutManager = new GridLayoutManager(getActivity(), 3);
		recyclerView.setLayoutManager(layoutManager);// 设置布局管理器
		mAdapter = new StRecycleAdapter(datas);
		mAdapter.setOnItemClickListener(recyleItemListener);
		recyclerView.setAdapter(mAdapter);// 设置适配器
	}

	private void initListData() {
		if (AppContext.getInstance().loginUser_kid != null) {
			datas.add(new RecycleItem(1, R.drawable.bg_item_s1, "Kid's Profile"));
			datas.add(new RecycleItem(2, R.drawable.bg_item_s2, "SafeZones"));
			datas.add(new RecycleItem(3, R.drawable.bg_item_s3,
					"Family Members"));
			datas.add(new RecycleItem(4, R.drawable.bg_item_s4, "Accompany"));
			datas.add(new RecycleItem(5, R.drawable.bg_item_s5,
					"Look for Device"));
			datas.add(new RecycleItem(6, R.drawable.bg_item_s6, "Qr Code"));

			// if (AppContext.getInstance().loginUser_kid.isAdmin == 1)
			if (!AppContext.getInstance().currentFamily.version.equals("461"))
				datas.add(new RecycleItem(7, R.drawable.bg_item_s7, "Top-up"));
			// this function for 362
			// datas.add(new RecycleItem(8, R.drawable.bg_item_s8,
			// "Kid's Friends"));
		}

		if (AppContext.getInstance().loginUser_kid != null
				&& AppContext.getInstance().loginUser_kid.isAdmin == 1) {
			datas.add(new RecycleItem(9, R.drawable.bg_item_s9, "Power-Off"));
		}
		if (AppContext.isLogin) {
			datas.add(new RecycleItem(10, R.drawable.bg_item_s10,
					"Locating Mode"));
			datas.add(new RecycleItem(11, R.drawable.bg_item_s11, "Add Device"));
		}
		if (AppContext.getInstance().loginUser_kid != null) {
			if (AppContext.getInstance().loginUser_kid.isAdmin == 1) {
				datas.add(new RecycleItem(12, R.drawable.bg_item_s12, "Discard"));
				if (AppContext.getInstance().currentFamily.version
						.equals("461"))
					datas.add(new RecycleItem(15, R.drawable.bg_item_s15,
							"Watch Setting"));
			} else {
				datas.add(new RecycleItem(14, R.drawable.bg_item_s14,
						"Stop Following"));
			}
		}

		datas.add(new RecycleItem(13, R.drawable.bg_item_s13, "General"));

		if (AppContext.getInstance().loginUser_kid != null) {
			if (AppContext.getInstance().loginUser_kid.isAdmin == 1
					&& AppContext.getInstance().currentFamily.version
							.equals("461"))
				datas.add(new RecycleItem(16, R.drawable.bg_item_s16,
						"Chanage SIM"));
		}
	}

	private OnItemClickListener recyleItemListener = new OnItemClickListener() {
		@Override
		public void onItemClick(int position, Object object) {
			RecycleItem bean = (RecycleItem) object;

			// showLongToast(bean.text);
			if (bean.type == 1) {
				Intent intent = new Intent(getActivity(),
						KidsProfileActivity.class);
				intent.putExtra(Contanst.KEY_ACTION, Contanst.ACTION_EDIT);
				startActivity(intent);
			}

			else if (bean.type == 2) {
				Intent intent = new Intent(getActivity(),
						SafeZoneActivity.class);
				// intent.putExtra(Contanst.KEY_ACTION, Contanst.ACTION_EDIT);
				startActivity(intent);

			} else if (bean.type == 3) {
				Intent intent = new Intent(getActivity(),
						FamilyMemberActivity.class);
				// intent.putExtra(Contanst.KEY_ACTION, Contanst.ACTION_EDIT);
				startActivity(intent);

			}

			else if (bean.type == 4) {
				Intent intent = new Intent(getActivity(),
						BleAccompanyActivity.class);
				// intent.putExtra(Contanst.KEY_ACTION, Contanst.ACTION_EDIT);
				startActivity(intent);

			} else if (bean.type == 5) {

				DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

						final String device_id = KidsWatConfig
								.getUserDeviceId();
						watch_buzzer(device_id, 1, 60);
					}
				};
				String title = "Look for Device";
				String message = "The watch would ring for one minute for noticing.";
				String positiveText = "OK";
				String negativeText = "Cancel";

				showConfirmDialog(title, message, positiveText, negativeText,
						positiveListener);
			} else if (bean.type == 6) {
				SqlDb db = SqlDb.get(getActivity());
				beanForDb___Family bb = db.getFamilyBy_fid(KidsWatConfig
						.getDefaultFamilyId());
				db.closeDb();

				Intent intent = new Intent(getActivity(), QrcodeActivity.class);
				intent.putExtra("qrcode", bb.qrcode);
				intent.putExtra("sim", bb.sim);
				startActivity(intent);
			} else if (bean.type == 7) {

				Intent intent = new Intent(getActivity(), TopUpActivity.class);
				// intent.putExtra("qrcode", bb.qrcode);
				// intent.putExtra("sim", bb.sim);
				startActivity(intent);
			} else if (bean.type == 9) {
				DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// showLongToast("power off");
						final String device_id = KidsWatConfig
								.getUserDeviceId();
						watch_shutdown(device_id);
					}
				};
				String title = "Remote Power-off";
				String message = "You can not receive any messages or notifications from the device after power-off.";
				String positiveText = "OK";
				String negativeText = "Cancel";

				showConfirmDialog(title, message, positiveText, negativeText,
						positiveListener);
			} else if (bean.type == 10) {
				Intent intent = new Intent(getActivity(),
						LocatingModeActivity.class);
				startActivity(intent);
			}

			else if (bean.type == 11) {
				Intent intent = new Intent(getActivity(),
						ScanGuideAActivity.class);
				startActivity(intent);
			} else if (bean.type == 12) {
				DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// showLongToast("xxxx");

						DialogInterface.OnClickListener positiveListener_2 = new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// showLongToast("xxxx");
								final String device_id = KidsWatConfig
										.getUserDeviceId();
								delete_watch(device_id);
							}
						};
						String title2 = "Reminder";
						String message2 = "The device will be reset.";
						String positiveText2 = "Yes";
						String negativeText2 = "Cancel";

						showConfirmDialog(title2, message2, positiveText2,
								negativeText2, positiveListener_2);

						// final String device_id = KidsWatConfig
						// .getUserDeviceId();
						// delete_watch(device_id);
					}
				};
				String title = "Discard Paring";
				String message = "By clicking Yes, all data and numbers paired with this device will be removed.";
				String positiveText = "Yes";
				String negativeText = "Cancel";

				showConfirmDialog(title, message, positiveText, negativeText,
						positiveListener);
			}

			else if (bean.type == 13) {
				Intent intent = new Intent(getActivity(),
						SystemSettingsActivity.class);
				startActivity(intent);
			}

			else if (bean.type == 14) {
				DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// showLongToast("power off");
						final String family_id = KidsWatConfig
								.getDefaultFamilyId();
						user_quit_from_family(family_id);
					}
				};
				String title = "Stop Following";
				String message = "If you stop following this kid, you will not receive any messages or notifications from this device.";
				String positiveText = "OK";
				String negativeText = "Cancel";

				showConfirmDialog(title, message, positiveText, negativeText,
						positiveListener);
			}

			else if (bean.type == 15) {
				Intent intent = new Intent(getActivity(),
						Watch_SettingsActivity.class);
				startActivity(intent);
			}

			/*
			 * 只有是当前宝宝的管理员才会触发的事件
			 */
			else if (bean.type == 16) {
				Intent intent = new Intent(getActivity(),
						ChangeSIMActivity.class);
				startActivity(intent);
			}

		}
	};

	@Override
	public void initView(View view) {
		// mImgMenu.setOnTouchListener(this);
		// mLayoutMenu.setOnTouchListener(this);
		if (!EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().register(this);
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	public void onEventMainThread(BaseEvent event) {
		TLog.log("------onEventMainThread----" + event.getMsg());
		int type = event.getType();
		if (type == BaseEvent.MSGTYPE_3___CHANGE_USER
				|| type == BaseEvent.MSGTYPE_3___LOAD_FAMILY_FIRST_ITEM) {
			datas.clear();
			initListData();
			mAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		if (EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().unregister(this);
		}
	}

	@Override
	public boolean onBackPressed() {
		return true;
	}

	public class RecycleItem {
		public int type;
		public int drawableId;
		public int bg_drawableid;
		public String text;

		private RecycleItem(int _type, int _drawableId, int _bg_drawableid,
				String _text) {
			type = _type;
			drawableId = _drawableId;
			bg_drawableid = _bg_drawableid;
			text = _text;
		}

		public RecycleItem(int _type, int _bg_drawableid, String _text) {
			this(_type, 0, _bg_drawableid, _text);
		}
	}

	/**
	 * 内部接口回调方法
	 */
	public interface OnItemClickListener {
		void onItemClick(int position, Object object);
	}

	public class StRecycleAdapter extends
			RecyclerView.Adapter<StRecycleAdapter.ViewHolder> {

		private List<RecycleItem> list;
		private OnItemClickListener listener;

		public StRecycleAdapter(List<RecycleItem> list) {
			this.list = list;
		}

		@Override
		public int getItemCount() {
			return list.size();
		}

		@Override
		public void onBindViewHolder(ViewHolder holder, final int position) {

			final RecycleItem bean = list.get(position);
			// holder.title.setText(bean.getTitle());
			// holder.imageView.setImageResource(bean.getResId())；

			holder.mImageView.setBackgroundResource(bean.bg_drawableid);
			// holder.mImageView.setImageResource(bean.drawableId);
			// holder.mImageView.setBackgroundResource(bean.drawableId);
			holder.mTextView.setText(bean.text);
			/**
			 * 调用接口回调
			 */
			holder.itemView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (null != listener)
						listener.onItemClick(position, bean);
				}
			});
		}

		/**
		 * 设置监听方法
		 * 
		 * @param listener
		 */
		public void setOnItemClickListener(OnItemClickListener listener) {
			this.listener = listener;
		}

		@Override
		public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
			View view = LayoutInflater.from(viewGroup.getContext()).inflate(
					R.layout.view_settings_item, viewGroup, false);
			ViewHolder holder = new ViewHolder(view);
			return holder;
		}

		public class ViewHolder extends RecyclerView.ViewHolder {
			public TextView mTextView;
			public ImageView mImageView;

			public ViewHolder(View itemView) {
				super(itemView);
				mTextView = (TextView) itemView.findViewById(R.id.txt);
				mImageView = (ImageView) itemView.findViewById(R.id.img);
			}
		}
	}

	private void watch_buzzer(String device_id, int type, int mtime) {

		// -----------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		// final String device_id = AppContext.getInstance().getProperty(
		// "user.device_id");
		// final String family_id = AppContext.getDefaultFamilyId();
		// -------------------------
		final String request_url = KidsWatApiUrl.getUrlFor___watch_buzzer(uid,
				access_token, device_id, type, mtime);

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));

				try {
					JSONObject response = new JSONObject(t);

					int code = response.getInt("error");
					if (code == 0) {
						// response={"data":{"mode":2},"error":0,"info":{"message":"get success!"}}
						// Operation Succeeds
						showConfirmInformation(null, "Operation Succeeds");
						// showLongToast("The operation is successful");
					} else if (code == 2015001) {
						// dismissDialog("Device is in sleep mode.", 800);
						dismissDialog(null, 10);
						showConfirmInformation(null,
								"You can not do this when the device is under sleep mode.");
					} else {
						showConfirmInformation(null, "The operation is failed");
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String
						.format("%s(error=%s)",
								errorNo == -1 ? "Connect to the server failed"
										: strMsg, errorNo);
				TLog.log(msg);
				showLongToast(msg);
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});
		// -------------------------

	}

	private void watch_shutdown(String device_id) {

		// -----------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		// final String device_id = AppContext.getInstance().getProperty(
		// "user.device_id");
		// final String family_id = AppContext.getDefaultFamilyId();
		// -------------------------
//		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");// 设置日期格式
		String wtime = df.format(new Date());
		final String request_url = KidsWatApiUrl.getUrlFor___watch_shutdown(
				uid, access_token, device_id, wtime);

		// -------------------------

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));

				try {
					JSONObject response = new JSONObject(t);

					int code = response.getInt("error");
					if (code == 0) {
						// response={"data":{"mode":2},"error":0,"info":{"message":"get success!"}}
						showLongToast("Shutdown successfully");
					} else if (code == 2015001) {
						// dismissDialog("Device is in sleep mode.", 800);
						dismissDialog(null, 10);
						showLongToast("You can not do this when the device is under sleep mode.");
					} else {
						showLongToast("Shutdown failed");
					}

				} catch (JSONException e) {
					e.printStackTrace();
					showLongToast("Shutdown failed");
				}
			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String
						.format("%s(error=%s)",
								errorNo == -1 ? "Connect to the server failed"
										: strMsg, errorNo);
				TLog.log(msg);
				showLongToast(msg);
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});

	}

	private void delete_watch(final String device_id) {

		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		final String version = AppContext.getInstance().currentFamily.version;
		// final String device_id = AppContext.getInstance().getProperty(
		// "user.device_id");
		// final String family_id = AppContext.getDefaultFamilyId();
		// -------------------------
		final String request_url = KidsWatApiUrl.getUrlFor___delete_watch(uid,
				access_token, device_id, version);

		// -------------------------

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));

				try {
					JSONObject response = new JSONObject(t);

					int code = response.getInt("error");
					if (code == 0) {
						SqlDb db = SqlDb.get(getActivity());
						db.clearMessage(uid);
						db.closeDb();

						showLongToast("The device has been discarded！");

						DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {

								if (AppContext.getInstance()
										.getFamilyListCount() > 0) {
									EventBus.getDefault()
											.post(new BaseEvent(
													BaseEvent.MSGTYPE_1___RELOGIN,
													"relogin"));
								} else {
									// Intent intent = new Intent(
									// KidsProfileActivity.this,
									// MainActivity.class);
									// startActivity(intent);
									AppManager.AppRestart(getActivity());
									getActivity().finish();
								}

							}
						};
						String title = "Kid's Watcher";
						String messagex = "The device has been discarded！";
						String positiveText = "OK";
						String negativeText = null;

						showConfirmDialog(title, messagex, positiveText,
								negativeText, positiveListener);

					} else {
						// showConfirmInformation(null, "Discard Fails");
						showConfirmInformation(
								null,
								response.getJSONObject("info").getString(
										"message"));
						// Toast.makeText(
						// getActivity(),
						// response.getJSONObject("info").getString(
						// "message"), Toast.LENGTH_SHORT).show();
						//
					}

				} catch (JSONException e) {
					e.printStackTrace();

				}
			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				if (errorNo == 500) {
					getActivity().finish();
					EventBus.getDefault().post(
							new BaseEvent(BaseEvent.MSGTYPE_1___RELOGIN,
									"relogin"));
				} else {
					String msg = String.format("%s(error=%s)",
							errorNo == -1 ? "Connect to the server failed"
									: strMsg, errorNo);
					TLog.log(msg);
					showLongToast(msg);
				}
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});
		// -------------------------

	}

	private void user_quit_from_family(String family_id) {

		// -----------------
		final String uid = KidsWatConfig.getUserUid();
		final String access_token = KidsWatConfig.getUserToken();
		final String version = AppContext.getInstance().currentFamily.version;
		final String device_id = AppContext.getInstance().currentFamily.device_id;
		// final String device_id = AppContext.getInstance().getProperty(
		// "user.device_id");
		// final String family_id = AppContext.getDefaultFamilyId();
		// -------------------------
		final String request_url = KidsWatApiUrl
				.getUrlFor___user_quit_from_family(uid, access_token,
						family_id, version, device_id);

		// -------------------------

		ApiHttpClient.get(request_url, new HttpCallBack() {

			@Override
			public void onPreStart() {
				showWaitDialog("...Загрузка...");
			}

			@Override
			public void onSuccess(String t) {
				super.onSuccess(t);
				TLog.log(String.format("url:%s\nt:%s", request_url, t));

				try {
					JSONObject response = new JSONObject(t);

					int code = response.getInt("error");
					if (code == 0) {
						// getActivity().finish();
						DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								if (AppContext.getInstance()
										.getFamilyListCount() > 0) {
									EventBus.getDefault()
											.post(new BaseEvent(
													BaseEvent.MSGTYPE_1___RELOGIN,
													"relogin"));
								} else {
									// Intent intent = new Intent(
									// KidsProfileActivity.this,
									// MainActivity.class);
									// startActivity(intent);
									AppManager.AppRestart(getActivity());
									getActivity().finish();
								}

							}
						};
						String title = "Kid's Watcher";
						String messagex = "You have stopped following the device.";
						String positiveText = "OK";
						String negativeText = null;

						showConfirmDialog(title, messagex, positiveText,
								negativeText, positiveListener);

					} else {
						showLongToast("stop following fails.");
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(int errorNo, String strMsg) {
				super.onFailure(errorNo, strMsg);
				String msg = String
						.format("%s(error=%s)",
								errorNo == -1 ? "Connect to the server failed"
										: strMsg, errorNo);
				TLog.log(msg);
				showLongToast(msg);
			}

			@Override
			public void onFinish() {
				dismissDialog(null, 10);
			}
		});
		// -------------------------

	}

	// -----------------

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		BaseEvent event = new BaseEvent(
				BaseEvent.MSGTYPE_3___CHANGE_KIDS_PHOTO, "change_image");
		EventBus.getDefault().post(event);

	}

}
