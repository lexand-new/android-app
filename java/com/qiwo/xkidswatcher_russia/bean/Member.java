package com.qiwo.xkidswatcher_russia.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Member {

	@Expose
	@SerializedName("uid")
	private String uid;

	@Expose
	@SerializedName("family_id")
	private String family_id;

	@Expose
	@SerializedName("datatype")
	private String datatype;

	@Expose
	@SerializedName("group")
	private String group;

	@Expose
	@SerializedName("nickname")
	private String nickname;

	@Expose
	@SerializedName("birthday")
	private String birthday;

	@Expose
	@SerializedName("sex")
	private String sex;

	@Expose
	@SerializedName("height")
	private String height;

	@Expose
	@SerializedName("weight")
	private String weight;

	@Expose
	@SerializedName("grade")
	private String grade;

	@Expose
	@SerializedName("doc_type")
	private String doc_type;

	@Expose
	@SerializedName("img")
	private String img;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getFamily_id() {
		return family_id;
	}

	public void setFamily_id(String family_id) {
		this.family_id = family_id;
	}

	public String getDatatype() {
		return datatype;
	}

	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getDoc_type() {
		return doc_type;
	}

	public void setDoc_type(String doc_type) {
		this.doc_type = doc_type;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

}
