package com.qiwo.xkidswatcher_russia.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.qiwo.xkidswatcher_russia.util.SimpleDateUtil;

//"baby": {
//    "uid": 10000001, 
//    "group": 10000008, 
//    "family_id": "10000008", 
//    "img": "", 
//    "nickname": "czzzz", 
//    "birthday": "1421057310517", 
//    "sex": "0", 
//    "height": "1.07", 
//    "weight": 20.5, 
//    "grade": "0"
//}

public class Baby {

	@Expose
	@SerializedName("family_id")
	private String family_id;

	public String getFamily_id() {
		return family_id;
	}

	public void setFamily_id(String _family_id) {
		family_id = _family_id;
	}

	@Expose
	@SerializedName("img")
	private String base64_img;
	
	@Expose
	@SerializedName("img_path")
	private String img_path;

	public String getImg_path() {
		return img_path;
	}

	public void setImg_path(String img_path) {
		this.img_path = img_path;
	}

	public String getBase64Img() {
		return base64_img;
	}

	public void setBase64Img(String _img) {
		base64_img = _img;
	}

	@Expose
	@SerializedName("nickname")
	private String nickname;

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String _nickname) {
		nickname = _nickname;
	}

	@Expose
	@SerializedName("birthday")
	private long birthday;

	public long getBirthday() {
		return birthday;
	}

	public String getBirthdayText() {
		return SimpleDateUtil.convert2String(birthday * 1000,
				SimpleDateUtil.DATE_FORMAT);
	}

	public void setBirthday(long _birthday) {
		birthday = _birthday;
	}

	@Expose
	@SerializedName("sex")
	private int sex;

	public int getSex() {
		return sex;
	}

	public String getSexText() {
		return sex == 1 ? "boy" : "girl";
	}

	public void setSex(int _sex) {
		sex = _sex;
	}

	@Expose
	@SerializedName("height")
	private float height;

	public float getHeight() {
		return height;
	}

	public void setHeight(float _height) {
		height = _height;
	}

	@Expose
	@SerializedName("weight")
	private float weight;

	public float getWeight() {
		return weight;
	}

	public void setWeight(float _weight) {
		weight = _weight;
	}

	@Expose
	@SerializedName("grade")
	private String grade;

	public String getGrade() {
		return grade;
	}

	public void setGrade(String _grade) {
		grade = _grade;
	}

	// @Expose
	// @SerializedName("relation")
	// private String relation;
	//
	// public String getRelation() {
	// return relation;
	// }
	//
	// public void setRelation(String _relation) {
	// relation = _relation;
	// }

	@Expose
	@SerializedName("uid")
	private String uid;

	public String getUid() {
		return uid;
	}

	public void setUid(String _uid) {
		uid = _uid;
	}

}
