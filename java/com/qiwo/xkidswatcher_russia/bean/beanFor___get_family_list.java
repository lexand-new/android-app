package com.qiwo.xkidswatcher_russia.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class beanFor___get_family_list {

	@Expose
	@SerializedName("error")
	private int error;

	@Expose
	@SerializedName("info")
	private FamilyListInfo info;

	public FamilyListInfo getInfo() {
		return info;
	}

	public void setInfo(FamilyListInfo info) {
		this.info = info;
	}

	public int getError() {
		return error;
	}

	public void setError(int error) {
		this.error = error;
	}

}
