package com.qiwo.xkidswatcher_russia.bean;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//{
//    "error": 0, 
//    "info": {
//        "data": {
//            "total_rows": 0, 
//            "rows": [ ]
//        }, 
//        "message": "Operation success!"
//    }
//}

//{
//    "error": 0, 
//    "info": {
//        "message": "Operation success!", 
//        "data": {
//            "total_rows": 1, 
//            "rows": [
//                {
//                    "family_id": 10000005, 
//                    "device_id": "0000000054d1885a", 
//                    "type": 2, 
//                    "address": "2104 Ren Min Lu, Baoan Qu, Shenzhen Shi, Guangdong Sheng, China", 
//                    "time": 1422994035, 
//                    "reason": "0", 
//                    "latitude": 22.6311126, 
//                    "longitude": 114.024665, 
//                    "electricity": "74", 
//                    "position_method": "1", 
//                    "motion_time": "7140", 
//                    "step_number": "0", 
//                    "calory": "0"
//                }
//            ]
//        }
//    }
//}

public class beanFor___get_watch_data {

	@Expose
	@SerializedName("error")
	public int error;

	@Expose
	@SerializedName("info")
	public CInfo info;

	public static class CInfo {
		@Expose
		@SerializedName("message")
		public String message;

		@Expose
		@SerializedName("data")
		public CData ddata;

	}

	public static class CData {

		@Expose
		@SerializedName("total_rows")
		public int total_rows;

		@Expose
		@SerializedName("rows")
		public List<CRows> rows;

		private int totalStep = 0;
		private int totalCalc = 0;

		private void calcStepCalc() {
			for (CRows cr : rows) {
				totalStep += cr.step_number;
				totalCalc += cr.calory;
			}
		}

		public int getTotalStep() {
			if (totalStep == 0)
				calcStepCalc();
			return totalStep;
		}

		public int getTotalCal() {
			if (totalCalc == 0)
				calcStepCalc();
			return totalCalc;
		}

	}

	public static class CRows implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -6381626991068561744L;

		@Expose
		@SerializedName("family_id")
		public String family_id;

		@Expose
		@SerializedName("device_id")
		public String device_id;

		@Expose
		@SerializedName("type")
		public int type;

		@Expose
		@SerializedName("address")
		public String address;

		@Expose
		@SerializedName("time")
		public long time;

		public long getLocalTime() {
			// return (time * 1000 + TimeZone.getDefault().getRawOffset()) /
			// 1000;
			return time;
		}

		@Expose
		@SerializedName("reason")
		public int reason;

		@Expose
		@SerializedName("latitude")
		public double latitude;

		@Expose
		@SerializedName("longitude")
		public double longitude;

		@Expose
		@SerializedName("electricity")
		public int electricity;

		@Expose
		@SerializedName("position_method")
		public int position_method;

		@Expose
		@SerializedName("motion_time")
		public int motion_time;

		@Expose
		@SerializedName("step_number")
		public int step_number;

		@Expose
		@SerializedName("calory")
		public int calory;

	}

}
