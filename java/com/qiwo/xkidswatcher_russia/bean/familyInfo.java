package com.qiwo.xkidswatcher_russia.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class familyInfo {

	// "info": {
	// "message": "Successful operation!",
	// "data": {
	// "device_id": "0000000000989686"
	// },
	// "baby": {
	// "uid": 10000001,
	// "group": 10000008,
	// "family_id": "10000008",
	// "img": "",
	// "nickname": "czzzz",
	// "birthday": "1421057310517",
	// "sex": "0",
	// "height": "1.07",
	// "weight": 20.5,
	// "grade": "0"
	// },
	// "relation": "sister"
	// }

	@Expose
	@SerializedName("version")
	private String version;

	@Expose
	@SerializedName("relation")
	private String relation;

	@Expose
	@SerializedName("data")
	private Device ddata;

	@Expose
	@SerializedName("message")
	private String message;

	@Expose
	@SerializedName("baby")
	private Baby baby;
	
	// "w461_device_country_code": "86",
	// "w461_device_sim_no": "18126165260"

	@Expose
	@SerializedName("w461_device_country_code")
	private String w461_device_country_code;

	@Expose
	@SerializedName("w461_device_sim_no")
	private String w461_device_sim_no;

	public Baby getBaby() {
		return baby;
	}

	public void setBaby(Baby baby) {
		this.baby = baby;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Device getDdata() {
		return ddata;
	}

	public void setDdata(Device ddata) {
		this.ddata = ddata;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getW461_device_country_code() {
		return w461_device_country_code;
	}

	public void setW461_device_country_code(String w461_device_country_code) {
		this.w461_device_country_code = w461_device_country_code;
	}

	public String getW461_device_sim_no() {
		return w461_device_sim_no;
	}

	public void setW461_device_sim_no(String w461_device_sim_no) {
		this.w461_device_sim_no = w461_device_sim_no;
	}

}
