package com.qiwo.xkidswatcher_russia.bean;

public class beanForDb___Family {
	public String family_id;
	public String device_id;
	public String create_uid;
	public String base64_img;
	public String img_path;
	public String nickname;
	public long birthday;
	public String birthdayText;
	public int sex;
	public String sexText;
	public float height;
	public float weight;
	public int grade;
	public String gradeText;

	public String sim;
	public String qrcode;
	public String verify;
	public String hard_code;
	public String device_key;
	public String bt_addr;
	public String bt_addr_ex;
	public long active_time;
	public String bt_addr_pwd;
	public String valid_date;
	public String last_record_no;
	public int d_mode = 0;
	public String version;
	public String device_iccid;
}
