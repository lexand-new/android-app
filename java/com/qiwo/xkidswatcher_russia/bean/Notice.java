package com.qiwo.xkidswatcher_russia.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notice {

	// "content_type": 6,//消息类型
	// "content_type_text": "Recording", //消息文字说明
	// "time": 1428586413, //消息记录时间
	// "content": "Baby is at Qiwo Rd, Qiwo City", //列表显示的副标题
	// "parameter": {//每种类型所需参数
	// "voice_id": "111111111", //获取录音时用,如需要其他参数,一并列出
	// "location": {"longitude":112," latitude":21},//定位经纬度
	// "location_time": 1428586413, //定位时间,utc标准时间
	// "location_type": "gps",//定位类型
	// "des": ""//点开消息后的详情显示处说明文字
	@Expose
	@SerializedName("json")
	public String json;

	@Expose
	@SerializedName("content_type")
	public int content_type = -100;

	@Expose
	@SerializedName("content_type_text")
	public String content_type_text;

	@Expose
	@SerializedName("message_id")
	public String message_id;

	@Expose
	@SerializedName("time")
	public long time;

	@Expose
	@SerializedName("content")
	public String content;

	@Expose
	@SerializedName("parameter")
	public Parameter parameter;

	public static class Parameter {
		@Expose
		@SerializedName("voice_id")
		public String voice_id;

		@Expose
		@SerializedName("Location")
		public Location location;

		@Expose
		@SerializedName("location_time")
		public long location_time;

		@Expose
		@SerializedName("location_type")
		public String location_type;

		@Expose
		@SerializedName("des")
		public String des;

	}

	public static class Location {
		@Expose
		@SerializedName("longitude")
		public double longitude;

		@Expose
		@SerializedName("latitude")
		public double latitude;

	}

	public String getTimeText() {
		String date = new java.text.SimpleDateFormat("MM-dd HH:mm")
				.format(new java.util.Date(time * 1000));
		return date;
	}

}
