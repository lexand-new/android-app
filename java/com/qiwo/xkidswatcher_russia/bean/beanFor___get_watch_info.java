package com.qiwo.xkidswatcher_russia.bean;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//{
//    "error": 0, 
//    "info": {
//        "message": "Successful operation!", 
//        "data": {
//            "uid": 10000001, 
//            "qrcode": "85e9f1877fae7ab1", 
//            "sim": "13049489420", 
//            "add_time": 1421057333, 
//            "verify": 5729, 
//            "hard_code": "a1300456", 
//            "device_key": "7afc3a1e789673fe", 
//            "status": 1, 
//            "device_id": "0000000000989686", 
//            "family_id": null, 
//            "bt_addr": "53UgayeL8ZYSOzz/"
//        }
//    }
//}

//{
//    "error": 0,
//    "info": {
//        "message": "Successful operation!",
//        "data": {
//            "uid": 10000007,
//            "qrcode": "ac0a0a8c8333b3ed",
//            "device_id": "00000000551bbcb6",
//            "sim": null,
//            "verify": 1259,
//            "hard_code": "a1330941",
//            "device_key": "80bd68901ccd9f38",
//            "status": 1,
//            "bt_addr": "wHNp/j94zpc1Hfd5",
//            "active_time": 1427881145,
//            "bt_addr_pwd": "00000000551bbcb9"
//        }
//    }
//}

public class beanFor___get_watch_info {

	@Expose
	@SerializedName("error")
	public int error;

	@Expose
	@SerializedName("info")
	public CInfo info;

	public static class CInfo {
		@Expose
		@SerializedName("message")
		public String message;

		@Expose
		@SerializedName("total")
		public int total;

		@Expose
		@SerializedName("data")
		public CData ddata;

	}

	public static class CData implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -6381626991068901744L;

		@Expose
		@SerializedName("uid")
		public String uid;

		@Expose
		@SerializedName("qrcode")
		public String qrcode;

		@Expose
		@SerializedName("sim")
		public String sim;

		@Expose
		@SerializedName("add_time")
		public long add_time;
		
		@Expose
		@SerializedName("active_time")
		public long active_time;

		@Expose
		@SerializedName("verify")
		public int verify;

		@Expose
		@SerializedName("hard_code")
		public String hard_code;

		@Expose
		@SerializedName("device_key")
		public String device_key;

		@Expose
		@SerializedName("status")
		public int status;

		@Expose
		@SerializedName("device_id")
		public String device_id;

		@Expose
		@SerializedName("family_id")
		public String family_id;

		@Expose
		@SerializedName("bt_addr")
		public String bt_addr;

		public String getBleAdd() {
			if (bt_addr != null && bt_addr.length() == 12) {
				String bleAdd = bt_addr.toUpperCase();
				String b1 = bleAdd.substring(0, 2);
				String b2 = bleAdd.substring(2, 4);
				String b3 = bleAdd.substring(4, 6);
				String b4 = bleAdd.substring(6, 8);
				String b5 = bleAdd.substring(8, 10);
				String b6 = bleAdd.substring(10, 12);

				String bleAddx = String.format("%s:%s:%s:%s:%s:%s", b1, b2, b3,
						b4, b5, b6);
				return bleAddx;
				// F8:35:DD:85:6B:78
			} else {
				return "";
			}
		}

		@Expose
		@SerializedName("bt_addr_pwd")
		public String bt_addr_pwd;

	}

}
