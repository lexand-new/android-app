package com.qiwo.xkidswatcher_russia.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.qiwo.xkidswatcher_russia.KidsWatConfig;

public class FamilyMemberMsg implements Comparable<FamilyMemberMsg> {

	@Expose
	@SerializedName("uid")
	public String uid;

	@Expose
	@SerializedName("admin")
	public Integer admin;

	// 1为家许成员,2为消息
	@Expose
	@SerializedName("msg_type")
	public int msg_type_xx;

	@Expose
	@SerializedName("status")
	public int status;

	@Expose
	@SerializedName("relation")
	public String relation;

	@Expose
	@SerializedName("country_code")
	public String country_code;

	@Expose
	@SerializedName("mobile")
	public String mobile;
	// -----------
	// family_id, status, mobile, relation,
	// admin, msg_type

	@Expose
	@SerializedName("family_id")
	public String family_id;

	@Expose
	@SerializedName("group_uid")
	public String group_uid;
	
	@Expose
	@SerializedName("image_path")
	public String image_path;

	@Expose
	@SerializedName("msg_type")
	public int msg_type;

	/*public int compareTo(FamilyMemberMsg arg0) {
		if(arg0.mobile.equals(KidsWatConfig.getUserPhone())){
			return Integer.MIN_VALUE;
		}else {
			return arg0.relation.compareTo(this.relation);
		}
	}*/

	public int compareTo(FamilyMemberMsg arg0) {
//		int i = this.mobile.equals(KidsWatConfig.getUserPhone()) ? 0 : 1;
//		int j = arg0.mobile.equals(KidsWatConfig.getUserPhone()) ? 0 : 1;
//		return i - j; 
		if(this.mobile.equals(KidsWatConfig.getUserPhone())){
			return Integer.MAX_VALUE;
		} else if(arg0.mobile.equals(KidsWatConfig.getUserPhone())){
			return Integer.MIN_VALUE;
		} else {
			return arg0.relation.compareTo(this.relation);
		}
		
	}
}
