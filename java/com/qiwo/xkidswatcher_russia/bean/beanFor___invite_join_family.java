package com.qiwo.xkidswatcher_russia.bean;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


//{
//    "error": 0, 
//    "info": {
//        "message": "Successful operation!", 
//        "total": 1, 
//        "data": [
//            {
//                "datatype": "group", 
//                "status": 1, 
//                "family_id": 10000008, 
//                "uid": 10000001, 
//                "admin": 1, 
//                "relation": "grandmother", 
//                "mobile": "13428795901", 
//                "msg_type": 0
//            }
//        ]
//    }
//}

public class beanFor___invite_join_family {

	@Expose
	@SerializedName("error")
	public int error;

	@Expose
	@SerializedName("info")
	public CInfo info;

	public static class CInfo {
		@Expose
		@SerializedName("message")
		public String message;

		@Expose
		@SerializedName("total")
		public int total;

		@Expose
		@SerializedName("data")
		public List<CData> ddata;

	}

	public static class CData implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -6011083663778480679L;

		@Expose
		@SerializedName("datatype")
		public String datatype;

		@Expose
		@SerializedName("family_id")
		public String family_id;

		@Expose
		@SerializedName("status")
		public int status;

		@Expose
		@SerializedName("admin")
		public int admin;

		@Expose
		@SerializedName("uid")
		public String uid;

		@Expose
		@SerializedName("relation")
		public String relation;

		@Expose
		@SerializedName("mobile")
		public String mobile;

		@Expose
		@SerializedName("msg_type")
		public int msg_type;

	}

}
