package com.qiwo.xkidswatcher_russia.bean;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//{
//    "error": 0, 
//    "info": "Success", 
//    "data": [
//        {
//            "value": {
//                "app_uids": [
//                    {
//                        "push_id": "4155446272920082709:962788698702806537", 
//                        "plat": "1", 
//                        "to_uid": "10000002", 
//                        "from_uid": "10000001", 
//                        "really_push": 1
//                    }
//                ], 
//                "message_type": 0, 
//                "expire": 180, 
//                "title": "refuse_join_family", 
//                "content": "13631699451 has refused to join the family!", 
//                "detail": "13631699451 has refused to join the family!", 
//                "custom_content": {
//                    "detail": {
//                        "detail": "13631699451 has refused to join the family!"
//                    }, 
//                    "content_type": 6
//                }, 
//                "apns_mode": 0, 
//                "time": 1428576109, 
//                "success": 1, 
//                "push_id": "4155446272920082709:962788698702806537", 
//                "really_push": 1, 
//                "from_uid": "10000001", 
//                "to_uid": "10000002"
//            }, 
//            "doc_id": "notification:app:users:b0134682054b65b2029ad3a01f328498"
//        }
//    ]
//}

public class beanFor___get_notice_list {

	@Expose
	@SerializedName("error")
	public int error;

	@Expose
	@SerializedName("info")
	public CInfo info;

	public static class CInfo {
		@Expose
		@SerializedName("message")
		public String message;

		@Expose
		@SerializedName("total")
		public int total;

		@Expose
		@SerializedName("data")
		public List<CData> ddata;

	}

	public static class CData implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -5089860053620074509L;

		@Expose
		@SerializedName("datatype")
		public String datatype;

		@Expose
		@SerializedName("family_id")
		public String family_id;

		@Expose
		@SerializedName("status")
		public int status;

		@Expose
		@SerializedName("admin")
		public int admin;

		@Expose
		@SerializedName("uid")
		public String uid;

		@Expose
		@SerializedName("relation")
		public String relation;

		@Expose
		@SerializedName("mobile")
		public String mobile;

		@Expose
		@SerializedName("msg_type")
		public int msg_type;

	}

}
