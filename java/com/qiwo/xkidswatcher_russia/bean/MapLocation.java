package com.qiwo.xkidswatcher_russia.bean;

import com.google.android.gms.maps.model.LatLng;

public class MapLocation {

	private String address;
	private LatLng latLng;

	public MapLocation(LatLng _latLng, String _address) {
		latLng = _latLng;
		address = _address;
	}

	public MapLocation(double latitude, double longitude, String _address) {
		this(new LatLng(latitude, longitude), _address);
		// ;LatLng xlng = new LatLng(latitude, longitude);
	}

	public String getAddress() {
		return address;
	}

	public LatLng getLatLng() {
		return latLng;
	}

}
