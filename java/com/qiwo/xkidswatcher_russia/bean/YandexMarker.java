package com.qiwo.xkidswatcher_russia.bean;

import android.graphics.drawable.Drawable;
import ru.yandex.yandexmapkit.utils.GeoPoint;

public class YandexMarker {
	private Drawable icon;
	
	private String desc;
	
	private GeoPoint geoPoint;
	
	public GeoPoint getGeoPoint(){
		return this.geoPoint;
	}
	
	//setIcon(KidsWatUtils.getBabyImage(getActivity(), family_id, sex));
	public void setIcon(Drawable img){
		this.icon = img;
	}
}
