package com.qiwo.xkidswatcher_russia.bean;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//"info": {
//    "data": [
//        {
//            "uid": 10000001, 
//            "status": 1, 
//            "admin": 1, 
//            "msg_type": 0, 
//            "relation": "sister", 
//            "family_id": 10000008, 
//            "datatype": "group", 
//            "mobile": "13428795901"
//        }
//    ], 
//    "total": 1, 
//    "message": "Successful operation!"
//}


public class FamilyListInfo {

	@Expose
	@SerializedName("data")
	private List<FamilyMember> ddata;

	public List<FamilyMember> getDdata() {
		return ddata;
	}

	public void setDdata(List<FamilyMember> ddata) {
		this.ddata = ddata;
	}

	@Expose
	@SerializedName("total")
	private int total;

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	@Expose
	@SerializedName("message")
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}



}
