package com.qiwo.xkidswatcher_russia.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class beanFor___get_watch_data_latest {
	@Expose
	@SerializedName("error")
	public int error;
	
	@Expose
	@SerializedName("info")
	public CInfo info;
	
	public class CInfo{
		@Expose
		@SerializedName("message")
		public String message;
		
		@Expose
		@SerializedName("lastest_point")
		public CData lastest_point;
		
		public class CData{
			@Expose
			@SerializedName("position_time")
			public String position_time;
			
			@Expose
			@SerializedName("longitude")
			public double longitude;
			
			@Expose
			@SerializedName("latitude")
			public double latitude;
			
			@Expose
			@SerializedName("electricity")
			public int electricity;
			
			@Expose
			@SerializedName("electricity_status")
			public int electricity_status;
			
			@Expose
			@SerializedName("address")
			public String address;
			
			@Expose
			@SerializedName("type")
			public int type;
			
			@Expose
			@SerializedName("reason")
			public int reason;
			
			@Expose
			@SerializedName("step_number")
			public int step_number;
			
			@Expose
			@SerializedName("calory")
			public int calory;
			
			@Expose
			@SerializedName("motion_time")
			public long motion_time;
		}
	}
	
}
