package com.qiwo.xkidswatcher_russia.thread;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

public class ThreadParent implements Runnable {

	private String StrJson, URL;
	private Handler handler;
	private int Code;

	public ThreadParent(String URL, String StrJson, Handler handler, int Code) {
		this.URL = URL;
		this.StrJson = StrJson;
		this.handler = handler;
		this.Code = Code;

		Log.e("", "StrJson:" + StrJson);
	}

	@Override
	public void run() {
		Looper.prepare();
		Message mess = new Message();
		mess.obj = HttpUtil.getObjectResult(URL, StrJson);
		mess.arg1 = Code;
		handler.sendMessage(mess);
		Looper.loop();
	}
}
