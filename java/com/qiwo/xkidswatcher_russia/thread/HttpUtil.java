package com.qiwo.xkidswatcher_russia.thread;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.CookieStore;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import android.util.Log;

public class HttpUtil {

	/**
	 * 
	 */
	private static final String TAG = "HttpUtil";

	private static DefaultHttpClient httpClient;
	public static CookieStore cookieStore;

	static {
		if (httpClient == null) {
			httpClient = createHttpClient();
		}
	}

	private static DefaultHttpClient createHttpClient() {
		Log.d(TAG, "createHttpClient");
		HttpParams params = new BasicHttpParams();

		return new DefaultHttpClient(params);
	}

	/*
	 * 
	 */

	public static String SendRequest(String adress_Http, String strJson) {

		String returnLine = "";
		try {

			System.out.println("**************锟斤拷始http通讯**************");
			System.out.println("**************锟斤拷锟矫的接口碉拷址为**************"
					+ adress_Http);
			System.out.println("**************锟斤拷锟斤拷锟酵碉拷锟斤拷锟斤拷为**************"
					+ strJson);
			URL my_url = new URL(adress_Http);
			HttpURLConnection connection = (HttpURLConnection) my_url
					.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("POST");
			connection.setConnectTimeout(5000);
			connection.setReadTimeout(5000);
			connection.setUseCaches(false);
			connection.setInstanceFollowRedirects(true);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.connect();
			DataOutputStream out = new DataOutputStream(
					connection.getOutputStream());

			byte[] content = strJson.getBytes("utf-8");

			out.write(content, 0, content.length);
			out.flush();
			out.close(); // flush and close

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					connection.getInputStream(), "utf-8"));

			// StringBuilder builder = new StringBuilder();

			String line = "";

			System.out.println("Contents of post request start");

			while ((line = reader.readLine()) != null) {
				// line = new String(line.getBytes(), "utf-8");
				returnLine += line;
				System.out.println(line);
			}
			System.out.println("Contents of post request ends");

			reader.close();
			connection.disconnect();
			System.out.println("========锟斤拷锟截的斤拷锟斤拷锟轿�=======" + returnLine);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnLine;
	}

	/**
	 * 正常HTTP交互
	 * 
	 * @param url
	 * @param StrJson
	 * @return
	 */
	public static String getObjectResult(String url, String StrJson) {
		String result = "";
		InputStream is = null;
		Log.e(url, StrJson);
		try {

			HttpPost httppost = new HttpPost(url);
			httppost.setHeader("content-type", "application/json");
			if (StrJson != null) {
				httppost.setEntity(new StringEntity(StrJson, "utf-8"));
			}

			HttpResponse response = (HttpResponse) getNewHttpClient().execute(
					httppost);

			HttpEntity entity = response.getEntity();
			is = entity.getContent();

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "utf-8"));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			is.close();
			result = sb.toString();
			Log.i("getResult", result);
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		}
		return result;
	}

	/**
	 * 绕过HTTPS验证
	 * 
	 * @param url
	 * @param StrJson
	 * @return
	 */
	public static String getObjectResult4(String url, String StrJson) {
		String result = "";
		InputStream is = null;
		Log.e(url, StrJson);
		try {

			HttpPost httppost = new HttpPost(url);
			httppost.setHeader("content-type", "application/json");
			if (StrJson != null) {
				httppost.setEntity(new StringEntity(StrJson, "utf-8"));
			}

			HttpResponse response = (HttpResponse) getNewHttpClient4().execute(
					httppost);

			HttpEntity entity = response.getEntity();
			is = entity.getContent();

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "utf-8"));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			is.close();
			result = sb.toString();
			Log.i("getResult", result);
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		}
		return result;
	}

	public static HttpClient getNewHttpClient() {
		try {
			KeyStore trustStore = KeyStore.getInstance(KeyStore
					.getDefaultType());
			trustStore.load(null, null);

			SSLSocketFactory sf = new SSLSocketFactoryEx(trustStore);
			sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			HttpParams params = new BasicHttpParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
			HttpConnectionParams.setConnectionTimeout(params, 20000);
			HttpConnectionParams.setSoTimeout(params, 20000);

			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory
					.getSocketFactory(), 80));
			registry.register(new Scheme("https", sf, 443));

			ClientConnectionManager ccm = new ThreadSafeClientConnManager(
					params, registry);

			return new DefaultHttpClient(ccm, params);
		} catch (Exception e) {
			return new DefaultHttpClient();
		}
	}

	public static HttpClient getNewHttpClient4() {
		try {
			KeyStore trustStore = KeyStore.getInstance(KeyStore
					.getDefaultType());
			trustStore.load(null, null);

			SSLSocketFactory sf = new SSLSocketFactoryEx(trustStore);
			sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			HttpParams params = new BasicHttpParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
			HttpConnectionParams.setConnectionTimeout(params, 4000);
			HttpConnectionParams.setSoTimeout(params, 4000);

			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory
					.getSocketFactory(), 80));
			registry.register(new Scheme("https", sf, 443));

			ClientConnectionManager ccm = new ThreadSafeClientConnManager(
					params, registry);

			return new DefaultHttpClient(ccm, params);
		} catch (Exception e) {
			return new DefaultHttpClient();
		}
	}

	/**
	 * get
	 */

	public static DefaultHttpClient getHttpClient() {
		if (cookieStore != null) {
			httpClient
					.setCookieStore((org.apache.http.client.CookieStore) cookieStore);
		}
		return httpClient;
	}

	public static String getStrResult(String url) {
		String result = "";
		InputStream is = null;

		Log.e("", "URL:" + url);
		try {
			DefaultHttpClient httpclient = getHttpClient();
			HttpPost httppost = new HttpPost(url);
			// httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
			// "UTF-8"));
			HttpResponse response = httpclient.execute(httppost);

			// if (httpclient.getCookieStore() != null) {
			// cookieStore = (CookieStore) httpclient.getCookieStore();
			// }
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "utf-8"));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			is.close();
			result = sb.toString();
			Log.e("getResult", result);
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		}
		return result;
	}

}
