package com.qiwo.xkidswatcher_russia.thread;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * A custom SSL Socket Factory class used to bypass the certificate verification
 * for {@link org.apache.http.client.HttpClient}. reference url:
 * http://stackoverflow.com/a/4837230
 */
public class SSLSocketFactory extends org.apache.http.conn.ssl.SSLSocketFactory {

	SSLContext sslContext = SSLContext.getInstance("TLS");

	public SSLSocketFactory(KeyStore truststore)
			throws NoSuchAlgorithmException, KeyManagementException,
			KeyStoreException, UnrecoverableKeyException {
		super(truststore);

		TrustManager tm = new X509TrustManager() {
			@Override
			public void checkClientTrusted(X509Certificate[] x509Certificates,
					String s) throws CertificateException {

			}

			@Override
			public void checkServerTrusted(X509Certificate[] x509Certificates,
					String s) throws CertificateException {
				// 是否信任服务器
				Principal principal = null;
				boolean isOK = false;
				for (X509Certificate x509Certificate : x509Certificates) {
					principal = x509Certificate.getSubjectDN();
					if (principal != null
							&& (principal.getName().indexOf("www.oschina.net") != -1)) {
						isOK = true;
						break;
					}
				}
				if (isOK == false) {
					return;
				}
			}

			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}
		};
		sslContext.init(null, new TrustManager[] { tm }, null);
	}

	@Override
	public Socket createSocket(Socket socket, String host, int port,
			boolean autoClose) throws IOException, UnknownHostException {
		return sslContext.getSocketFactory().createSocket(socket, host, port,
				autoClose);
	}

	@Override
	public Socket createSocket() throws IOException {
		return sslContext.getSocketFactory().createSocket();
	}
}
