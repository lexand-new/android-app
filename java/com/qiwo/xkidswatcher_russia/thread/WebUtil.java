package com.qiwo.xkidswatcher_russia.thread;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

public class WebUtil {

	private static final String TAG = "WebUtils";
	private static DefaultHttpClient httpClient;
	public static int timeoutConnection = 8000;
	public static int timeoutSocket = 10000;
	public static CookieStore cookieStore;

	static {
		if (httpClient == null) {
			httpClient = createHttpClient();
		}
	}

	public static DefaultHttpClient getHttpClient() {
		if (cookieStore != null) {
			httpClient.setCookieStore(cookieStore);
		}
		return httpClient;
	}

	public static JSONArray getResult(String url,
			ArrayList<NameValuePair> nameValuePairs) {
		JSONArray json = null;
		String result = "";
		InputStream is = null;
		Log.e(url, String.valueOf(nameValuePairs));
		try {
			DefaultHttpClient httpclient = getHttpClient();
			HttpPost httppost = new HttpPost(url);
			if (nameValuePairs != null) {
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
						"UTF-8"));
			}
			HttpResponse response = httpclient.execute(httppost);

			if (httpclient.getCookieStore() != null) {
				cookieStore = httpclient.getCookieStore();
			}
			HttpEntity entity = response.getEntity();
			is = entity.getContent();

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "utf-8"));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			is.close();
			result = sb.toString();
			json = new JSONArray(result);
			Log.e("getResult", result);
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		}
		return json;
	}

	public static String getResultReuturnStr(String url,
			ArrayList<NameValuePair> nameValuePairs) {
		String result = "";
		InputStream is = null;
		Log.e(url, String.valueOf(nameValuePairs));
		try {
			DefaultHttpClient httpclient = getHttpClient();
			HttpPost httppost = new HttpPost(url);
			if (nameValuePairs != null) {
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
						"UTF-8"));
			}
			HttpResponse response = httpclient.execute(httppost);

			if (httpclient.getCookieStore() != null) {
				cookieStore = httpclient.getCookieStore();
			}
			HttpEntity entity = response.getEntity();
			is = entity.getContent();

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "utf-8"));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			is.close();
			result = sb.toString();
			// json = new JSONArray(result);
			Log.e("getResult", result);
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		}
		return result;
	}

	public static JSONObject getObjectResult(String url,
			ArrayList<NameValuePair> nameValuePairs) {
		JSONObject json = null;
		String result = "";
		InputStream is = null;
		Log.e(url, String.valueOf(nameValuePairs));
		try {
			DefaultHttpClient httpclient = getHttpClient();
			HttpPost httppost = new HttpPost(url);
			if (nameValuePairs != null) {
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
						"UTF-8"));
			}

			HttpResponse response = httpclient.execute(httppost);

			if (httpclient.getCookieStore() != null) {
				cookieStore = httpclient.getCookieStore();
			}
			HttpEntity entity = response.getEntity();
			is = entity.getContent();

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "utf-8"));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			is.close();
			result = sb.toString();
			json = new JSONObject(result);
			Log.i("getResult", result);
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		}
		return json;
	}

	public static JSONObject getResult(String url) {
		JSONObject json = null;
		String result = "";
		InputStream is = null;
		try {
			DefaultHttpClient httpclient = getHttpClient();
			HttpPost httppost = new HttpPost(url);
			// httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
			// "UTF-8"));
			HttpResponse response = httpclient.execute(httppost);

			if (httpclient.getCookieStore() != null) {
				cookieStore = httpclient.getCookieStore();
			}
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "utf-8"));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			is.close();
			result = sb.toString();
			json = new JSONObject(result);
			Log.i("getResult", result);
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		}
		return json;
	}

	public static String getStrResult(String url) {
		String result = "";
		InputStream is = null;

		Log.e("", "URL:" + url);
		try {
			DefaultHttpClient httpclient = getHttpClient();
			HttpPost httppost = new HttpPost(url);
			// httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,
			// "UTF-8"));
			HttpResponse response = httpclient.execute(httppost);

			if (httpclient.getCookieStore() != null) {
				cookieStore = httpclient.getCookieStore();
			}
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "utf-8"));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			is.close();
			result = sb.toString();
			Log.e("getResult", result);
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		}
		return result;
	}

	private static DefaultHttpClient createHttpClient() {
		Log.d(TAG, "createHttpClient");
		HttpParams params = new BasicHttpParams();
		return new DefaultHttpClient(params);
	}

}
