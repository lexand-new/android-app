package com.qiwo.xkidswatcher_russia.thread;

import java.util.Map;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

public class CommonThreadMethod {

	/**
	 * 格式化StrJSON
	 */

	public static String JsonFormat(Map<String, String> Str) {

		if (Str == null || Str.size() == 0)
			return null;

		Set<Map.Entry<String, String>> entrySet = Str.entrySet();

		JSONObject StrJson = new JSONObject();
		try {
			for (Map.Entry<String, String> entry : entrySet) {
				StrJson.put(entry.getKey(), entry.getValue());
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return StrJson.toString();
	}
}
