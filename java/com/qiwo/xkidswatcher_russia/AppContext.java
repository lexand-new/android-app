package com.qiwo.xkidswatcher_russia;


import java.io.File;

//import static com.qiwo.xkidswatcher.AppConfig.KEY_FAMILY_ID;
//import static com.qiwo.xkidswatcher.AppConfig.KEY_FRITST_START;
//import static com.qiwo.xkidswatcher.AppConfig.KEY_GCM_APP_ID;

import java.util.HashMap;
import java.util.Map;

import org.kymjs.kjframe.KJHttp;
import org.kymjs.kjframe.bitmap.BitmapConfig;
import org.kymjs.kjframe.http.HttpConfig;
import org.kymjs.kjframe.utils.KJLoger;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.qiwo.xkidswatcher_russia.api.ApiHttpClient;
import com.qiwo.xkidswatcher_russia.base.BaseApplication;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Family;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___LoginMember;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_family_info;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_family_list;
import com.qiwo.xkidswatcher_russia.util.TLog;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.support.multidex.MultiDex;

import ru.yandex.yandexmapkit.utils.GeoPoint;

/**
 * 
 */
public class AppContext extends BaseApplication {

	private static AppContext instance;
	private SharedPreferences.Editor mEditor;
	private SharedPreferences mSpSettingInfo;

	// ----------------
	// public static final int PAGE_SIZE = 20;
	// public static String loginUid;
	public static String google_gcm_appid = "";
	public static boolean isLogin;

	public beanFor___get_family_list family_list = null;
	//获取家庭圈信息，保存家庭圈信息的，全局
	public beanForDb___Family currentFamily = null;
	//没用
	public beanFor___get_family_info currentBeanFamily = null;
	// public List<beanForDb___Family> list_familyinfo = null;

	//家庭圈和手表的映射关系
	public Map<String, beanForDb___Family> map_familyinfo = null;
	public beanForDb___LoginMember loginUser_kid = null;
	
	public static ImageLoader imageLoader;// 处理图片
	public static DisplayImageOptions options;// 处理图片的参数
	public static DisplayImageOptions commenOptions;// 处理图片的参数
	
	public static int codess = 361;

	//保存设备的最新定位点
	public static GeoPoint lng = null;

	@Override
	protected void attachBaseContext(Context context) {
		super.attachBaseContext(context);
		MultiDex.install(this);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		init();
		kidsInit();
		
		// 图片加载
		initImageLoader();
	}

	private void init() {

//-------yandex appmetrica start----------	    
	    //Creating the extended initialization constructor
        YandexMetricaConfig.Builder configBuilder = YandexMetricaConfig.newConfigBuilder("a38dc216-9f0f-413e-be93-e4bfc3f3663f");
        //Setting necessary parameters (for example, enabling logging)
        configBuilder.setLogEnabled();
        //Creating an extended configuration object
        YandexMetricaConfig extendedConfig = configBuilder.build();
        // Initializing the AppMetrica SDK
        YandexMetrica.activate(getApplicationContext(), extendedConfig);
        // Tracking user activity
        YandexMetrica.enableActivityAutoTracking(this);
//-------yandex appmetrica end----------

        
		mSpSettingInfo = instance.getSharedPreferences("SettingInfo",
				Context.MODE_PRIVATE);
		mEditor = mSpSettingInfo.edit();

		HttpConfig.DEBUG = false;
		HttpConfig.CACHEPATH = "kidswatcher/cache";
		HttpConfig.TIMEOUT = 20000;
		// 初始化网络请求
		KJHttp client = new KJHttp();
		ApiHttpClient.setHttpClient(client);
		// Log控制器
		KJLoger.openDebutLog(false);
		TLog.DEBUG = BuildConfig.DEBUG;
		// Bitmap缓存地址
		BitmapConfig.CACHEPATH = "kidswatcher/imagecache";
	}

	private void kidsInit() {
		// list_familyinfo = new ArrayList<beanForDb___Family>();
		map_familyinfo = new HashMap<String, beanForDb___Family>();
	}

	public void clearUserInfo() {
		//
	}

	/**
	 * 获得当前app运行的AppContext
	 * 
	 * @return
	 */
	public static AppContext getInstance() {
		return instance;
	}

	public SharedPreferences getSharedPref() {
		return mSpSettingInfo;
	}

	public SharedPreferences.Editor getSharedPrefEditor() {
		return mEditor;
	}

	/**
	 * 获取App安装包信息
	 * 
	 * @return
	 */
	public PackageInfo getPackageInfo() {
		PackageInfo info = null;
		try {
			info = getPackageManager().getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace(System.err);
		}
		if (info == null)
			info = new PackageInfo();
		return info;
	}

	private Gson mGson;

	public Gson getGson() {
		if (mGson == null) {
			mGson = new Gson();
		}
		return mGson;
	}

	// ------------------------------------
	public int getFamilyListCount() {
		if (map_familyinfo != null) {
			return map_familyinfo.size();
		}
		return 0;
	}

	/**
	 * 判断当前版本是否兼容目标版本的方法
	 * 
	 * @param VersionCode
	 * @return
	 */
	public static boolean isMethodsCompat(int VersionCode) {
		int currentVersion = android.os.Build.VERSION.SDK_INT;
		return currentVersion >= VersionCode;
	}

	/**
	 * 初始化加载图片的工具
	 */
	private void initImageLoader() {
		File cacheDir = StorageUtils.getOwnCacheDirectory(instance, "imageloader/Cache");
		// 加载图片的工具以及参数
		imageLoader = ImageLoader.getInstance();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				getApplicationContext())
				.threadPriority(Thread.NORM_PRIORITY - 2)
				.denyCacheImageMultipleSizesInMemory()
				.memoryCache(new UsingFreqLimitedMemoryCache(2 * 1024 * 1024))
				.memoryCacheSize(2 * 1024 * 1024)
				.discCacheSize(50 * 1024 * 1024)
				.discCacheFileCount(100) //缓存的文件数量  
				.discCache(new UnlimitedDiscCache(cacheDir))//自定义缓存路径  
				.defaultDisplayImageOptions(DisplayImageOptions.createSimple())  
				.discCacheFileNameGenerator(new Md5FileNameGenerator())
				.tasksProcessingOrder(QueueProcessingType.LIFO)
				.build();
		imageLoader.init(config);
		
		options = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.my_user_photo) // 加载开始默认的图片
				.showImageForEmptyUri(R.drawable.my_user_photo) // url爲空会显示该图片，自己放在drawable里面的
				.showImageOnFail(R.drawable.my_user_photo) // 加载图片出现问题，会显示该图片
				.cacheInMemory(true) // 缓存用
				.cacheOnDisc(true) // 缓存用
				.displayer(new RoundedBitmapDisplayer(180)) // 图片圆角显示，值为整数
				.build();
		
		commenOptions = new DisplayImageOptions.Builder()
				.cacheInMemory(true) // 缓存用
				.cacheOnDisc(true) // 缓存用
//				.displayer(new RoundedBitmapDisplayer(180)) // 图片圆角显示，值为整数
				.build();
	}
	
}
