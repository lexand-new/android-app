package com.qiwo.xkidswatcher_russia.api;

import org.kymjs.kjframe.KJHttp;
import org.kymjs.kjframe.http.HttpCallBack;
import org.kymjs.kjframe.http.HttpConfig;
import org.kymjs.kjframe.http.HttpParams;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import com.qiwo.xkidswatcher_russia.util.TLog;

import android.widget.Toast;

public class ApiHttpClient {

	public static KJHttp client;

	public ApiHttpClient() {
	}

	public static KJHttp getHttpClient() {
		return client;
	}

	public static void cancelAll() {
		client.cancelAll();
	}

	public static void get(String url, HttpCallBack handler) {
		boolean isNetworkAvailable = KidsWatUtils.isNetworkAvailable(AppContext
				.getInstance());
		if (isNetworkAvailable) {
			// client.get(url, null, false, handler);
			client.get(url, null, false, handler);
			
			HttpConfig config = new HttpConfig();
			config.TIMEOUT = 1000 * 10;
			client.setConfig(config);
		} else {
			Toast.makeText(AppContext.getInstance(),
					"пожалуйста, проверьте подключение и попробуйте снова.",
					Toast.LENGTH_LONG).show();
		}

	}

	public static void log(String log) {
		TLog.log("Test", log);
	}

	public static void post(String url, HttpParams params, HttpCallBack handler) {
		
		boolean isNetworkAvailable = KidsWatUtils.isNetworkAvailable(AppContext
				.getInstance());
		if (isNetworkAvailable) {
			client.post(url, params, false, handler);
			log(new StringBuilder("POST ").append(url).append("&").append(params)
					.toString());
			
			HttpConfig config = new HttpConfig();
			config.TIMEOUT = 1000 * 20;
			client.setConfig(config);
		} else {
			Toast.makeText(AppContext.getInstance(),
					"пожалуйста, проверьте подключение и попробуйте снова.",
					Toast.LENGTH_LONG).show();
		}

	}

	public static void setHttpClient(KJHttp kj) {
		client = kj;
	}

}
