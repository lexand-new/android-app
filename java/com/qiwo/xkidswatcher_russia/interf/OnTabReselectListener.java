package com.qiwo.xkidswatcher_russia.interf;

/** 
 * 当tabHost再次被点击时
 */
public interface OnTabReselectListener {
	
	public void onTabReselect();
}
