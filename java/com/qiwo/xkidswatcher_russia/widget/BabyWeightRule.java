package com.qiwo.xkidswatcher_russia.widget;

import com.qiwo.xkidswatcher_russia.R;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;


public class BabyWeightRule extends View{

	private Context  	mContext = null ;
	private Drawable 	mRuleBackground = null ;
	private Drawable 	mRuleForntground = null ;
	private Paint  	 	mPaint = null ;
	private Rect		mPaintRect = null ;
	private float 		mHandCurAngle = 0 , mHandPenAngle = 0 ;
    private int 		mRuleWidth = 0 , mRuleHeight = 0 ;
	
    public BabyWeightRule(Context context) {
        this(context, null);
    }

    public BabyWeightRule(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BabyWeightRule(Context context, AttributeSet attrs,
                       int defStyle) {
        super(context, attrs, defStyle);
        
        mContext = context ;
        Resources r = mContext.getResources();
        
        //获得 上次保存的值 初始化 角度 和 体重信息
        
        mHandCurAngle = 0 ;
        
        //背景可读盘
        mRuleBackground = r.getDrawable(R.drawable.s_weight_rule);
        //前面的标尺
        mRuleForntground = r.getDrawable(R.drawable.s_w_hand);
        
        mRuleWidth = mRuleBackground.getIntrinsicWidth();
        mRuleHeight = mRuleBackground.getIntrinsicHeight();
        
        //设置画笔
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setTextSize(mRuleWidth>>4) ;
        mPaint.setColor(0xffcf3f1c);
        mPaintRect = new Rect();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        
        //清除资源
        mRuleForntground = null ;
        mRuleBackground = null ;
        mPaint = null ;
        mPaintRect = null ;
    }
    
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        setMeasuredDimension(getDefaultSize(0, widthMeasureSpec),
                getDefaultSize(0, heightMeasureSpec));
    }
    
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        
        int availableWidth = getRight() - getLeft();
        int availableHeight = getBottom() - getTop();

        int x = availableWidth >> 1;  //居中
        int y = availableHeight ;    //距低 显示  
        
        //显示 背景 刻度盘
        final Drawable bg = mRuleBackground;
        int w = bg.getIntrinsicWidth();
        int h = bg.getIntrinsicHeight();    
        int offset_y = w >>3 ;
        canvas.save();
        canvas.rotate((int)(mHandCurAngle / 180 * 360), x, y + offset_y);
  //      canvas.scale(scale, scale, x, y);
        bg.setBounds(x - (w>>1), y - (h>>1) + offset_y, x + (w>>1), y + (h>>1)+ offset_y);       
        bg.draw(canvas);
        canvas.restore();
        
        //显示覆盖上面的 指针
        final Drawable fg = mRuleForntground;
        w = fg.getIntrinsicWidth();
        h = fg.getIntrinsicHeight();  
        canvas.save();
 //       canvas.scale(scale, scale, x, y);
        fg.setBounds(x - (w>>1), y - (h>>1) + offset_y, x + (w>>1), y + (h>>1)+ offset_y);       
        fg.draw(canvas);  
        canvas.restore();
        
        //显示多少kg 文字
        Rect rect = mPaintRect ;
		//更新体重 文字			
        String value = "" + ((180 - (int)mHandCurAngle)%180)/2.0f + "кг" ;
        mPaint.getTextBounds(value, 0, value.length(), rect); 
        canvas.drawText(value, (availableWidth - rect.width())>> 1  , y - (rect.height()>>1), mPaint) ;
        
    }
    
    @Override 
    public boolean onTouchEvent(MotionEvent event) {
    	
		float offset_x  = event.getX() ;
		float offset_y  = getRight() - getLeft() - offset_x - 1;
		float angle ;
		
		//只接受 自己的消息
		if (event.getY() < getBottom() - getTop()  - (mRuleHeight >> 1))
		{
			return false ;			
		}
		
        switch (event.getAction()) {

        	case MotionEvent.ACTION_DOWN:
        	{
    			mHandPenAngle = getAngleByXY(offset_x, offset_y);
    			return true ;
        	}
        		
        	case MotionEvent.ACTION_MOVE:
        		{
        			angle = mHandPenAngle;
        			
        			mHandPenAngle = getAngleByXY(offset_x, offset_y);


            		mHandCurAngle = (360 + mHandCurAngle - (mHandPenAngle - angle)) %180;       				
        			
        			invalidate();
        		}
        			
        		return true ;
        }
        
        return false;
    }
    
    //根据 x, y获得 角度
    private int getAngleByXY(float x , float y)
    {
    	return (int)Math.toDegrees(Math.atan2(y, x)) ;
    }
    
    
    //对外提供 获得设置体重接口
    public float getWeightValue()
    {
    	return  (180 - mHandCurAngle)/2.0f ;
    }
    //设置体重
    public void setWeightValue(float angle)
    {
    	mHandCurAngle = ((180 - angle * 2.0f)%180)  ;
    }
}
