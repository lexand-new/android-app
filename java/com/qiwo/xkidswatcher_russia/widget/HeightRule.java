package com.qiwo.xkidswatcher_russia.widget;

import com.qiwo.xkidswatcher_russia.R;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.widget.Scroller;
import android.widget.TextView;


public class HeightRule extends View {

	private Drawable mRuleBackground = null; // 刻度背景图
	private Drawable mRuleHand = null; // 刻度上的标签
	private int mScreenWidth = 0, mScreenHeight = 0; // 显示区域的矿高
	private int mHandWidth = 0, mHandHeight = 0;
	private int mRuleWidth = 0, mRuleHeight = 0;
	private TextView mHeightView = null;
	private float mHandOffsetY = 0, mRuleOffsetY = 0;
	private int mSettingData = 0;
	private int mRuleX = 0, mRuleY = 0, mRuleVHeight = 0;
	private float mMinOffset = 20; // 最大和最小值
	private float mPenStartY = 0;

	private Scroller mScroller;
	private VelocityTracker mVelocityTracker;
	private static final int SNAP_VELOCITY = 1000;
	private static final int MIN_HEIGHT = 20, MAX_HEIGHT = 200;

	public HeightRule(Context context) {
		this(context, null);
	}

	public HeightRule(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public HeightRule(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		Resources r = context.getResources();
		// 背景可读盘
		mRuleBackground = r.getDrawable(R.drawable.s_height_rule);
		// 前面的标尺
		mRuleHand = r.getDrawable(R.drawable.s_h_hand);

		mScroller = new Scroller(context);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);

		mScreenWidth = w; // 可用区域的宽高
		mScreenHeight = h;

		mHandWidth = mRuleHand.getIntrinsicWidth(); // 针的宽高
		mHandHeight = mRuleHand.getIntrinsicHeight();

		mRuleWidth = mRuleBackground.getIntrinsicWidth(); // 刻度的宽高
		mRuleHeight = mRuleBackground.getIntrinsicHeight();

		// 设置背景的偏移
		mRuleX = mScreenWidth - mHandWidth / 3 - mRuleWidth;

		mRuleVHeight = mRuleHeight >> 2; // 可视高度

		mMinOffset = getHeightOffsetY(1);

		mHandOffsetY = (mScreenHeight >> 1) - getHeightOffsetY(30)
				- getHandOffsetDeta(); // 设置针的位置

		mRuleY = (int) (mHandOffsetY - getHeightOffsetY(10)); // 初始高度

		// 计算开始的偏移
		mRuleOffsetY = (mScreenHeight >> 1)
				- getHeightOffsetY(200 - mSettingData + 30);
	}

	private int getHandOffsetDeta() {
		float deta = (mHandHeight - getHeightOffsetY(2)) / 2.0f;
		return (int) Math.ceil(deta);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		int x = mScreenWidth; // 居右
		int y = mScreenHeight; // 距低 显示

		// 显示 背景 刻度盘
		final Drawable bg = mRuleBackground;
		int w = mRuleWidth;
		int h = mRuleHeight;
		int offset_y = (int) mRuleOffsetY;
		int offset_x = mRuleX;

		canvas.save();
		canvas.clipRect(offset_x, mRuleY, offset_x + w, mRuleY + mRuleVHeight);
		bg.setBounds(offset_x, offset_y, offset_x + w, h + offset_y);
		bg.draw(canvas);
		canvas.restore();

		// 显示覆盖上面的 指针
		final Drawable fg = mRuleHand;
		w = fg.getIntrinsicWidth();
		h = fg.getIntrinsicHeight();
		fg.setBounds(x - w, (int) mHandOffsetY, x, (int) mHandOffsetY + h);
		fg.draw(canvas);

		// 更新高度textview
		// String value = "" + (int)(200 + 30 - ((mScreenHeight >> 1) -
		// mRuleOffsetY)/mMinOffset) + "cm";

		// String value = "" + mSettingData + "cm";
		String value = "" + (double) mSettingData / 100 + "м";
		mHeightView.setText(value);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {

		float offset_x = event.getX();
		float offset_y = event.getY();

		// 只接受 自己的消息
		if (offset_y < mRuleY || offset_y > mRuleY + mRuleVHeight
				|| offset_x < mRuleX - mRuleWidth) // 超出范围不处理
		{
			return false;
		}

		if (mVelocityTracker == null) {
			mVelocityTracker = VelocityTracker.obtain();
		}
		mVelocityTracker.addMovement(event);

		switch (event.getAction()) {

		case MotionEvent.ACTION_DOWN: {
			if (!mScroller.isFinished()) {
				mScroller.abortAnimation();
			}
			mPenStartY = offset_y;
			return true;
		}

		case MotionEvent.ACTION_MOVE: {
			float movedistance = offset_y - mPenStartY;

			if (Math.abs(movedistance) > mMinOffset) {
				mPenStartY = offset_y; // 有效位移才记录

				if (movedistance > 0) // 每次移动一个刻度
				{
					mSettingData++;
				} else {
					mSettingData--;
				}

				// 限制最大 和 最小偏移
				mSettingData = getHeightContain(mSettingData, MIN_HEIGHT,
						MAX_HEIGHT);

				mRuleOffsetY = (mScreenHeight >> 1)
						- ((200 - mSettingData + 30) * mRuleHeight) / 202.0f;

				invalidate();
			}
		}
			return true;

		case MotionEvent.ACTION_UP: {
			mVelocityTracker.computeCurrentVelocity(1000);
			int velocityY = (int) mVelocityTracker.getYVelocity();
			if (velocityY > SNAP_VELOCITY || velocityY < -SNAP_VELOCITY) {
				int destYOffset = (int) ((velocityY / 100));
				mScroller.startScroll(0, (int) mSettingData, 0,
						(int) (destYOffset), 2000);
				postInvalidate();
			}

			if (mVelocityTracker != null) {
				mVelocityTracker.recycle();
				mVelocityTracker = null;
			}
			return true;
		}

		}

		return false;
	}

	// 设置个你更新的View
	public void setUpdateView(TextView view) {
		mHeightView = view;
	}

	// 设置 初始高度
	public void setHeightValue(float height) {
		mSettingData = (int) height;
	}

	// 获得计算的高度
	public float getHeightValue() {
		return mSettingData;// (int)(200 + 30 - ((mScreenHeight >> 1) -
							// mRuleOffsetY)/mMinOffset) ;
	}

	@Override
	public void computeScroll() {
		if (mScroller.computeScrollOffset()) {
			// mRuleOffsetY = mScroller.getCurrY();

			mSettingData = mScroller.getCurrY();
			// mSettingData = (int)(200 + 30 - ((mScreenHeight >> 1) -
			// mRuleOffsetY)/mMinOffset) ;

			// 限制最大 和 最小偏移
			mSettingData = getHeightContain(mSettingData, MIN_HEIGHT,
					MAX_HEIGHT);

			mRuleOffsetY = (mScreenHeight >> 1)
					- ((200 - mSettingData + 30) * mRuleHeight) / 202.0f;
			postInvalidate();
		}
	}

	private float getHeightOffsetY(float num) {
		return (num * mRuleHeight) / 202.0f;
	}

	private int getHeightContain(int num, int min, int max) {
		if (num > max) {
			num = max;
		} else if (num < min) {
			num = min;
		}

		return num;
	}
}
