package com.qiwo.xkidswatcher_russia.widget;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.widget.wheel.ArrayWheelAdapter;
import com.qiwo.xkidswatcher_russia.widget.wheel.OnWheelChangedListener;
import com.qiwo.xkidswatcher_russia.widget.wheel.WheelView;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;


public class TimesPickerPopupWindow extends BottomPopupWindow implements
		OnWheelChangedListener, OnClickListener {

	/*
	 * 起始时间选择数组
	 */
	private static final String[] starTimesArray = { "22:00", "23:00", "00:00" };
	private static final String[] endTimesArray = { "06:00", "07:00", "08:00" };

	private WheelView wheel_start;
	private WheelView wheel_end;

	private TextView bt_cancel, bt_ok;

	private int currstartIndex, currendIndex;

	private OnTimesPickerListener mOnTimesPickerListener = null;

	// private int silenceStart = 22;
	// private int silenceEnd = 7;

	public TimesPickerPopupWindow(Context context, int bgColor, View relyview) {//int start, int end
		super(context, bgColor, relyview);
		// silenceStart = start;
		// silenceEnd = end;
		// TODO Auto-generated constructor stub
		initView();
		initData();
		initEvent();
	}

	@Override
	public void onChanged(WheelView wheel, int oldValue, int newValue) {
		// TODO Auto-generated method stub
		if (wheel == wheel_start) {
			currstartIndex = newValue;
		} else {
			currendIndex = newValue;
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.tv_popup_datapicker_cancel:
			dismiss();
			break;
		case R.id.tv_popup_datapicker_ok:
			dismiss();
			String startTime = starTimesArray[currstartIndex];
			String endTime = endTimesArray[currendIndex];
			if (mOnTimesPickerListener != null) {
				mOnTimesPickerListener.onTimesPick(startTime, endTime);
			}
			break;
		default:
			break;
		}
	}

	public void setOnTimesPickerListener(OnTimesPickerListener listener) {
		this.mOnTimesPickerListener = listener;
	}

	// -------------------
	public interface OnTimesPickerListener {
		void onTimesPick(String start, String end);
	}

	// ----------------------
	private void initView() {
		LayoutInflater inflater = (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.popup_timespicker, null);
		bt_cancel = (TextView) view
				.findViewById(R.id.tv_popup_datapicker_cancel);
		bt_ok = (TextView) view.findViewById(R.id.tv_popup_datapicker_ok);

		wheel_start = (WheelView) view.findViewById(R.id.wheel_star);
		wheel_end = (WheelView) view.findViewById(R.id.wheel_end);

		this.setContentView(view);
		this.setWidth(android.view.ViewGroup.LayoutParams.MATCH_PARENT);
		this.setHeight(android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	private void initData() {
		// --------------
		wheel_start.setVisibleItems(starTimesArray.length + 1);
		ArrayWheelAdapter<String> start_adapter = new ArrayWheelAdapter<String>(
				getContext(), starTimesArray);
		start_adapter.setTextColor(Color.GRAY);
		wheel_start.setViewAdapter(start_adapter);
		wheel_start.setCurrentItem(currstartIndex);

		// ---------------
		wheel_end.setVisibleItems(starTimesArray.length + 1);
		ArrayWheelAdapter<String> end_adapter = new ArrayWheelAdapter<String>(
				getContext(), endTimesArray);
		end_adapter.setTextColor(Color.GRAY);
		wheel_end.setViewAdapter(end_adapter);
		wheel_end.setCurrentItem(currendIndex);
	}

	private void initEvent() {
		wheel_start.addChangingListener(this);
		wheel_end.addChangingListener(this);

		bt_cancel.setOnClickListener(this);
		bt_ok.setOnClickListener(this);
	}
}
