package com.qiwo.xkidswatcher_russia.base;

import com.qiwo.xkidswatcher_russia.R;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class BaseDialog extends Dialog {

	private static final String TAG = "BaseDialog";

	protected int screenWidth;
	private float mDensity = 1f;
	private int paddingSize = 0;

	protected Context ctx;
	protected View view;

	protected ImageView dialog_titleicon;
	protected TextView dialog_titletext;
	protected TextView dialog_message;
	protected Button mPositiveButton; // 确定
	protected Button mNegativeButton; // 取消
	protected Button mNeutralButton; // 中间

	protected LinearLayout dialog_title;
	protected LinearLayout dialog_body;
	protected LinearLayout dialog_space_bar;
	protected LinearLayout dialog_botton;
	protected LinearLayout layoutContainer;
	protected ScrollView dialog_scroll_body;
	protected LinearLayout dialog_fix_body;

	private View.OnClickListener backListener = null;

	private boolean isCancelable = true;

	public BaseDialog(Context context) {
		super(context, R.style.dialog);
		this.ctx = context;
		// 初始化
		initView();
	}

	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);

		// 添加布局
		LayoutParams viewLayoutParams = new LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		// viewLayoutParams.width = screenWidth;
		super.setContentView(view, viewLayoutParams);
	}

	protected void initView() {
		// 获取系统数据
		screenWidth = ctx.getApplicationContext().getResources()
				.getDisplayMetrics().widthPixels;
		mDensity = ctx.getResources().getDisplayMetrics().density;
		paddingSize = (int) (6 * mDensity);

		// 初始化布局
		this.view = LayoutInflater.from(this.ctx).inflate(
				R.layout.layout_dialog, null);
		this.layoutContainer = (LinearLayout) this.view
				.findViewById(R.id.layout_container);
		this.dialog_titleicon = (ImageView) this.view
				.findViewById(R.id.title_icon);
		this.dialog_titletext = (TextView) this.view
				.findViewById(R.id.title_text);
		this.dialog_title = (LinearLayout) this.view
				.findViewById(R.id.dialog_title);
		this.dialog_body = (LinearLayout) this.view
				.findViewById(R.id.dialog_body);
		this.dialog_botton = (LinearLayout) this.view
				.findViewById(R.id.dialog_botton);
		this.dialog_space_bar = (LinearLayout) this.view
				.findViewById(R.id.dialog_space_bar);
		this.dialog_scroll_body = (ScrollView) this.view
				.findViewById(R.id.dialog_scroll_body);
		this.dialog_fix_body = (LinearLayout) this.view
				.findViewById(R.id.dialog_fix_body);

		// 设置边距
		int padding = screenWidth * 5 / 100;
		view.setPadding(padding, padding, padding, padding);

		// setTitleIcon(R.drawable.popup_icon_warning);
		// 设置点击空白处不关闭
		// setCanceledOnTouchOutside(false);
	}

	/**
	 * 设置类型(类型WindowManager.LayoutParams.TYPE_SYSTEM_ALERT需要权限 <uses-permission
	 * android:name="android.permission.SYSTEM_ALERT_WINDOW" />)
	 * 
	 * @param type
	 *            类型（WindowManager.LayoutParams.TYPE_SYSTEM_DIALOG用于Acitivty内，
	 *            WindowManager.LayoutParams.TYPE_SYSTEM_ALERT用于Acitivty外，效果如浮层）
	 */
	public void setType(int type) {
		getWindow().setType(type);
	}

	public void setOnlyContent() {
		this.dialog_title.setVisibility(View.GONE);
		this.dialog_botton.setVisibility(View.GONE);
	}

	public void setTitleIcon(int iconRes) {
		this.dialog_titleicon.setVisibility(View.VISIBLE);
		this.dialog_titleicon.setImageResource(iconRes);
	}

	public void setTitleIcon(BitmapDrawable headIcon) {
		this.dialog_titleicon.setVisibility(View.VISIBLE);
		this.dialog_titleicon.setImageDrawable(headIcon);
	}

	private void setButtonStyle(Button btn, int btnType) {
		// if (btnType == R.style.button_default) {
		// btn.setBackgroundResource(R.drawable.selector_button_bg_default);
		// btn.setTextSize(18);
		// btn.setTextColor(ctx.getResources().getColor(R.color.button_text_default));
		// btn.setShadowLayer(0.5f, 1, 1,
		// ctx.getResources().getColor(R.color.button_text_shadow_default));
		// btn.setPadding(0, (int)(12*mDensity), 0, (int)(12*mDensity));
		// }

		btn.setTextSize(TypedValue.COMPLEX_UNIT_PX, ctx.getResources()
				.getDimensionPixelSize(R.dimen.dialog_button_text_size));
		btn.setTextColor(ctx.getResources().getColor(
				R.color.selector_dialog_button_text_default));
		// btn.setShadowLayer(0.5f, 1, 1,
		// ctx.getResources().getColor(R.color.base_dialog_button_text_shadow_default));
	}

	public void setPositiveButton(int resStrId,
			View.OnClickListener paramOnClickListener, int btnType) {
		setPositiveButton(resStrId, paramOnClickListener, btnType,
				LayoutParams.MATCH_PARENT);
	}

	/**
	 * @param resStrId
	 * @param paramOnClickListener
	 * @param btnType
	 * @param fillType
	 *            填充类型（LayoutParams.MATCH_PARENT 或 LayoutParams.WRAP_CONTENT）
	 */
	public void setPositiveButton(int resStrId,
			View.OnClickListener paramOnClickListener, int btnType, int fillType) {
		if (mPositiveButton != null) {
			this.dialog_botton.removeView(this.mPositiveButton);
		}
		this.mPositiveButton = new Button(this.ctx);
		setButtonStyle(mPositiveButton, btnType);

		this.dialog_space_bar.setVisibility(View.VISIBLE);
		this.dialog_botton.setVisibility(View.VISIBLE);
		this.mPositiveButton.setText(ctx.getString(resStrId));
		this.mPositiveButton.setOnClickListener(paramOnClickListener);
		this.mPositiveButton.setGravity(Gravity.CENTER);
	}

	public void setNegativeButton(int resStrId,
			View.OnClickListener paramOnClickListener, int btnType) {
		setNegativeButton(resStrId, paramOnClickListener, btnType,
				LayoutParams.MATCH_PARENT);
	}

	/**
	 * @param resStrId
	 * @param paramOnClickListener
	 * @param btnType
	 * @param fillType
	 *            填充类型（LayoutParams.MATCH_PARENT 或 LayoutParams.WRAP_CONTENT）
	 */
	public void setNegativeButton(int resStrId,
			View.OnClickListener paramOnClickListener, int btnType, int fillType) {
		if (mNegativeButton != null) {
			this.dialog_botton.removeView(this.mNegativeButton);
		}
		this.mNegativeButton = new Button(this.ctx);
		setButtonStyle(mNegativeButton, btnType);

		this.dialog_space_bar.setVisibility(View.VISIBLE);
		this.dialog_botton.setVisibility(View.VISIBLE);
		this.mNegativeButton.setText(ctx.getString(resStrId));
		this.mNegativeButton.setOnClickListener(paramOnClickListener);
		this.mNegativeButton.setGravity(Gravity.CENTER);
	}

	public void setNeutralButton(int resStrId,
			View.OnClickListener paramOnClickListener, int btnType) {
		setNeutralButton(resStrId, paramOnClickListener, btnType,
				LayoutParams.MATCH_PARENT);
	}

	public void setNeutralButton(int resStrId,
			View.OnClickListener paramOnClickListener, int btnType, int fillType) {
		if (mNeutralButton != null) {
			this.dialog_botton.removeView(this.mNeutralButton);
		}
		this.mNeutralButton = new Button(this.ctx);
		setButtonStyle(mNeutralButton, btnType);

		this.dialog_space_bar.setVisibility(View.VISIBLE);
		this.dialog_botton.setVisibility(View.VISIBLE);
		this.mNeutralButton.setText(ctx.getString(resStrId));
		this.mNeutralButton.setOnClickListener(paramOnClickListener);
		this.mNeutralButton.setGravity(Gravity.CENTER);
	}

	public void setContentView(View paramView) {
		this.dialog_body.removeAllViews();
		LinearLayout.LayoutParams paras = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		paras.gravity = Gravity.CENTER;
		// int i1 =
		// this.ctx.getApplicationContext().getResources().getDisplayMetrics().widthPixels
		// * 95 / 100;
		// paramView.setMinimumWidth(i1);
		this.dialog_body.addView(paramView, paras);
		// this.dialog_body.addView(paramView);
	}

	public void setContetViewBackground(int drawableId) {
		this.dialog_body.setBackgroundResource(drawableId);
	}

	public void setContentViewNoPadding(View paramView) {
		this.dialog_body.removeAllViews();
		LinearLayout.LayoutParams paras = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		paras.gravity = Gravity.CENTER;
		// int i1 =
		// this.ctx.getApplicationContext().getResources().getDisplayMetrics().widthPixels
		// * 95 / 100;
		// paramView.setMinimumWidth(i1);
		// this.dialog_body.addView(paramView, paras);
		dialog_body.setPadding(0, 0, 0, 0);
		this.dialog_body.addView(paramView);
	}

	// public void setContentViewNoPadding(View paramView) {
	// this.dialog_body.removeAllViews();
	// LinearLayout.LayoutParams paras = new
	// LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
	// LayoutParams.WRAP_CONTENT);
	// paras.gravity = Gravity.CENTER;
	// int i1 =
	// this.ctx.getApplicationContext().getResources().getDisplayMetrics().widthPixels
	// * 95 / 100;
	// paramView.setMinimumWidth(i1);
	// this.dialog_body.setPadding(0, 0, 0, 0);
	// this.dialog_body.addView(paramView, paras);
	// }

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// back key down
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (backListener != null) {
				backListener.onClick(null);
			} else {
				if (isCancelable) {
					this.dismiss();
				}
			}
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_SEARCH) {
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	public void setOnBackListener(View.OnClickListener listener) {
		backListener = listener;
	}

	public void setMessage(int msg) {
		setMessage(ctx.getString(msg));
	}

	public void setMessage(String msg) {
		this.dialog_body.removeAllViews();
		dialog_message = new TextView(ctx);
		dialog_message.setTextColor(ctx.getResources().getColor(
				R.color.base_dialog_content_text));
		dialog_message.setTextSize(17f);
		dialog_message.setGravity(Gravity.CENTER_VERTICAL);

		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		params.weight = 1.0F;
		this.dialog_body.addView(dialog_message, params);
		dialog_message.setText(msg);
	}

	public void setMessage(Spanned msg) {
		this.dialog_body.removeAllViews();
		dialog_message = new TextView(ctx);
		dialog_message.setTextColor(ctx.getResources().getColor(
				R.color.base_dialog_content_text));
		dialog_message.setTextSize(17f);
		dialog_message.setGravity(Gravity.CENTER_VERTICAL);

		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		params.weight = 1.0F;
		this.dialog_body.addView(dialog_message, params);
		dialog_message.setText(msg);
		dialog_message.setMovementMethod(LinkMovementMethod.getInstance());
	}

	public void setCancelable(boolean isCancelable) {
		this.isCancelable = isCancelable;
		super.setCancelable(isCancelable);
	}

	public void setTitle(int titleResId) {
		String title = this.ctx.getResources().getString(titleResId);
		setTitle(title);
	}

	public void setTitle(CharSequence title) {
		this.dialog_title.setVisibility(View.VISIBLE);
		this.dialog_titletext.setText(title);
	}

	public void setFixContentView(View view, LayoutParams params) {
		dialog_scroll_body.setVisibility(View.GONE);
		dialog_fix_body.setVisibility(View.VISIBLE);
		dialog_fix_body.removeAllViews();
		dialog_fix_body.addView(view, params);
	}

	private void addButton(Button button) {
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		layoutParams.gravity = Gravity.CENTER_VERTICAL;
		layoutParams.weight = 1.0F;
		button.setPadding(paddingSize, paddingSize, paddingSize, paddingSize);
		dialog_botton.addView(button, layoutParams);
	}

	private void addButtonLine() {
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
				(int) mDensity, LayoutParams.MATCH_PARENT);
		layoutParams.gravity = Gravity.CENTER_VERTICAL;
		LinearLayout line = new LinearLayout(ctx);
		line.setBackgroundResource(R.color.base_dialog_button_line);
		dialog_botton.addView(line, layoutParams);
	}

	private void formatButtons() {
		dialog_botton.removeAllViewsInLayout();

		if (mNegativeButton != null) {
			addButton(mNegativeButton);

			if (mNeutralButton != null || mPositiveButton != null) {
				mNegativeButton
						.setBackgroundResource(R.drawable.selector_button_bg_dialog_default_left);
			} else {
				mNegativeButton
						.setBackgroundResource(R.drawable.selector_button_bg_dialog_default_mid);
			}
		}

		if (mNeutralButton != null) {
			if (mNegativeButton != null) {
				addButtonLine();
			}

			if (mNegativeButton != null || mPositiveButton != null) {
				if (mNegativeButton == null) {
					mNeutralButton
							.setBackgroundResource(R.drawable.selector_button_bg_dialog_default_left);
				} else if (mPositiveButton == null) {
					mNeutralButton
							.setBackgroundResource(R.drawable.selector_button_bg_dialog_default_right);
				} else {
					mNeutralButton
							.setBackgroundResource(R.drawable.selector_button_bg_dialog_default);
				}
			} else {
				mNeutralButton
						.setBackgroundResource(R.drawable.selector_button_bg_dialog_default);
			}

			addButton(mNeutralButton);
		}

		if (mPositiveButton != null) {
			if (mNegativeButton != null || mNeutralButton != null) {
				addButtonLine();
				mPositiveButton
						.setBackgroundResource(R.drawable.selector_button_bg_dialog_default_right);
			} else {
				mPositiveButton
						.setBackgroundResource(R.drawable.selector_button_bg_dialog_default);
			}
			addButton(mPositiveButton);
		}
	}

	@Override
	public void show() {
		// TODO 格式化button
		formatButtons();
		super.show();
	}

}
