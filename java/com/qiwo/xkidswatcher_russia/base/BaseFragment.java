package com.qiwo.xkidswatcher_russia.base;

import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.interf.BaseFragmentInterface;
import com.qiwo.xkidswatcher_russia.util.StringUtils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 */
public class BaseFragment extends Fragment implements
		android.view.View.OnClickListener, BaseFragmentInterface {
	public static final int STATE_NONE = 0;
	public static final int STATE_REFRESH = 1;
	public static final int STATE_LOADMORE = 2;
	public static final int STATE_NOMORE = 3;
	public static final int STATE_PRESSNONE = 4;// 正在下拉但还没有到刷新的状态
	public static int mState = STATE_NONE;

	protected LayoutInflater mInflater;

	protected Handler mHandler;
	private ProgressDialog progressDialog = null;

	public AppContext getApplication() {
		return (AppContext) getActivity().getApplication();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.mInflater = inflater;
		View view = super.onCreateView(inflater, container, savedInstanceState);
		mHandler = new Handler();
		return view;
	}

	@Override
	public void onResume() {
//		if(){
//			
//		}
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	protected int getLayoutId() {
		return 0;
	}

	protected View inflateView(int resId) {
		return this.mInflater.inflate(resId, null);
	}

	public boolean onBackPressed() {
		return false;
	}

	protected void hideWaitDialog() {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
	}

	protected void showWaitDialog() {
		showWaitDialog(getActivity().getResources().getString(R.string.loading));
	}

	protected void dismissDialog(String msg) {
		dismissDialog(msg, 800);
	}

	protected void dismissDialog(String msg, int delayTicks) {
		if (!StringUtils.isEmpty(msg)) {
			showWaitDialog(msg);
		}

		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				hideWaitDialog();
			}
		}, delayTicks);
	}

	protected void showWaitDialog(String resid) {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.setMessage(resid);
		} else {
			progressDialog = ProgressDialog.show(getActivity(), null, resid);
		}
	}

	protected void showLongToast(String message) {
		Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
	}

	protected void showShortToast(String message) {
		Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
	}

	protected void showConfirmDialog(String title, String message,
			String positiveText, String negativeText,
			DialogInterface.OnClickListener positiveListener,
			DialogInterface.OnClickListener negativeListener) {
		new AlertDialog.Builder(getActivity()).setMessage(message)
				.setTitle(title)
				.setPositiveButton(positiveText, positiveListener)
				.setNegativeButton(negativeText, negativeListener).show();
	}

	protected void showConfirmDialog(String title, String message,
			String positiveText, String negativeText,
			DialogInterface.OnClickListener positiveListener) {

		DialogInterface.OnClickListener negativeListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		};
		showConfirmDialog(title, message, positiveText, negativeText,
				positiveListener, negativeListener);
	}

	protected void showConfirmInformation(String title, String message) {
		new AlertDialog.Builder(getActivity()).setMessage(message)
				.setTitle(title).setPositiveButton("OK", null).show();
	}

	@Override
	public void initView(View view) {

	}

	@Override
	public void initData() {

	}

	@Override
	public void onClick(View v) {

	}
}
