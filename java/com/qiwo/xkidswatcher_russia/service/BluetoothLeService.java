/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.qiwo.xkidswatcher_russia.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.qiwo.xkidswatcher_russia.bean.bean___BleConnect;
import com.qiwo.xkidswatcher_russia.event.BleDisconnect;
import com.qiwo.xkidswatcher_russia.event.BleWritePwdSuccess;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import com.qiwo.xkidswatcher_russia.util.TLog;

import android.annotation.SuppressLint;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import de.greenrobot.event.EventBus;

/**
 * Service for managing connection and data communication with a GATT server
 * hosted on a given Bluetooth LE device.
 */
@SuppressLint("NewApi")
public class BluetoothLeService extends Service {
	private final static String TAG = BluetoothLeService.class.getSimpleName();
	public static String HEART_RATE_MEASUREMENT = "00002a37-0000-1000-8000-00805f9b34fb";
	public static String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";

	private BluetoothManager mBluetoothManager;
	private BluetoothAdapter mBluetoothAdapter;
	// private String mBluetoothDeviceAddress;
	// private String mBluetoothPwd;
	// private BluetoothGatt mBluetoothGatt;

	public final static UUID UUID_HEART_RATE_MEASUREMENT = UUID
			.fromString(HEART_RATE_MEASUREMENT);

	// ----------------
	private static Map<String, bean___BleConnect> bleMap = new HashMap<String, bean___BleConnect>();

	// Implements callback methods for GATT events that the app cares about. For
	// example,
	// connection change and services discovered.
	private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status,
				int newState) {
			if (newState == BluetoothProfile.STATE_CONNECTED) {

				// writePassword___ForWatcher(mBluetoothPwd);
				TLog.log("-------connected---------");
				boolean result = gatt.discoverServices();
				Log.i(TAG, "Attempting to start service discovery:" + result);

			} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
				String bluetoothName = gatt.getDevice().getName();
				String bluetoothAddress = gatt.getDevice().getAddress();
				TLog.log("bluetoothName=" + bluetoothName);
				TLog.log("bluetoothAddress=" + bluetoothAddress);
				TLog.log("-------disconnected---------");

				int connectionState = 0;
				try {
					if (bleMap.containsKey(bluetoothAddress)) {
						connectionState = bleMap.get(bluetoothAddress).connectionState;
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					// connectionState = 50;
				}
				TLog.log("state=" + connectionState);
				if (connectionState == 50) {
					TLog.log("-------connectionState ==50---------");
				} else if (connectionState == 0 || connectionState == 3
						|| connectionState == 4) {
					if (bleMap.containsKey(bluetoothAddress)) {
						System.out
								.println("-------eventbus.post.BleDisconnect---------");
						EventBus.getDefault().post(
								new BleDisconnect(bluetoothName,
										bluetoothAddress, connectionState));
					}
				}

				close(bluetoothAddress);
				// broadcastUpdate(intentAction);
			}
		}

		@Override
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {
			if (status == BluetoothGatt.GATT_SUCCESS) {
				TLog.log("-------GATT_SUCCESS---------");
				if (bleMap.containsKey(gatt.getDevice().getAddress())) {
					writePassword___ForWatcher(gatt.getDevice().getAddress());
					List<BluetoothGattService> gattServices = getSupportedGattServices(gatt
							.getDevice().getAddress());
					// Loops through available GATT Services.
					for (BluetoothGattService gattService : gattServices) {
						String serveic_uuid = gattService.getUuid().toString();
						TLog.log("serveic_uuid=" + serveic_uuid);

						List<BluetoothGattCharacteristic> gattCharacteristics = gattService
								.getCharacteristics();
						for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
							String ble_g_char_uuid = gattCharacteristic
									.getUuid().toString();
							TLog.log("char_uuid=" + ble_g_char_uuid);
						}
					}
				}
			} else {
				Log.w(TAG, "onServicesDiscovered received: " + status);
			}
		}

		@Override
		public void onCharacteristicRead(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			if (status == BluetoothGatt.GATT_SUCCESS) {
				String blePwd = bleMap.get(gatt.getDevice().getAddress()).bluetoothDevicePassword;
				writePassword___ForWatcher(blePwd);// (mBluetoothPwd);
			}
		}

		@Override
		public void onCharacteristicWrite(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			Log.e("", "UUID:" + characteristic.getUuid().toString());
			Log.e("", "写入数据返回：" + status + " (成功 " + BluetoothGatt.GATT_SUCCESS
					+ ",失败 " + BluetoothGatt.GATT_FAILURE + ")");
			if (status == BluetoothGatt.GATT_SUCCESS) {
				String bluetoothAddress = gatt.getDevice().getAddress();
				bleMap.get(bluetoothAddress).connectionState = 4;
				TLog.log("state="
						+ bleMap.get(bluetoothAddress).connectionState);
			}
			TLog.log("value:" + characteristic.getValue().toString());
			TLog.log("写入数据返回：" + status + " (成功 " + BluetoothGatt.GATT_SUCCESS
					+ ",失败 " + BluetoothGatt.GATT_FAILURE + ")");
		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic) {
		}
	};

	public class LocalBinder extends Binder {
		public BluetoothLeService getService() {
			return BluetoothLeService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		// After using a given device, you should make sure that
		// BluetoothGatt.close() is called
		// such that resources are cleaned up properly. In this particular
		// example, close() is
		// invoked when the UI is disconnected from the Service.
		// closeAll();
		return super.onUnbind(intent);
	}

	private final IBinder mBinder = new LocalBinder();

	/**
	 * Initializes a reference to the local Bluetooth adapter.
	 * 
	 * @return Return true if the initialization is successful.
	 */
	public boolean initialize() {
		// For API level 18 and above, get a reference to BluetoothAdapter
		// through
		// BluetoothManager.
		if (mBluetoothManager == null) {
			mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
			if (mBluetoothManager == null) {
				Log.e(TAG, "Unable to initialize BluetoothManager.");
				return false;
			}
		}

		mBluetoothAdapter = mBluetoothManager.getAdapter();
		if (mBluetoothAdapter == null) {
			Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
			return false;
		}

		return true;
	}

	public Map<String, bean___BleConnect> getBleMap() {
		return bleMap;
	}

	public bean___BleConnect getBleConnect(String mAddress) {
		return bleMap.get(mAddress);
	}

	public boolean getBleIsConnect(String mAddress) {
		return bleMap.containsKey(mAddress);
	}

	public int getBleConnectionState(String mAddress) {
		return bleMap.get(mAddress).connectionState;
	}

	/**
	 * Connects to the GATT server hosted on the Bluetooth LE device.
	 * 
	 * @param address
	 *            The device address of the destination device.
	 * 
	 * @return Return true if the connection is initiated successfully. The
	 *         connection result is reported asynchronously through the
	 *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
	 *         callback.
	 */
	public boolean connect(final String address, final String pwd,
			String nickname) {
		// mBluetoothPwd = pwd;
		if (mBluetoothAdapter == null || address == null) {
			Log.w(TAG,
					"BluetoothAdapter not initialized or unspecified address.");
			return false;
		}
		
		final BluetoothDevice device = mBluetoothAdapter
				.getRemoteDevice(address);
		if (device == null) {
			Log.w(TAG, "Device not found.  Unable to connect.");
			return false;
		}
		// We want to directly connect to the device, so we are setting the
		// autoConnect
		// parameter to false.
		// disconnect(address);
		BluetoothGatt mBluetoothGatt = device.connectGatt(this, true,
				mGattCallback);

		Log.e("", "nickname:" + nickname);

		bean___BleConnect bean_bleconn = new bean___BleConnect();
		bean_bleconn.blueGatt = mBluetoothGatt;
		bean_bleconn.bluetoothDeviceAddress = address;
		bean_bleconn.bluetoothDevicePassword = pwd;
		bean_bleconn.connectionState = -1;
		bean_bleconn.BabyName = nickname;
		bleMap.put(address, bean_bleconn);

		Log.d(TAG, "Trying to create a new connection.");
		// mBluetoothDeviceAddress = address;
		TLog.log("-------connecting---------");
		return true;
	}

	/**
	 * Disconnects an existing connection or cancel a pending connection. The
	 * disconnection result is reported asynchronously through the
	 * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
	 * callback.
	 */
	public void disconnect(String mAddress) {
		if (mBluetoothAdapter == null || bleMap.get(mAddress) == null) {
			Log.w(TAG, "BluetoothAdapter not initialized");
			return;
		}
		bleMap.get(mAddress).connectionState = 50;
		close(mAddress);
		// bleMap.get(mAddress).blueGatt.disconnect();
	}

	/**
	 * After using a given BLE device, the app must call this method to ensure
	 * resources are released properly.
	 */
	private void close(String mAddress) {
		if (bleMap.get(mAddress) == null) {
			return;
		}

		try {
			bleMap.get(mAddress).blueGatt.disconnect();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		try {
			bleMap.get(mAddress).blueGatt.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		// bleMap.get(mAddress).blueGatt = null;
		bleMap.remove(mAddress);
	}

	public void closeAll() {
		for (String key : bleMap.keySet()) {
			close(key);
		}
	}

	/**
	 * Request a read on a given {@code BluetoothGattCharacteristic}. The read
	 * result is reported asynchronously through the
	 * {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
	 * callback.
	 * 
	 * @param characteristic
	 *            The characteristic to read from.
	 */
	public void readCharacteristic(String mAddress,
			BluetoothGattCharacteristic characteristic) {
		if (mBluetoothAdapter == null || bleMap.get(mAddress).blueGatt == null) {
			Log.w(TAG, "BluetoothAdapter not initialized");
			return;
		}
		bleMap.get(mAddress).blueGatt.readCharacteristic(characteristic);
	}

	/**
	 * Enables or disables notification on a give characteristic.
	 * 
	 * @param characteristic
	 *            Characteristic to act on.
	 * @param enabled
	 *            If true, enable notification. False otherwise.
	 */
	public void setCharacteristicNotification(String mAddress,
			BluetoothGattCharacteristic characteristic, boolean enabled) {
		if (mBluetoothAdapter == null || bleMap.get(mAddress).blueGatt == null) {
			Log.w(TAG, "BluetoothAdapter not initialized");
			return;
		}
		bleMap.get(mAddress).blueGatt.setCharacteristicNotification(
				characteristic, enabled);

		// This is specific to Heart Rate Measurement.
		if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
			BluetoothGattDescriptor descriptor = characteristic
					.getDescriptor(UUID
							.fromString(CLIENT_CHARACTERISTIC_CONFIG));
			descriptor
					.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
			bleMap.get(mAddress).blueGatt.writeDescriptor(descriptor);
		}
	}

	public boolean writeCharacteristic(String mAddress,
			BluetoothGattCharacteristic characteristic, byte[] bytes) {
		if (mBluetoothAdapter == null || bleMap.get(mAddress).blueGatt == null) {
			Log.w(TAG, "BluetoothAdapter not initialized");
			return false;
		}
		characteristic.setValue(bytes);
		characteristic
				.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);// .WRITE_TYPE_DEFAULT.PROPERTY_NOTIFY

		return bleMap.get(mAddress).blueGatt
				.writeCharacteristic(characteristic);

	}

	private boolean writePassword___ForWatcher(String mAddress) {
		if (!bleMap.containsKey(mAddress)) {
			TLog.log("no------");
			// bleMap.get(mAddress).connectionState = 50;
			close(mAddress);
			return false;
		}
		byte[] bPwd = KidsWatUtils.StringToByte("0"
				+ bleMap.get(mAddress).bluetoothDevicePassword + "\0\0",
				"ASCII");//
		// 00000000553e0b0d//+ "00"00000000554c8158
		// byte[] bPwd = Common.StringToByte("hjazcy130618@arthur",
		// "ASCII");// 00000000553e0b0d//+ "00"
		List<BluetoothGattService> list = getSupportedGattServices(mAddress);
		if (list != null)
			for (int i = 0; i < list.size(); i++) {

				if (list.get(i).getUuid().toString().toLowerCase()
						.startsWith("0000e030")) {
					TLog.log("00e030");
					BluetoothGattService gattservice = list.get(i);
					List<BluetoothGattCharacteristic> list2 = gattservice
							.getCharacteristics();
					for (int j = 0; j < list2.size(); j++) {

						if (list2.get(j).getUuid().toString().toLowerCase()
								.startsWith("0000e031")) {
							TLog.log("0e031");
							BluetoothGattCharacteristic gattcharacteristic = list2
									.get(j);
							setCharacteristicNotification(mAddress,
									gattcharacteristic, true);
							boolean writeState = writeCharacteristic(mAddress,
									gattcharacteristic, bPwd);// mBluetoothLeService//UTF-8
							// bleMap.get("mAddress").connectionState = 1;
							TLog.log("写密码成功=" + writeState);
							if (bleMap.containsKey(mAddress)) {
								bleMap.get(mAddress).connectionState = 3;
								TLog.log("state="
										+ bleMap.get(mAddress).connectionState);
								String Name = bleMap.get(mAddress).BabyName;

								if (writeState) {
									
									Log.e("", "Name:"+Name);
									
									EventBus.getDefault().post(
											new BleWritePwdSuccess(mAddress,
													Name));
								}
							}

							return writeState;
						}
					}
				}
			}
		return false;
	}

	/**
	 * Retrieves a list of supported GATT services on the connected device. This
	 * should be invoked only after {@code BluetoothGatt#discoverServices()}
	 * completes successfully.
	 * 
	 * @return A {@code List} of supported services.
	 */
	public List<BluetoothGattService> getSupportedGattServices(String mAddress) {
		if (bleMap.get(mAddress).blueGatt == null)
			return null;

		return bleMap.get(mAddress).blueGatt.getServices();
	}

	// ---------------------
}
