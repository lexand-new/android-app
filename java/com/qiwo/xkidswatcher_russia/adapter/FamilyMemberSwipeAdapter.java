package com.qiwo.xkidswatcher_russia.adapter;

import java.util.List;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.qiwo.xkidswatcher_russia.AppContext;
import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.bean.FamilyMemberMsg;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class FamilyMemberSwipeAdapter extends BaseAdapter {
	/**
	 * 上下文对象
	 */
	private Context mContext = null;

	/**
     * 
     */
	private int mRightWidth = 0;

	private List<FamilyMemberMsg> list;

	/**
	 * 单击事件监听器
	 */
	private IOnItemRightClickListener mListener = null;

	public interface IOnItemRightClickListener {
		void onRightClick(View v, int position);
	}

	/**
	 * @param mainActivity
	 */
	public FamilyMemberSwipeAdapter(Context ctx, int rightWidth,
			IOnItemRightClickListener l) {
		mContext = ctx;
		mRightWidth = rightWidth;
		mListener = l;
	}

	public void setList(List<FamilyMemberMsg> list) {
		this.list = list;
	}

	@Override
	public int getCount() {
		return list.size();
		// return 10;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder item;
		final int thisPosition = position;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.view_family_member_listeview_item, parent, false);
			item = new ViewHolder();
			item.item_left = (View) convertView.findViewById(R.id.item_left);
			item.item_right = (View) convertView.findViewById(R.id.item_right);

			item.imageView_tx = (ImageView) convertView
					.findViewById(R.id.imageView_tx);
			item.textView_relation = (TextView) convertView
					.findViewById(R.id.textView_relation);
			item.textView_country_code = (TextView) convertView
					.findViewById(R.id.textView_country_code);
			item.textView_mobileno = (TextView) convertView
					.findViewById(R.id.textView_mobileno);
			item.textView_admin = (TextView) convertView
					.findViewById(R.id.textView_admin);

			item.item_right_txt = (TextView) convertView
					.findViewById(R.id.item_right_txt);
			convertView.setTag(item);
		} else {// 有直接获得ViewHolder
			item = (ViewHolder) convertView.getTag();
		}
		LinearLayout.LayoutParams lp1 = new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		item.item_left.setLayoutParams(lp1);
		LinearLayout.LayoutParams lp2 = new LayoutParams(mRightWidth,
				LayoutParams.MATCH_PARENT);
		item.item_right.setLayoutParams(lp2);
		// item.textView_relation.setText("Me(Baby's Grandma)" + thisPosition);
		FamilyMemberMsg info = list.get(thisPosition);
		if (info != null) {
			String tRelation = null;
			if (info.uid
					.equalsIgnoreCase(AppContext.getInstance().loginUser_kid.uid)) {
				tRelation = "Я ("
						+ info.relation
						+ " для " + AppContext.getInstance().currentFamily.nickname + ")";
			} else {
				tRelation = info.relation + " для " + AppContext.getInstance().currentFamily.nickname;
			}
			item.textView_relation.setText(tRelation);
			item.textView_country_code.setText("+" + info.country_code);
			item.textView_mobileno.setText(info.mobile);

			int srcid = 0;

			if (info.relation.equalsIgnoreCase(mContext.getApplicationContext().getResources().getString(R.string.father))) {
				srcid = R.drawable.icon_dad;
			} else if (info.relation.equalsIgnoreCase(mContext.getApplicationContext().getResources().getString(R.string.mother))) {
				srcid = R.drawable.icon_mom;
			} else if (info.relation.equalsIgnoreCase(mContext.getApplicationContext().getResources().getString(R.string.grandfather))) {
				srcid = R.drawable.icon_grandpa;
			} else if (info.relation.equalsIgnoreCase(mContext.getApplicationContext().getResources().getString(R.string.grandmother))) {
				srcid = R.drawable.icon_grandma;
			} else {
				srcid = R.drawable.icon_other;
			}

			item.textView_admin.setVisibility(info.admin == 1 ? View.VISIBLE
					: View.INVISIBLE);

//			item.imageView_tx.setBackgroundResource(srcid);

			DisplayImageOptions options = new DisplayImageOptions.Builder()
					.showStubImage(srcid) // 加载开始默认的图片
					.showImageForEmptyUri(srcid) // url爲空会显示该图片，自己放在drawable里面的
					.showImageOnFail(srcid) // 加载图片出现问题，会显示该图片
					.cacheInMemory(true) // 缓存用
					.cacheOnDisc(true) // 缓存用
					.displayer(new RoundedBitmapDisplayer(180)) // 图片圆角显示，值为整数
					.build();

			String uri = info.image_path;
			if (!TextUtils.isEmpty(uri))
				AppContext.getInstance().imageLoader.displayImage("http://" + uri, item.imageView_tx, options);
			else
				item.imageView_tx.setBackgroundResource(srcid);

			// AppContext.getInstance().loginUser_kid.isAdmin == 1&&
			// || AppContext.getInstance().loginUser_kid.isAdmin == 0
			if (info.uid
					.equalsIgnoreCase(AppContext.getInstance().loginUser_kid.uid)
					|| AppContext.getInstance().loginUser_kid.isAdmin == 0) {
				item.item_right.setVisibility(View.GONE);
			} else {
				item.item_right.setVisibility(View.VISIBLE);
				item.item_right_txt.setText(mContext.getResources().getString(R.string.delete));
				item.item_right.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (mListener != null) {
							mListener.onRightClick(v, thisPosition);
						}
					}
				});
			}
		}
		return convertView;
	}

	private class ViewHolder {
		View item_left;
		View item_right;

		ImageView imageView_tx;
		TextView textView_relation;
		TextView textView_country_code;
		TextView textView_mobileno;
		TextView textView_admin;

		TextView item_right_txt;
	}
}
