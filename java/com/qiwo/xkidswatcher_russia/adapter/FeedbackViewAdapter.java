package com.qiwo.xkidswatcher_russia.adapter;

import java.util.List;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.bean.ChatMsgEntity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class FeedbackViewAdapter extends BaseAdapter {

	public static interface IMsgViewType {
		int IMVT_COM_MSG = 0;
		int IMVT_TO_MSG = 1;
	}

	private List<ChatMsgEntity> coll;

	private LayoutInflater mInflater;

	public FeedbackViewAdapter(Context context, List<ChatMsgEntity> coll) {
		this.coll = coll;
		mInflater = LayoutInflater.from(context);
	}

	public int getCount() {
		return coll.size();
	}

	public Object getItem(int position) {
		return coll.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		ChatMsgEntity entity = coll.get(position);

		if (entity.getMsgType()) {
			return IMsgViewType.IMVT_COM_MSG;
		} else {
			return IMsgViewType.IMVT_TO_MSG;
		}

	}

	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		final ChatMsgEntity entity = coll.get(position);
		boolean isComMsg = entity.getMsgType();

		ViewHolder viewHolder = null;
		if (convertView == null) {
			if (isComMsg) {
				convertView = mInflater.inflate(R.layout.feecback_item_left,
						null);
			} else {
				convertView = mInflater.inflate(R.layout.feedback_item_right,
						null);
			}

			viewHolder = new ViewHolder();
			viewHolder.tvContent = (TextView) convertView
					.findViewById(R.id.tv_chatcontent);
			viewHolder.isComMsg = isComMsg;

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		viewHolder.tvContent.setText(entity.getText());
		viewHolder.tvContent
				.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		viewHolder.tvContent.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				//
			}
		});

		return convertView;
	}

	static class ViewHolder {
		public TextView tvContent;
		public boolean isComMsg = true;
	}

}
