package com.qiwo.xkidswatcher_russia.adapter;

import java.util.Calendar;
import java.util.List;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.bean.beanForDb___Message;
import com.qiwo.xkidswatcher_russia.fragment.Message2Fragment;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import com.qiwo.xkidswatcher_russia.util.TLog;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.renderscript.RSInvalidStateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class Message2Adapter extends BaseAdapter {
	/**
	 * 上下文对象
	 */
	private Context mContext = null;

	/**
     * 
     */
	private int mRightWidth = 0;
	private int playIndex = -1;

	private List<beanForDb___Message> list;

	/**
	 * 单击事件监听器
	 */
	private IOnItemRightClickListener mListener = null;
	private IOnItemRightClickListener mPlayListener = null;

	public interface IOnItemRightClickListener {
		void onRightClick(View v, int position, Object object);
	}

	/**
	 * @param mainActivity
	 */
	public Message2Adapter(Context ctx, int rightWidth,
			IOnItemRightClickListener l) {
		mContext = ctx;
		mRightWidth = rightWidth;
		mListener = l;
	}

	public void setPlayListener(IOnItemRightClickListener l) {
		mPlayListener = l;
	}

	public void setList(List<beanForDb___Message> list) {
		this.list = list;
	}

	public void setAudioPlayingIndex(int index) {
		playIndex = index;
	}

	public List<beanForDb___Message> getList() {
		return list;
	}

	@Override
	public int getCount() {
		return list.size();
		// return 10;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder item;
		final int thisPosition = position;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.view_message2_list_item, parent, false);
			item = new ViewHolder();
			item.item_left = (View) convertView.findViewById(R.id.item_left);
			item.item_right = (View) convertView.findViewById(R.id.item_right);
			item.item_right_txt = (TextView) convertView
					.findViewById(R.id.item_right_txt);
			// ------------------
			item.imageView_icon = (ImageView) convertView
					.findViewById(R.id.imageView_icon);
			item.textView_type = (TextView) convertView
					.findViewById(R.id.textView_type);
			item.textView_des = (TextView) convertView
					.findViewById(R.id.textView_des);
			item.textView_time = (TextView) convertView
					.findViewById(R.id.textView_time);
			item.linearLayout_play = (LinearLayout) convertView
					.findViewById(R.id.linearLayout_play);
			// -------------------
			convertView.setTag(item);
		} else {// 有直接获得ViewHolder
			item = (ViewHolder) convertView.getTag();
		}
		LinearLayout.LayoutParams lp1 = new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		item.item_left.setLayoutParams(lp1);
		LinearLayout.LayoutParams lp2 = new LayoutParams(mRightWidth,
				LayoutParams.MATCH_PARENT);
		item.item_right.setLayoutParams(lp2);
		// item.textView_relation.setText("Me(Baby's Grandma)" + thisPosition);
		final beanForDb___Message info = list.get(thisPosition);
		if (info != null) {
			item.textView_type.setText(info.content_type_text);

			if (info.isRead == 0) {
				item.imageView_icon
						.setBackgroundResource(R.drawable.icon_new_msg);
				item.imageView_icon.setVisibility(View.VISIBLE);
			} else {
				item.imageView_icon.setVisibility(View.INVISIBLE);
			}
			item.textView_des.setText(info.content);
			Calendar a = Calendar.getInstance();
			Calendar b = Calendar.getInstance();
			a.setTimeInMillis(System.currentTimeMillis());
			b.setTimeInMillis(info.time * 1000);

			String strdate;
			if (KidsWatUtils.isSameDay(a, b)) {
				strdate = new java.text.SimpleDateFormat("HH:mm")
						.format(new java.util.Date(info.time * 1000));
			} else {
//				strdate = new java.text.SimpleDateFormat("MM-dd")
//						.format(new java.util.Date(info.time * 1000));
				strdate = new java.text.SimpleDateFormat("dd-MM")
						.format(new java.util.Date(info.time * 1000));
			}
			item.textView_time.setText(strdate);
			if (info.content_type == 1) {
				TLog.log(String.format("%s-%s", position, playIndex));
				if (position == playIndex) {
					item.linearLayout_play
							.setBackgroundResource(R.anim.msg_play_animations);
					AnimationDrawable animationDrawable = (AnimationDrawable) item.linearLayout_play
							.getBackground();

					if (Message2Fragment.playState) {
						animationDrawable.start();
					} else {
						animationDrawable.stop();
						item.linearLayout_play
								.setBackgroundResource(R.drawable.icon_msg_p3);
					}

				} else {
					item.linearLayout_play
							.setBackgroundResource(R.drawable.icon_msg_p3);
				}
				// ------------
				// ---------------
				item.linearLayout_play.setVisibility(View.VISIBLE);
				item.linearLayout_play
						.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								if (mPlayListener != null) {

									Message2Fragment.playState = true;

									mPlayListener.onRightClick(v, thisPosition,
											info);
								}
							}
						});
			} else {
				item.linearLayout_play.setVisibility(View.GONE);
			}

			// ---------------------
			item.item_right_txt.setText(mContext.getResources().getString(R.string.delete));
			item.item_right.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mListener != null) {
						mListener.onRightClick(v, thisPosition, info);
					}
				}
			});
		}

		return convertView;
	}

	private class ViewHolder {
		View item_left;
		View item_right;
		TextView item_right_txt;

		ImageView imageView_icon;
		TextView textView_type;
		TextView textView_des;
		TextView textView_time;
		LinearLayout linearLayout_play;

	}
}
