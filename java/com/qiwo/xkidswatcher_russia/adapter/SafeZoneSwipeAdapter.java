package com.qiwo.xkidswatcher_russia.adapter;

import java.util.List;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.bean.beanFor___get_safe_area_config;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class SafeZoneSwipeAdapter extends BaseAdapter {
	/**
	 * 上下文对象
	 */
	private Context mContext = null;

	/**
     * 
     */
	private int mRightWidth = 0;

	private List<beanFor___get_safe_area_config.CConfig> list;

	/**
	 * 单击事件监听器
	 */
	private IOnItemRightClickListener mListener = null;

	public interface IOnItemRightClickListener {
		void onRightClick(View v, int position, Object object);
	}

	/**
	 * @param mainActivity
	 */
	public SafeZoneSwipeAdapter(Context ctx, int rightWidth,
			IOnItemRightClickListener l) {
		mContext = ctx;
		mRightWidth = rightWidth;
		mListener = l;
	}

	public void setList(List<beanFor___get_safe_area_config.CConfig> list) {
		this.list = list;
	}

	public List<beanFor___get_safe_area_config.CConfig> getList() {
		return list;
	}

	@Override
	public int getCount() {
		return list.size();
		// return 10;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder item;
		final int thisPosition = position;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.view_safezone_list_item, parent, false);
			item = new ViewHolder();
			item.item_left = (View) convertView.findViewById(R.id.item_left);
			item.item_right = (View) convertView.findViewById(R.id.item_right);
			item.textView_title = (TextView) convertView
					.findViewById(R.id.textView_title);
			item.textView_des = (TextView) convertView
					.findViewById(R.id.textView_des);
			item.textView_radius = (TextView) convertView
					.findViewById(R.id.textView_radius);
			item.textView_author = (TextView) convertView
					.findViewById(R.id.textView_author);

			item.item_right_txt = (TextView) convertView
					.findViewById(R.id.item_right_txt);
			convertView.setTag(item);
		} else {// 有直接获得ViewHolder
			item = (ViewHolder) convertView.getTag();
		}
		LinearLayout.LayoutParams lp1 = new LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		item.item_left.setLayoutParams(lp1);
		LinearLayout.LayoutParams lp2 = new LayoutParams(mRightWidth,
				LayoutParams.MATCH_PARENT);
		item.item_right.setLayoutParams(lp2);
		// item.textView_relation.setText("Me(Baby's Grandma)" + thisPosition);
		final beanFor___get_safe_area_config.CConfig info = list
				.get(thisPosition);
		if (info != null) {
			item.textView_title.setText(info.name);
			item.textView_des.setText(info.address);
			item.textView_radius.setText("радиус ： " + info.radius+" m");
			item.textView_author.setText("добавлена " + info.create_user);

			item.item_right_txt.setText(mContext.getResources().getString(R.string.delete));
			item.item_right.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mListener != null) {
						mListener.onRightClick(v, thisPosition, info);
					}
				}
			});
		}

		return convertView;
	}

	private class ViewHolder {
		View item_left;
		View item_right;

		TextView textView_title;
		TextView textView_des;

		TextView textView_radius;
		TextView textView_author;

		TextView item_right_txt;
	}
}
