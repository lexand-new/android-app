package com.qiwo.xkidswatcher_russia.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.qiwo.xkidswatcher_russia.R;
import com.qiwo.xkidswatcher_russia.bean.TimeZone;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/***
 * 选择时区适配器
 */
public class TimeZoneAdapter extends BaseAdapter {

    public List<TimeZone> mTimeZones;
    private Context context;

    public TimeZoneAdapter(List<TimeZone> mTimeZones, Context context) {
        this.context = context;
        this.mTimeZones = mTimeZones;
    }

    @Override
    public int getCount() {
        return null == mTimeZones ? 0 : mTimeZones.size();
    }

    @Override
    public Object getItem(int position) {
        return null == mTimeZones ? null : mTimeZones.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder mViewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_time_zone,
                    parent, false);
            mViewHolder = new ViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {// 有直接获得ViewHolder
            mViewHolder = (ViewHolder) convertView.getTag();
        }
        TimeZone timeZone=mTimeZones.get(position);
        if(timeZone!=null) {
            mViewHolder.time_zone_name.setText(timeZone.getTimeZone());
            mViewHolder.time_zone_tv.setImageResource(timeZone.isOK()?R.drawable.check_time_zone_press:R.drawable.check_time_zone);
        }
        return convertView;
    }

    static class ViewHolder {
        @InjectView(R.id.time_zone_name)
        TextView time_zone_name;
        @InjectView(R.id.time_zone_tv)
        ImageView time_zone_tv;

        public ViewHolder(View v) {
            ButterKnife.inject(this, v);
        }
    }
}
