/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qiwo.xkidswatcher_russia;

import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;
import com.qiwo.xkidswatcher_russia.db.SqlDb;
import com.qiwo.xkidswatcher_russia.event.BaseEvent;
import com.qiwo.xkidswatcher_russia.ui.EventAlertActivity;
import com.qiwo.xkidswatcher_russia.ui.MainActivity;
import com.qiwo.xkidswatcher_russia.util.KidsWatUtils;
import com.qiwo.xkidswatcher_russia.util.StringUtils;
import com.qiwo.xkidswatcher_russia.util.TLog;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import de.greenrobot.event.EventBus;

/**
 * IntentService responsible for handling GCM messages.
 */
public class GCMIntentService extends GCMBaseIntentService {

	private static final String TAG = "GCMIntentService";
	public static final String SENDER_ID = "648991208617";
	
	//正式环境	apikey ： AIzaSyDvTjAd89qIRZxO6KqRs-50ED93zQ7jNvI
	//正式环境 sender_id : "648991208617"
												
	//开发/测试环境 apikey ： AIzaSyCxYwIr_zORIZsLBWfmmgkO_oo6p0Qpelg
	//开发/测试环境 sender_id ： 567665979722
	
	public GCMIntentService() {
		super(SENDER_ID);
	}

	@Override
	protected void onRegistered(Context context, String registrationId) {
		Log.i(TAG, "Device registered: regId = " + registrationId);
		AppContext.google_gcm_appid = registrationId;
		KidsWatConfig.setGcmAppid(registrationId);

		displayMessage("registered:" + registrationId);
		ServerUtilities.register(context, registrationId);
	}

	@Override
	protected void onUnregistered(Context context, String registrationId) {
		Log.i(TAG, "Device unregistered");
		displayMessage("unregistered:" + registrationId);
		if (GCMRegistrar.isRegisteredOnServer(context)) {
			ServerUtilities.unregister(context, registrationId);
		} else {
			// This callback results from the call to unregister made on
			// ServerUtilities when the registration to the server failed.
			Log.i(TAG, "Ignoring unregister callback");
		}
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		System.out.println("--------------------receive GCM Message------------------"+intent.getExtras().toString());
		//[{content={"content_type":23,"message_id":"MSEESAGE_CENTER::14559378608069","time":1455937860,"detail":{"content_type":23,"detail":{"family_id":10000673,"address":"This location","latitude":22.63059425,"kid_name":"SOS","time":1455937858,"type":2,"location_type":2,"longitude":114.0248489}},"title":"SOS","content":"SOS."}, body=motion detection files!click here to view., collapse_key=do_not_collapse, from=567665979722, type=motion}]

		Set<String> keys = intent.getExtras().keySet();
		for (String s : keys) {
			String value = intent.getExtras().getString(s);
		}
		if (AppContext.isLogin) {
			// ApplicationHelper.getInstance().get_notice_list_ex();
			KidsWatUtils.get_notice_list_ex();
		}

		String content_type = intent.getStringExtra("content_type");
		System.out.println("content type = "+content_type);
		String title = intent.getStringExtra("title");
		String content = intent.getStringExtra("content");
		String detail = intent.getStringExtra("detail");
		
		SqlDb db = SqlDb.get(context);
		db.deleteGcm_message();
		db.saveGcmMessage(title, content, intent.getExtras().toString());
		db.closeDb();

		if (!StringUtils.isEmpty(content_type) && AppContext.isLogin) {
			// define('PUSH_CONTENT_TYPE__DEVICE_CHANGE_TO_FLIGHT_MODE', 32);
			//	设备进入飞行模式
			// define('PUSH_CONTENT_TYPE__DEVICE_CHANGE_TO_NORMAL_MODE', 33);
			//	设备进行正常模式
			String family_id = null;
			
			if(AppContext.getInstance().currentFamily != null){
				family_id = AppContext.getInstance().currentFamily.family_id;
			}
			
			if (content_type.equalsIgnoreCase("23")) {
				// EventBus.getDefault().post(new Sos(""));
				JSONObject json;
				try {
					json = new JSONObject(intent.getStringExtra("detail"));
					String nickname = json.getJSONObject("detail").getString(
							"kid_name");
//					String message = String
//							.format("%s has sent out an SOS alert.\nPlease confirm the safety of your kid",
//									nickname);
					String message = String.format("ВНИМАНИЕ! Ваш ребенок %s подает сигнал тревоги! Проверьте, в безопасности ли он.", nickname);
					Intent mintent = new Intent(context, EventAlertActivity.class);
					mintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					mintent.putExtra("message", message);
					startActivity(mintent);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (content_type.equalsIgnoreCase("32")) {
				try {
					JSONObject js_detail = new JSONObject(detail);
					String msg_family_id = js_detail.getString("family_id");
					SqlDb dbx = SqlDb.get(context);
					dbx.setDeviceModeBy_fid(msg_family_id, 2);
					if (msg_family_id.equalsIgnoreCase(family_id)) {
						dbx.setDeviceModeBy_fid(family_id, 2);
						AppContext.getInstance().currentFamily = dbx
								.getFamilyBy_fid(family_id);
						// EventBus.getDefault().post(
						// new DeviceModeChange(family_id, 2));
						BaseEvent event = new BaseEvent(
								BaseEvent.MSGTYPE_3___CHANGE_DEVICE_MODE,
								"DeviceModeChange");
						EventBus.getDefault().post(event);
					}
					dbx.closeDb();
				} catch (JSONException e) {
					e.printStackTrace();
				}

			} else if (content_type.equalsIgnoreCase("33")) {

				try {
					JSONObject js_detail = new JSONObject(detail);
					String msg_family_id = js_detail.getString("family_id");
					SqlDb dbx = SqlDb.get(context);
					dbx.setDeviceModeBy_fid(msg_family_id, 1);
					if (msg_family_id.equalsIgnoreCase(family_id)) {

						dbx.setDeviceModeBy_fid(family_id, 1);
						AppContext.getInstance().currentFamily = dbx
								.getFamilyBy_fid(family_id);
						dbx.closeDb();
						// EventBus.getDefault().post(
						// new DeviceModeChange(family_id, 1));
						BaseEvent event = new BaseEvent(
								BaseEvent.MSGTYPE_3___CHANGE_DEVICE_MODE,
								"DeviceModeChange");
						EventBus.getDefault().post(event);
					}
					dbx.closeDb();
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			else if (content_type.equalsIgnoreCase("20")) {
				// location_msg
				TLog.log("--------receive location msg:20--------");
				BaseEvent event = new BaseEvent(
						BaseEvent.MSGTYPE_3___GCM_LOCATION, detail);
				EventBus.getDefault().post(event);
			}

			else if (content_type.equalsIgnoreCase("3")) {
				// location_msg
				TLog.log("--------receive location msg:3--------");
				BaseEvent event = new BaseEvent(
						BaseEvent.MSGTYPE_1___DELETE_DEVICE, content);
				EventBus.getDefault().post(event);
			} else if (content_type.equalsIgnoreCase("9")) {
				// location_msg
				TLog.log("--------receive location msg:9--------");
				BaseEvent event = new BaseEvent(
						BaseEvent.MSGTYPE_1___ADMIN_ADD_MEMBER, content);
				EventBus.getDefault().post(event);
			} else if (content_type.equalsIgnoreCase("10")) {
				// location_msg
				TLog.log("--------receive location msg:10--------");
				BaseEvent event = new BaseEvent(
						BaseEvent.MSGTYPE_1___ADMIN_DELETE_MEMBER, content);
				EventBus.getDefault().post(event);
			} else if (content_type.equalsIgnoreCase("1")) {
				// location_msg

				try {
					JSONObject json = new JSONObject(
							intent.getStringExtra("detail"));

					String command_no = json.getJSONObject("detail").getString(
							"command_no");
					String device_id = json.getJSONObject("detail").getString(
							"device_id");

					TLog.log("--------receive location msg:1--------");

					String str = command_no + "," + device_id;

					BaseEvent event = new BaseEvent(
							BaseEvent.MSGTYPE_6___WHITE_HOME_VOICE_PUSH, str);
					EventBus.getDefault().post(event);

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}
		}

		// if (content_type != null && content_type.equalsIgnoreCase("23")
		// && AppContext.getInstance().isLogin) {
		// // EventBus.getDefault().post(new Sos(""));
		// String nickname = AppContext.getInstance().currentFamily.nickname;
		// String message = String.format("%s\nSOS Warning", nickname);
		//
		// Intent mintent = new Intent(context, QrcodeActivity.class);
		// mintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		// mintent.putExtra("message", message);
		// // mintent.putExtra("bleadd", event.getBleAddress());
		// startActivity(mintent);
		// }
		//
		// if (StringUtils.isEmpty(content_type)
		// && AppContext.getInstance().isLogin) {
		// // define('PUSH_CONTENT_TYPE__DEVICE_CHANGE_TO_FLIGHT_MODE', 32);
		// // //设备进入飞行模式
		// // define('PUSH_CONTENT_TYPE__DEVICE_CHANGE_TO_NORMAL_MODE', 33);
		// // //设备进行正常模式
		// String family_id = AppContext.getInstance().currentFamily.family_id;
		// if (content_type.equalsIgnoreCase("32")) {
		//
		// SqlDb dbx = SqlDb.get(context);
		// dbx.setDeviceModeBy_fid(family_id, 2);
		// AppContext.getInstance().currentFamily = dbx
		// .getFamilyBy_fid(family_id);
		// dbx.closeDb();
		// EventBus.getDefault().post(new DeviceModeChange(family_id, 2));
		// } else if (content_type.equalsIgnoreCase("33")) {
		// SqlDb dbx = SqlDb.get(context);
		// dbx.setDeviceModeBy_fid(family_id, 1);
		// AppContext.getInstance().currentFamily = dbx
		// .getFamilyBy_fid(family_id);
		// dbx.closeDb();
		// EventBus.getDefault().post(new DeviceModeChange(family_id, 1));
		// }
		// }

		// displayMessage(content);
		// notifies user

		if (title != null && title.length() >= 3) {
			generateNotification(context, title, content, detail);
		}
	}

	@Override
	protected void onDeletedMessages(Context context, int total) {
		Log.i(TAG, "Received deleted messages notification");
		String message = "deleted " + total;
		displayMessage(message);
		// notifies user

	}

	@Override
	public void onError(Context context, String errorId) {
		Log.i(TAG, "Received error: " + errorId);
		displayMessage("error:" + errorId);
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		// log message
		Log.i(TAG, "Received recoverable error: " + errorId);
		displayMessage("error:" + errorId);
		return super.onRecoverableError(context, errorId);
	}

	static void displayMessage(String message) {
		System.out.println(message);
	}

	static int notify_id = 0;

	/**
	 * Issues a notification to inform the user that server has sent a message.
	 */
	private static void generateNotification(Context context, String title,
			String message, String detail) {

		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(android.content.Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(R.drawable.ic_launcher,
				message, System.currentTimeMillis());
		// FLAG_AUTO_CANCEL 该通知能被状态栏的清除按钮给清除掉
		// FLAG_NO_CLEAR 该通知不能被状态栏的清除按钮给清除掉
		// FLAG_ONGOING_EVENT 通知放置在正在运行
		// FLAG_INSISTENT 是否一直进行，比如音乐一直播放，知道用户响应
		notification.flags |= Notification.FLAG_SHOW_LIGHTS;
		// DEFAULT_ALL 使用所有默认值，比如声音，震动，闪屏等等
		// DEFAULT_LIGHTS 使用默认闪光提示
		// DEFAULT_SOUNDS 使用默认提示声音
		// DEFAULT_VIBRATE 使用默认手机震动，需加上<uses-permission
		// android:name="android.permission.VIBRATE" />权限
		// notification.defaults = Notification.DEFAULT_LIGHTS;
		notification.defaults = Notification.DEFAULT_SOUND;

		Intent notificationIntent = new Intent(context, MainActivity.class); // 点击该通知后要跳转的Activity
		notificationIntent.putExtra("detail", detail);
		PendingIntent contentItent = PendingIntent.getActivity(context, 0,
				notificationIntent, 0);// notificationIntent

		if (message.contains("time location successfully")) {
			return;
		}

		notification.setLatestEventInfo(context, title, message, contentItent);

		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(notify_id++, notification);

		// 把Notification传递给NotificationManager
		// notificationManager.notify(0, notification);

		// int icon = R.drawable.ic_launcher;
		// long when = System.currentTimeMillis();
		// NotificationManager notificationManager = (NotificationManager)
		// context
		// .getSystemService(Context.NOTIFICATION_SERVICE);
		// Notification notification = new Notification(icon, message, when);
		//
		// Intent notificationIntent = new Intent(context, MainActivity.class);
		// // set intent so it does not start a new activity
		// notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
		// | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		// PendingIntent intent = PendingIntent.getActivity(context, 0,
		// notificationIntent, 0);
		// notification.setLatestEventInfo(context, title, message, intent);
		// // notification.flags |= Notification.FLAG_AUTO_CANCEL;
		// notification.defaults = Notification.DEFAULT_SOUND; // 调用系统自带声音
		// notification.defaults = Notification.DEFAULT_VIBRATE;// 设置默认震动
		// notificationManager.notify(0, notification);
		
//		Intent intent = new Intent(context, MainActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
//                PendingIntent.FLAG_ONE_SHOT);
//
//        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        android.support.v4.app.NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
//                .setSmallIcon(R.drawable.icon_sos)
//                .setContentTitle(title)
//                .setContentText(message)
//                .setAutoCancel(true)
//                .setSound(defaultSoundUri)
//                .setContentIntent(pendingIntent);
//
//        NotificationManager notificationManager =
//                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//       
//        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
	}

}
